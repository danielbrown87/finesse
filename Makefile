# $Id: Makefile 3068 2008-04-16 13:29:30Z adf $
#
# Main Makefile for Finesse

# work out the name of the host we're on
HOSTNAME = $(shell uname -n)

.PHONY: clean lib src doc/manual api api_clean realclean setup


default: kat

kat: lib src 

fast: kat

setup:
# work out the name of the host we're on
HOSTNAME = $(shell uname -n)

# Set defaults for including libraries
ifeq "$(ARCH)" "mac"
	INCLUDE_NICSLU=1
	INCLUDE_CUBA=1
	INCLUDE_NET=1
	INCLUDE_PARALUTION=0
else ifeq "$(ARCH)" "linux"
	INCLUDE_NICSLU=1
	INCLUDE_CUBA=1
	INCLUDE_NET=1
	INCLUDE_PARALUTION=0
else ifeq "$(ARCH)" "win32"
	ifeq "$(shell uname -o)" "Cygwin"
		INCLUDE_NICSLU=0
		INCLUDE_CUBA=1
		INCLUDE_NET=0
		INCLUDE_PARALUTION=0
	else
		INCLUDE_NICSLU=0
		INCLUDE_CUBA=0
		INCLUDE_NET=0
		INCLUDE_PARALUTION=0
	endif
else ifeq "$(ARCH)" "win64"
	ifeq "$(shell uname -o)" "Cygwin"
		INCLUDE_NICSLU=0
		INCLUDE_CUBA=1
		INCLUDE_NET=0
		INCLUDE_PARALUTION=0
	else
		INCLUDE_NICSLU=0
		INCLUDE_CUBA=0
		INCLUDE_NET=0
		INCLUDE_PARALUTION=0
	endif
endif

testarch: 
	$(eval archs := mac win32 win64 linux)
	$(eval archflag := $(filter $(ARCH), $(archs)))
	$(if $(filter $(ARCH), $(archs)), \
	@echo "Arch flag found for '$(archflag)'", \
	$(error Arch flag must be mac, win32, win64 or linux)) 

# library building target
lib: testarch setup
	$(MAKE) --directory=lib ARCH=$(ARCH) INCLUDE_NICSLU=$(INCLUDE_NICSLU) INCLUDE_CUBA=$(INCLUDE_CUBA) INCLUDE_NET=$(INCLUDE_NET) INCLUDE_PARALUTION=$(INCLUDE_PARALUTION)

# finesse building targets
src: lib
	$(MAKE) --directory=src ARCH=$(ARCH) INCLUDE_NICSLU=$(INCLUDE_NICSLU) INCLUDE_CUBA=$(INCLUDE_CUBA) INCLUDE_NET=$(INCLUDE_NET) INCLUDE_PARALUTION=$(INCLUDE_PARALUTION) kat 

debug: lib
	$(MAKE) --directory=src ARCH=$(ARCH) INCLUDE_NICSLU=$(INCLUDE_NICSLU) INCLUDE_CUBA=$(INCLUDE_CUBA) INCLUDE_NET=$(INCLUDE_NET) INCLUDE_PARALUTION=$(INCLUDE_PARALUTION) debug

prof: lib
	$(MAKE) --directory=src ARCH=$(ARCH) $@

config: lib
	$(MAKE) --directory=src ARCH=$(ARCH) $@

cover: lib
	$(MAKE) --directory=src ARCH=$(ARCH) $@

versionnumber: lib
	$(MAKE) --directory=src ARCH=$(ARCH) $@

test: lib
	$(MAKE) --directory=src ARCH=$(ARCH) $@

test_cover: lib
	$(MAKE) --directory=src ARCH=$(ARCH) $@

# documentation building targets
doc: doc/manual

doc/manual:
	$(MAKE) --directory=$@

api:
	$(MAKE) --directory=src $@

api_clean:
	$(MAKE) --directory=src $@

cover_report:
	$(MAKE) --directory=src $@

# cleanup targets
clean:
	$(MAKE) --directory=src $@

realclean:
	for d in lib src;  \
	do  \
	     $(MAKE) --directory=$$d clean; \
	done

	if ls lib/Cuba-3.2/*.a 1> /dev/null 2>&1; then \
		rm -f lib/Cuba-3.2/*.a; \
	fi;

	if ls lib/KLUsparse/*/Lib/*.a 1> /dev/null 2>&1; then \
		rm -f lib/KLUsparse/*/Lib/*.a; \
	fi;

# print out the possible options to pass to make
help:
	@echo "Possible targets are:"
	@echo "For building Finesse:"
	@echo "    kat: build with all optimisations"
	@echo "    fast: an alias for kat"
	@echo "    prof: build with profiling information switched on"
	@echo "    cover: build with code coverage information switched on"
	@echo "    debug: build with debugging information switched on"
	@echo "    versionnumber: build with all optimisations, and append"
	@echo "         svn (or svk) revision number to binary name"
	@echo "Testing:"
	@echo "    test: build and runs the unit tests"
	@echo "    test_cover: build and runs the unit tests, with coverage"
	@echo "                information switched on"
	@echo "For building the associated libraries:"
	@echo "    lib: builds all libraries required to build Finesse"
	@echo "Documentation:"
	@echo "    api: build the API level documentation"
	@echo "    cover_report: generate the coverage report with lcov"
	@echo "Other targets:"
	@echo "    clean: remove relevant object and binary files"
	@echo "    realclean: remove all object and binary files for Finesse,"
	@echo "         libraries, and documentation"
	@echo "    help: print this information to the screen"

# vim: shiftwidth=4:
