from subprocess import Popen, PIPE
import shutil
import os

p = Popen(['cygcheck', 'kat'], stdin=PIPE, stdout=PIPE, stderr=PIPE)

out, err = p.communicate()

a = out.split("\n")
b = [c.strip() for c in a if c.find("cygwin") >= 0]

if os.path.exists(".windlls"):
    shutil.rmtree(".windlls")
    
shutil.os.mkdir(".windlls")

for file in b:
    print "cp ", file.replace('\\','\\\\'), "."
    shutil.copy2(file, os.path.join("./.windlls", os.path.basename(file.replace('\\','/'))))

print "Copied dlls into .windlls folder."