
Patch for klu_version.h

43 	+diff -Nru ../SuiteSparse/KLU/Include/klu_version.h ./KLU/Include/klu_version.h
44 	+--- ../SuiteSparse/KLU/Include/klu_version.h   2006-05-23 22:32:28.000000000 +0300
45 	++++ ./KLU/Include/klu_version.h        2006-11-20 09:18:27.000000000 +0200
46 	+@@ -203,6 +203,8 @@
47 	+
48 	+ */
49 	+
50 	++#include <math.h>
51 	++#if !defined(__complex_t) /* Mac OS X PowerPC defines this */
52 	+ typedef struct
53 	+ {
54 	+     double component [2] ;    /* real and imaginary parts */
55 	+@@ -213,6 +215,10 @@
56 	+ #define Entry DoubleComplex
57 	+ #define Real component [0]
58 	+ #define Imag component [1]
59 	++#else
60 	++typedef struct __complex_s Unit ;
61 	++#define Entry __complex_t
62 	++#endif
63 	+