#ifndef __COMPLEX__
#define __COMPLEX__

#include "nics_config.h"
#include "math.h"

#define C_GETR(a)		((a).real)
#define C_GETI(a)		((a).image)

/*a=(r,i)*/
#define C_SET(a, r, i)	\
{						\
	(a).real = r;		\
	(a).image = i;		\
}

/*a=0*/
#define C_CLR(a)		\
{						\
	(a).real = 0.;		\
	(a).image = 0.;		\
}

/*a=b*/
#define C_CPY(a, b)			\
{							\
	(a).real = (b).real;	\
	(a).image = (b).image;	\
}

/*==0, return bool*/
#define C_EQZ(a)	\
((a).real == 0. && (a).image == 0.)

/*!=0, return bool*/
#define C_NEZ(a)	\
((a).real != 0. || (a).image != 0.)

/*is nan, return bool*/
#define C_NAN(a)	\
((a).real !=(a).real || (a).image != (a).image)

/*|a|, return real*/
#define C_ABS(a)	\
sqrt((a).real*(a).real + (a).image*(a).image)

#define C_LEN(a)	\
C_ABS(a)

#define C_SQSUM(a)	\
((a).real*(a).real + (a).image*(a).image)

#define C_POSI(a)								\
{												\
	if ((a).real < 0.) (a).real = -(a).real;	\
	if ((a).image < 0.) (a).image = -(a).image;	\
}

#define C_NEGA(a)								\
{												\
	if ((a).real > 0.) (a).real = -real;		\
	if ((a).image > 0.) (a).image = -(a).image;	\
}

/*c=a+b*/
#define C_ADD(c, a, b)					\
{										\
	(c).real = (a).real + (b).real;		\
	(c).image = (a).image + (b).image;	\
}

/*c=a-b*/
#define C_SUB(c, a, b)					\
{										\
	(c).real = (a).real - (b).real;		\
	(c).image = (a).image - (b).image;	\
}

/*c=a*b*/
#define C_MUL(c, a, b)										\
{															\
	(c).real = (a).real*(b).real - (a).image*(b).image;		\
	(c).image = (a).real*(b).image + (a).image*(b).real;	\
}

/*c=a/b*/
#define C_DIV(c, a, b)											\
{																\
	real__t _d = (b).real*(b).real + (b).image*(b).image;		\
	(c).real = ((a).real*(b).real+(a).image*(b).image) / _d;	\
	(c).image = ((a).image*(b).real-(a).real*(b).image) / _d;	\
}

/*a+=b*/
#define C_ADDSELF(a, b)		\
{							\
	(a).real += (b).real;	\
	(a).image += (b).image;	\
}

/*a-=b*/
#define C_SUBSELF(a, b)		\
{							\
	(a).real -= (b).real;	\
	(a).image -= (b).image;	\
}

/*a*=b*/
#define C_MULSELF(a, b)								\
{													\
	real__t _r, _i;									\
	_r = (a).real*(b).real - (a).image*(b).image;	\
	_i = (a).real*(b).image + (a).image*(b).real;	\
	(a).real = _r;									\
	(a).image = _i;									\
}

/*a/=b*/
#define C_DIVSELF(a, b)									\
{														\
	real__t _r, _i, _d;									\
	_d = (b).real*(b).real + (b).image*(b).image;		\
	_r = ((a).real*(b).real+(a).image*(b).image) / _d;	\
	_i = ((a).image*(b).real-(a).real*(b).image) / _d;	\
	(a).real = _r;										\
	(a).image = _i;										\
}

/*a *= b*/
#define C_MULSELFR(a, b)	\
{							\
	(a).real *= b;			\
	(a).image *= b;			\
}

/*a /= b*/
#define C_DIVSELFR(a, b)	\
{							\
	(a).real /= b;			\
	(a).image /= b;			\
}

/*a=b*c*/
#define C_MULR(a, b, c)			\
{								\
	(a).real = (b).real * c;	\
	(a).image = (b).image * c;	\
}

/*a=b/c*/
#define C_DIVR(a, b, c)			\
{								\
	(a).real = (b).real / c;	\
	(a).image = (b).image / c;	\
}

#endif
