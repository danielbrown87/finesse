/*destroy the NicsLU structure*/
/*last modified: july 11, 2013*/
/*author: Chen, Xiaoming*/

#include "nicsluc.h"
#include "nicsluc_internal.h"

int NicsLUc_Destroy(SNicsLUc *nicslu)
{
	if (NULL == nicslu)
	{
		return NICSLU_ARGUMENT_ERROR;
	}

	_I_NicsLUc_DestroyMatrix(nicslu);

	free(nicslu->timer);
	nicslu->timer = NULL;

	free(nicslu->flag);
	nicslu->flag = NULL;

	free(nicslu->stat);
	nicslu->stat = NULL;

	free(nicslu->cfgi);
	nicslu->cfgi = NULL;

	free(nicslu->cfgf);
	nicslu->cfgf = NULL;

	return NICS_OK;
}
