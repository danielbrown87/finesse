/*scale the matrix by the mc64 algorithm*/
/*last modified: aug 17, 2013*/
/*author: Chen, Xiaoming*/

#include "nicsluc.h"
#include "nicsluc_internal.h"
#include "complex.h"

void _I_NicsLUc_MC64Scale(SNicsLUc *nicslu)
{
	uint__t end;
	uint__t i, j;
	uint__t n;
	complex__t *ax;
	real__t *rs, *csp;
	uint__t *ai, *ap;
	real__t s, t;
#ifdef NICSLUC_DEBUG
	FILE *fp;
#endif

	if (!nicslu->cfgi[1]) return;

	n = nicslu->n;
	ax = nicslu->ax;
	ai = nicslu->ai;/*changed in preprocess*/
	ap = nicslu->ap;
	rs = nicslu->row_scale;
	csp = nicslu->col_scale_perm;

#ifdef NICSLUC_DEBUG
	fp = fopen("smat0.txt", "w");
	for (i=0; i<nicslu->nnz; ++i)
	{
		fprintf(fp, "%.16g\n", ax[i]);
	}
	fclose(fp);
#endif

	for (i=0; i<n; ++i)
	{
		s = rs[i];/*not permuted*/
		end = ap[i+1];/*not permuted*/
		for (j=ap[i]; j<end; ++j)
		{
			t = s * csp[ai[j]];
			C_MULSELFR(ax[j], t);
		}
	}

#ifdef NICSLUC_DEBUG
	fp = fopen("smat1.txt", "w");
	for (i=0; i<nicslu->nnz; ++i)
	{
		fprintf(fp, "%.16g\n", ax[i]);
	}
	fclose(fp);
#endif
}

void _I_NicsLUc_MC64ScaleForRefact(SNicsLUc *nicslu, complex__t *ax0)
{
	uint__t end;
	uint__t i, j;
	uint__t n;
	complex__t *ax;
	real__t *rs, *csp;
	uint__t *ai, *ap;
	real__t s;
	complex__t t;

	if (!nicslu->cfgi[1]) return;

	n = nicslu->n;
	ax = nicslu->ax;
	ai = nicslu->ai;/*changed in preprocess*/
	ap = nicslu->ap;
	rs = nicslu->row_scale;
	csp = nicslu->col_scale_perm;

	for (i=0; i<n; ++i)
	{
		s = rs[i];/*not permuted*/
		end = ap[i+1];/*not permuted*/
		for (j=ap[i]; j<end; ++j)
		{
			C_MULR(t, ax0[j], s);
			C_MULR(ax[j], t, csp[ai[j]]);
		}
	}
}
