/*last modified: july 11, 2013*/

#include "nicsluc.h"
#include "nicsluc_internal.h"
#include "complex.h"

int NicsLUc_MergeDuplicateEntries(uint__t n, uint__t *nnz, complex__t **ax, uint__t **ai, uint__t **ap)
{
	complex__t *nax, *bx;
	uint__t *nai, *nap, *bi, *bp, nz;
	uint__t i, j, end, col;
	int err;
	int__t pre;

	if (NULL == nnz || NULL == ax || NULL == ai || NULL == ap)
	{
		return NICSLU_ARGUMENT_ERROR;
	}
	if (NULL == *ax || NULL == *ai || NULL == *ap)
	{
		return NICSLU_ARGUMENT_ERROR;
	}
	if (0 == n || 0 == *nnz || *nnz != (*ap)[n])
	{
		return NICSLU_MATRIX_INVALID;
	}

	bx = *ax;
	bi = *ai;
	bp = *ap;
	nz = *nnz;

	nax = (complex__t *)malloc(sizeof(complex__t)*nz);
	nai = (uint__t *)malloc(sizeof(uint__t)*nz);
	nap = (uint__t *)malloc(sizeof(uint__t)*(1+n));

	if (NULL == nax || NULL == nai || NULL == nap)
	{
		if (nax != NULL) free(nax);
		if (nai != NULL) free(nai);
		if (nap != NULL) free(nap);
		return NICSLU_MEMORY_OVERFLOW;
	}

	err = _I_NicsLUc_Sort(n, bx, bi, bp, nax, nai, nap);
	if (FAIL(err))
	{
		free(nax);
		free(nai);
		free(nap);
		return err;
	}

	nz = 0;
	*nap = 0;
	for (i=0; i<n; ++i)
	{
		pre = -1;
		end = bp[i+1];
		for (j=bp[i]; j<end; ++j)
		{
			col = bi[j];
			if (((int__t)col) != pre)
			{
				pre = col;
				C_CPY(nax[nz], bx[j]);
				nai[nz] = col;
				++nz;
			}
			else
			{
				C_ADDSELF(nax[nz-1], bx[j]);
			}
		}
		nap[i+1] = nz;
	}

	*nnz = nz;
	nax = (complex__t *)realloc(nax, sizeof(complex__t)*nz);
	nai = (uint__t *)realloc(nai, sizeof(uint__t)*nz);
	/*here both arrays are shortened, so no errors can occur*/

	free(*ax);
	free(*ai);
	free(*ap);
	*ax = nax;
	*ai = nai;
	*ap = nap;

	return NICS_OK;
}
