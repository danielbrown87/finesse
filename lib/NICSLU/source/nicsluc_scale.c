/*scale the matrix*/
/*last modified: aug 11, 2013*/
/*author: Chen, Xiaoming*/

#include "nicsluc.h"
#include "nicsluc_internal.h"
#include "complex.h"

int _I_NicsLUc_Scale(SNicsLUc *nicslu)
{
	uint__t n, i, j, col;
	uint__t scale;
	complex__t *cscale;
	complex__t *ax;
	uint__t *ai, *ap;
	uint__t cend, oldrow;
	uint__t *rowperm;

	scale = nicslu->cfgi[2];
	if (scale == 0 || scale > 2) return NICS_OK;

	n = nicslu->n;
	cscale = nicslu->cscale;
	
	ax = nicslu->ax;
	ai = nicslu->ai;
	ap = nicslu->ap;
	rowperm = nicslu->row_perm;
	
	memset(cscale, 0, sizeof(complex__t)*n);

	if (scale == 1)/*maximum*/
	{
		real__t val, *sqsum;
		sqsum = (real__t *)nicslu->workspace;
		memset(sqsum, 0, sizeof(real__t)*n);

		for (i=0; i<n; ++i)
		{
			oldrow = rowperm[i];
			cend = ap[oldrow+1];
			for (j=ap[oldrow]; j<cend; ++j)
			{
				col = ai[j];
				val = C_SQSUM(ax[j]);
				if (val > sqsum[col])
				{
					C_CPY(cscale[col], ax[j]);
					sqsum[col] = val;
				}
			}
		}
	}
	else if (scale == 2)/*sum*/
	{
		complex__t val;
		for (i=0; i<n; ++i)
		{
			oldrow = rowperm[i];
			cend = ap[oldrow+1];
			for (j=ap[oldrow]; j<cend; ++j)
			{
				col = ai[j];
				C_CPY(val, ax[j]);
				C_POSI(val);
				C_ADDSELF(cscale[col], val);
			}
		}
	}

	for (i=0; i<n; ++i)
	{
		if (C_EQZ(cscale[i])) return NICSLU_MATRIX_NUMERIC_SINGULAR;
	}

	return NICS_OK;
}
