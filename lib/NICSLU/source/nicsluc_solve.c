/*solve Ly=b and Ux=y*/
/*last modified: aug 17, 2013*/
/*author: Chen, Xiaoming*/

#include "nicsluc.h"
#include "nicsluc_internal.h"
#include "timer_c.h"
#include "complex.h"

extern size_t cg_si, cg_sd, cg_sp;

int NicsLUc_Solve(SNicsLUc *nicslu, complex__t *rhs)
{
	uint__t mc64_scale, scale;
	real__t *rows, *cols;
	uint__t j, p, n;
	int__t jj;
	complex__t *b;
	uint__t *rp, *cp;
	complex__t sum, t;
	size_t *up;
	uint__t *ip;
	complex__t *x;
	uint__t ul, ll;
	uint__t *llen, *ulen;
	complex__t *ldiag;
	void *lu;
	uint__t *piv;
	complex__t *cscale;

	/*check inputs*/
	if (NULL == nicslu || NULL == rhs)
	{
		return NICSLU_ARGUMENT_ERROR;
	}
	if (!nicslu->flag[2])
	{
		return NICSLU_MATRIX_NOT_FACTORIZED;
	}

	/*begin*/
	TimerStart((STimer *)(nicslu->timer));

	n = nicslu->n;
	b = nicslu->rhs;
	up = nicslu->up;
	llen = nicslu->llen;
	ulen = nicslu->ulen;
	ldiag = nicslu->ldiag;
	lu = nicslu->lu_array;
	mc64_scale = nicslu->cfgi[1];
	scale = nicslu->cfgi[2];
	cscale = nicslu->cscale;

	if (nicslu->cfgi[0] == 0)
	{
		rp = nicslu->row_perm;
		cp = nicslu->col_perm_inv;
		piv = (uint__t *)(nicslu->pivot_inv);
		rows = nicslu->row_scale;
		cols = nicslu->col_scale_perm;

		if (mc64_scale)
		{
			for (j=0; j<n; ++j)
			{
				p = rp[j];
				C_MULR(b[j], rhs[p], rows[p]);
			}
		}
		else
		{
			for (j=0; j<n; ++j)
			{
				C_CPY(b[j], rhs[rp[j]]);
			}
		}

		for (j=0; j<n; ++j)
		{
			C_CLR(sum);
			ll = llen[j];
			ul = ulen[j];

			ip = (uint__t *)(((byte__t *)lu) + up[j] + ul*cg_sp);
			x = (complex__t *)(ip + ll);

			for (p=0; p<ll; ++p)
			{
				C_MUL(t, x[p], b[ip[p]]);
				C_ADDSELF(sum, t);
			}

			C_SUBSELF(b[j], sum);
			C_DIVSELF(b[j], ldiag[j]);
		}

		for (jj=n-1; jj>=0; --jj)
		{
			C_CLR(sum);
			ul = ulen[jj];

			ip = (uint__t *)(((byte__t *)lu) + up[jj]);
			x = (complex__t *)(ip + ul);

			for (p=0; p<ul; ++p)
			{
				C_MUL(t, x[p], b[ip[p]]);
				C_ADDSELF(sum, t);
			}

			C_SUBSELF(b[jj], sum);
		}

		if (scale == 1 || scale == 2)
		{
			if (mc64_scale)
			{
				for (j=0; j<n; ++j)
				{
					p = cp[j];
					C_MULR(t, b[piv[p]], cols[p]);
					C_DIV(rhs[j], t, cscale[p]);
				}
			}
			else
			{
				for (j=0; j<n; ++j)
				{
					p = cp[j];
					C_DIV(rhs[j], b[piv[p]], cscale[p]);
				}
			}
		}
		else
		{
			if (mc64_scale)
			{
				for (j=0; j<n; ++j)
				{
					p = cp[j];
					C_MULR(rhs[j], b[piv[p]], cols[p]);
				}
			}
			else
			{
				for (j=0; j<n; ++j)
				{
					C_CPY(rhs[j], b[piv[cp[j]]]);
				}
			}
		}
	}
	else/*CSC*/
	{
		rp = nicslu->col_perm;
		cp = nicslu->row_perm_inv;
		piv = (uint__t *)(nicslu->pivot);
		rows = nicslu->col_scale_perm;
		cols = nicslu->row_scale;

		if (scale == 1 || scale == 2)
		{
			if (mc64_scale)
			{
				for (j=0; j<n; ++j)
				{
					p = piv[j];
					C_MULR(t, rhs[rp[p]], rows[p]);
					C_DIV(b[j], t, cscale[p]);
				}
			}
			else
			{
				for (j=0; j<n; ++j)
				{
					p = piv[j];
					C_DIV(b[j], rhs[rp[p]], cscale[p]);
				}
			}
		}
		else
		{
			if (mc64_scale)
			{
				for (j=0; j<n; ++j)
				{
					p = piv[j];
					C_MULR(b[j], rhs[rp[p]], rows[p]);
				}
			}
			else
			{
				for (j=0; j<n; ++j)
				{
					C_CPY(b[j], rhs[rp[piv[j]]]);
				}
			}
		}

		for (j=0; j<n; ++j)
		{
			C_CPY(sum, b[j]);
			ul = ulen[j];

			ip = (uint__t *)(((byte__t *)lu) + up[j]);
			x = (complex__t *)(ip + ul);

			for (p=0; p<ul; ++p)
			{
				C_MUL(t, x[p], sum);
				C_SUBSELF(b[ip[p]], t);
			}
		}

		for (jj=n-1; jj>=0; --jj)
		{
			C_CPY(sum, b[jj]);
			ll = llen[jj];
			ul = ulen[jj];

			ip = (uint__t *)(((byte__t *)lu) + up[jj] + ul*cg_sp);
			x = (complex__t *)(ip + ll);

			C_DIVSELF(sum, ldiag[jj]);
			C_CPY(b[jj], sum);

			for (p=0; p<ll; ++p)
			{
				C_MUL(t, x[p], sum);
				C_SUBSELF(b[ip[p]], t);
			}
		}

		if (mc64_scale)
		{
			for (j=0; j<n; ++j)
			{
				C_MULR(rhs[j], b[cp[j]], cols[j]);
			}
		}
		else
		{
			for (j=0; j<n; ++j)
			{
				C_CPY(rhs[j], b[cp[j]]);
			}
		}
	}

	/*finish*/
	TimerStop((STimer *)(nicslu->timer));
	nicslu->stat[3] = TimerGetRuntime((STimer *)(nicslu->timer));

	return NICS_OK;
}

int NicsLUc_SolveFast(SNicsLUc *nicslu, complex__t *rhs)
{
	uint__t mc64_scale, scale;
	real__t *rows, *cols;
	uint__t j, p, n;
	int__t jj;
	complex__t *b;
	uint__t *rp, *cp;
	complex__t sum, t;
	size_t *up;
	uint__t *ip;
	complex__t *x;
	uint__t ul, ll;
	uint__t *llen, *ulen;
	complex__t *ldiag;
	void *lu;
	uint__t *piv;
	complex__t *cscale;
	int__t first;
	uint__t col;
	complex__t val;

	/*check inputs*/
	if (NULL == nicslu || NULL == rhs)
	{
		return NICSLU_ARGUMENT_ERROR;
	}
	if (!nicslu->flag[2])
	{
		return NICSLU_MATRIX_NOT_FACTORIZED;
	}

	/*begin*/
	TimerStart((STimer *)(nicslu->timer));

	n = nicslu->n;
	b = nicslu->rhs;
	up = nicslu->up;
	llen = nicslu->llen;
	ulen = nicslu->ulen;
	ldiag = nicslu->ldiag;
	lu = nicslu->lu_array;
	mc64_scale = nicslu->cfgi[1];
	scale = nicslu->cfgi[2];
	cscale = nicslu->cscale;
	first = -1;

	if (nicslu->cfgi[0] == 0)
	{
		rp = nicslu->row_perm;
		cp = nicslu->col_perm_inv;
		piv = (uint__t *)(nicslu->pivot_inv);
		rows = nicslu->row_scale;
		cols = nicslu->col_scale_perm;

		if (mc64_scale)
		{
			for (j=0; j<n; ++j)
			{
				p = rp[j];
				C_MULR(b[j], rhs[p], rows[p]);
				if (first < 0 && C_NEZ(b[j])) first = j;
			}
		}
		else
		{
			for (j=0; j<n; ++j)
			{
				C_CPY(b[j], rhs[rp[j]]);
				if (first < 0 && C_NEZ(b[j])) first = j;
			}
		}

		/*all 0*/
		if (first < 0)
		{
			memset(rhs, 0, sizeof(complex__t)*n);
			return NICS_OK;
		}

		for (j=first; j<n; ++j)
		{
			C_CLR(sum);
			ll = llen[j];
			ul = ulen[j];

			ip = (uint__t *)(((byte__t *)lu) + up[j] + ul*cg_sp);
			x = (complex__t *)(ip + ll);

			for (p=0; p<ll; ++p)
			{
				col = ip[p];
				C_CPY(val, b[col]);
				if (col >= (uint__t)first && C_NEZ(val))
				{
					C_MUL(t, x[p], val);
					C_ADDSELF(sum, t);
				}
			}

			C_SUBSELF(b[j], sum);
			C_DIVSELF(b[j], ldiag[j]);
		}

		for (jj=n-1; jj>=0; --jj)
		{
			C_CLR(sum);
			ul = ulen[jj];

			ip = (uint__t *)(((byte__t *)lu) + up[jj]);
			x = (complex__t *)(ip + ul);

			for (p=0; p<ul; ++p)
			{
				C_CPY(val, b[ip[p]]);
				if (C_NEZ(val))
				{
					C_MUL(t, x[p], val);
					C_ADDSELF(sum, t);
				}
			}

			C_SUBSELF(b[jj], sum);
		}

		if (scale == 1 || scale == 2)
		{
			if (mc64_scale)
			{
				for (j=0; j<n; ++j)
				{
					p = cp[j];
					C_MULR(t, b[piv[p]], cols[p]);
					C_DIV(rhs[j], t, cscale[p]);
				}
			}
			else
			{
				for (j=0; j<n; ++j)
				{
					p = cp[j];
					C_DIV(rhs[j], b[piv[p]], cscale[p]);
				}
			}
		}
		else
		{
			if (mc64_scale)
			{
				for (j=0; j<n; ++j)
				{
					p = cp[j];
					C_MULR(rhs[j], b[piv[p]], cols[p]);
				}
			}
			else
			{
				for (j=0; j<n; ++j)
				{
					C_CPY(rhs[j], b[piv[cp[j]]]);
				}
			}
		}
	}
	else/*CSC*/
	{
		rp = nicslu->col_perm;
		cp = nicslu->row_perm_inv;
		piv = (uint__t *)(nicslu->pivot);
		rows = nicslu->col_scale_perm;
		cols = nicslu->row_scale;

		if (scale == 1 || scale == 2)
		{
			if (mc64_scale)
			{
				for (j=0; j<n; ++j)
				{
					p = piv[j];
					C_MULR(t, rhs[rp[p]], rows[p]);
					C_DIV(b[j], t, cscale[p]);
					if (first < 0 && C_NEZ(b[j])) first = j;
				}
			}
			else
			{
				for (j=0; j<n; ++j)
				{
					p = piv[j];
					C_DIV(b[j], rhs[rp[p]], cscale[p]);
					if (first < 0 && C_NEZ(b[j])) first = j;
				}
			}
		}
		else
		{
			if (mc64_scale)
			{
				for (j=0; j<n; ++j)
				{
					p = piv[j];
					C_MULR(b[j], rhs[rp[p]], rows[p]);
					if (first < 0 && C_NEZ(b[j])) first = j;
				}
			}
			else
			{
				for (j=0; j<n; ++j)
				{
					C_CPY(b[j], rhs[rp[piv[j]]]);
					if (first < 0 && C_NEZ(b[j])) first = j;
				}
			}
		}

		/*all 0*/
		if (first < 0)
		{
			memset(rhs, 0, sizeof(complex__t)*n);
			return NICS_OK;
		}

		for (j=(uint__t)first; j<n; ++j)
		{
			C_CPY(sum, b[j]);

			if (C_NEZ(sum))
			{
				ul = ulen[j];

				ip = (uint__t *)(((byte__t *)lu) + up[j]);
				x = (complex__t *)(ip + ul);

				for (p=0; p<ul; ++p)
				{
					C_MUL(t, x[p], sum);
					C_SUBSELF(b[ip[p]], t);
				}
			}
		}

		for (jj=n-1; jj>=0; --jj)
		{
			C_CPY(sum, b[jj]);

			if (C_NEZ(sum))
			{
				C_DIVSELF(sum, ldiag[jj]);
				C_CPY(b[jj], sum);

				ll = llen[jj];
				ul = ulen[jj];

				ip = (uint__t *)(((byte__t *)lu) + up[jj] + ul*cg_sp);
				x = (complex__t *)(ip + ll);

				for (p=0; p<ll; ++p)
				{
					C_MUL(t, x[p], sum);
					C_SUBSELF(b[ip[p]], t);
				}
			}
			else
			{
				C_CLR(b[jj]);
			}
		}

		if (mc64_scale)
		{
			for (j=0; j<n; ++j)
			{
				C_MULR(rhs[j], b[cp[j]], cols[j]);
			}
		}
		else
		{
			for (j=0; j<n; ++j)
			{
				C_CPY(rhs[j], b[cp[j]]);
			}
		}
	}

	/*finish*/
	TimerStop((STimer *)(nicslu->timer));
	nicslu->stat[3] = TimerGetRuntime((STimer *)(nicslu->timer));

	return NICS_OK;
}
