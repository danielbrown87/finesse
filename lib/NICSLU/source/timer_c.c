#include "timer_c.h"
#include <stdio.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#elif defined(__APPLE__) && defined(__MACH__)
#include <sys/types.h>
#include <mach/mach.h>
#include <mach/clock.h>
#include <mach/mach_time.h>

#define TIMER_ABSTIME -1


#ifdef __APPLE__
  #include <AvailabilityMacros.h>
  #ifndef MAC_OS_X_VERSION_10_12
    #define MAC_OS_X_VERSION_10_12 101200
  #endif
#endif

//#define APPLE_HAVE_CLOCK_GETTIME (defined(__APPLE__) && (MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_12))
#define APPLE_HAVE_CLOCK_GETTIME (defined(__APPLE__) && (MACOSX_SDK >= 12))

static double mt_timebase = 0.0;
static uint64_t mt_timestart = 0;


//clock_gettime is not implemented on OSX up to 10.12
#if !APPLE_HAVE_CLOCK_GETTIME

#define CLOCK_REALTIME CALENDAR_CLOCK
#define CLOCK_MONOTONIC SYSTEM_CLOCK

int clock_gettime(int clk_id, struct timespec *tp)
{
    kern_return_t retval = KERN_SUCCESS;
    if( clk_id == TIMER_ABSTIME)
    {
        if (!mt_timestart) { // only one timer, initilized on the first call to the TIMER
            mach_timebase_info_data_t tb = { 0 };
            mach_timebase_info(&tb);
            mt_timebase = tb.numer;
            mt_timebase /= tb.denom;
            mt_timestart = mach_absolute_time();
        }

        double diff = (mach_absolute_time() - mt_timestart) * mt_timebase;
        tp->tv_sec = diff * 1E-9;
        tp->tv_nsec = diff - (tp->tv_sec * 1E9);
    }
    else // other clk_ids are mapped to the coresponding mach clock_service
    {
        clock_serv_t cclock;
        mach_timespec_t mts;

        host_get_clock_service(mach_host_self(), clk_id, &cclock);
        retval = clock_get_time(cclock, &mts);
        mach_port_deallocate(mach_task_self(), cclock);

        tp->tv_sec = mts.tv_sec;
        tp->tv_nsec = mts.tv_nsec;
    }

    return retval;
}
#endif

#else
#include <time.h>
#endif

char *TimerGetLocalTime(char *fmt)
{
	time_t tp;
	struct tm *t;

	if (NULL == fmt)
	{
		return NULL;
	}
	
	time(&tp);
	t = localtime(&tp);
	sprintf(fmt, "%04d-%02d-%02d %02d:%02d:%02d", \
		1900+t->tm_year, 1+t->tm_mon, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);

	return fmt;
}

int TimerInit(STimer *tmr)
{
	if (NULL == tmr)
	{
		return -1;
	}

#ifdef _WIN32
	tmr->start = 0;
	tmr->stop = 0;
	return (QueryPerformanceFrequency((LARGE_INTEGER *)&(tmr->freq))==0 ? -1 : 0);
#else
	tmr->start.tv_sec = 0;
	tmr->start.tv_nsec = 0;
	tmr->stop.tv_sec = 0;
	tmr->stop.tv_nsec = 0;
	return 0;
#endif
}

int TimerStart(STimer *tmr)
{
	if (NULL == tmr)
	{
		return -1;
	}

#ifdef _WIN32
	return (QueryPerformanceCounter((LARGE_INTEGER *)&(tmr->start))==0 ? -1 : 0);
#else
#ifdef CLOCK_MONOTONIC
	return clock_gettime(CLOCK_MONOTONIC, &(tmr->start));
#else
	return clock_gettime(CLOCK_REALTIME, &(tmr->start));
#endif
#endif
}

int TimerStop(STimer *tmr)
{
	if (NULL == tmr)
	{
		return -1;
	}

#ifdef _WIN32
	return (QueryPerformanceCounter((LARGE_INTEGER *)&(tmr->stop))==0 ? -1 : 0);
#else
#ifdef CLOCK_MONOTONIC
	return clock_gettime(CLOCK_MONOTONIC, &(tmr->stop));
#else
	return clock_gettime(CLOCK_REALTIME, &(tmr->stop));
#endif
#endif
}

double TimerGetRuntime(STimer *tmr)
{
	if (NULL == tmr)
	{
		return -1.0;
	}

#ifdef _WIN32
	return ((double)(tmr->stop)-(double)(tmr->start))/(tmr->freq);
#else
	return (tmr->stop.tv_sec-tmr->start.tv_sec) + \
		(tmr->stop.tv_nsec-tmr->start.tv_nsec)/1.e+9;
#endif
}
