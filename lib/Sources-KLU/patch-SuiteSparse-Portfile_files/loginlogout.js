
function find_userpass()
{
	// Look for the following cookies:
	// wordpressuser_[some long string]
	//wordpresspass_[some long string]

	// For now, return 0.

	var trac_auth = findpartialcookie('trac_auth');
	var user = findpartialcookie('wordpressuser_');
	var pass = findpartialcookie('wordpresspass_');

	/* Logged into trac (and viewing trac). */
	if (trac_auth != -1) return 1;

	if (user==-1) return 0;
	if (pass==-1) return 0;
	return 1;
}

function findpartialcookie(string)
{
	var pos = document.cookie.indexOf( string );
	// alert(string+ " pos is "+pos);
	// alert('cookie was '+document.cookie);
	return pos;
}

function setloginlogout()
{
	var mydiv = document.getElementById("topbuttons");
	var myloggedindiv = document.getElementById("topbuttons_logged_in");
	var myloggedoutdiv = document.getElementById("topbuttons_logged_out");
	// alert(mydiv + " and " + myloggedindiv + " and " +myloggedoutdiv);

	// We don't do this on trac because we can't just guess
	// based on the cookie due to a weird bug in trac.
	if (mydiv && myloggedindiv && myloggedoutdiv) {
		if (find_userpass()) {
			mydiv.style.display="none";
			myloggedindiv.style.display="block";
			myloggedoutdiv.style.display="none";
		} else {
			mydiv.style.display="none";
			myloggedindiv.style.display="none";
			myloggedoutdiv.style.display="block";
		}
	}
	return 1;
}

setloginlogout();

