
	six_is_home=1; /* 1 = home, 0 = help */

	function gohome()
	{
		var pathparts = document.URL.split('/');
		var name = pathparts[4].toLowerCase();
		// alert('name is '+name);
		switch(name) {
			/* These cases have home on wordpress. */
			case 'launchd':
			case 'kernel':
			case 'dss':
			case 'stage':
			case 'bonjour':
			case 'odcctools':
				document.location = 'http://'+name+'.macosforge.org/'; break;

			/* These cases have home on trac (wiki). */
			case 'calendarserver':
			case 'appscript':
			case 'darwinbuild':
			case 'rubycocoa':
				document.location = 'http://trac.macosforge.org/projects/'+name; break;

			/* These projects live on their own vhosts. */
			case 'macports':
				document.location = 'http://www.macports.org/'; break;
			case 'webkit':
				document.location = 'http://www.webkit.org/'; break;
			default:
				/* Fallback. */
				document.location = 'http://www.macosforge.org/';
		}
		return 1;
	}

	function getheadname()
	{
		var name=document.URL.split('/')[5];
		var jumppos = name.indexOf('#');
		if (jumppos != -1) {
			name = name.substr(0, jumppos);
		}
		name = name.toLowerCase();
		// alert('name is '+name);
                switch(name) {
                        case 'wiki':
                        case 'timeline':
                        case 'roadmap':
                        case 'browser':
                        case 'report':
                        case 'search':
                        case 'admin':
				return name;
                        case '': return 'wiki';
                        case 'newticket': return 'report';
                }
		return name; // best guess.
	}

	function fixhead()
	{
		var name = getheadname();
		if (name == '') return;
		var buttonname = 'button_'+name;
		// alert(buttonname);
                var img = document.getElementsByName(buttonname)[0];
		var origpath = img.src;
		origpath = origpath.replace(/\-dark\.gif/, "-light.gif");
		img.src = origpath;
                // alert('Changed to '+img.src+'\n');
        }

        // this function gets the cookie, if it exists
        // liberally lifted from "free scripts" at:
        // http://techpatterns.com/downloads/javascript_cookies.php
        function Get_Cookie( name )
	{
                var start = document.cookie.indexOf( name + "=" );
                var len = start + name.length + 1;
                if ( ( !start ) &&
                        ( name != document.cookie.substring( 0, name.length ) ) )
                {       
                        return null;
                }
                if ( start == -1 ) return null;
                var end = document.cookie.indexOf( ";", len );
                if ( end == -1 ) end = document.cookie.length;
                return unescape( document.cookie.substring( len, end ) );
        }
        document.onload = fixhead();
