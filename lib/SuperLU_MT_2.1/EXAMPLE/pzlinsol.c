
/*
 * -- SuperLU MT routine (version 2.0) --
 * Lawrence Berkeley National Lab, Univ. of California Berkeley,
 * and Xerox Palo Alto Research Center.
 * September 10, 2007
 *
 */
#include "pzsp_defs.h"
#include "math.h"

void fill_mirror_matrix(doublecomplex *optM, int *rowind, int *colind, int mtxpos, double r, 
						double t, double phi, double k, double L, int a2src, int a4src,
						 int *rix, int *cix);

void zPrint_CCS(doublecomplex *nzval, int *ridx, int *cidx, int nnz, int n, int m);

const double PI = 3.141592653589793;
const double c = 299792458;
	
main(int argc, char *argv[])
{
    SuperMatrix   A;
    NCformat *Astore;
	DNformat *BStore, *XStore;
    doublecomplex   *a;
	int      *asub, *xa;
    int      *perm_r; /* row permutations from partial pivoting */
    int      *perm_c; /* column permutation vector */
    SuperMatrix   L;       /* factor L */
    SCPformat *Lstore;
    SuperMatrix   U;       /* factor U */
    NCPformat *Ustore;
    SuperMatrix   B, X;
	double      u, drop_tol, rpg, rcond;
    int      nrhs, ldx, info, m, n, nnz, b;
    int      nprocs; /* maximum number of processors to use. */
    int      panel_size, relax, maxsup;
    int      permc_spec;
    trans_t  trans;
    doublecomplex      *rhsb, *rhsx, *xact;
    superlu_memusage_t  memusage;
	double      *R, *C;
    double      *ferr, *berr;
	equed_t     equed;
	
    nrhs              = 1;
    trans             = NOTRANS;
    nprocs            = 1;
    n                 = 8;
	m                 = 8;
    b                 = 1;
    panel_size        = sp_ienv(1);
    relax             = sp_ienv(2);
    maxsup            = sp_ienv(3);
	equed			  = NOEQUIL;
	
    superlumt_options_t options;
				   
	double r1 = sqrt(0.9);
	double t1 = sqrt(1-r1*r1);
	double r2 = sqrt(0.9);
	double t2 = sqrt(1-r1*r1);
	
	double Lcav = 1;
	double ETMphi = 0; // tuning in degrees
	double ITMphi = 0; // tuning in degrees
	
	double lambda = 1064e-9;
	double f = c / lambda;
	const int optMX = 8, optMY = 8;
	double k = 2*PI / lambda;
	
	nnz = 18;
	
	a = doublecomplexMalloc(nnz);
	asub = intMalloc(nnz);
	xa = intMalloc(optMY + 1);
		
	xa[optMY] = nnz;
	
	/*
	printf("row ind\n");
	
	for(int i=0; i<nnz; i++){
		printf("%i: %i = %f+%f\n",i, asub[i], a[i].r, a[i].i);
	}
	
	printf("col ind\n");
	
	for(int i=0; i<optMY+1; i++){
		printf("%i: %i\n",i, xa[i]);
	}
	*/
	
    zCreate_CompCol_Matrix(&A, m, n, nnz, a, asub, xa, SLU_NC, SLU_Z, SLU_GE);
    
	Astore = A.Store;
    	    
    if (!(rhsx = doublecomplexMalloc(m * nrhs)))
		SUPERLU_ABORT("Malloc fails for rhsx[].");
		
	if (!(rhsb = doublecomplexMalloc(m * nrhs)))
		SUPERLU_ABORT("Malloc fails for rhsb[].");
		
    zCreate_Dense_Matrix(&B, m, nrhs, rhsb, m, SLU_DN, SLU_Z, SLU_GE);
	zCreate_Dense_Matrix(&X, m, nrhs, rhsx, m, SLU_DN, SLU_Z, SLU_GE);
		
	XStore = (DNformat*)X.Store;
	BStore = (DNformat*)B.Store;
	
	doublecomplex *xnzval = (doublecomplex*)XStore->nzval;		
	doublecomplex *bnzval = (doublecomplex*)BStore->nzval;
	
	for(int i=0; i<n; i++){
		rhsx[i].r = 0;
		rhsx[i].i = 0;
		rhsb[i].r = 0;
		rhsb[i].i = 0;
	}
	
	// set lasers
	rhsb[0].r = 1;
	rhsb[0].i = 0;
	
    xact = doublecomplexMalloc(n * nrhs);
    
	ldx = n;
	
    if (!(perm_r = intMalloc(m))) 
		SUPERLU_ABORT("Malloc fails for perm_r[].");
		
    if (!(perm_c = intMalloc(n)))
		SUPERLU_ABORT("Malloc fails for perm_c[].");

	if (!(R = (double *) SUPERLU_MALLOC(A.nrow * sizeof(double)))) 
        SUPERLU_ABORT("SUPERLU_MALLOC fails for R[].");
    
	if ( !(C = (double *) SUPERLU_MALLOC(A.ncol * sizeof(double))) )
        SUPERLU_ABORT("SUPERLU_MALLOC fails for C[].");
		
	if ( !(ferr = (double *) SUPERLU_MALLOC(nrhs * sizeof(double))) )
        SUPERLU_ABORT("SUPERLU_MALLOC fails for ferr[].");
    
	if ( !(berr = (double *) SUPERLU_MALLOC(nrhs * sizeof(double))) ) 
        SUPERLU_ABORT("SUPERLU_MALLOC fails for berr[].");
		
	options.nprocs = nprocs;
    options.fact = EQUILIBRATE;
    options.trans = NOTRANS;
    options.refact = NO;
    options.panel_size = sp_ienv(1);
    options.relax = sp_ienv(2);
    options.diag_pivot_thresh = 1.0;
    options.usepr = NO;
    options.drop_tol = 1e-6;
    options.SymmetricMode = NO;
    options.PrintStat = NO;
    options.perm_c = perm_c;
    options.perm_r = perm_r;
    options.lwork = 0;
	
    /*
     * Get column permutation vector perm_c[], according to permc_spec:
     *   permc_spec = 0: natural ordering 
     *   permc_spec = 1: minimum degree ordering on structure of A'*A
     *   permc_spec = 2: minimum degree ordering on structure of A'+A
     *   permc_spec = 3: approximate minimum degree for unsymmetric matrices
     */    	
    permc_spec = 1;
    get_perm_c(permc_spec, &A, perm_c);
	
	FILE *file = fopen("output.mat","w");
	fprintf(file,"M=[\n");
	const int N = 10000;
	
	for(int i=0; i < N; i++){
		int cix=0, rix=0;
		ITMphi = 360.0 * i / (double)N;
		
		fill_mirror_matrix(a, asub, xa, 0, r1, t1, ITMphi, k, Lcav, 4, -1, &rix, &cix);
		fill_mirror_matrix(a, asub, xa, 4, r2, t2, ETMphi, k, Lcav, -1, 2	, &rix, &cix);
		
		//zPrint_CCS((doublecomplex*)Astore->nzval, Astore->rowind, Astore->colptr, nnz, n, m);
		
		pzgssvx(nprocs, &options, &A, perm_c, perm_r,
			&equed, R, C, &L, &U, &B, &X, &rpg, &rcond,
			ferr, berr, &memusage, &info);
		
		fprintf(file, "%f ", ITMphi);
		
		for(int i=0; i<n; i++){
			fprintf(file,"%.15f%s%.15fi ", xnzval[i].r, (xnzval[i].i < 0) ? "-" : "+" ,fabs(xnzval[i].i));
		}
		
		fprintf(file,";\n");
	}
	
	fprintf(file,"]");
	fclose(file);
	
    SUPERLU_FREE (rhsx);
	SUPERLU_FREE (rhsb);
    SUPERLU_FREE (xact);
    SUPERLU_FREE (perm_r);
    SUPERLU_FREE (perm_c);
    Destroy_CompCol_Matrix(&A);
    Destroy_SuperMatrix_Store(&B);
    Destroy_SuperNode_SCP(&L);
    Destroy_CompCol_NCP(&U);

}

void fill_mirror_matrix(doublecomplex *optM, int *rowind, int *colind, int mtxpos, double r, 
						double t, double phi, double k, double L, int a2src, int a4src,
						 int *rix, int *cix){
	int col = *cix;
	int el = *rix;
	
	// column 1
	colind[col++] = 0 + el;
	
	rowind[el] = 0 + mtxpos;
	optM[el++].r = 1;
	
	rowind[el] = 1 + mtxpos;
	optM[el++].i = -t;
		
	rowind[el] = 3 + mtxpos;
	optM[el].r =   -r * cos(phi*2.0*PI/360.0);
	optM[el++].i = -r * sin(phi*2.0*PI/360.0);
	
	// column 2
	colind[col++] = el;
	rowind[el] = 1 + mtxpos;
	optM[el++].r = 1;
	
	// if space is attached
	if(a2src >= 0){
		rowind[el]   = a2src;
		optM[el].r   = -cos(-k*L);
		optM[el++].i = -sin(-k*L);
	}
	
	// column 3
	colind[col++] = el;	
	
	rowind[el] = 1 + mtxpos;
	optM[el].r =   -r * cos(-phi*2.0*PI/360.0);
	optM[el++].i = -r * sin(-phi*2.0*PI/360.0);
	
	rowind[el] = 2 + mtxpos;
	optM[el++].r = 1;
	
	rowind[el] = 3 + mtxpos;
	optM[el++].i = -t;
		
	// column 4
	colind[col++] = el;
	
	rowind[el] = 3 + mtxpos;
	optM[el++].r = 1;
	
	// if space is attached
	if(a4src >= 0){
		rowind[el]   = a4src;
		optM[el].r   = -cos(-k*L);
		optM[el++].i = -sin(-k*L);
	}
	
	*rix = el;
	*cix = col;
}

void zPrint_CCS(doublecomplex *nzval, int *ridx, int *cidx, int nnz, int n, int m){
	doublecomplex *M = doublecomplexMalloc(n*m);
	
	int c = -1;
	int col = 0;
	
	for (int i=0; i<nnz; i++){
		int r = ridx[i];
		
		if (cidx[col] == i){
			c++;
			col++;
		}
		
		int ix = r*m+c;
		printf("%i %i : %i = %f %f\n",r,c,ix,nzval[i].r,nzval[i].i);
		M[ix] = nzval[i];
	}
	
	printf("\nM=[\n");
	for (int i=0; i<m; i++){
		for (int j=0; j<n; j++){
			int ix = i*m+j;
			if(M[ix].i<0)
				printf("%.2f-%.2fi ", M[ix].r,fabs(M[ix].i));
			else
				printf("%.2f+%.2fi ", M[ix].r,fabs(M[ix].i));
		}
		printf(";\n");
	}
	printf("];\n");
	
	SUPERLU_FREE(M);
}


