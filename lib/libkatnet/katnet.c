
#include "katnet.h"

/*!
	Disconnect from the host
	\retval OK
*/
/*!
 *  Quit a connection
 */
int kat_disconnect(mstream *s)
{
  printf("*** kat_disconnect: closing connection...");
  // now quit
  fsendString(s, "QUIT");
  fflush(s->out);
  fclose(s->in);
  fclose(s->out);
  free(s);
  printf("done.\n");
  
  return 0; 
}

/*!
	This will open a socket connection to the host computer.
	
	\retval	mstream on success
	\retval	ERROR on error
*/
mstream * kat_connect(char *hostname, int port)
{
  mstream             *s;
	struct sockaddr_in 	 pin;					  // socket address structure
	struct hostent 			*server;				// server structure
  int                  sd;
  
  // check port against our chosen port range
	if( (port < 11000) || (port > 11010) )
	{
		fprintf(stderr, "### kat_connect: invalid port number\n");
		return NULL;
	}
  
  // look up the hostname of the server
	if((server = gethostbyname(hostname)) == NULL)
	{
		fprintf(stderr, "### kat_connect: error getting internet name for %s\n", hostname);
		return NULL;
	}
  
  // create an mstream for communicating on
  s = (mstream*)malloc(sizeof(mstream));
  if(s == NULL)
  {
    fprintf(stderr, "### kat_connect: error allocating memory.\n"); 
    return NULL;
  }
  
  // setup connection variables
	bzero(&pin, sizeof(pin));	
	pin.sin_family      = AF_INET;
	pin.sin_addr.s_addr = ((struct in_addr *)(server->h_addr))->s_addr;
	pin.sin_port        = htons(port);

  // create a socket
	if((sd = socket(AF_INET, SOCK_STREAM, 0))<0)
	{
		fprintf(stderr, "### kat_connect: error creating socket.\n");
    free(s);
		return NULL;
	}
    
  // try to connect
	printf("*** kat_connect: connecting to %s\n", server->h_name);
	if(connect(sd, (const struct sockaddr*)&pin, sizeof(pin)) < 0)
	{
		fprintf(stderr, "### kat_connect: couldn't connect to %s on %hi\n", 
				inet_ntoa(pin.sin_addr), ntohs(pin.sin_port));
    free(s);
		return NULL;
	}
	else
	{		
    // initialise read/write descriptors on this stream
		netinit(s, sd);	
  }
  
	return(s);
}
