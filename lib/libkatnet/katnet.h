/*
	Header file for libMNet
  
  version:
  release date:
  
  M Hewitson
	
	$Id: katnet.h 184 2006-03-31 08:54:07Z hewitson $
*/

#ifndef KATNET_H
#define KATNET_H


#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>       
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <dirent.h>
#include <fnmatch.h>
#include <netdb.h>
#include "mnet.h"

#define MINPORT 11000
#define MAXPORT 11010

/*----------------------------------------------*/
/*               from katnet.c                  */
/*----------------------------------------------*/
mstream * kat_connect(char *hostname, int port);
int kat_disconnect(mstream *s);



#endif /* KATNET_H */
