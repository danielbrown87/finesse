@ECHO ON
SET LookForFile="%SRC_DIR%\kat.exe"

chdir /d %SRC_DIR%

IF "%ARCH%"=="64" (
    c:\msys64\mingw64 /bin/bash ./finesse.sh --build-win64
)

IF "%ARCH%"=="32" (
    c:\msys32\mingw32 /bin/bash ./finesse.sh --build-win32
)

@ECHO OFF
:CheckForFile
IF EXIST %LookForFile% GOTO FoundIt

TIMEOUT /T 2 >nul

GOTO CheckForFile

:FoundIt
ECHO Found: %LookForFile%
@ECHO ON

TIMEOUT /T 2 >nul 

xcopy %SRC_DIR%\kat.exe "%PREFIX%\Library\bin\"
xcopy %SRC_DIR%\kat.ini "%PREFIX%\Library\bin\"

mkdir %PREFIX%\etc\conda\activate.d
mkdir %PREFIX%\etc\conda\deactivate.d

type NUL > %PREFIX%\etc\conda\activate.d\finesse_vars.bat
type NUL > %PREFIX%\etc\conda\deactivate.d\finesse_vars.bat

echo @ECHO OFF >> %PREFIX%\etc\conda\activate.d\finesse_vars.bat
echo SET KATINI=%%CONDA_PREFIX%%\Library\bin\kat.ini >> %PREFIX%\etc\conda\activate.d\finesse_vars.bat
echo SET FINESSE_DIR=%%CONDA_PREFIX%%\Library\bin >> %PREFIX%\etc\conda\activate.d\finesse_vars.bat

echo @ECHO OFF >> %PREFIX%\etc\conda\deactivate.d\finesse_vars.bat
echo SET KATINI= >> %PREFIX%\etc\conda\deactivate.d\finesse_vars.bat
echo SET FINESSE_DIR= >> %PREFIX%\etc\conda\deactivate.d\finesse_vars.bat