#!/bin/bash

platform='unknown'
unamestr=`uname`
unamem=`uname -m`

if [[ "$unamestr" == 'Linux' ]]; then
    if [[ "${ARCH}" == '32' ]]; then
        ./finesse.sh --build-linux32
    else
        ./finesse.sh --build-linux
    fi
    
    cp ./kat ${PREFIX}/bin
    cp ./kat.ini ${PREFIX}/bin
    
elif [[ "$unamestr" == 'Darwin' ]]; then
    if [[ "${ARCH}" == '32' ]]; then
       ./finesse.sh --build-mac32
    else
       ./finesse.sh --build-mac
    fi
    
    cp ./kat ${PREFIX}/bin
    cp ./kat.ini ${PREFIX}/bin
    
elif [[ "$unamestr" == *CYGWIN* ]]; then
    if [[ "$unamem" == 'i686' ]]; then
        ./finesse.sh --build-win32
    else
        ./finesse.sh --build-win64
    fi
    
    cp ./kat.exe ${PREFIX}/bin
    cp ./kat.ini ${PREFIX}/bin
    
elif [[ "$unamestr" == *MINGW32* ]]; then
    echo "Platform not supported" | tee -a $LOGFILE
    failure
else
   echo "Platform could not be determined" | tee -a $LOGFILE
   failure
fi

# Setup saved env vars https://conda.io/docs/using/envs.html
mkdir -p ${PREFIX}/etc/conda/activate.d
mkdir -p ${PREFIX}/etc/conda/deactivate.d

touch ${PREFIX}/etc/conda/activate.d/finesse_vars.sh
touch ${PREFIX}/etc/conda/deactivate.d/finesse_vars.sh

echo "#!/bin/bash" >> ${PREFIX}/etc/conda/activate.d/finesse_vars.sh
echo "export KATINI=\${CONDA_PREFIX}/bin/kat.ini" >> ${PREFIX}/etc/conda/activate.d/finesse_vars.sh
echo "export FINESSE_DIR=\${CONDA_PREFIX}/bin/" >> ${PREFIX}/etc/conda/activate.d/finesse_vars.sh

echo "#!/bin/bash" >> ${PREFIX}/etc/conda/deactivate.d/finesse_vars.sh
echo "unset KATINI" >> ${PREFIX}/etc/conda/deactivate.d/finesse_vars.sh
echo "unset FINESSE_DIR" >> ${PREFIX}/etc/conda/deactivate.d/finesse_vars.sh