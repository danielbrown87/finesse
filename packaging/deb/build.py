import os
import textwrap

from subprocess import call, check_output

from collections import defaultdict
import string

class SafeDict(dict):
    def __missing__(self, key):
        return '{' + key + '}'

os.chdir("/root/finesse")

call("git pull".split())

call("./finesse.sh --build-linux".split())

os.chdir("/root/finesse/packaging/deb")

call("mkdir -p finesse/DEBIAN".split())

# e.g. Finesse 2.2 (2.2-18-g4e88fe48), 29.05.2018
git_describe = str(check_output('/root/finesse/kat -v'.split()))

version = git_describe.split()[2].strip('(').strip(')').split('-')

vals = {
    "version": version[0],
    "release": version[1],
}


print(vals)

call('mkdir -p finesse/DEBIAN'.format(**vals).split())
call('mkdir -p finesse/usr/bin'.format(**vals).split())
call('mkdir -p finesse/etc/finesse'.format(**vals).split())

call('cp /root/finesse/kat /root/finesse/packaging/deb/finesse/usr/bin'.format(**vals).split())
call('cp /root/finesse/kat.ini /root/finesse/packaging/deb/finesse/etc/finesse'.format(**vals).split())

with open("/root/finesse/packaging/deb/finesse/DEBIAN/control", "w") as f:
    s = string.Formatter().vformat(textwrap.dedent("""
            Package: finesse
            Version: {version}.{release}
            Section: base
            Priority: optional
            Architecture: amd64
            Depends: gsl-bin
            Maintainer: Daniel Brown <finesse@star.sr.bham.ac.uk>
            Description: FINESSE: Frequency domain INterfErometer Simulation SoftwarE
            """), (), SafeDict(**vals))
            
    f.write(s)
            
call("dpkg-deb --build finesse".split())

call("cp /root/finesse/packaging/deb/finesse.deb /host/finesse.{version}.{release}.deb".format(**vals).split())