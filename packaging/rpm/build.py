import os
import textwrap

from subprocess import call, check_output

from collections import defaultdict
import string

class SafeDict(dict):
    def __missing__(self, key):
        return '{' + key + '}'

os.chdir("/root/finesse")

call("git pull".split())

git_describe = str(check_output('git describe --tags'.split())).split("-")

call("./finesse.sh --build-linux".split())

os.chdir("/root/finesse/packaging/rpm")

for d in "RPMS,SRPMS,BUILD,SOURCES,SPECS,tmp".split(","):
    call("mkdir -p rpmbuild/{d}".format(d=d).split())

with open("/root/.rpmmacros", "w") as f:
    f.write("%_topdir   /root/finesse/packaging/rpm/rpmbuild\n")
    f.write("%_tmppath  %{_topdir}/tmp\n")

os.chdir("/root/finesse/packaging/rpm/rpmbuild")

vals = {
    "version": git_describe[0],
    "release": git_describe[1],
}

call('mkdir -p finesse-{version}'.format(**vals).split())
call('mkdir -p finesse-{version}/usr/bin'.format(**vals).split())
call('mkdir -p finesse-{version}/etc/finesse'.format(**vals).split())

call('cp /root/finesse/kat /root/finesse/packaging/rpm/rpmbuild/finesse-{version}/usr/bin'.format(**vals).split())
call('cp /root/finesse/kat.ini /root/finesse/packaging/rpm/rpmbuild/finesse-{version}/etc/finesse'.format(**vals).split())

call('tar -zcvf ./SOURCES/finesse-{version}-{release}.tar.gz finesse-{version}/'.format(**vals).split())

with open("/root/finesse/packaging/rpm/rpmbuild/SPECS/finesse.spec", "w") as f:
    s = string.Formatter().vformat(textwrap.dedent("""
            %define        __spec_install_post %{nil}
            %define          debug_package %{nil}
            %define        __os_install_post %{_dbpath}/brp-compress

            Summary: FINESSE: Frequency domain INterfErometer Simulation SoftwarE
            Name: finesse
            Version: {version}
            Release: {release}
            Requires: gsl
            License: GPL
            Group: Development/Tools
            SOURCE0 : %{name}-%{{version}}-%{{release}}.tar.gz
            URL: http://gwoptics.org/finesse

            BuildRoot: %{_tmppath}/%{name}-%{{version}}-%{{release}}-root

            %description
            %{summary}

            %prep
            %setup -q

            %build
            # Empty section.

            %install
            rm -rf %{buildroot}
            mkdir -p  %{buildroot}

            # in builddir
            cp -a * %{buildroot}


            %clean
            rm -rf %{buildroot}


            %files
            %defattr(-,root,root,-)
            %{_sysconfdir}/%{name}/kat.ini
            %{_bindir}/*

            %changelog
            """), (), SafeDict(**vals))
            
    f.write(s)
            
call("rpmbuild -bb SPECS/finesse.spec".split())

call("cp /root/finesse/packaging/rpm/rpmbuild/RPMS/x86_64/finesse-{version}-{release}.x86_64.rpm /host".format(**vals).split())