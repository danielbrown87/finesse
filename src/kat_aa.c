// $Id: kat_aa.c 3374 2011-09-12 16:29:43Z dzb610 $

/*!  
 * \file kat_aa.c 
 * \brief Routines for the automatic alignment of the interferometer
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#include "kat.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_fortran.h"
#include "kat_optics.h"
#include "kat_aux.h"
#include "kat_dump.h"
#include "kat_aa.h"
#include "kat_check.h"
#include "kat_knm_mirror.h"
#include "kat_knm_bs.h"
#include "kat_knm_bayer.h"
#include "kat_knm_aperture.h"
#include "kat_calc.h"
#include "kat_knm_int.h"
#include <gsl/gsl_cblas.h>
#include <stdlib.h>

//#include <omp.h>

extern init_variables_t init;
extern interferometer_t inter;
extern options_t options;

extern local_var_t vlocal;

extern FILE *fp_log;

extern const complex_t complex_i; //!<  sqrt(-1) or 0 + i
extern const complex_t complex_1; //!<  1 but in complex space:  1 + 0i
extern const complex_t complex_m1; //!< -1 but in complex space: -1 + 0i
extern const complex_t complex_0; //!<  0 but in complex space:  0 + 0i

// define some temporary variable for computations
// these are extern definined in other files
bs_knm_t bstmp;

FILE * ipfile, * idfile;

const ABCD_t Unity = {1.0, 0.0, 0.0, 1.0}; //!< The identity matrix

int n_index; //!< node index; number of nodes in beam trace
bool defaultKNMChangeWarning = false;

zmatrix tmp_knm = NULL;

double mismatch(complex_t q1, complex_t q2) {
    /**
     * Given two beam parameters this computes a figure of merit for
     * how much they overlap, or the amount of mismatch between them.
     * 
     *    1 being no matching
     *    0 being perfect matching
     * 
     * Will return nan if the beam parameter imaginary part is zero or negative.
     * 
     * See ddb thesis equation 1.63 (this uses 1 - Eq.1.63 though)
     * 
     * @param q1
     * @param q2
     * @return Mismatch figure of merit or nan if not calculable
     */
    double result;
    
    if(q1.im <= 0 || q2.im <= 0) {
        result = NAN;
    } else {
        complex_t t1 = z_m_z(cconj(q1), q2);
        double abs_t1 = zmod(t1);
        result = 4.0 * fabs(q1.im * q2.im) / (abs_t1 * abs_t1);
    }
    
    assert((result >= 0 && result <=(1+1e-15)) || isnan(result));
    
    return 1- result;
}

bool __print_mismatch(const char *dir, double mx, double my, char *output) {
    const char *fmt1 = "%s\t%s: %18.10g (x) %18.10g (y)\n";
    const char *fmt2 = "%s\t%s: %18.10g\n";
    
    if(!(isnan(mx) && isnan(my))){
        if(inter.mismatches_options & AVERAGE_Q){
            double am = (mx+my)/2;
            
            if(am >= inter.mismatches_lower){
                sprintf(output, fmt2, output, dir, am);
                return true;
            }
        } else {
            if(mx >= inter.mismatches_lower || my >= inter.mismatches_lower){
                sprintf(output, fmt1, output, dir, mx, my);
                return true;
            }
        }
    }
    
    return false;
}

void __print_two_port_mismatches(const char *component_name, char *name, q_io_t *qm12, q_io_t *qm21, char *output){

    bool displayed = false;

    displayed |= __print_mismatch("1->2", mismatch(qm12->qxi, qm12->qxo),
                                          mismatch(qm12->qyi, qm12->qyo),
                                          output);

    displayed |= __print_mismatch("2->1", mismatch(qm21->qxi, qm21->qxo),
                                          mismatch(qm21->qyi, qm21->qyo),
                                          output);

    if(displayed) {
        message("%s: %s\n", component_name, name);
        message(output);
    }
}

/**
 * When called this will output the current mismatch status of the interferometer.
 */
void output_mismatch() {
    if(!inter.mismatches)
        return;
    
    // Idea here is to loop through each component and output the mode matching
    // for each beam direction within the component.
    
    int i = 0;
   
    message("\n---- Start interferometer mismatches ----\n");
    
    char output[1000] = {0};
    
    for(i=0; i<inter.num_mirrors; i++) {
        mirror_t *m = &inter.mirror_list[i];
        output[0] = '\0';
        
        bool print_refl  = !((inter.mismatches_options & IGNORE_LOW_R_MIRROR_BS) && m->R < inter.mismatches_lower);
        
        bool print_trans = !((inter.mismatches_options & IGNORE_LOW_T_MIRROR_BS) && m->T < inter.mismatches_lower);
        
        bool displayed = false;
        
        if(print_refl) {
            displayed |= __print_mismatch("1->1", mismatch(m->qm.qxt1_11, m->qm.qxt2_11),
                                                  mismatch(m->qm.qyt1_11, m->qm.qyt2_11),
                                                  output);
        }
        
        if(print_trans){
            displayed |= __print_mismatch("1->2", mismatch(m->qm.qxt1_12, m->qm.qxt2_12),
                                                  mismatch(m->qm.qyt1_12, m->qm.qyt2_12),
                                                  output);

            displayed |= __print_mismatch("2->1", mismatch(m->qm.qxt1_21, m->qm.qxt2_21),
                                                  mismatch(m->qm.qyt1_21, m->qm.qyt2_21),
                                                  output);
        }
        
        if(print_refl) {
            displayed |= __print_mismatch("2->2", mismatch(m->qm.qxt1_22, m->qm.qxt2_22),
                                                  mismatch(m->qm.qyt1_22, m->qm.qyt2_22),
                                                  output);
        }
        
        if(displayed) {
            message("mirror: %s\n", m->name);
            message(output);
        }
    }
    
    for(i=0; i<inter.num_beamsplitters; i++) {
        beamsplitter_t *bs = &inter.bs_list[i];
        output[0] = '\0';
        
        bool print_refl  = !((inter.mismatches_options & IGNORE_LOW_R_MIRROR_BS) && bs->R < inter.mismatches_lower);
        
        bool print_trans = !((inter.mismatches_options & IGNORE_LOW_T_MIRROR_BS) && bs->T < inter.mismatches_lower);
        
        bool displayed = false;
       
        if(print_refl) displayed |= __print_mismatch("1->2", mismatch(bs->qm.qxt1_12, bs->qm.qxt2_12),
                               mismatch(bs->qm.qyt1_12, bs->qm.qyt2_12), output);
        
        if(print_trans) displayed |= __print_mismatch("1->3", mismatch(bs->qm.qxt1_13, bs->qm.qxt2_13),
                               mismatch(bs->qm.qyt1_13, bs->qm.qyt2_13), output);
        
        if(print_refl) displayed |= __print_mismatch("2->1", mismatch(bs->qm.qxt1_21, bs->qm.qxt2_21),
                               mismatch(bs->qm.qyt1_21, bs->qm.qyt2_21), output);
        
        if(print_trans) displayed |= __print_mismatch("2->4", mismatch(bs->qm.qxt1_24, bs->qm.qxt2_24),
                               mismatch(bs->qm.qyt1_24, bs->qm.qyt2_24), output);
        
        if(print_trans) displayed |= __print_mismatch("3->1", mismatch(bs->qm.qxt1_31, bs->qm.qxt2_31),
                               mismatch(bs->qm.qyt1_31, bs->qm.qyt2_31), output);
        
        if(print_refl) displayed |= __print_mismatch("3->4", mismatch(bs->qm.qxt1_34, bs->qm.qxt2_34),
                               mismatch(bs->qm.qyt1_34, bs->qm.qyt2_34), output);
        
        if(print_trans) displayed |= __print_mismatch("4->2", mismatch(bs->qm.qxt1_42, bs->qm.qxt2_42),
                               mismatch(bs->qm.qyt1_42, bs->qm.qyt2_42), output);
        
        if(print_refl) displayed |= __print_mismatch("4->3", mismatch(bs->qm.qxt1_43, bs->qm.qxt2_43),
                               mismatch(bs->qm.qyt1_43, bs->qm.qyt2_43), output);
        
        if(displayed) {
            message("bs: %s\n", bs->name);
            message(output);
        }
    }
    
    for(i=0; i<inter.num_modulators; i++) {
        modulator_t *m = &inter.modulator_list[i];
        __print_two_port_mismatches("modulator",
                                    m->name,
                                    &m->qm12,
                                    &m->qm21,
                                    output);
    }
    
    for(i=0; i<inter.num_dbss; i++) {
        dbs_t *dbs = &inter.dbs_list[i];
        output[0] = '\0';
        
        bool displayed = false;

        displayed |= __print_mismatch("1->3", mismatch(dbs->qm13.qxi, dbs->qm13.qxo),
                                              mismatch(dbs->qm13.qyi, dbs->qm13.qyo),
                                              output);
        
        displayed |= __print_mismatch("2->1", mismatch(dbs->qm21.qxi, dbs->qm21.qxo),
                                              mismatch(dbs->qm21.qyi, dbs->qm21.qyo),
                                              output);
        
        displayed |= __print_mismatch("3->4", mismatch(dbs->qm34.qxi, dbs->qm34.qxo),
                                              mismatch(dbs->qm34.qyi, dbs->qm34.qyo),
                                              output);
        
        displayed |= __print_mismatch("4->2", mismatch(dbs->qm42.qxi, dbs->qm42.qxo),
                                              mismatch(dbs->qm42.qyi, dbs->qm42.qyo),
                                              output);
        
        if(displayed) {
            message("dbs: %s\n", dbs->name);
            message(output);
        }
    }
    
    for(i=0; i<inter.num_lenses; i++) {
        lens_t *m = &inter.lens_list[i];
        __print_two_port_mismatches("lens",
                                    m->name,
                                    &m->qm12,
                                    &m->qm21,
                                    output);
    }
    
    for(i=0; i<inter.num_diodes; i++) {
        diode_t *m = &inter.diode_list[i];
        __print_two_port_mismatches("diode",
                                    m->name,
                                    &m->qm12,
                                    &m->qm21,
                                    output);
    }
    
    message("\n---- End interferometer mismatches ----\n");
}


//! Compute coupling coefficients between two beams

/**
 * Computes matrix product:
 *  C = A*B
 * 
 * @param A
 * @param B
 * @param C
 */
void knm_matrix_mult(zmatrix A, zmatrix B, zmatrix C) {
    assert(A != NULL);
    assert(B != NULL);
    assert(C != NULL);

    zmatrix tmp = NULL;

    // if result != A | B then no need to use a temporary matrix to store result 
    // just bung it in the result matrix
    if ((C == A) || (C == B))
        tmp = tmp_knm;
    else
        tmp = C;

    size_t n, m, l;

    for (n = 0; n < mem.num_fields; n++) {
        for (m = 0; m < mem.num_fields; m++) {
            // initialise to 0 here just incase some other data is there
            tmp[n][m] = complex_0;

            for (l = 0; l < mem.num_fields; l++) {
                tmp[n][m] = z_pl_z(tmp[n][m], z_by_z(A[n][l], B[l][m]));
            }
        }
    }

    // if either A or B is the target matrix to store the result
    // we need to copy over the result
    if ((C == A) || (C == B))
        memcpy(C[0], tmp[0], mem.num_fields * mem.num_fields * sizeof (complex_t));
}

/*!
 * Each coefficient describes the coupling of the TEM_n1m1 mode into the
 * TEM_n2m2 mode as a function of the misalignments (gamma_x, gamma_y). In
 * TEM_nm, n gives the mode number in the horizontal plane and m the number
 * for the vertical modes. qx gives the beam parameter in the horizontal
 * plane, qy for the vertical modes. Please note that the notation of
 * gamma_x and gamma_y is not intuitive: gamma_x describes the rotation
 * around the vertical axis and gamma_y the rotation around a horizontal
 * axis through the optical component.

 * Utility functions called by the routines below:
 * - cconj(z) : return complex conjugate of z
 * - ceq(z1, z2) : check whether two complex numbers z1, z2 are equal
 * - z_by_z(z1, z2) : return multiplication of two complex numbers
 * - z_pl_z(z1, z2) : return sum of two complex numbers
 * - z_by_x(z, x) : multiply complex number by a real number
 * - inv_complex(z) : return the inverse of a complex number
 * - zsqrt(z) : complex square root of z
 * - zr(q) : compute Rayleigh range from Gaussian beam parameter q
 * - w0_size(q, nr) : compute beam waist size from beam parameter q and 
 *                   refrective index nr
 * - pow_complex(z, n1, n2) : return z**(n1/n2) with n1, n2 as integers
 * - msign(n) : return -1.0**n, with n as an integer
 * - fac(n) : return factorial of n
 *
 * \param n1 mode number of beam 1
 * \param m1 mode number of beam 1
 * \param n2 mode number of beam 2
 * \param m2 mode number of beam 2
 * \param qx1 Gaussian beam q parameter in x-plane for beam 1
 * \param qy1 Gaussian beam q parameter in y-plane for beam 1
 * \param qx2 Gaussian beam q parameter in x-plane for beam 2
 * \param qy2 Gaussian beam q parameter in y-plane for beam 2
 * \param gamma_x misalignment in x-plane 
 * \param gamma_y misalignment in y-plane
 * \param nr refractive index
 *
 * \see Test_k_mnmn()
 */
complex_t k_nmnm(int n1, int m1, int n2, int m2, complex_t qx1, complex_t qy1,
        complex_t qx2, complex_t qy2, double gamma_x, double gamma_y,
        double nr, const unsigned int knm_flag) {
    complex_t z, z1, z2, ztmp;

    // sanity checks on inputs
    // the mode numbers shouldn't be less than zero
    assert(n1 >= 0);
    assert(n2 >= 0);
    assert(m1 >= 0);
    assert(m2 >= 0);
    // the refractive index shouldn't be zero or less
    assert(nr > 0.0);

    // if the misalignment is zero and ...
    if ((gamma_x == 0) && (gamma_y == 0)) {
        // if the Gaussian beam parameters are equal 
        // (i.e. there is no mode mismatch) ...
        if (ceq(qx1, qx2) && ceq(qy1, qy2)) {
            // then return 1 for coupling into the same mode number and zero 
            // for all other couplings
            if (n1 == n2 && m1 == m2) {
                return (complex_1);
            } else {
                return (complex_0);
            }
        }
    }

    // check for inconsistent mode numbers
    if (n1 < 0 || n2 < 0 || m1 < 0 || m2 < 0) {
        bug_error("negative mode numbers");
    }

    // inter.knm is a global setting for chosing the type of algorithm that 
    // is used for computing the coupling coefficients (mostly for debugging)

    // The sign of gamma_y has to be different between the numeric algorithm
    // and the Bayer-Helms formula. This is probaly due to a wrong definition from
    // me when implementing angles.

    /*
        if ((inter.knm & 2 && !(gamma_x == 0.0) && !(gamma_y == 0.0)) ||
                (inter.knm & 4 && !(gamma_x == 0.0 && gamma_y == 0.0)) || inter.knm & 8) {
     */
    // added new knm debug for old routine
    if (knm_flag & INT_BAYER_HELMS_OLD) {
        if ((knm_flag & 1) && !options.quiet) { // TODO should be replaced by ENUM too?
            z1 = k_mm(n1, n2, qx1, qx2, gamma_x, nr);
            z2 = k_mm(m1, m2, qy1, qy2, -gamma_y, nr); // gamma + oder - ? 251004
            ztmp = z_by_z(z1, z2);

            message("%d %d %d %d | qx1 %s qy1 %s qx2 %s qy2 %s | "
                    "gammax %g, gammay %g) ",
                    n1, m1, n2, m2, complex_form(qx1), complex_form(qy1),
                    complex_form(qx2), complex_form(qy2), gamma_x, -gamma_y);
        }

        // do a numerical integration on a grid
        // with adaptive integration routine `dcuhre':
        z = faltung_adapt(n1, n2, m1, m2, qx1, qy1, qx2, qy2, gamma_x, gamma_y, nr, knm_flag);

        // compare Bayer-Helms with numericly derived coefficients
        if ((knm_flag & 1) && !options.quiet) {
            message("\nNumeric:     %s\nBayer-Helms: %s\n", complex_form(z),
                    complex_form(ztmp));
            if (capprox(z, ztmp)) {
                message("--> OK   ------\n");
            } else {
                message("--> DIFF ++++++\n");
            }
        }
    } else {
        // use coupling coefficients as in F. Bayer-Helms (see below)
        if (n1 == vlocal.kn1 && n2 == vlocal.kn2 && ceq(vlocal.kqx1, qx1)
                && ceq(vlocal.kqx2, qx2) && (gamma_x == vlocal.kgammax)
                && (nr == vlocal.knr)) {
            z1 = vlocal.zk1;
        } else {
            //omp_set_lock(&knmLock);
            z1 = k_mm(n1, n2, qx1, qx2, gamma_x, nr);
            vlocal.zk1 = z1;
            vlocal.kn1 = n1;
            vlocal.kn2 = n2;
            vlocal.kqx1 = qx1;
            vlocal.kqx2 = qx2;
            vlocal.kgammax = gamma_x;
            vlocal.knr = nr;
            //omp_unset_lock(&knmLock);
        }

        // -gammy_y, probably because of my definition of rotation as right-handed 
        // (around y axis)
        z2 = k_mm(m1, m2, qy1, qy2, -gamma_y, nr);
        z = z_by_z(z1, z2);

        if ((knm_flag & 1) && !options.quiet) {
            message("%d %d %d %d | qx1 %s qy1 %s qx2 %s qy2 %s | "
                    "gammax %g, gammay %g) ",
                    n1, m1, n2, m2,
                    complex_form(qx1), complex_form(qy1),
                    complex_form(qx2), complex_form(qy2), gamma_x, -gamma_y);
            message("\nBayer-Helms: %s\n", complex_form(z));
        }
    }

    // `reverse Gouy phase': I found out that on changing from one TEM 
    // base system to another I have to turn back the Gouy phase with 
    // respect to the old beam parameter and add the Gouy phase with 
    // respect to the new beam parameter. This is a required because of 
    // the way the coupling coefficients are defined here and the way 
    // FINESSE handles the Gouy phase: the Gouy phase is added explicitly 
    // to the complex amplitude coefficiants in every `space' whereas the 
    // coupling coefficients are derived using a formula in which the Gouy 
    // phase is implicitly given by spacial distribution.
    z = rev_gouy(z, n1, m1, n2, m2, qx1, qx2, qy1, qy2);

    return (z);
}

// same as above but without rev_gouy, to be used with new set_k_mirror function

complex_t k_nmnm_new(int n1, int m1, int n2, int m2, complex_t qx1, complex_t qy1,
        complex_t qx2, complex_t qy2, double gamma_x, double gamma_y, double nr, const unsigned int knm_flag) {
    complex_t z, z1, z2, ztmp;

    // sanity checks on inputs
    // the mode numbers shouldn't be less than zero
    assert(n1 >= 0);
    assert(n2 >= 0);
    assert(m1 >= 0);
    assert(m2 >= 0);
    // the refractive index shouldn't be zero or less
    assert(nr > 0.0);

    // if the misalignment is zero and ...
    if ((gamma_x == 0) && (gamma_y == 0)) {
        // if the Gaussian beam parameters are equal 
        // (i.e. there is no mode mismatch) ...
        if (ceq(qx1, qx2) && ceq(qy1, qy2)) {
            // then return 1 for coupling into the same mode number and zero 
            // for all other couplingsf
            if (n1 == n2 && m1 == m2) {
                return (complex_1);
            } else {
                return (complex_0);
            }
        }
    }

    // check for inconsistent mode numbers
    if (n1 < 0 || n2 < 0 || m1 < 0 || m2 < 0) {
        bug_error("negative mode numbers");
    }

    // inter.knm is a global setting for chosing the type of algorithm that 
    // is used for computing the coupling coefficients (mostly for debugging)

    // The sign of gamma_y has to be different between the numeric algorithm
    // and the Bayer-Helms formula. This is probaly due to a wrong definition from
    // me when implementing angles.

    /*
        if ((inter.knm & 2 && !(gamma_x == 0.0) && !(gamma_y == 0.0)) ||
                (inter.knm & 4 && !(gamma_x == 0.0 && gamma_y == 0.0)) || inter.knm & 8) {
     */
    // added new knm debug for old routine

    if (knm_flag & INT_BAYER_HELMS_OLD) {
        if ((knm_flag & 1) && !options.quiet) {
            z1 = k_mm(n1, n2, qx1, qx2, gamma_x, nr);
            z2 = k_mm(m1, m2, qy1, qy2, -gamma_y, nr); // gamma + oder - ? 251004
            ztmp = z_by_z(z1, z2);

            message("%d %d %d %d | qx1 %s qy1 %s qx2 %s qy2 %s | "
                    "gammax %g, gammay %g) ",
                    n1, m1, n2, m2, complex_form(qx1), complex_form(qy1),
                    complex_form(qx2), complex_form(qy2), gamma_x, -gamma_y);
        }

        // do a numerical integration on a grid
        // with adaptive integration routine `dcuhre':
        z = faltung_adapt(n1, n2, m1, m2, qx1, qy1, qx2, qy2, gamma_x, gamma_y, nr, knm_flag);

        // compare Bayer-Helms with numericly derived coefficients
        if ((knm_flag & 1) && !options.quiet) {
            message("\nNumeric:     %s\nBayer-Helms: %s\n", complex_form(z),
                    complex_form(ztmp));
            if (capprox(z, ztmp)) {
                message("--> OK   ------\n");
            } else {
                message("--> DIFF ++++++\n");
            }
        }
    } else {

        // use coupling coefficients as in F. Bayer-Helms (see below)
        // cache previously computed value for z1 if nothing has changed

        if (n1 == vlocal.kn1 && n2 == vlocal.kn2 && ceq(vlocal.kqx1, qx1)
                && ceq(vlocal.kqx2, qx2) && (gamma_x == vlocal.kgammax)
                && (nr == vlocal.knr)) {
            z1 = vlocal.zk1;
        } else {

            z1 = k_mm(n1, n2, qx1, qx2, gamma_x, nr);
            vlocal.zk1 = z1;
            vlocal.kn1 = n1;
            vlocal.kn2 = n2;
            vlocal.kqx1 = qx1;
            vlocal.kqx2 = qx2;
            vlocal.kgammax = gamma_x;
            vlocal.knr = nr;

        }

        // -gammy_y, probably because of my definition of rotation as right-handed 
        // (around y axis)
        z2 = k_mm(m1, m2, qy1, qy2, -gamma_y, nr);
        z = z_by_z(z1, z2);

        if ((knm_flag & 1) && !options.quiet) {
            message("%d %d %d %d | qx1 %s qy1 %s qx2 %s qy2 %s | "
                    "gammax %g, gammay %g) ",
                    n1, m1, n2, m2,
                    complex_form(qx1), complex_form(qy1),
                    complex_form(qx2), complex_form(qy2), gamma_x, -gamma_y);
            message("\nBayer-Helms: %s\n", complex_form(z));
        }
    }

    // `reverse Gouy phase': I found out that on changing from one TEM 
    // base system to another I have to turn back the Gouy phase with 
    // respect to the old beam parameter and add the Gouy phase with 
    // respect to the new beam parameter. This is a required because of 
    // the way the coupling coefficients are defined here and the way 
    // FINESSE handles the Gouy phase: the Gouy phase is added explicitly 
    // to the complex amplitude coefficiants in every `space' whereas the 
    // coupling coefficients are derived using a formula in which the Gouy 
    // phase is implicitly given by spacial distribution.
    // 22/02/2012 This will be moved to after the knm merging - Daniel
    //z = rev_gouy(z, n1, m1, n2, m2, qx1, qx2, qy1, qy2);

    return (z);
}



//! Coupling coefficients for TEM modes

/*!
 * F.  Bayer-Helms `Coupling coefficients of an incident wave and modes
 * of an spherical optical resonator in the case of mismatching and
 * misalignment', Appl. Optics Vol23 No9 1369-1380 (1984).
 * There is also a report (in German) which gives a more detailed
 * derivation: `Ableitung der Kopplunskoeffizienten fuer die Eigenfunktionen
 * eines sphaerischen optischen Resonators bei Fehlanpassung und 
 * Fehljustierung der einfallenden Welle' PTB Bericht PTB-Me-43 (1983)
 * 
 * k_mm ist for one degree of freedom (x or y) only. m1 and m2 are the 
 * mode numbers of the incoming and outgoing beam, q1 and q2 the 
 * respective Gaussian beam parameters, gamma is the misalignment angle 
 * between the beams(!) and nr the index of refraction of the medium in 
 * which the coupling happens. The latter is needed to compute the diffraction 
 * angle from the Gaussian beam parameter.
 * 
 * The function calls S_u and S_g (see below). The names of parameters and
 * variables are chosen such that they are similar to the notation used in the
 * paper.
 *
 * \param m1 mode number for incoming beam
 * \param m2 mode number for outgoing beam
 * \param q1 Gaussian beam parameter for incoming beam
 * \param q2 Gaussian beam parameter for outgoing beam
 * \param gamma misalignment angle between the beams
 * \param nr refractive index
 */
complex_t k_mm(int m1, int m2, complex_t q1, complex_t q2,
        double gamma, double nr) {
    double Theta, K0;
    double gg0, z2z0;
    double zr;
    double out1;
    complex_t K;
    complex_t XS, X, FS, F;
    complex_t ex;
    complex_t k1, x1, x2;
    complex_t out2, out3;

    // sanity checks on inputs
    // mode numbers bounds; they should not be less than zero
    assert(m1 >= 0);
    assert(m2 >= 0);

    // refractive index should be > 0 
    assert(nr > 0.0);

    if (ceq(q1, q2)) {
        K = complex_0;
        K0 = 0.0;
        k1 = complex_1;
        FS = complex_0;
        F = complex_0;
    } else {
        zr = 1.0 / q2.im;
        K.re = 0.5 * (q1.im - q2.im) * zr;
        K.im = 0.5 * (q1.re - q2.re) * zr;
        K0 = 2 * K.re;
        k1 = inv_complex(zsqrt(z_pl_z(complex_1, cconj(K))));
        FS = z_by_x(K, 1.0 / (2 * (1.0 + K0)));
        F = z_by_x(cconj(K), 0.5);
    }

    // I think this theta variable is the denominator that comes from bayer-helms equation 12
    Theta = w0_size(q2, nr) / z_r(q2);

    //printf("\nw0=%g\n",w0_size(q2, nr) );
    //printf("zr=%g\n",z_r(q2));

    if (gamma == 0) {
        x1 = x2 = XS = X = ex = complex_0;
        gg0 = 0.0;
    } else {
        //    gg0 = -1.0 * gamma / Theta; // sin (gamma) ???

        //See equation 12, gamma/gamma^hat_0
        gg0 = gamma / Theta; // changed sign of gamma 010708

        //See equation 19 bayer-helms, z^hat_2 / z^hat_0
        z2z0 = z_q(q2) / z_r(q2);

        x1 = z_m_z(co(z2z0, 0), complex_i); // inner bracket of X^hat eq 19
        x2 = z_pl_z(co(z2z0, 0), ci(z_pl_z(complex_1, z_by_x(cconj(K), 2)))); //inner bracket of X eq 19

        // multiply by k1 which is (1+K*)^-0.5
        // also multiply inner brackets calculated above by gamma/gamma^hat_0 factor
        // TODO it appears as though we are missing minus signs according to equation 19 bayer-helms?
        XS = z_by_z(k1, z_by_x(x1, gg0)); //X^hat eq 19
        X = z_by_z(k1, z_by_x(x2, gg0)); //X eq 19
        ex = z_by_x(z_by_z(XS, X), 0.5);
    }

    out1 = msign(m2);
    out2 = z_by_x(pow_complex(z_pl_z(complex_1, cconj(K)), -m1 - m2 - 1, 2),
            sqrt(fac(m1) * fac(m2) * pow(1.0 + K0, m1 + 0.5)));

    complex_t sg = S_g(X, XS, F, FS, m1, m2);
    complex_t su = S_u(X, XS, F, FS, m1, m2);

    out3 = z_by_xphr(z_by_z(out2, z_m_z(sg, su)), out1 * exp(-ex.re), -ex.im);

    return (out3);
}

//! 

/*!
 * 
 * \param X ???
 * \param XS ???
 * \param F ???
 * \param FS ???
 * \param m1 ???
 * \param m2 ???
 *
 * \see Test_S_u()
 */
complex_t S_u(complex_t X, complex_t XS, complex_t F, complex_t FS,
        int m1, int m2) {
    int s3, s4;
    complex_t nom1, nom2;
    double denom1, denom2;
    complex_t z, z1, z2;

    if (m1 == 0 || m2 == 0) {
        return (complex_0);
    }

    if (m1 == 1 || m1 == 2) {
        s3 = 0;
    } else {
        if (odd(m1 - 1)) {
            s3 = (int) ((m1 - 2) / 2);
        } else {
            s3 = (int) ((m1 - 1) / 2);
        }
    }

    if (m2 == 1 || m2 == 2) {
        s4 = 0;
    } else {
        if (odd(m2 - 1)) {
            s4 = (int) ((m2 - 2) / 2);
        } else {
            s4 = (int) ((m2 - 1) / 2);
        }
    }

    z = complex_0;
    int i, j;
    for (i = 0; i <= s3; i++) {
        for (j = 0; j <= s4; j++) {
            nom1 = z_by_z(pow_complex(XS, m1 - 2 * i - 1, 1),
                    pow_complex(X, m2 - 2 * j - 1, 1));
            denom1 = fac(m1 - 2 * i - 1) * fac(m2 - 2 * j - 1);
            z1 = z_by_x(nom1, msign(i) / denom1);
            z2 = complex_0;
            int k;
            for (k = 0; k <= nmin(i, j); k++) {
                nom2 = z_by_z(pow_complex(FS, i - k, 1), pow_complex(F, j - k, 1));
                denom2 = fac(2 * k + 1) * fac(i - k) * fac(j - k);
                z2 = z_pl_z(z2, z_by_x(nom2, msign(k) / denom2));
            }
            z = z_pl_z(z, z_by_z(z1, z2));
        }
    }

    return (z);
}

//! 

/*! 
 * 
 * \param X ???
 * \param XS ???
 * \param F ???
 * \param FS ???
 * \param m1 ???
 * \param m2 ???
 *
 * \see Test_S_g()
 */
complex_t S_g(complex_t X, complex_t XS, complex_t F, complex_t FS,
        int m1, int m2) {
    int s1, s2;
    complex_t z, z1, z2;
    complex_t nom1, nom2;
    double denom1, denom2;

    if (m1 == 0 || m1 == 1) {
        s1 = 0;
    } else {
        if (odd(m1)) {
            s1 = (int) ((m1 - 1) / 2);
        } else {
            s1 = (int) (m1 / 2);
        }
    }

    if (m2 == 0 || m2 == 1) {
        s2 = 0;
    } else {
        if (odd(m2)) {
            s2 = (int) ((m2 - 1) / 2);
        } else {
            s2 = (int) (m2 / 2);
        }
    }

    z = complex_0;
    int i, j;
    for (i = 0; i <= s1; i++) {
        for (j = 0; j <= s2; j++) {
            nom1 =
                    z_by_z(pow_complex(XS, m1 - 2 * i, 1), pow_complex(X, m2 - 2 * j, 1));
            denom1 = fac(m1 - 2 * i) * fac(m2 - 2 * j);
            z1 = z_by_x(nom1, msign(i) / denom1);
            z2 = complex_0;
            int k;
            for (k = 0; k <= nmin(i, j); k++) {
                nom2 = z_by_z(pow_complex(FS, i - k, 1), pow_complex(F, j - k, 1));
                denom2 = fac(2 * k) * fac(i - k) * fac(j - k);
                z2 = z_pl_z(z2, z_by_x(nom2, msign(k) / denom2));
            }
            z = z_pl_z(z, z_by_z(z1, z2));
        }
    }

    return (z);
}

void __print_coupling_info_line(const char* name, coupling_info_t *c) {
    if (inter.debug)
        message("%10.10s %5.5s  max=%i \n", name, c->coupling_off ? "off" : "on", c->max_coupling_order);
}

void __set_no_reduce_coupling_info(coupling_info_t *c) {
    c->coupling_off = false;
    c->has_xeven = true;
    c->has_yeven = true;
    c->has_xodd = true;
    c->has_yodd = true;
    c->max_coupling_order = inter.tem;
}

/**
 * Iterates through each objects and sets up the
 * coupling_info_t objects. Must be called before
 * matrices are built as this information is used to
 * determine which elements to allocate.
 */
void set_coupling_info() {
    int i;

    int max = inter.tem;

    if (inter.debug && options.use_coupling_reduction) {
        message("-----------------------------------\n");
        message("Reduced order coupling information\n");
        message("-----------------------------------\n");
    }

    for (i = 0; i < inter.num_mirrors; i++) {
        mirror_t *mirror = &inter.mirror_list[i];

        coupling_info_t * v[4] = {&mirror->a_cplng_11, &mirror->a_cplng_22,
            &mirror->a_cplng_12, &mirror->a_cplng_21};

        int j;

        for (j = 0; j < 4; j++) {
            if (!options.use_coupling_reduction) {
                __set_no_reduce_coupling_info(v[j]);
            } else {
                // switch all coupling off to begin with
                v[j]->coupling_off = true;
                v[j]->has_xeven = true;
                v[j]->has_yeven = true;
                v[j]->has_xodd = true;
                v[j]->has_yodd = true;
                v[j]->max_coupling_order = max;

                // if there is any mismatch turn coupling on
                if (mirror->mismatching & pow_two(j)) {
                    v[j]->coupling_off = false;
                }

                if (mirror->beta_x != 0.0 || is_var_tuned(&mirror->beta_x)) {
                    v[j]->coupling_off = false;
                }

                if (mirror->beta_y != 0.0 || is_var_tuned(&mirror->beta_y)) {
                    v[j]->coupling_off = false;
                }

                // if there are maps then ensure coupling is on
                // could probably be more picky about which type of 
                // map triggers this, e.g. a transmission map doesn't
                // affect reflection.
                if (mirror->num_maps > 0) {
                    v[j]->coupling_off = false;
                }

                if (mirror->map_rom != NULL) {
                    v[j]->coupling_off = false;
                }

                __print_coupling_info_line(mirror->name, v[j]);
            }
        }
    }

    for (i = 0; i < inter.num_beamsplitters; i++) {
        beamsplitter_t *bs = &inter.bs_list[i];

        coupling_info_t * v[12] = {&bs->a_cplng_12, &bs->a_cplng_21,
            &bs->a_cplng_34, &bs->a_cplng_43,
            &bs->a_cplng_13, &bs->a_cplng_31,
            &bs->a_cplng_24, &bs->a_cplng_42,
            &bs->a_cplng_11, &bs->a_cplng_22,
            &bs->a_cplng_33, &bs->a_cplng_44};

        int j;

        for (j = 0; j < 12; j++) {
            if (!options.use_coupling_reduction) {
                __set_no_reduce_coupling_info(v[j]);
            } else {
                // switch all coupling off to begin with
                v[j]->coupling_off = true;
                v[j]->has_xeven = true;
                v[j]->has_yeven = true;
                v[j]->has_xodd = true;
                v[j]->has_yodd = true;
                v[j]->max_coupling_order = max;

                // if there is any mismatch turn coupling on
                if (bs->mismatching & pow_two(j)) {
                    v[j]->coupling_off = false;
                }

                if (bs->beta_x != 0.0 || is_var_tuned(&bs->beta_x)) {
                    v[j]->coupling_off = false;
                }

                if (bs->beta_y != 0.0 || is_var_tuned(&bs->beta_y)) {
                    v[j]->coupling_off = false;
                }

                // if there are maps then ensure coupling is on
                // could probably be more picky about which type of 
                // map triggers this, e.g. a transmission map doesn't
                // affect reflection.
                if (bs->num_maps > 0) {
                    v[j]->coupling_off = false;
                }

                __print_coupling_info_line(bs->name, v[j]);
            }
        }
    }

    for (i = 0; i < inter.num_spaces; i++) {
        space_t *space = &inter.space_list[i];

        // These should be order so that they
        // represent the same mode mismatch ordering
        // in the bitflag space->mismatching
        coupling_info_t * v[2] = {&space->a_cplng_12, &space->a_cplng_21};

        int j;

        // check if each direction has any mode mismatch
        // as that is the only coupling possible for a space
        for (j = 0; j < 2; j++) {
            if (!options.use_coupling_reduction) {
                __set_no_reduce_coupling_info(v[j]);
            } else {
                v[j]->coupling_off = !(space->mismatching & pow_two(j));
                v[j]->has_xeven = true;
                v[j]->has_yeven = true;
                v[j]->has_xodd = true;
                v[j]->has_yodd = true;
                v[j]->max_coupling_order = max;

                __print_coupling_info_line(space->name, v[j]);
            }
        }
    }

    for (i = 0; i < inter.num_modulators; i++) {
        modulator_t *mod = &inter.modulator_list[i];

        coupling_info_t * v[2] = {&mod->a_cplng_12, &mod->a_cplng_21};

        int j;

        for (j = 0; j < 2; j++) {
            if (!options.use_coupling_reduction || (mod->type == MODTYPE_YAW || mod->type == MODTYPE_PITCH)) {
                __set_no_reduce_coupling_info(v[j]);
            } else {
                v[j]->coupling_off = !(mod->mismatching & pow_two(j));
                v[j]->has_xeven = true;
                v[j]->has_yeven = true;
                v[j]->has_xodd = true;
                v[j]->has_yodd = true;
                v[j]->max_coupling_order = max;

                __print_coupling_info_line(mod->name, v[j]);
            }
        }
    }

    for (i = 0; i < inter.num_lenses; i++) {
        lens_t *lens = &inter.lens_list[i];

        coupling_info_t * v[2] = {&lens->a_cplng_12, &lens->a_cplng_21};

        int j;

        for (j = 0; j < 2; j++) {
            if (!options.use_coupling_reduction) {
                __set_no_reduce_coupling_info(v[j]);
            } else {
                v[j]->coupling_off = !(lens->mismatching & pow_two(j));
                v[j]->has_xeven = true;
                v[j]->has_yeven = true;
                v[j]->has_xodd = true;
                v[j]->has_yodd = true;
                v[j]->max_coupling_order = max;

                __print_coupling_info_line(lens->name, v[j]);
            }
        }
    }

    for (i = 0; i < inter.num_diodes; i++) {
        diode_t *diode = &inter.diode_list[i];

        coupling_info_t * v[4] = {&diode->a_cplng_12, &diode->a_cplng_21, &diode->a_cplng_32, &diode->a_cplng_23};

        int j;

        for (j = 0; j < 4; j++) {
            if (!options.use_coupling_reduction) {
                __set_no_reduce_coupling_info(v[j]);
            } else {
                v[j]->coupling_off = !(diode->mismatching & pow_two(j));
                v[j]->has_xeven = true;
                v[j]->has_yeven = true;
                v[j]->has_xodd = true;
                v[j]->has_yodd = true;
                v[j]->max_coupling_order = max;

                __print_coupling_info_line(diode->name, v[j]);
            }
        }
    }
    
    for (i = 0; i < inter.num_dbss; i++) {
        dbs_t *dbs = &inter.dbs_list[i];

        coupling_info_t * v[4] = {&dbs->a_cplng_13, &dbs->a_cplng_21, &dbs->a_cplng_34, &dbs->a_cplng_42};

        int j;

        for (j = 0; j < 4; j++) {
            if (!options.use_coupling_reduction) {
                __set_no_reduce_coupling_info(v[j]);
            } else {
                v[j]->coupling_off = !(dbs->mismatching & pow_two(j));
                v[j]->has_xeven = true;
                v[j]->has_yeven = true;
                v[j]->has_xodd = true;
                v[j]->has_yodd = true;
                v[j]->max_coupling_order = max;

                __print_coupling_info_line(dbs->name, v[j]);
            }
        }
    }

    if (inter.debug && options.use_coupling_reduction) {
        message("-----------------------------------\n");
    }
}

/**
 * Set the respective coupling coefficients for the given mirror
 * \param mirror_index index of mirror to set the coupling coefficient
 *
 * \return parameter describing the mismatch (if any)
 *
 * \see Test_set_k_mirror()
 */
int set_k_mirror(int mirror_index) {

    int component_index;
    int node1_index, node2_index;
    bitflag astigmatism = 0;
    char buf[LINE_LEN];

    double nr1 = 1.0, nr2 = 1.0;

    complex_t qx1, qy1, qx2, qy2;
    mirror_t *mirror;

    node_t node1, node2;

    // sanity check on input
    // mirror index should be in the range 0 <= mirror_index < inter.num_mirrors
    assert(mirror_index >= 0);
    assert(mirror_index < inter.num_mirrors);

    mirror = &(inter.mirror_list[mirror_index]);
    component_index = get_overall_component_index(MIRROR, mirror_index);

    node1_index = mirror->node1_index;
    node2_index = mirror->node2_index;

    node1 = inter.node_list[node1_index];
    node2 = inter.node_list[node2_index];

    if (NOT node1.gnd_node) {
        nr1 = *node1.n;

        if (node1.component_index == component_index) {
            // reverse q so that beam comes _to_ the mirror from node1
            qx1 = cminus(cconj(node1.qx));
            qy1 = cminus(cconj(node1.qy));
        } else {
            qx1 = node1.qx;
            qy1 = node1.qy;
        }
    } else {
        qx1.re = 0;
        qx1.im = 0;
        qy1.re = 0;
        qy1.im = 0;
    }

    if (NOT node2.gnd_node) {
        nr2 = *node2.n;

        if (node2.component_index == component_index) {
            qx2 = node2.qx;
            qy2 = node2.qy;
        } else {
            // reverse q so that beam goes _from_ the mirror to node2,
            // this requires to use -beta for the reflection node2 -> node2 
            qx2 = cminus(cconj(node2.qx));
            qy2 = cminus(cconj(node2.qy));
        }
    } else {
        qx2.re = 0;
        qx2.im = 0;
        qy2.re = 0;
        qy2.im = 0;
    }

    if (inter.debug & 32 || (inter.trace & 64 && (mirror->rebuild > 1 || !inter.setup))) {
        message("%s :\n", mirror->name);
    }

    mirror->knm_calc_flags = 0;
    mirror_knm_q_t knm_q;

    // calculate the individual q values for each KNM and if we should even
    // bother calculating them    
    if (NOT node1.gnd_node) {
        mirror->knm_calc_flags |= MR11Calc;
        calculate_mirror_qt_qt2(MR11, &knm_q, qx1, qy1, qx2, qy2, nr1, nr2, node1_index, node2_index, component_index);

    } else {
        knm_q.qxt1_11 = complex_0;
        knm_q.qxt2_11 = complex_0;
        knm_q.qyt1_11 = complex_0;
        knm_q.qyt2_11 = complex_0;
    }

    if (NOT node2.gnd_node) {
        mirror->knm_calc_flags |= MR22Calc;
        calculate_mirror_qt_qt2(MR22, &knm_q, qx1, qy1, qx2, qy2, nr1, nr2, node1_index, node2_index, component_index);

    } else {
        knm_q.qxt1_22 = complex_0;
        knm_q.qxt2_22 = complex_0;
        knm_q.qyt1_22 = complex_0;
        knm_q.qyt2_22 = complex_0;
    }

    if (NOT node1.gnd_node && NOT node2.gnd_node) {
        mirror->knm_calc_flags |= MR12Calc;
        mirror->knm_calc_flags |= MR21Calc;

        calculate_mirror_qt_qt2(MR12, &knm_q, qx1, qy1, qx2, qy2, nr1, nr2, node1_index, node2_index, component_index);
        calculate_mirror_qt_qt2(MR21, &knm_q, qx1, qy1, qx2, qy2, nr1, nr2, node1_index, node2_index, component_index);

    } else {
        knm_q.qxt1_12 = complex_0;
        knm_q.qxt2_12 = complex_0;
        knm_q.qyt1_12 = complex_0;
        knm_q.qyt2_12 = complex_0;
        knm_q.qxt1_21 = complex_0;
        knm_q.qxt2_21 = complex_0;
        knm_q.qyt1_21 = complex_0;
        knm_q.qyt2_21 = complex_0;
    }
    
    memcpy(&mirror->qm, &knm_q, sizeof(mirror_knm_q_t));
    
    if (inter.debug & 128) {
        message("* Mirror %s, knm to be calculated: ", mirror->name);
        if (CALC_MR_KNM(mirror, 11)) {
            message("K11 ");
        }
        if (CALC_MR_KNM(mirror, 12)) {
            message("K12 ");
        }
        if (CALC_MR_KNM(mirror, 21)) {
            message("K21 ");
        }
        if (CALC_MR_KNM(mirror, 22)) {
            message("K22 ");
        }
        message("\n");
    }

    check_mirror_knm_mismatch_astig(mirror->knm_calc_flags, &knm_q, &mirror->mismatching, &astigmatism);

    // flagged integer that keeps track of which effects need to be integrated.
    bitflag integrating = 0;
    int i;

    // check if we should integrate the bayer_helms knm's. If we do there is no
    // need to calculate the rest of this function
    if (mirror->knm_flags & INT_BAYER_HELMS) {
        gerror("Integrating the Bayer-Helms coefficients using the Cuba or Riemann integrators is not implemented yet\n");
        integrating |= 1;
    }

    if (should_integrate_mirror_aperture_knm(mirror, mirror->mismatching, astigmatism)) {
        integrating |= 2;
    }

    // We need to check here if the user has set the knm conf options that would
    // not produce the correct results. This happens in the case when
    //          - No integration is happening
    //          - Mode-Mismatch, i.e. q'_1 != q_2
    //          - The knm_change_q is set so that Bayer-helms compute an inner product
    //            between the same q values
    // In this case the integration knm matrix won't be computed and the BH
    // knm matrix will not handle the mode-mismatch
    if (!integrating && mirror->num_maps == 0 && mirror->knm_change_q == 1) {

        if (inter.debug && !options.quiet && !defaultKNMChangeWarning) {
            defaultKNMChangeWarning = true;
            warn("Defaulting knm_change_q=1 and knm_order = 21 for mirror %s as the previous combination would result in an incorrect result as no map integration has been set.\n", mirror->name);
        }

        mirror->knm_change_q = DEFAULT_KNM_CHANGE_Q;
        mirror->knm_order[0] = DEFAULT_KNM_ORDER_FIRST;
        mirror->knm_order[1] = DEFAULT_KNM_ORDER_SECOND;
        mirror->knm_order[2] = DEFAULT_KNM_ORDER_THIRD;
        mirror->knm_order[3] = DEFAULT_KNM_ORDER_FOURTH;
    }

    // the inner product calculations uses the mirrors own knm_q structure 
    // for storing the incoming q(x/y)t1_(direction) and outgoing q(x/y)t2_(direction)
    // beam parameters. These need to be set before the solver are called below

    // here we need to set the initial q values used for the first coupling coefficient
    // matrix this always goes from incoming q_L to outgoing q_2
    if (CALC_MR_KNM(mirror, 11)) {
        mirror->knm_q.qxt1_11 = knm_q.qxt1_11;
        mirror->knm_q.qyt1_11 = knm_q.qyt1_11;
        mirror->knm_q.qxt2_11 = knm_q.qxt1_11;
        mirror->knm_q.qyt2_11 = knm_q.qyt1_11;
    }

    if (CALC_MR_KNM(mirror, 12)) {
        mirror->knm_q.qxt1_12 = knm_q.qxt1_12;
        mirror->knm_q.qyt1_12 = knm_q.qyt1_12;
        mirror->knm_q.qxt2_12 = knm_q.qxt1_12;
        mirror->knm_q.qyt2_12 = knm_q.qyt1_12;
    }

    if (CALC_MR_KNM(mirror, 21)) {
        mirror->knm_q.qxt1_21 = knm_q.qxt1_21;
        mirror->knm_q.qyt1_21 = knm_q.qyt1_21;
        mirror->knm_q.qxt2_21 = knm_q.qxt1_21;
        mirror->knm_q.qyt2_21 = knm_q.qyt1_21;
    }

    if (CALC_MR_KNM(mirror, 22)) {
        mirror->knm_q.qxt1_22 = knm_q.qxt1_22;
        mirror->knm_q.qyt1_22 = knm_q.qyt1_22;
        mirror->knm_q.qxt2_22 = knm_q.qxt1_22;
        mirror->knm_q.qyt2_22 = knm_q.qyt1_22;
    }

    // set tmp matrix to identity, this is the matrix we will apply each knm too
    // inside the loop
    mirror_knm_matrix_ident(&(mirror->knm));
    mirror_knm_matrix_ident(&(mirror->knm_no_rgouy));

    if (inter.debug & 128) {
        message("* Order of knm distortions\n");

        for (i = 0; i < NUM_KNM_TYPES; i++) {
            int num = mirror->knm_order[i]; // the distortion which should be applied

            switch (num) {
                case 1:
                    message("  %i Merged map (1)", i + 1);
                    break;
                case 2:
                    message("  %i Bayer-Helms (2)", i + 1);
                    break;
                case 3:
                    if (mirror->aperture_type == CIRCULAR)
                        message("  %i Circular aperture (3)", i + 1);
                    else
                        message("  %i Square sperture (3)", i + 1);

                    break;
                case 4:
                    message("  %i ROMHOM (4)", i + 1);
                    break;
                default:
                    bug_error("not handled");
            }

            if (num == mirror->knm_change_q) {
                message(" [Computes mode-mismatching]\n");
            } else {
                message("\n");
            }
        }
    }

    // loop over the 4 distortions that can be applied
    for (i = 0; i < NUM_KNM_TYPES; i++) {
        int num = mirror->knm_order[i]; // the distortion which should be applied
        bitflag mm = 0, astig = 0;

        // Check if this solver should handle the mode mismatch
        if (mirror->knm_change_q == mirror->knm_order[i]) {
            // if so them the second q for this solver should be the output q2
            if (CALC_MR_KNM(mirror, 11)) {
                mirror->knm_q.qxt2_11 = knm_q.qxt2_11;
                mirror->knm_q.qyt2_11 = knm_q.qyt2_11;
            }

            if (CALC_MR_KNM(mirror, 12)) {
                mirror->knm_q.qxt2_12 = knm_q.qxt2_12;
                mirror->knm_q.qyt2_12 = knm_q.qyt2_12;
            }

            if (CALC_MR_KNM(mirror, 21)) {
                mirror->knm_q.qxt2_21 = knm_q.qxt2_21;
                mirror->knm_q.qyt2_21 = knm_q.qyt2_21;
            }

            if (CALC_MR_KNM(mirror, 22)) {
                mirror->knm_q.qxt2_22 = knm_q.qxt2_22;
                mirror->knm_q.qyt2_22 = knm_q.qyt2_22;
            }
        }

        if (inter.debug & 128) {
            if (i == 0)
                message("* knm q values used:\n");

            switch (num) {
                case 1:
                    message("  - Merged map\n");
                    break;
                case 2:
                    message("  - Bayer-Helms\n");
                    break;
                case 3:
                    message("  - Aperture\n");
                    break;
                case 4:
                    message("  - ROMHOM\n");
                    break;

                default:
                    bug_error("not handled");
            }

            message("    11: q_in  = x:%s y:%s\n", complex_form(mirror->knm_q.qxt1_11), complex_form(mirror->knm_q.qyt1_11));
            message("    11: q_out = x:%s y:%s\n", complex_form(mirror->knm_q.qxt2_11), complex_form(mirror->knm_q.qyt2_11));
            message("    12: q_in  = x:%s y:%s\n", complex_form(mirror->knm_q.qxt1_12), complex_form(mirror->knm_q.qyt1_12));
            message("    12: q_out = x:%s y:%s\n", complex_form(mirror->knm_q.qxt2_12), complex_form(mirror->knm_q.qyt2_12));
            message("    21: q_in  = x:%s y:%s\n", complex_form(mirror->knm_q.qxt1_21), complex_form(mirror->knm_q.qyt1_21));
            message("    21: q_out = x:%s y:%s\n", complex_form(mirror->knm_q.qxt2_21), complex_form(mirror->knm_q.qyt2_21));
            message("    22: q_in  = x:%s y:%s\n", complex_form(mirror->knm_q.qxt1_22), complex_form(mirror->knm_q.qyt1_22));
            message("    22: q_out = x:%s y:%s\n", complex_form(mirror->knm_q.qxt2_22), complex_form(mirror->knm_q.qyt2_22));
        }

        switch (num) {
            case 1: //merged-map
                check_mirror_knm_mismatch_astig(mirror->knm_calc_flags, &mirror->knm_q, &mm, &astig);

                // In the kat_init.c in prepare_maps() we merged all the maps together to
                // form an absolute and phase map for reflection and transmission. This now
                // needs to be integrated over to calculate the knm's. However to reduce the
                // amount of integrating needed we can also compute all the above effects
                // (aperture, curvature, misalignment, etc.) in the same integral if they
                // have not been calculated using some analytic technique. So this
                // is not just called when maps are present.
                compute_mirror_knm_integral(mirror, nr1, nr2, integrating, mm, astig);

                // here we need to check if:
                //  - anything has had to be integrated
                //  - any maps are present
                // Basically anything that has altered the map_merged.knm variable that also
                // includes effects that are integrated instead of calculated analytically
                if ((integrating > 0) || mirror->map_merged.knm_calculated) {
                    mirror_knm_matrix_mult(&(mirror->knm_map), &(mirror->knm_no_rgouy), &(mirror->knm_no_rgouy));
                }

                if ((mirror->knm_flags & PRINT_COEFFS) && mirror->map_merged.save_knm_matrices) {
                    sprintf(buf, "%s_merged", mirror->map_merged.filename);
                    write_mirror_knm_to_matrix_file(buf, &(mirror->knm_map));
                }

                break;

            case 2: //bayer-helms
                if (!(integrating & 1)) {
                    // TODO: This computes the bayer-helms coefficients or it analytically computes
                    // them using the old integration routine. Ideally, later once the rest
                    // has been tested, this will be moved over to use the Cuba library. Especially
                    // as the old fortran integrator does not run on windows!
                    compute_mirror_bayer_helms_knm(mirror, nr1, nr2);

                    if (!(mirror->knm_flags & INT_BAYER_HELMS) && mirror->map_merged.save_knm_matrices) {
                        sprintf(buf, "%s_%s_bayerhelms", mirror->name, mirror->map_merged.filename);
                        write_mirror_knm_to_matrix_file(buf, &(mirror->knm_bayer_helms));
                    }

                    // now finally merge the bayer helms coefficients with the others
                    // and stick the final result in the mirror coefficients
                    mirror_knm_matrix_mult(&(mirror->knm_bayer_helms), &(mirror->knm_no_rgouy), &(mirror->knm_no_rgouy));
                }

                break;


            case 3: //aperture
                if (!(integrating & 2) && mirror->r_aperture > 0) {

                    if (mirror->aperture_type == SQUARE)
                        fill_mirror_knm_square_aperture(mirror, nr1, nr2);
                    else
                        bug_error("not handled");

                    if (!(mirror->knm_flags & INT_APERTURE) && mirror->map_merged.save_knm_matrices) {
                        sprintf(buf, "%s_aperture", mirror->map_merged.filename);
                        write_mirror_knm_to_matrix_file(buf, &(mirror->knm_aperture));
                    }

                    // if we have calculated aperture knm analytically do matrix multiplication
                    mirror_knm_matrix_mult(&(mirror->knm_aperture), &(mirror->knm_no_rgouy), &(mirror->knm_no_rgouy));
                }
                break;

            case 4:
                if (mirror->map_rom) {
                    // ROMHOM
                    check_mirror_knm_mismatch_astig(mirror->knm_calc_flags, &mirror->knm_q, &mm, &astig);

                    compute_mirror_knm_romhom(mirror, nr1, nr2, mm, astig);

                    mirror_knm_matrix_mult(&(mirror->knm_romhom), &(mirror->knm_no_rgouy), &(mirror->knm_no_rgouy));

                    if ((mirror->knm_flags & PRINT_COEFFS)) {
                        sprintf(buf, "%s_%s_romhom", mirror->name, mirror->map_rom->filename);
                        write_mirror_knm_to_matrix_file(buf, &(mirror->knm_romhom));
                    }
                }

                break;

            default:
                bug_error("Could not handle knm_order digit %i", mirror->knm_order[i]);
        }

        // if this solver handled the mode mismatch then the following solvers
        // just see q2
        if (mirror->knm_change_q == mirror->knm_order[i]) {
            // If the final solver should handle mode mismatch...
            if (CALC_MR_KNM(mirror, 11)) {
                mirror->knm_q.qxt1_11 = knm_q.qxt2_11;
                mirror->knm_q.qyt1_11 = knm_q.qyt2_11;
            }

            if (CALC_MR_KNM(mirror, 12)) {
                mirror->knm_q.qxt1_12 = knm_q.qxt2_12;
                mirror->knm_q.qyt1_12 = knm_q.qyt2_12;
            }

            if (CALC_MR_KNM(mirror, 21)) {
                mirror->knm_q.qxt1_21 = knm_q.qxt2_21;
                mirror->knm_q.qyt1_21 = knm_q.qyt2_21;
            }

            if (CALC_MR_KNM(mirror, 22)) {
                mirror->knm_q.qxt1_22 = knm_q.qxt2_22;
                mirror->knm_q.qyt1_22 = knm_q.qyt2_22;
            }
        }
    }

    int n = 0, m = 0, n1 = 0, m1 = 0, n2 = 0, m2 = 0, k = 0;

    // Now we compute any surface motion map couplings, purely in reflection.
    if (mirror->num_surface_motions) {
        // as we are computing this coupling matrix first we must use
        // the outgoing beam parameters.
        calc_mirror_knm_surf_motions_map(mirror, nr1, nr2, knm_q.qxt2_11, knm_q.qyt2_11, knm_q.qxt2_22, knm_q.qyt2_22);
        calc_mirror_knm_surf_motions_rom(mirror, nr1, nr2, knm_q.qxt2_11, knm_q.qyt2_11, knm_q.qxt2_22, knm_q.qyt2_22, astigmatism);

        for (k = 0; k < mirror->num_surface_motions; k++) {
            complex_t **A = mirror->knm_surf_motion_1o[k];
            complex_t **B = mirror->knm_no_rgouy.k11;
            complex_t **C = mirror->knm_surf_x_a_1[k];

            // zero the output matrix
            memset(&(C[0][0]), 0, inter.num_fields * inter.num_fields * sizeof (complex_t));

            int l = 0, _n1 = 0, _m1 = 0;
            
            for (n = 0; n < inter.num_fields; n++) {
                get_tem_modes_from_field_index(&_n1, &_m1, n);

                for (m = 0; m < inter.num_fields; m++) {
                    get_tem_modes_from_field_index(&n1, &m1, m);

                    for (l = 0; l < inter.num_fields; l++) {
                        get_tem_modes_from_field_index(&n2, &m2, l);
                        z_inc_z(&C[n][m], z_by_z(B[n][l], A[l][m]));
                    }
                    //warn("surf %i%i -> %i%i %s %s\n", n1, m1, _n1, _m1, complex_form(A[n][m]), complex_form(B[n][m]));
                }
            }

            A = mirror->knm_surf_motion_2o[k];
            B = mirror->knm_no_rgouy.k22;
            C = mirror->knm_surf_x_a_2[k];

            memset(&(C[0][0]), 0, inter.num_fields * inter.num_fields * sizeof (complex_t));

            for (n = 0; n < inter.num_fields; n++) {
                for (m = 0; m < inter.num_fields; m++) {
                    get_tem_modes_from_field_index(&n1, &m1, m);
                    for (l = 0; l < inter.num_fields; l++) {
                        get_tem_modes_from_field_index(&n2, &m2, l);
                        z_inc_z(&C[n][m], z_by_z(B[n][l], A[l][m]));
                    }
                }
            }
        }
    }

    // Here we adjust the odd reflected horizontal modes. We do this because on
    // reflection, just like in a mirror, the horizontal direction is flipped
    // left to right. We also reverse the extra gouy phase present and rescale the
    // phase.
    for (n = 0; n < inter.num_fields; n++) {
        get_tem_modes_from_field_index(&n1, &m1, n);

        for (m = 0; m < inter.num_fields; m++) {
            get_tem_modes_from_field_index(&n2, &m2, m);

            // 1) the old map code had wrong qx1/qx2 for k12/k21 at this place and I assume
            // this was copied over. Fixed this now.
            // 2) the old code uses the `turned' q's to compute thre reverse gouy, so
            // maybe this is needed here as well
            mirror->knm.k11[n][m] = rev_gouy(mirror->knm_no_rgouy.k11[n][m], n1, m1, n2, m2, knm_q.qxt1_11, knm_q.qxt2_11, knm_q.qyt1_11, knm_q.qyt2_11);
            mirror->knm.k12[n][m] = rev_gouy(mirror->knm_no_rgouy.k12[n][m], n1, m1, n2, m2, knm_q.qxt1_12, knm_q.qxt2_12, knm_q.qyt1_12, knm_q.qyt2_12);
            mirror->knm.k21[n][m] = rev_gouy(mirror->knm_no_rgouy.k21[n][m], n1, m1, n2, m2, knm_q.qxt1_21, knm_q.qxt2_21, knm_q.qyt1_21, knm_q.qyt2_21);
            mirror->knm.k22[n][m] = rev_gouy(mirror->knm_no_rgouy.k22[n][m], n1, m1, n2, m2, knm_q.qxt1_22, knm_q.qxt2_22, knm_q.qyt1_22, knm_q.qyt2_22);

            for (k = 0; k < mirror->num_surface_motions; k++) {
                // for input beams the q value is reversed so the n1/n2 are the other way around here.
                mirror->knm_surf_motion_1i[k][n][m] = rev_gouy(mirror->knm_surf_motion_1i[k][n][m], n2, m2, n1, m1, knm_q.qxt1_11, knm_q.qxt2_11, knm_q.qyt1_11, knm_q.qyt2_11);
                mirror->knm_surf_motion_1o[k][n][m] = rev_gouy(mirror->knm_surf_motion_1o[k][n][m], n1, m1, n2, m2, knm_q.qxt1_11, knm_q.qxt2_11, knm_q.qyt1_11, knm_q.qyt2_11);
                mirror->knm_surf_motion_2i[k][n][m] = rev_gouy(mirror->knm_surf_motion_2i[k][n][m], n2, m2, n1, m1, knm_q.qxt1_22, knm_q.qxt2_22, knm_q.qyt1_22, knm_q.qyt2_22);
                mirror->knm_surf_motion_2o[k][n][m] = rev_gouy(mirror->knm_surf_motion_2o[k][n][m], n1, m1, n2, m2, knm_q.qxt1_22, knm_q.qxt2_22, knm_q.qyt1_22, knm_q.qyt2_22);

                mirror->knm_surf_x_a_1[k][n][m] = rev_gouy(mirror->knm_surf_x_a_1[k][n][m], n1, m1, n2, m2, knm_q.qxt1_11, knm_q.qxt2_11, knm_q.qyt1_11, knm_q.qyt2_11);
                mirror->knm_surf_x_a_2[k][n][m] = rev_gouy(mirror->knm_surf_x_a_2[k][n][m], n1, m1, n2, m2, knm_q.qxt1_22, knm_q.qxt2_22, knm_q.qyt1_22, knm_q.qyt2_22);
            }
        }
    }
    
    // Here we store the phases of the TEM00 so that we can 'zero' the phase, as
    // typically we do not care for the exact phase here just the relative ones
    // between the modes
    double p11 = 0, p12 = 0, p21 = 0, p22 = 0, psm1[MAX_MAPS] = {0}, psm2[MAX_MAPS] = {0};

    if (inter.set_tem_phase_zero & 1) {
        p11 = zphase(mirror->knm.k11[0][0]);
        p12 = zphase(mirror->knm.k12[0][0]);
        p21 = zphase(mirror->knm.k21[0][0]);
        p22 = zphase(mirror->knm.k22[0][0]);
    }

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            get_tem_modes_from_field_index(&n1, &m1, n);
            get_tem_modes_from_field_index(&n2, &m2, m);

            if (inter.set_tem_phase_zero & 1) {
                mirror->knm.k11[n][m] = z_by_phr(mirror->knm.k11[n][m], -p11);
                mirror->knm.k12[n][m] = z_by_phr(mirror->knm.k12[n][m], -p12);
                mirror->knm.k21[n][m] = z_by_phr(mirror->knm.k21[n][m], -p21);
                mirror->knm.k22[n][m] = z_by_phr(mirror->knm.k22[n][m], -p22);
            }

            // check if we need to flip the left and right, like looking in a mirror
            if (turnit(n2) == -1) {
                mirror->knm.k11[n][m] = z_by_x(mirror->knm.k11[n][m], -1);
                mirror->knm.k22[n][m] = z_by_x(mirror->knm.k22[n][m], -1);

                for (k = 0; k < mirror->num_surface_motions; k++) {
                    mirror->knm_surf_motion_1o[k][n][m] = cminus(mirror->knm_surf_motion_1o[k][n][m]);
                    mirror->knm_surf_motion_1i[k][n][m] = cminus(mirror->knm_surf_motion_1i[k][n][m]);
                    mirror->knm_surf_motion_2o[k][n][m] = cminus(mirror->knm_surf_motion_2o[k][n][m]);
                    mirror->knm_surf_motion_2i[k][n][m] = cminus(mirror->knm_surf_motion_2i[k][n][m]);

                    mirror->knm_surf_x_a_1[k][n][m] = cminus(mirror->knm_surf_x_a_1[k][n][m]);
                    mirror->knm_surf_x_a_2[k][n][m] = cminus(mirror->knm_surf_x_a_2[k][n][m]);
                }
            }

            //            for(k=0; k<mirror->num_surface_motions; k++){
            //                warn("s1: %i%i->%i%i = %s\n",n1,m1,n2,m2, complex_form(mirror->knm_surf_x_a_1[k][n][m]));
            //                warn("s2: %i%i->%i%i = %s\n",n1,m1,n2,m2, complex_form(mirror->knm_surf_x_a_2[k][n][m]));
            //                warn("1i: %i%i->%i%i = %s\n",n1,m1,n2,m2, complex_form(mirror->knm_surf_motion_1i[k][n][m]));
            //                warn("1o: %i%i->%i%i = %s\n",n1,m1,n2,m2, complex_form(mirror->knm_surf_motion_1o[k][n][m]));
            //                warn("2i: %i%i->%i%i = %s\n",n1,m1,n2,m2, complex_form(mirror->knm_surf_motion_2i[k][n][m]));
            //                warn("2o: %i%i->%i%i = %s\n",n1,m1,n2,m2, complex_form(mirror->knm_surf_motion_2o[k][n][m]));
            //            }
        }
    }
               

    memset(mirror->k11_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(mirror->k12_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(mirror->k21_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(mirror->k22_sqrd_sum, 0, sizeof (double) * inter.num_fields);

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            mirror->k11_sqrd_sum[n] += zabs(mirror->knm.k11[n][m]);
            mirror->k12_sqrd_sum[n] += zabs(mirror->knm.k12[n][m]);
            mirror->k21_sqrd_sum[n] += zabs(mirror->knm.k21[n][m]);
            mirror->k22_sqrd_sum[n] += zabs(mirror->knm.k22[n][m]);
        }
    }

    if (inter.debug & 4096) {
        double k_sqrd_sum[4] = {0};

        debug_msg("%s\n", mirror->name);

        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                k_sqrd_sum[0] += z_mag_sqrd(mirror->knm.k11[n][m]);
                k_sqrd_sum[1] += z_mag_sqrd(mirror->knm.k12[n][m]);
                k_sqrd_sum[2] += z_mag_sqrd(mirror->knm.k21[n][m]);
                k_sqrd_sum[3] += z_mag_sqrd(mirror->knm.k22[n][m]);
            }

            int i;
            for (i = 0; i < 4; i++) {
                if (i == 0 && CALC_MR_KNM(mirror, 11)) {
                    debug_msg("K11:%i,%i: ", n1, m1);
                } else if (i == 1 && CALC_MR_KNM(mirror, 12)) {
                    debug_msg("K12:%i,%i: ", n1, m1);
                } else if (i == 2 && CALC_MR_KNM(mirror, 21)) {
                    debug_msg("K21:%i,%i: ", n1, m1);
                } else if (i == 3 && CALC_MR_KNM(mirror, 22)) {
                    debug_msg("K22:%i,%i: ", n1, m1);
                }

                debug_msg("%.15f\n", sqrt(k_sqrd_sum[i]));
                k_sqrd_sum[i] = 0;
            }
        }
    }

    if ((mirror->knm_flags & PRINT_COEFFS) && mirror->map_merged.save_knm_matrices) {
        sprintf(buf, "%s_mergedalt", mirror->map_merged.filename);
        write_mirror_knm_to_matrix_file(buf, &(mirror->knm));
    }

    return (mirror->mismatching);
}

inline bool check_mismatch(complex_t qx1, complex_t qx2, complex_t qy1, complex_t qy2) {
    return (!ceq(qx1, qx2) || !ceq(qy1, qy2));
}

inline bool check_astigmatism(complex_t qx1, complex_t qx2, complex_t qy1, complex_t qy2) {
    return (!ceq(qx1, qy1) || !ceq(qx2, qy2));
}

//! Set the coupling coefficients for the given beam splitter

/*!
 * \param bs_index the beam splitter index
 *
 * \see Test_set_k_beamsplitter()
 */
int set_k_beamsplitter(int bs_index) {
    int component;
    int node1_index, node2_index, node3_index, node4_index;

    bitflag astigmatism = 0;
    char buf[LINE_LEN];

    complex_t qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4;
    beamsplitter_t *bs;

    node_t node1, node2, node3, node4;

    // sanity checks on input
    // bs_index should be in the range 0 <= bs_index < inter.num_beamsplitters
    assert(bs_index >= 0);
    assert(bs_index < inter.num_beamsplitters);

    bs = &(inter.bs_list[bs_index]);

    component = get_overall_component_index(BEAMSPLITTER, bs_index);
    node1_index = bs->node1_index;
    node2_index = bs->node2_index;
    node3_index = bs->node3_index;
    node4_index = bs->node4_index;

    node1 = inter.node_list[node1_index];
    node2 = inter.node_list[node2_index];
    node3 = inter.node_list[node3_index];
    node4 = inter.node_list[node4_index];

    double nr1 = 1, nr2 = 1;
    bs_get_nr(bs, &nr1, &nr2);
    
    if (NOT node1.gnd_node) {
        node1 = inter.node_list[node1_index];
        if (node1.component_index == component) {
            // reverse q so that beam comes _to_ the bs from node1
            qx1 = cminus(cconj(node1.qx));
            qy1 = cminus(cconj(node1.qy));
        } else {
            qx1 = node1.qx;
            qy1 = node1.qy;
        }
    } else {
        qx1 = complex_0;
        qy1 = complex_0;
    }

    if (NOT node2.gnd_node) {
        node2 = inter.node_list[node2_index];
        if (node2.component_index == component) {
            qx2 = node2.qx;
            qy2 = node2.qy;
        } else {
            // reverse q so that beam goes _from_ the bs to node2
            qx2 = cminus(cconj(node2.qx));
            qy2 = cminus(cconj(node2.qy));
        }
    } else {
        qx2 = complex_0;
        qy2 = complex_0;
    }

    if (NOT node3.gnd_node) {
        node3 = inter.node_list[node3_index];
        if (node3.component_index == component) {
            // reverse q so that beam comes _to_ the bs from node3
            qx3 = cminus(cconj(node3.qx));
            qy3 = cminus(cconj(node3.qy));
        } else {
            qx3 = node3.qx;
            qy3 = node3.qy;
        }
    } else {
        qx3 = complex_0;
        qy3 = complex_0;
    }

    if (NOT node4.gnd_node) {
        node4 = inter.node_list[node4_index];
        if (node4.component_index == component) {
            qx4 = node4.qx;
            qy4 = node4.qy;
        } else {
            // reverse q so that beam goes _from_ the bs to node4
            qx4 = cminus(cconj(node4.qx));
            qy4 = cminus(cconj(node4.qy));
        }
    } else {
        qx4 = complex_0;
        qy4 = complex_0;
    }

    if (inter.debug & 32 || (inter.trace & 64 && (bs->rebuild > 1 || !inter.setup))) {
        message("%s :\n", bs->name);
    }

    bs->knm_calc_flags = 0;

    bs_knm_q_t knm_q;

    // calculate the individual q values for each KNM and if we should even
    // bother calculating them    
    if (NOT node1.gnd_node && NOT node2.gnd_node) {
        FLAG_BS_KNM(bs, 12);
        FLAG_BS_KNM(bs, 21);

        calculate_bs_qt_qt2(BS12, &knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                node1_index, node2_index, node3_index, node4_index, component);
        calculate_bs_qt_qt2(BS21, &knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                node1_index, node2_index, node3_index, node4_index, component);
    } else {
        knm_q.qxt1_12 = complex_0;
        knm_q.qyt1_12 = complex_0;
        knm_q.qxt2_12 = complex_0;
        knm_q.qyt2_12 = complex_0;
        knm_q.qxt1_21 = complex_0;
        knm_q.qyt1_21 = complex_0;
        knm_q.qxt2_21 = complex_0;
        knm_q.qyt2_21 = complex_0;
    }

    if (NOT node3.gnd_node && NOT node4.gnd_node) {
        FLAG_BS_KNM(bs, 34);
        FLAG_BS_KNM(bs, 43);

        calculate_bs_qt_qt2(BS34, &knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                node1_index, node2_index, node3_index, node4_index, component);
        calculate_bs_qt_qt2(BS43, &knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                node1_index, node2_index, node3_index, node4_index, component);
    } else {
        knm_q.qxt1_34 = complex_0;
        knm_q.qyt1_34 = complex_0;
        knm_q.qxt2_34 = complex_0;
        knm_q.qyt2_34 = complex_0;
        knm_q.qxt1_43 = complex_0;
        knm_q.qyt1_43 = complex_0;
        knm_q.qxt2_43 = complex_0;
        knm_q.qyt2_43 = complex_0;
    }

    if (NOT node1.gnd_node && NOT node3.gnd_node) {
        FLAG_BS_KNM(bs, 13);
        FLAG_BS_KNM(bs, 31);

        calculate_bs_qt_qt2(BS13, &knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                node1_index, node2_index, node3_index, node4_index, component);
        calculate_bs_qt_qt2(BS31, &knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                node1_index, node2_index, node3_index, node4_index, component);
    } else {
        knm_q.qxt1_13 = complex_0;
        knm_q.qyt1_13 = complex_0;
        knm_q.qxt2_13 = complex_0;
        knm_q.qyt2_13 = complex_0;
        knm_q.qxt1_31 = complex_0;
        knm_q.qyt1_31 = complex_0;
        knm_q.qxt2_31 = complex_0;
        knm_q.qyt2_31 = complex_0;
    }

    if (NOT node2.gnd_node && NOT node4.gnd_node) {
        FLAG_BS_KNM(bs, 24);
        FLAG_BS_KNM(bs, 42);

        calculate_bs_qt_qt2(BS24, &knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                node1_index, node2_index, node3_index, node4_index, component);
        calculate_bs_qt_qt2(BS42, &knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                node1_index, node2_index, node3_index, node4_index, component);
    } else {
        knm_q.qxt1_24 = complex_0;
        knm_q.qyt1_24 = complex_0;
        knm_q.qxt2_24 = complex_0;
        knm_q.qyt2_24 = complex_0;
        knm_q.qxt1_42 = complex_0;
        knm_q.qyt1_42 = complex_0;
        knm_q.qxt2_42 = complex_0;
        knm_q.qyt2_42 = complex_0;
    }
    
    memcpy(&bs->qm, &knm_q, sizeof(bs_knm_q_t));
    
    // Give the user some debug feedback on what is being calculated
    if (inter.debug & 128) {
        message("* Beamsplitter %s, knm to be calculated: ", bs->name);
        if (CALC_BS_KNM(bs, 12)) message("K12 ");
        if (CALC_BS_KNM(bs, 21)) message("K21 ");
        if (CALC_BS_KNM(bs, 34)) message("K34 ");
        if (CALC_BS_KNM(bs, 43)) message("K43 ");
        if (CALC_BS_KNM(bs, 13)) message("K13 ");
        if (CALC_BS_KNM(bs, 31)) message("K31 ");
        if (CALC_BS_KNM(bs, 24)) message("K24 ");
        if (CALC_BS_KNM(bs, 42)) message("K42 ");
        message("\n");
    }

    check_bs_knm_mismatch_astig(bs->knm_calc_flags, &knm_q, &bs->mismatching, &astigmatism);

    // flagged integer that keeps track of which effects need to be integrated.
    unsigned int integrating = 0;

    // need to loop over the order in which the knm should be applied
    // then compute what q values they should be given, see issue #112 on Redmine
    int i;

    // set tmp matrix to identity, this is the matrix we will apply each knm too
    // inside the loop
    bs_knm_matrix_ident(&(bs->knm_no_rgouy));

    // check if we should integrate the bayer_helms knm's. If we do there is no
    // need to calculate the rest of this function
    if (bs->knm_flags & INT_BAYER_HELMS) {
        gerror("Integrating the Bayer-Helms coefficients using the Cuba or Riemann integrators is not implemented yet\n");
        integrating |= 1;
    }

    if (should_integrate_bs_aperture_knm(bs, bs->mismatching, astigmatism)) {
        integrating |= 2;
    }

    // We need to check here if the user has set the knm conf options that would
    // not produce the correct results. This happens in the case when
    //          - No integration is happening
    //          - Mode-Mismatch, i.e. q'_1 != q_2
    //          - The knm_change_q is set so that Bayer-helms compute an inner product
    //            between the same q values
    // In this case the integration knm matrix won't be computed and the BH
    // knm matrix will not handle the mode-mismatch
    if (!integrating && bs->num_maps == 0 && ((bs->knm_change_q == 1 && bs->knm_order[0] == 1)
            || (bs->knm_change_q == 2 && bs->knm_order[0] == 2))) {
        if (inter.debug && !options.quiet) {
            warn("Defaulting knm_change_q=1 and knm_order = 21 for beamsplitter %s as the previous combination would result in an incorrect result as no map integration has been set. ", bs->name);
        }
        bs->knm_change_q = 1;
        bs->knm_order[0] = 2;
        bs->knm_order[1] = 1;
    }

    complex_t qLx_12 = complex_0, qLx_21 = complex_0, qLx_34 = complex_0, qLx_43 = complex_0;
    complex_t qLy_12 = complex_0, qLy_21 = complex_0, qLy_34 = complex_0, qLy_43 = complex_0;
    complex_t qLx_13 = complex_0, qLx_31 = complex_0, qLx_24 = complex_0, qLx_42 = complex_0;
    complex_t qLy_13 = complex_0, qLy_31 = complex_0, qLy_24 = complex_0, qLy_42 = complex_0;

    // compute what q_L is, the beam parameter of the unit expansion inserted into the
    // coupling coefficient inner product.
    if (bs->knm_change_q == 1) {
        qLx_12 = knm_q.qxt1_12;
        qLy_12 = knm_q.qyt1_12;
        qLx_21 = knm_q.qxt1_21;
        qLy_21 = knm_q.qyt1_21;
        qLx_34 = knm_q.qxt1_34;
        qLy_34 = knm_q.qyt1_34;
        qLx_43 = knm_q.qxt1_43;
        qLy_43 = knm_q.qyt1_43;
        qLx_13 = knm_q.qxt1_13;
        qLy_13 = knm_q.qyt1_13;
        qLx_31 = knm_q.qxt1_31;
        qLy_31 = knm_q.qyt1_31;
        qLx_24 = knm_q.qxt1_24;
        qLy_24 = knm_q.qyt1_24;
        qLx_42 = knm_q.qxt1_42;
        qLy_42 = knm_q.qyt1_42;

    } else if (bs->knm_change_q == 2) {
        qLx_12 = knm_q.qxt2_12;
        qLy_12 = knm_q.qyt2_12;
        qLx_21 = knm_q.qxt2_21;
        qLy_21 = knm_q.qyt2_21;
        qLx_34 = knm_q.qxt2_34;
        qLy_34 = knm_q.qyt2_34;
        qLx_43 = knm_q.qxt2_43;
        qLy_43 = knm_q.qyt2_43;
        qLx_13 = knm_q.qxt2_13;
        qLy_13 = knm_q.qyt2_13;
        qLx_31 = knm_q.qxt2_31;
        qLy_31 = knm_q.qyt2_31;
        qLx_24 = knm_q.qxt2_24;
        qLy_24 = knm_q.qyt2_24;
        qLx_42 = knm_q.qxt2_42;
        qLy_42 = knm_q.qyt2_42;
    } else
        bug_error("value of bs->knm_change_q was %i, which is not handled.", bs->knm_change_q);

    // the inner product calculations uses the mirrors own knm_q structure 
    // for storing the incoming q(x/y)t1_(direction) and outgoing q(x/y)t2_(direction)
    // beam parameters. These need to be set before the solver are called below

    // here we need to set the initial q values used for the first coupling coefficient
    // matrix this always goes from incoming q_L to outgoing q_2
    if (CALC_BS_KNM(bs, 12)) {
        bs->knm_q.qxt1_12 = qLx_12;
        bs->knm_q.qyt1_12 = qLy_12;
        bs->knm_q.qxt2_12 = knm_q.qxt2_12;
        bs->knm_q.qyt2_12 = knm_q.qyt2_12;
    }

    if (CALC_BS_KNM(bs, 21)) {
        bs->knm_q.qxt1_21 = qLx_21;
        bs->knm_q.qyt1_21 = qLy_21;
        bs->knm_q.qxt2_21 = knm_q.qxt2_21;
        bs->knm_q.qyt2_21 = knm_q.qyt2_21;
    }

    if (CALC_BS_KNM(bs, 34)) {
        bs->knm_q.qxt1_34 = qLx_34;
        bs->knm_q.qyt1_34 = qLy_34;
        bs->knm_q.qxt2_34 = knm_q.qxt2_34;
        bs->knm_q.qyt2_34 = knm_q.qyt2_34;
    }

    if (CALC_BS_KNM(bs, 43)) {
        bs->knm_q.qxt1_43 = qLx_43;
        bs->knm_q.qyt1_43 = qLy_43;
        bs->knm_q.qxt2_43 = knm_q.qxt2_43;
        bs->knm_q.qyt2_43 = knm_q.qyt2_43;
    }

    if (CALC_BS_KNM(bs, 13)) {
        bs->knm_q.qxt1_13 = qLx_13;
        bs->knm_q.qyt1_13 = qLy_13;
        bs->knm_q.qxt2_13 = knm_q.qxt2_13;
        bs->knm_q.qyt2_13 = knm_q.qyt2_13;
    }

    if (CALC_BS_KNM(bs, 31)) {
        bs->knm_q.qxt1_31 = qLx_31;
        bs->knm_q.qyt1_31 = qLy_31;
        bs->knm_q.qxt2_31 = knm_q.qxt2_31;
        bs->knm_q.qyt2_31 = knm_q.qyt2_31;
    }

    if (CALC_BS_KNM(bs, 24)) {
        bs->knm_q.qxt1_24 = qLx_24;
        bs->knm_q.qyt1_24 = qLy_24;
        bs->knm_q.qxt2_24 = knm_q.qxt2_24;
        bs->knm_q.qyt2_24 = knm_q.qyt2_24;
    }

    if (CALC_BS_KNM(bs, 42)) {
        bs->knm_q.qxt1_42 = qLx_42;
        bs->knm_q.qyt1_42 = qLy_42;
        bs->knm_q.qxt2_42 = knm_q.qxt2_42;
        bs->knm_q.qyt2_42 = knm_q.qyt2_42;
    }

    // if debugging then output the order in which the distortions are applied and
    // where the change in q and n happens as well as the ABCD matrix
    if (inter.debug & 128) {
        message("* Order of knm distortions\n");

        for (i = 0; i < NUM_KNM_TYPES; i++) {
            int num = bs->knm_order[i]; // the distortion which should be applied

            switch (num) {
                case 1:
                    message("  %i merged map\n", i + 1);
                    break;
                case 2:
                    message("  %i Bayer-Helms\n", i + 1);
                    break;
            }
        }
    }

    // loop over the 3 distortions that can be applied
    for (i = 0; i < NUM_KNM_TYPES; i++) {
        int num = bs->knm_order[i]; // the distortion which should be applied
        bitflag mm = 0, astig = 0;

        if (inter.debug & 128) {
            if (i == 0)
                message("* knm q values used:\n");

            switch (num) {
                case 1:
                    message("  - Merged map\n");
                    break;
                case 2:
                    message("  - Bayer-Helms\n");
                    break;
            }

            message("    12: q_in  = x:%s y:%s\n", complex_form(bs->knm_q.qxt1_12), complex_form(bs->knm_q.qyt1_12));
            message("    12: q_out = x:%s y:%s\n", complex_form(bs->knm_q.qxt2_12), complex_form(bs->knm_q.qyt2_12));
            message("    21: q_in  = x:%s y:%s\n", complex_form(bs->knm_q.qxt1_21), complex_form(bs->knm_q.qyt1_21));
            message("    21: q_out = x:%s y:%s\n", complex_form(bs->knm_q.qxt2_21), complex_form(bs->knm_q.qyt2_21));
            message("    13: q_in  = x:%s y:%s\n", complex_form(bs->knm_q.qxt1_34), complex_form(bs->knm_q.qyt1_34));
            message("    13: q_out = x:%s y:%s\n", complex_form(bs->knm_q.qxt2_34), complex_form(bs->knm_q.qyt2_34));
            message("    43: q_in  = x:%s y:%s\n", complex_form(bs->knm_q.qxt1_43), complex_form(bs->knm_q.qyt1_43));
            message("    43: q_out = x:%s y:%s\n", complex_form(bs->knm_q.qxt2_43), complex_form(bs->knm_q.qyt2_43));
            message("    13: q_in  = x:%s y:%s\n", complex_form(bs->knm_q.qxt1_13), complex_form(bs->knm_q.qyt1_13));
            message("    13: q_out = x:%s y:%s\n", complex_form(bs->knm_q.qxt2_13), complex_form(bs->knm_q.qyt2_13));
            message("    31: q_in  = x:%s y:%s\n", complex_form(bs->knm_q.qxt1_31), complex_form(bs->knm_q.qyt1_31));
            message("    31: q_out = x:%s y:%s\n", complex_form(bs->knm_q.qxt2_31), complex_form(bs->knm_q.qyt2_31));
            message("    24: q_in  = x:%s y:%s\n", complex_form(bs->knm_q.qxt1_24), complex_form(bs->knm_q.qyt1_24));
            message("    24: q_out = x:%s y:%s\n", complex_form(bs->knm_q.qxt2_24), complex_form(bs->knm_q.qyt2_24));
            message("    42: q_in  = x:%s y:%s\n", complex_form(bs->knm_q.qxt1_42), complex_form(bs->knm_q.qyt1_42));
            message("    42: q_out = x:%s y:%s\n", complex_form(bs->knm_q.qxt2_42), complex_form(bs->knm_q.qyt2_42));
        }

        switch (num) {
            case 1: //merged-map
                check_bs_knm_mismatch_astig(bs->knm_calc_flags, &bs->knm_q, &mm, &astig);

                // In the kat_init.c in prepare_maps() we merged all the maps together to
                // form an absolute and phase map for reflection and transmission. This now
                // needs to be integrated over to calculate the knm's. However to reduce the
                // amount of integrating needed we can also compute all the above effects
                // (aperture, curvature, misalignment, etc.) in the same integral if they
                // have not been calculated using some analytic technique. So this
                // is not just called when maps are present.
                compute_bs_knm_integral(bs, nr1, nr2, integrating, mm, astig);

                // here we need to check if:
                //  - anything has had to be integrated
                //  - any maps are present
                // Basically anything that has altered the map_merged.knm variable that also
                // includes effects that are integrated instead of calculated analytically
                if ((integrating > 0) || bs->map_merged.knm_calculated)
                    bs_knm_matrix_mult(&(bs->knm_map), &(bs->knm_no_rgouy), &(bs->knm_no_rgouy));

                if ((bs->knm_flags & PRINT_COEFFS) && bs->map_merged.save_knm_matrices) {
                    sprintf(buf, "%s_merged", bs->map_merged.filename);
                    write_bs_knm_to_matrix_file(buf, &(bs->knm_map));
                }

                break;

            case 3:
                break;

            case 2: //bayer-helms
                if (!(integrating & 1)) {
                    // TODO: This computes the bayer-helms coefficients or it analytically computes
                    // them using the old integration routine. Ideally, later once the rest
                    // has been tested, this will be moved over to use the Cuba library. Especially
                    // as the old fortran integrator does not run on windows!
                    compute_bs_bayer_helms_knm(bs, nr1, nr2);

                    if (!(bs->knm_flags & INT_BAYER_HELMS) && bs->map_merged.save_knm_matrices) {
                        sprintf(buf, "%s_bayerhelms", bs->map_merged.filename);
                        write_bs_knm_to_matrix_file(buf, &(bs->knm_bayer_helms));
                    }

                    // now finally merge the bayer helms coefficients with the others
                    // and stick the final result in the mirror coefficients
                    bs_knm_matrix_mult(&(bs->knm_bayer_helms), &(bs->knm_no_rgouy), &(bs->knm_no_rgouy));
                }
                break;

            case 4:
                // TODO ROMHOM FOR BEAMSPLITTERS
                break;
            default:
                bug_error("Could not handle knm_order digit %i", bs->knm_order[i]);
        }

        if (i == 0) {
            // Once we have computed the first matrix using q_L -> q_2 we now have to compute
            // q'_1 -> q_L, seeing as we only have 2 separations currently
            if (CALC_BS_KNM(bs, 12)) {
                bs->knm_q.qxt1_12 = knm_q.qxt1_12;
                bs->knm_q.qyt1_12 = knm_q.qyt1_12;
                bs->knm_q.qxt2_12 = qLx_12;
                bs->knm_q.qyt2_12 = qLy_12;
            }

            if (CALC_BS_KNM(bs, 21)) {
                bs->knm_q.qxt1_21 = knm_q.qxt1_21;
                bs->knm_q.qyt1_21 = knm_q.qyt1_21;
                bs->knm_q.qxt2_21 = qLx_21;
                bs->knm_q.qyt2_21 = qLy_21;
            }

            if (CALC_BS_KNM(bs, 34)) {
                bs->knm_q.qxt1_34 = knm_q.qxt1_34;
                bs->knm_q.qyt1_34 = knm_q.qyt1_34;
                bs->knm_q.qxt2_34 = qLx_34;
                bs->knm_q.qyt2_34 = qLy_34;
            }

            if (CALC_BS_KNM(bs, 43)) {
                bs->knm_q.qxt1_43 = knm_q.qxt1_43;
                bs->knm_q.qyt1_43 = knm_q.qyt1_43;
                bs->knm_q.qxt2_43 = qLx_43;
                bs->knm_q.qyt2_43 = qLy_43;
            }

            if (CALC_BS_KNM(bs, 13)) {
                bs->knm_q.qxt1_13 = knm_q.qxt1_13;
                bs->knm_q.qyt1_13 = knm_q.qyt1_13;
                bs->knm_q.qxt2_13 = qLx_13;
                bs->knm_q.qyt2_13 = qLy_13;
            }

            if (CALC_BS_KNM(bs, 31)) {
                bs->knm_q.qxt1_31 = knm_q.qxt1_31;
                bs->knm_q.qyt1_31 = knm_q.qyt1_31;
                bs->knm_q.qxt2_31 = qLx_31;
                bs->knm_q.qyt2_31 = qLy_31;
            }

            if (CALC_BS_KNM(bs, 24)) {
                bs->knm_q.qxt1_24 = knm_q.qxt1_24;
                bs->knm_q.qyt1_24 = knm_q.qyt1_24;
                bs->knm_q.qxt2_24 = qLx_24;
                bs->knm_q.qyt2_24 = qLy_24;
            }

            if (CALC_BS_KNM(bs, 42)) {
                bs->knm_q.qxt1_42 = knm_q.qxt1_42;
                bs->knm_q.qyt1_42 = knm_q.qyt1_42;
                bs->knm_q.qxt2_42 = qLx_42;
                bs->knm_q.qyt2_42 = qLy_42;
            }
        }
    }

    int n, m, n1, n2, m1, m2;
    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    int turn;

    // Here we adjust the odd reflected horizontal modes. We do this because on
    // reflection, just like in a mirror, the horizontal direction is flipped
    // left to right. We also reverse the extra gouy phase present and rescale the
    // phase.
    for (n = 0; n < num_fields; n++) {
        for (m = 0; m < num_fields; m++) {
            get_tem_modes_from_field_index(&n1, &m1, n);
            get_tem_modes_from_field_index(&n2, &m2, m);


            // 1) the old map code had wrong qx1/qx2 for k12/k21 at this place and I assume
            // this was copied over. Fixed this now.
            // 2) the old code uses the `turned' q's to compute thre reverse gouy, so
            // maybe this is needed here as well
            bs->knm.k12[n][m] = rev_gouy(bs->knm_no_rgouy.k12[n][m], n1, m1, n2, m2, knm_q.qxt1_12, knm_q.qxt2_12, knm_q.qyt1_12, knm_q.qyt2_12);
            bs->knm.k21[n][m] = rev_gouy(bs->knm_no_rgouy.k21[n][m], n1, m1, n2, m2, knm_q.qxt1_21, knm_q.qxt2_21, knm_q.qyt1_21, knm_q.qyt2_21);
            bs->knm.k34[n][m] = rev_gouy(bs->knm_no_rgouy.k34[n][m], n1, m1, n2, m2, knm_q.qxt1_34, knm_q.qxt2_34, knm_q.qyt1_34, knm_q.qyt2_34);
            bs->knm.k43[n][m] = rev_gouy(bs->knm_no_rgouy.k43[n][m], n1, m1, n2, m2, knm_q.qxt1_43, knm_q.qxt2_43, knm_q.qyt1_43, knm_q.qyt2_43);
            bs->knm.k13[n][m] = rev_gouy(bs->knm_no_rgouy.k13[n][m], n1, m1, n2, m2, knm_q.qxt1_13, knm_q.qxt2_13, knm_q.qyt1_13, knm_q.qyt2_13);
            bs->knm.k31[n][m] = rev_gouy(bs->knm_no_rgouy.k31[n][m], n1, m1, n2, m2, knm_q.qxt1_31, knm_q.qxt2_31, knm_q.qyt1_31, knm_q.qyt2_31);
            bs->knm.k24[n][m] = rev_gouy(bs->knm_no_rgouy.k24[n][m], n1, m1, n2, m2, knm_q.qxt1_24, knm_q.qxt2_24, knm_q.qyt1_24, knm_q.qyt2_24);
            bs->knm.k42[n][m] = rev_gouy(bs->knm_no_rgouy.k42[n][m], n1, m1, n2, m2, knm_q.qxt1_42, knm_q.qxt2_42, knm_q.qyt1_42, knm_q.qyt2_42);
        }
    }

    // Here we store the phases of the TEM00 so that we can 'zero' the phase, as
    // typically we do not care for the exact phase here just the relative ones
    // between the modes
    double p12 = zphase(bs->knm.k12[0][0]);
    double p21 = zphase(bs->knm.k21[0][0]);
    double p34 = zphase(bs->knm.k34[0][0]);
    double p43 = zphase(bs->knm.k43[0][0]);
    double p13 = zphase(bs->knm.k13[0][0]);
    double p31 = zphase(bs->knm.k31[0][0]);
    double p24 = zphase(bs->knm.k24[0][0]);
    double p42 = zphase(bs->knm.k42[0][0]);

    for (n = 0; n < num_fields; n++) {
        for (m = 0; m < num_fields; m++) {
            get_tem_modes_from_field_index(&n1, &m1, n);
            get_tem_modes_from_field_index(&n2, &m2, m);

            if (inter.set_tem_phase_zero & 1) {
                bs->knm.k12[n][m] = z_by_phr(bs->knm.k12[n][m], -p12);
                bs->knm.k21[n][m] = z_by_phr(bs->knm.k21[n][m], -p21);
                bs->knm.k34[n][m] = z_by_phr(bs->knm.k34[n][m], -p34);
                bs->knm.k43[n][m] = z_by_phr(bs->knm.k43[n][m], -p43);
                bs->knm.k13[n][m] = z_by_phr(bs->knm.k13[n][m], -p13);
                bs->knm.k31[n][m] = z_by_phr(bs->knm.k31[n][m], -p31);
                bs->knm.k24[n][m] = z_by_phr(bs->knm.k24[n][m], -p24);
                bs->knm.k42[n][m] = z_by_phr(bs->knm.k42[n][m], -p42);
            }
            // check if we need to flip the left and right, like looking in a mirror
            turn = turnit(n2);

            bs->knm.k12[n][m] = z_by_x(bs->knm.k12[n][m], turn);
            bs->knm.k21[n][m] = z_by_x(bs->knm.k21[n][m], turn);
            bs->knm.k34[n][m] = z_by_x(bs->knm.k34[n][m], turn);
            bs->knm.k43[n][m] = z_by_x(bs->knm.k43[n][m], turn);
        }
    }

    memset(bs->k12_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(bs->k21_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(bs->k34_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(bs->k43_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(bs->k13_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(bs->k31_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(bs->k24_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(bs->k42_sqrd_sum, 0, sizeof (double) * inter.num_fields);

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            bs->k12_sqrd_sum[n] += zabs(bs->knm.k12[n][m]);
            bs->k21_sqrd_sum[n] += zabs(bs->knm.k21[n][m]);
            bs->k34_sqrd_sum[n] += zabs(bs->knm.k34[n][m]);
            bs->k43_sqrd_sum[n] += zabs(bs->knm.k43[n][m]);
            bs->k13_sqrd_sum[n] += zabs(bs->knm.k13[n][m]);
            bs->k31_sqrd_sum[n] += zabs(bs->knm.k31[n][m]);
            bs->k24_sqrd_sum[n] += zabs(bs->knm.k24[n][m]);
            bs->k42_sqrd_sum[n] += zabs(bs->knm.k42[n][m]);
        }
    }

    if ((bs->knm_flags & PRINT_COEFFS) && bs->map_merged.save_knm_matrices) {
        sprintf(buf, "%s_mergedalt", bs->map_merged.filename);
        write_bs_knm_to_matrix_file(buf, &(bs->knm));
    }

    return (bs->mismatching);
}

//! Set coupling coefficient for a free space element

/*!
 * \param space_index index of free space element
 *
 * \see Test_set_k_space()
 */
int set_k_space(int space_index) {
    int err;
    int component;
    int node1_index, node2_index;
    int n1, m1, n2, m2;
    double nr;
    complex_t kx, ky;
    double kmx, kmy;
    double phase = 0.0;

    ABCD_t trans1;
    complex_t qx1, qy1, qx2, qy2;
    complex_t qxt, qyt;
    complex_t qxt2, qyt2;
    space_t *space;

    node_t node1, node2;

    // do sanity check on input
    // space_index should be in the range 0 <= space_index < inter.num_spaces
    assert(space_index >= 0);
    assert(space_index < inter.num_spaces);

    space = &(inter.space_list[space_index]);
    nr = space->n;
    component = get_overall_component_index(SPACE, space_index);

    node1_index = space->node1_index;
    node2_index = space->node2_index;

    node1 = inter.node_list[node1_index];
    node2 = inter.node_list[node2_index];

    //if (NOT node1.gnd_node) {
    //node1 = inter.node_list[node1_index];
    //}

    //if (NOT node2.gnd_node) {
    //node2 = inter.node_list[node2_index];
    //}
    space->mismatching = 0;

    if (NOT node1.gnd_node) {
        if (node1.component_index == component) {
            // reverse q so that beam comes _to_ the space from node1
            qx1 = cminus(cconj(node1.qx));
            qy1 = cminus(cconj(node1.qy));
        } else {
            qx1 = node1.qx;
            qy1 = node1.qy;
        }
    }

    if (NOT node2.gnd_node) {
        if (node2.component_index == component) {
            qx2 = node2.qx;
            qy2 = node2.qy;
        } else {
            // reverse q so that beam goes _from_ the space to node2
            qx2 = cminus(cconj(node2.qx));
            qy2 = cminus(cconj(node2.qy));
        }
    }

    if (inter.debug & 32 || inter.trace & 64) {
        message("%s :\n", space->name);
    }

    if (NOT node1.gnd_node &&
            NOT node2.gnd_node) {
        // transmission1
        err = component_matrix(&trans1, component, node1_index, node2_index,
                TANGENTIAL);
        if (!err) {
            qxt = q1_q2(trans1, qx1, nr, nr);
            err = component_matrix(&trans1, component, node1_index, node2_index,
                    SAGITTAL);
            qyt = q1_q2(trans1, qy1, nr, nr);

            qxt2 = qx2;
            qyt2 = qy2;
            
            space->qm12.qxi = qxt;
            space->qm12.qxo = qxt2;
            space->qm12.qyi = qyt;
            space->qm12.qyo = qyt2;
            
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                space->mismatching |= 1;
            }

            if (inter.trace & 64) {
                if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                    mode_match(qxt, qxt2, &kx, &kmx);
                    mode_match(qyt, qyt2, &ky, &kmy);
                    message(" t12: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                            complex_form(kx), complex_form(ky), kmx, kmy);
                }
            }

            if (inter.debug & 32) {
                message("k12");
            }

            if (inter.set_tem_phase_zero & 1) {
                phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr, space->knm_flags));
            } else {
                phase = 0.0;
            }

            int n, m;
            for (n = 0; n < inter.num_fields; n++) {
                for (m = 0; m < inter.num_fields; m++) {
                    get_tem_modes_from_field_index(&n1, &m1, n);
                    get_tem_modes_from_field_index(&n2, &m2, m);
                    if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
                            n2 == NOT_FOUND || m2 == NOT_FOUND) {
                        bug_error("notfound 1");
                    }
                    space->k12[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2,
                            qyt2, 0.0, 0.0, nr, space->knm_flags), -phase);
                    if (inter.debug & 32 && !ceq(complex_0, space->k12[n][m])) {
                        message(" [%d, %d]=%s, ", n, m, complex_form(space->k12[n][m]));
                    }
                }

                if (inter.debug & 32) {
                    message("\n   ");
                }
            }

            if (inter.debug & 32) {
                message("\n");
            }
        }

        // transmission2
        err = component_matrix(&trans1, component, node2_index, node1_index,
                TANGENTIAL);
        if (!err) {
            qxt = q1_q2(trans1, cminus(cconj(qx2)), nr, nr);
            err = component_matrix(&trans1, component, node2_index, node1_index,
                    SAGITTAL);
            qyt = q1_q2(trans1, cminus(cconj(qy2)), nr, nr);

            qxt2 = cminus(cconj(qx1));
            qyt2 = cminus(cconj(qy1));
            
            space->qm21.qxi = qxt;
            space->qm21.qxo = qxt2;
            space->qm21.qyi = qyt;
            space->qm21.qyo = qyt2;
                
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                space->mismatching |= 2;
            }

            if (inter.trace & 64) {
                if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                    mode_match(qxt, qxt2, &kx, &kmx);
                    mode_match(qyt, qyt2, &ky, &kmy);
                    message(" t21: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                            complex_form(kx), complex_form(ky), kmx, kmy);
                }
            }

            if (inter.debug & 32) {
                message("k21");
            }

            if (inter.set_tem_phase_zero & 1) {
                phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr, space->knm_flags));
            } else {
                phase = 0.0;
            }

            int n, m;
            for (n = 0; n < inter.num_fields; n++) {
                for (m = 0; m < inter.num_fields; m++) {
                    get_tem_modes_from_field_index(&n1, &m1, n);
                    get_tem_modes_from_field_index(&n2, &m2, m);
                    if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
                            n2 == NOT_FOUND || m2 == NOT_FOUND) {
                        bug_error("notfound 2");
                    }
                    space->k21[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2,
                            qyt2, 0.0, 0.0, nr, space->knm_flags), -phase);
                    if (inter.debug & 32 && !ceq(complex_0, space->k21[n][m])) {
                        message(" [%d, %d]=%s, ", n, m, complex_form(space->k21[n][m]));
                    }
                }

                if (inter.debug & 32) {
                    message("\n   ");
                }
            }

            if (inter.debug & 32) {
                message("\n");
            }
        }
    }

    int n, m;

    memset(space->k12_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(space->k21_sqrd_sum, 0, sizeof (double) * inter.num_fields);

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            space->k12_sqrd_sum[n] += zabs(space->k12[n][m]);
            space->k21_sqrd_sum[n] += zabs(space->k21[n][m]);
        }
    }

    return (space->mismatching);
}

//! Set coupling coefficient for a lens

/*!
 * \param lens_index lens index
 *
 * \see Test_set_k_lens()
 */
int set_k_lens(int lens_index) {
    int err;
    int component;
    int node1_index, node2_index;
    int n1, m1, n2, m2;
    double nr1, nr2;
    complex_t kx, ky;
    double kmx, kmy;
    double phase = 0.0;

    ABCD_t trans1;
    complex_t qx1, qy1, qx2, qy2;
    complex_t qxt, qyt;
    complex_t qxt2, qyt2;
    lens_t *lens;

    node_t node1, node2;

    // sanity check on input
    // lens index should be in the range 0 <= lens_index < inter.num_lenses
    assert(lens_index >= 0);
    assert(lens_index < inter.num_lenses);

    lens = &(inter.lens_list[lens_index]);
    component = get_overall_component_index(LENS, lens_index);
    node1_index = lens->node1_index;
    node2_index = lens->node2_index;

    nr1 = nr2 = init.n0;

    node1 = inter.node_list[node1_index];
    node2 = inter.node_list[node2_index];

    if (NOT node1.gnd_node) {
        //node1 = inter.node_list[node1_index];
        nr1 = *node1.n;
        if (node1.component_index == component) {
            // reverse q so that beam comes _to_ the lens from node1
            qx1 = cminus(cconj(node1.qx));
            qy1 = cminus(cconj(node1.qy));
        } else {
            qx1 = node1.qx;
            qy1 = node1.qy;
        }
    }

    if (NOT node2.gnd_node) {
        //node2 = inter.node_list[node2_index];
        nr2 = *node2.n;
        if (node2.component_index == component) {
            qx2 = node2.qx;
            qy2 = node2.qy;
        } else {
            // reverse q so that beam goes _from_ the lens to node2
            qx2 = cminus(cconj(node2.qx));
            qy2 = cminus(cconj(node2.qy));
        }
    }

    if (nr1 != nr2) {
        gerror("index of refraction not consistent at lens '%s'\n", lens->name);
    }

    if (inter.debug & 32 || inter.trace & 64) {
        message("%s :\n", lens->name);
    }

    lens->mismatching = 0;

    if (NOT node1.gnd_node &&
            NOT node2.gnd_node) {
        // transmission1
        err = component_matrix(&trans1, component, node1_index, node2_index,
                TANGENTIAL);
        if (!err) {
            qxt = q1_q2(trans1, qx1, nr1, nr2);
            err = component_matrix(&trans1, component, node1_index, node2_index,
                    SAGITTAL);
            qyt = q1_q2(trans1, qy1, nr1, nr2);

            qxt2 = qx2;
            qyt2 = qy2;
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                lens->mismatching |= 1;
                
                lens->qm12.qxi = qxt;
                lens->qm12.qxo = qxt2;
                lens->qm12.qyi = qyt;
                lens->qm12.qyo = qyt2;
            }

            if (inter.trace & 64) {
                if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                    mode_match(qxt, qxt2, &kx, &kmx);
                    mode_match(qyt, qyt2, &ky, &kmy);
                    message(" t12: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                            complex_form(kx), complex_form(ky), kmx, kmy);
                }
            }

            if (inter.debug & 32) {
                message("k12");
            }

            if (inter.set_tem_phase_zero & 1) {
                phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr2, lens->knm_flags));
            } else {
                phase = 0;
            }

            int n, m;
            for (n = 0; n < inter.num_fields; n++) {
                for (m = 0; m < inter.num_fields; m++) {
                    get_tem_modes_from_field_index(&n1, &m1, n);
                    get_tem_modes_from_field_index(&n2, &m2, m);
                    if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
                            n2 == NOT_FOUND || m2 == NOT_FOUND) {
                        bug_error("notfound 1");
                    }
                    lens->k12[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2,
                            qyt2, 0.0, 0.0, nr2, lens->knm_flags), -phase);
                    if (inter.debug & 32 && !ceq(complex_0, lens->k12[n][m])) {
                        message(" [%d, %d]=%s, ", n, m, complex_form(lens->k12[n][m]));
                    }
                }

                if (inter.debug & 32) {
                    message("\n   ");
                }
            }

            if (inter.debug & 32) {
                message("\n");
            }
        }

        // transmission2
        err = component_matrix(&trans1, component, node2_index, node1_index,
                TANGENTIAL);
        if (!err) {
            qxt = q1_q2(trans1, cminus(cconj(qx2)), nr2, nr1);
            err = component_matrix(&trans1, component, node2_index, node1_index,
                    SAGITTAL);
            qyt = q1_q2(trans1, cminus(cconj(qy2)), nr2, nr1);

            qxt2 = cminus(cconj(qx1));
            qyt2 = cminus(cconj(qy1));
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                lens->mismatching |= 2;
                lens->qm21.qxi = qxt;
                lens->qm21.qxo = qxt2;
                lens->qm21.qyi = qyt;
                lens->qm21.qyo = qyt2;
            }

            if (inter.trace & 64) {
                if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                    mode_match(qxt, qxt2, &kx, &kmx);
                    mode_match(qyt, qyt2, &ky, &kmy);
                    message(" t21: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                            complex_form(kx), complex_form(ky), kmx, kmy);
                }
            }

            if (inter.debug & 32) {
                message("k21");
            }

            if (inter.set_tem_phase_zero & 1) {
                phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr1, lens->knm_flags));
            } else {
                phase = 0;
            }

            int n, m;
            for (n = 0; n < inter.num_fields; n++) {
                for (m = 0; m < inter.num_fields; m++) {
                    get_tem_modes_from_field_index(&n1, &m1, n);
                    get_tem_modes_from_field_index(&n2, &m2, m);
                    if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
                            n2 == NOT_FOUND || m2 == NOT_FOUND) {
                        bug_error("notfound 2");
                    }
                    lens->k21[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2,
                            qyt2, 0.0, 0.0, nr1, lens->knm_flags), -phase);
                    if (inter.debug & 32 && !ceq(complex_0, lens->k21[n][m])) {
                        message(" [%d, %d]=%s, ", n, m, complex_form(lens->k21[n][m]));
                    }
                }

                if (inter.debug & 32) {
                    message("\n   ");
                }
            }

            if (inter.debug & 32) {
                message("\n");
            }
        }
    }

    int n, m;

    memset(lens->k12_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(lens->k21_sqrd_sum, 0, sizeof (double) * inter.num_fields);

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            lens->k12_sqrd_sum[n] += zabs(lens->k12[n][m]);
            lens->k21_sqrd_sum[n] += zabs(lens->k21[n][m]);
        }
    }

    return (lens->mismatching);
}

//! Set coupling coefficient for a modulator

/*!
 * \param modulator_index modulator index
 *
 * \see Test_set_k_modulator
 */
int set_k_modulator(int modulator_index) {
    int component;
    int node1_index, node2_index;
    int n1, m1, n2, m2;
    double nr1, nr2;
    complex_t kx, ky;
    double kmx, kmy;

    complex_t qx1, qy1, qx2, qy2;
    complex_t qxt, qyt;
    complex_t qxt2, qyt2;
    modulator_t *mod;
    node_t node1, node2;

    // sanity check on input
    // modulator index should be in the range 
    // 0 <= modulator_index < inter.num_modulators
    assert(modulator_index >= 0);
    assert(modulator_index < inter.num_modulators);

    mod = &(inter.modulator_list[modulator_index]);
    component = get_overall_component_index(MODULATOR, modulator_index);
    node1_index = mod->node1_index;
    node2_index = mod->node2_index;

    nr1 = nr2 = init.n0;

    node1 = inter.node_list[node1_index];
    node2 = inter.node_list[node2_index];

    if (NOT node1.gnd_node) {
        //node1 = inter.node_list[node1_index];
        nr1 = *node1.n;
        if (node1.component_index == component) {
            // reverse q so that beam comes _to_ the mod from node1
            qx1 = cminus(cconj(node1.qx));
            qy1 = cminus(cconj(node1.qy));
        } else {
            qx1 = node1.qx;
            qy1 = node1.qy;
        }
    }

    if (NOT node2.gnd_node) {
        //node2 = inter.node_list[node2_index];
        nr2 = *node2.n;
        if (node2.component_index == component) {
            qx2 = node2.qx;
            qy2 = node2.qy;
        } else {
            // reverse q so that beam goes _from_ the mod to node2
            qx2 = cminus(cconj(node2.qx));
            qy2 = cminus(cconj(node2.qy));
        }
    }

    if (nr1 != nr2) {
        gerror("index of refraction not consistent at modulator '%s'\n", mod->name);
    }

    if (inter.debug & 32 || inter.trace & 64) {
        message("%s :\n", mod->name);
    }

    mod->mismatching = 0;

    if (NOT node1.gnd_node && NOT node2.gnd_node) {
        qxt = qx1;
        qyt = qy1;

        qxt2 = qx2;
        qyt2 = qy2;

        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            mod->mismatching |= 1;
            mod->qm12.qxi = qxt;
            mod->qm12.qxo = qxt2;
            mod->qm12.qyi = qyt;
            mod->qm12.qyo = qyt2;
        }

        if (inter.trace & 64) {
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                mode_match(qxt, qxt2, &kx, &kmx);
                mode_match(qyt, qyt2, &ky, &kmy);
                message(" t12: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                        complex_form(kx), complex_form(ky), kmx, kmy);
            }
        }

        if (inter.debug & 32) {
            message("k12");
        }

        int n, m;

        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
                        n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 1");
                }
                mod->k12[n][m] = k_nmnm_new(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr2, mod->knm_flags);

                if (inter.debug & 32 && !ceq(complex_0, mod->k12[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(mod->k12[n][m]));
                }
            }

            if (inter.debug & 32) {
                message("\n   ");
            }
        }

        if (inter.debug & 32) {
            message("\n");
        }

        // transmission2
        qxt = cminus(cconj(qx2));
        qyt = cminus(cconj(qy2));
        qxt2 = cminus(cconj(qx1));
        qyt2 = cminus(cconj(qy1));

        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            mod->mismatching |= 2;
            mod->qm21.qxi = qxt;
            mod->qm21.qxo = qxt2;
            mod->qm21.qyi = qyt;
            mod->qm21.qyo = qyt2;
        }

        if (inter.trace & 64) {
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                mode_match(qxt, qxt2, &kx, &kmx);
                mode_match(qyt, qyt2, &ky, &kmy);
                message(" t21: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                        complex_form(kx), complex_form(ky), kmx, kmy);
            }
        }

        if (inter.debug & 32) {
            message("k21");
        }

        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
                        n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 2");
                }
                mod->k21[n][m] = k_nmnm_new(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr1, mod->knm_flags);

                if (inter.debug & 32 && !ceq(complex_0, mod->k21[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(mod->k21[n][m]));
                }
            }

            if (inter.debug & 32) {
                message("\n   ");
            }
        }

        if (inter.debug & 32) {
            message("\n");
        }
    }

    if (mod->type == MODTYPE_PITCH || mod->type == MODTYPE_YAW) {
        // If we have a tilt modulation we need to compute the HOM scattering
        // into the HOM's of the RF sidebands.

        // First zero the scattering matrices...
        memset(&(mod->ktm12[0][0]), 0, inter.num_fields * inter.num_fields * sizeof (complex_t));
        memset(&(mod->ktm21[0][0]), 0, inter.num_fields * inter.num_fields * sizeof (complex_t));

        /**
         * Here I assume that the modulation index has been chosen to be small.
         * Thus the Bessel functions can be linearised and the coupling matrix
         * is similar to that of the equations in Daniel Brown's PhD thesis
         * appendix C.11 and C.12.
         **/
        {
            complex_t q1 = complex_0, q2 = complex_0;

            if (!node1.gnd_node) {
                // q at node 1 calculated is that of the mode coming into the
                // modulator from node 1, thus needs to be reversed
                if (mod->type == MODTYPE_YAW) {
                    q1 = cminus(cconj(qx1));
                } else {
                    q1 = cminus(cconj(qy1));
                }
            }

            if (!node2.gnd_node) {
                if (mod->type == MODTYPE_YAW) {
                    q2 = qx2;
                } else {
                    q2 = qy2;
                }
            }

            int i = 0, d = 0, u = 0;

            double gouy_1 = gouy(q1);
            double gouy_2 = gouy(q2);
            double w1 = w_size(q1, nr1);
            double w2 = w_size(q2, nr2);

            complex_t factor12 = complex_1;
            complex_t factor21 = complex_1;

            for (i = 0; i < inter.num_fields; i++) {
                int tn = 0, tm = 0;

                get_tem_modes_from_field_index(&tn, &tm, i);

                u = -1;
                d = -1;

                if (mod->type == MODTYPE_YAW) {
                    if (tn + 1 + tm <= inter.tem) {
                        u = get_field_index_from_tem(tn + 1, tm);

                        if (!node1.gnd_node) mod->ktm21[i][u] = z_by_xphr(complex_1, w1 * sqrt(tn + 1) / 2.0, -gouy_1);
                        if (!node2.gnd_node) mod->ktm12[i][u] = z_by_xphr(complex_1, w2 * sqrt(tn + 1) / 2.0, -gouy_2);
                    }

                    if (tn - 1 >= 0) {
                        d = get_field_index_from_tem(tn - 1, tm);

                        if (!node1.gnd_node) mod->ktm21[i][d] = z_by_xphr(complex_1, w1 * sqrt(tn) / 4.0, gouy_1);
                        if (!node2.gnd_node) mod->ktm12[i][d] = z_by_xphr(complex_1, w2 * sqrt(tn) / 4.0, gouy_2);
                    }
                } else {
                    if (tn + tm + 1 <= inter.tem) {
                        u = get_field_index_from_tem(tn, tm + 1);

                        if (!node1.gnd_node) mod->ktm21[i][u] = z_by_xphr(complex_1, w1 * sqrt(tm + 1) / 2.0, -gouy_1);
                        if (!node2.gnd_node) mod->ktm12[i][u] = z_by_xphr(complex_1, w2 * sqrt(tm + 1) / 2.0, -gouy_2);
                    }

                    if (tm - 1 >= 0) {
                        d = get_field_index_from_tem(tn, tm - 1);

                        if (!node1.gnd_node) mod->ktm21[i][d] = z_by_xphr(complex_1, w1 * sqrt(tm) / 4.0, gouy_1);
                        if (!node2.gnd_node) mod->ktm12[i][d] = z_by_xphr(complex_1, w2 * sqrt(tm) / 4.0, gouy_2);
                    }
                }

                if (u >= 0) {
                    // Set the transpose elements and scaling
                    mod->ktm12[u][i] = z_by_zc(factor12, mod->ktm12[i][u]);
                    mod->ktm21[u][i] = z_by_zc(factor21, mod->ktm21[i][u]);

                    // apply scaling factors which aren't conjugated
                    mod->ktm12[i][u] = z_by_z(factor12, mod->ktm12[i][u]);
                    mod->ktm21[i][u] = z_by_z(factor21, mod->ktm21[i][u]);
                }

                if (d >= 0) {
                    // Set the transpose elements and scaling
                    mod->ktm12[d][i] = z_by_zc(factor12, mod->ktm12[i][d]);
                    mod->ktm21[d][i] = z_by_zc(factor21, mod->ktm21[i][d]);

                    // apply scaling factors which aren't conjugated
                    mod->ktm12[i][d] = z_by_z(factor12, mod->ktm12[i][d]);
                    mod->ktm21[i][d] = z_by_z(factor21, mod->ktm21[i][d]);
                }
            }
        }

        // Now combine any mode mismatch in the Bayer-Helms matrix
        // with the tilt scattering matrix to get the full coupling
        knm_matrix_mult(mod->k12, mod->ktm12, mod->ktm12);
        knm_matrix_mult(mod->k21, mod->ktm21, mod->ktm21);
    }

    // Need to merge matrices and do some reversing of the Gouy phase on the 
    // combined matrices now.

    int n, m;

    for (n = 0; n < inter.num_fields; n++) {
        get_tem_modes_from_field_index(&n1, &m1, n);

        for (m = 0; m < inter.num_fields; m++) {
            get_tem_modes_from_field_index(&n2, &m2, m);

            //warn("knm %i%i->%i%i: %s %s\n", n1,m1,n2,m2, complex_form(mod->k12[n][m]), complex_form(mod->k21[n][m]));

            mod->k12[n][m] = rev_gouy(mod->k12[n][m], n1, m1, n2, m2, qx1, qx2, qy1, qy2);
            mod->k21[n][m] = rev_gouy(mod->k21[n][m], n1, m1, n2, m2, cminus(cconj(qx2)), cminus(cconj(qx1)), cminus(cconj(qy2)), cminus(cconj(qy1)));

            //warn("ktm %i%i->%i%i: %s %s\n", n1,m1,n2,m2, complex_form(mod->ktm12[n][m]), complex_form(mod->ktm21[n][m]));

            mod->ktm12[n][m] = rev_gouy(mod->ktm12[n][m], n1, m1, n2, m2, qx1, qx2, qy1, qy2);
            mod->ktm21[n][m] = rev_gouy(mod->ktm21[n][m], n1, m1, n2, m2, cminus(cconj(qx2)), cminus(cconj(qx1)), cminus(cconj(qy2)), cminus(cconj(qy1)));
        }
    }

    double p12 = 0, p21 = 0, p12tm = 0, p21tm = 0;

    if (inter.set_tem_phase_zero & 1) {
        p12 = zphase(mod->k12[0][0]);
        p21 = zphase(mod->k21[0][0]);
        p12tm = zphase(mod->ktm12[0][0]);
        p21tm = zphase(mod->ktm21[0][0]);


        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                mod->k12[n][m] = z_by_phr(mod->k12[n][m], -p12);
                mod->k21[n][m] = z_by_phr(mod->k21[n][m], -p21);
                mod->ktm12[n][m] = z_by_phr(mod->ktm12[n][m], -p12tm);
                mod->ktm21[n][m] = z_by_phr(mod->ktm21[n][m], -p21tm);
            }
        }
    }

    memset(mod->k12_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(mod->k21_sqrd_sum, 0, sizeof (double) * inter.num_fields);

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            mod->k12_sqrd_sum[n] += zabs(mod->k12[n][m]);
            mod->k21_sqrd_sum[n] += zabs(mod->k21[n][m]);
        }
    }

    return (mod->mismatching);
}

int set_k_dbs(int dbs_index) {
    int component;
    int node1_index, node2_index, node3_index, node4_index;
    int n1, m1, n2, m2;

    double nr1, nr2, nr3, nr4;

    nr1 = nr2 = nr3 = nr4 = init.n0;

    complex_t kx, ky;

    double kmx, kmy;
    double phase = 0.0;

    complex_t qx1i, qy1i, qx2i, qy2i, qx3i, qy3i, qx4i, qy4i;
    complex_t qx1o, qy1o, qx2o, qy2o, qx3o, qy3o, qx4o, qy4o;
    
    dbs_t *dbs;

    node_t node1, node2, node3, node4;

    // sanity checks on input
    // bs_index should be in the range 0 <= bs_index < inter.num_beamsplitters
    assert(dbs_index >= 0);
    assert(dbs_index < inter.num_dbss);

    dbs = &(inter.dbs_list[dbs_index]);

    component = get_overall_component_index(DBS, dbs_index);

    node1_index = dbs->node1_index;
    node2_index = dbs->node2_index;
    node3_index = dbs->node3_index;
    node4_index = dbs->node4_index;

    node1 = inter.node_list[node1_index];
    node2 = inter.node_list[node2_index];
    node3 = inter.node_list[node3_index];
    node4 = inter.node_list[node4_index];

    if (NOT node1.gnd_node) {
        node1 = inter.node_list[node1_index];

        nr1 = *node1.n;
        if (node1.component_index == component) {
            qx1i = cminus(cconj(node1.qx));
            qy1i = cminus(cconj(node1.qy));
            qx1o = node1.qx;
            qy1o = node1.qy;
        } else {
            qx1i = node1.qx;
            qy1i = node1.qy;
            qx1o = cminus(cconj(node1.qx));
            qy1o = cminus(cconj(node1.qy));
        }
    } else {
        qx1i = complex_0;
        qy1i = complex_0;
        qx1o = complex_0;
        qy1o = complex_0;
    }

    if (NOT node2.gnd_node) {
        node2 = inter.node_list[node2_index];

        nr2 = *node2.n;
        if (node2.component_index == component) {
            qx2i = cminus(cconj(node2.qx));
            qy2i = cminus(cconj(node2.qy));
            qx2o = node2.qx;
            qy2o = node2.qy;
        } else {
            qx2i = node2.qx;
            qy2i = node2.qy;
            qx2o = cminus(cconj(node2.qx));
            qy2o = cminus(cconj(node2.qy));
        }
    } else {
        qx2i = complex_0;
        qy2i = complex_0;
        qx2o = complex_0;
        qy2o = complex_0;
    }

    if (NOT node3.gnd_node) {
        node3 = inter.node_list[node3_index];
        nr3 = *node3.n;
        if (node3.component_index == component) {
            qx3i = cminus(cconj(node3.qx));
            qy3i = cminus(cconj(node3.qy));
            qx3o = node3.qx;
            qy3o = node3.qy;
        } else {
            qx3i = node3.qx;
            qy3i = node3.qy;
            qx3o = cminus(cconj(node3.qx));
            qy3o = cminus(cconj(node3.qy));
        }
    } else {
        qx3i = complex_0;
        qy3i = complex_0;
        qx3o = complex_0;
        qy3o = complex_0;
    }

    if (NOT node4.gnd_node) {
        node4 = inter.node_list[node4_index];
        nr4 = *node4.n;
        if (node4.component_index == component) {
            qx4i = cminus(cconj(node4.qx));
            qy4i = cminus(cconj(node4.qy));
            qx4o = node4.qx;
            qy4o = node4.qy;
        } else {
            qx4i = node4.qx;
            qy4i = node4.qy;
            qx4o = cminus(cconj(node4.qx));
            qy4o = cminus(cconj(node4.qy));
        }
    } else {
        qx4i = complex_0;
        qy4i = complex_0;
        qx4o = complex_0;
        qy4o = complex_0;
    }

    if (inter.debug & 32 || inter.trace & 64) {
        message("%s :\n", dbs->name);
    }

    dbs->mismatching = 0;

    complex_t qxt, qyt, qxt2, qyt2;

    if (NOT node1.gnd_node && NOT node3.gnd_node) {
        qxt = qx1i;
        qyt = qy1i;

        qxt2 = qx3o;
        qyt2 = qy3o;

        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            dbs->mismatching |= 1;
            dbs->qm13.qxi = qxt;
            dbs->qm13.qxo = qxt2;
            dbs->qm13.qyi = qyt;
            dbs->qm13.qyo = qyt2;
        }

        if (inter.trace & 64) {
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                mode_match(qxt, qxt2, &kx, &kmx);
                mode_match(qyt, qyt2, &ky, &kmy);
                message(" t13: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                        complex_form(kx), complex_form(ky), kmx, kmy);
            }
        }

        if (inter.debug & 32) {
            message("k13");
        }

        if (inter.set_tem_phase_zero & 1) {
            phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr3, dbs->knm_flags));
        } else {
            phase = 0.0;
        }

        int n, m;
        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                if (n1 == NOT_FOUND || m1 == NOT_FOUND || n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 1");
                }
                dbs->k13[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr3, dbs->knm_flags), -phase);

                if (inter.debug & 32 && !ceq(complex_0, dbs->k13[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(dbs->k13[n][m]));
                }
            }

            if (inter.debug & 32) {
                message("\n   ");
            }
        }

        if (inter.debug & 32) {
            message("\n");
        }
    }

    if (NOT node2.gnd_node && NOT node1.gnd_node) {
        qxt = qx2i;
        qyt = qy2i;

        qxt2 = qx1o;
        qyt2 = qy1o;

        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            dbs->mismatching |= 2;
            
            dbs->qm21.qxi = qxt;
            dbs->qm21.qxo = qxt2;
            dbs->qm21.qyi = qyt;
            dbs->qm21.qyo = qyt2;
        }

        if (inter.trace & 64) {
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                mode_match(qxt, qxt2, &kx, &kmx);
                mode_match(qyt, qyt2, &ky, &kmy);
                message(" t21: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                        complex_form(kx), complex_form(ky), kmx, kmy);
            }
        }

        if (inter.debug & 32) {
            message("k21");
        }

        if (inter.set_tem_phase_zero & 1) {
            phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr1, dbs->knm_flags));
        } else {
            phase = 0.0;
        }

        int n, m;
        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                if (n1 == NOT_FOUND || m1 == NOT_FOUND || n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 1");
                }
                dbs->k21[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr1, dbs->knm_flags), -phase);

                if (inter.debug & 32 && !ceq(complex_0, dbs->k21[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(dbs->k21[n][m]));
                }
            }

            if (inter.debug & 32) {
                message("\n   ");
            }
        }

        if (inter.debug & 32) {
            message("\n");
        }
    }

    if (NOT node3.gnd_node && NOT node4.gnd_node) {
        qxt = qx3i;
        qyt = qy3i;

        qxt2 = qx4o;
        qyt2 = qy4o;

        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            dbs->mismatching |= 4;
            
            dbs->qm34.qxi = qxt;
            dbs->qm34.qxo = qxt2;
            dbs->qm34.qyi = qyt;
            dbs->qm34.qyo = qyt2;
        }

        if (inter.trace & 64) {
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                mode_match(qxt, qxt2, &kx, &kmx);
                mode_match(qyt, qyt2, &ky, &kmy);
                message(" t34: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                        complex_form(kx), complex_form(ky), kmx, kmy);
            }
        }

        if (inter.debug & 32) {
            message("k34");
        }

        if (inter.set_tem_phase_zero & 1) {
            phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr4, dbs->knm_flags));
        } else {
            phase = 0.0;
        }

        int n, m;
        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                if (n1 == NOT_FOUND || m1 == NOT_FOUND || n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 1");
                }
                dbs->k34[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr4, dbs->knm_flags), -phase);

                if (inter.debug & 32 && !ceq(complex_0, dbs->k34[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(dbs->k34[n][m]));
                }
            }

            if (inter.debug & 32) {
                message("\n   ");
            }
        }

        if (inter.debug & 32) {
            message("\n");
        }
    }

    if (NOT node4.gnd_node && NOT node2.gnd_node) {
        qxt = qx4i;
        qyt = qy4i;

        qxt2 = qx2o;
        qyt2 = qy2o;

        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            dbs->mismatching |= 8;
            
            dbs->qm42.qxi = qxt;
            dbs->qm42.qxo = qxt2;
            dbs->qm42.qyi = qyt;
            dbs->qm42.qyo = qyt2;
        }

        if (inter.trace & 64) {
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                mode_match(qxt, qxt2, &kx, &kmx);
                mode_match(qyt, qyt2, &ky, &kmy);
                message(" t42: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                        complex_form(kx), complex_form(ky), kmx, kmy);
            }
        }

        if (inter.debug & 32) {
            message("k42");
        }

        if (inter.set_tem_phase_zero & 1) {
            phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr2, dbs->knm_flags));
        } else {
            phase = 0.0;
        }

        int n, m;
        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                if (n1 == NOT_FOUND || m1 == NOT_FOUND || n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 1");
                }
                dbs->k42[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr2, dbs->knm_flags), -phase);

                if (inter.debug & 32 && !ceq(complex_0, dbs->k42[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(dbs->k42[n][m]));
                }
            }

            if (inter.debug & 32) {
                message("\n   ");
            }
        }

        if (inter.debug & 32) {
            message("\n");
        }
    }

    int n, m;

    memset(dbs->k13_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(dbs->k21_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(dbs->k34_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(dbs->k42_sqrd_sum, 0, sizeof (double) * inter.num_fields);

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            dbs->k13_sqrd_sum[n] += zabs(dbs->k13[n][m]);
            dbs->k21_sqrd_sum[n] += zabs(dbs->k21[n][m]);
            dbs->k34_sqrd_sum[n] += zabs(dbs->k34[n][m]);
            dbs->k42_sqrd_sum[n] += zabs(dbs->k42[n][m]);
        }
    }

    return (dbs->mismatching);
}

//! Set coupling coefficients for the given diode

/*!
 * \param diode_index diode index
 *
 * \see Test_set_k_diode()
 */
int set_k_diode(int diode_index) {
    int component;
    int node1_index, node2_index, node3_index;
    int n1, m1, n2, m2;
    double nr1, nr2, nr3;
    complex_t kx, ky;
    double kmx, kmy;
    double phase = 0.0;

    complex_t qx1, qy1, qx2, qy2, qx3, qy3;
    complex_t qxt, qyt;
    complex_t qxt2, qyt2;
    diode_t *diode;
    node_t node1, node2, node3;

    // sanity checks on input
    // diode index should be in the range: 0 <= diode_index < inter.num_diodes
    assert(diode_index >= 0);
    assert(diode_index < inter.num_diodes);

    diode = &(inter.diode_list[diode_index]);
    component = get_overall_component_index(DIODE, diode_index);

    node1_index = diode->node1_index;
    node2_index = diode->node2_index;
    node3_index = diode->node3_index;

    nr1 = nr2 = nr3 = init.n0;

    node1 = inter.node_list[node1_index];
    node2 = inter.node_list[node2_index];
    node3 = inter.node_list[node3_index];

    if (NOT node1.gnd_node) {
        //node1 = inter.node_list[node1_index];
        nr1 = *node1.n;
        if (node1.component_index == component) {
            // reverse q so that beam comes _to_ the diode from node1
            qx1 = cminus(cconj(node1.qx));
            qy1 = cminus(cconj(node1.qy));
        } else {
            qx1 = node1.qx;
            qy1 = node1.qy;
        }
    }

    if (NOT node2.gnd_node) {
        //node2 = inter.node_list[node2_index];
        nr2 = *node2.n;
        if (node2.component_index == component) {
            qx2 = node2.qx;
            qy2 = node2.qy;
        } else {
            // reverse q so that beam goes _from_ the diode to node2
            qx2 = cminus(cconj(node2.qx));
            qy2 = cminus(cconj(node2.qy));
        }
    }

    if (NOT node3.gnd_node) {
        //node2 = inter.node_list[node2_index];
        nr3 = *node3.n;
        if (node3.component_index == component) {
            // reverse q so that beam goes _from_ the diode to node3
            qx3 = cminus(cconj(node3.qx));
            qy3 = cminus(cconj(node3.qy));
        } else {
            qx3 = node3.qx;
            qy3 = node3.qy;
        }
    }

    if ((nr1 != nr2 && !node1.gnd_node && !node2.gnd_node) || (nr3 != nr2 && !node3.gnd_node && !node2.gnd_node)) {
        gerror("index of refraction not consistent at diode '%s' (%g, %g, %g)\n", diode->name, nr1, nr2, nr3);
    }

    if (inter.debug & 32 || inter.trace & 64) {
        message("%s :\n", diode->name);
    }

    diode->mismatching = 0;

    if (NOT node1.gnd_node &&
            NOT node2.gnd_node) {
        qxt = qx1;
        qyt = qy1;

        qxt2 = qx2;
        qyt2 = qy2;
        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            diode->mismatching |= 1;
            
            diode->qm12.qxi = qxt;
            diode->qm12.qxo = qxt2;
            diode->qm12.qyi = qyt;
            diode->qm12.qyo = qyt2;
        }

        if (inter.trace & 64) {
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                mode_match(qxt, qxt2, &kx, &kmx);
                mode_match(qyt, qyt2, &ky, &kmy);
                message(" t12: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                        complex_form(kx), complex_form(ky), kmx, kmy);
            }
        }

        if (inter.debug & 32) {
            message("k12");
        }

        if (inter.set_tem_phase_zero & 1) {
            phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr2, diode->knm_flags));
        } else {
            phase = 0.0;
        }

        int n, m;
        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
                        n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 1");
                }
                diode->k12[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2,
                        0.0, 0.0, nr2, diode->knm_flags), -phase);
                if (inter.debug & 32 && !ceq(complex_0, diode->k12[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(diode->k12[n][m]));
                }
            }

            if (inter.debug & 32) {
                message("\n   ");
            }
        }

        if (inter.debug & 32) {
            message("\n");
        }

        // transmission2
        qxt = cminus(cconj(qx2));
        qyt = cminus(cconj(qy2));
        qxt2 = cminus(cconj(qx1));
        qyt2 = cminus(cconj(qy1));
        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            diode->mismatching |= 2;
            
            diode->qm21.qxi = qxt;
            diode->qm21.qxo = qxt2;
            diode->qm21.qyi = qyt;
            diode->qm21.qyo = qyt2;
        }

        if (inter.trace & 64) {
            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
                mode_match(qxt, qxt2, &kx, &kmx);
                mode_match(qyt, qyt2, &ky, &kmy);
                message(" t21: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                        complex_form(kx), complex_form(ky), kmx, kmy);
            }
        }

        if (inter.debug & 32) {
            message("k21");
        }

        if (inter.set_tem_phase_zero & 1) {
            phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr1, diode->knm_flags));
        } else {
            phase = 0.0;
        }

        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
                        n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 2");
                }
                diode->k21[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2,
                        0.0, 0.0, nr1, diode->knm_flags), -phase);
                if (inter.debug & 32 && !ceq(complex_0, diode->k21[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(diode->k21[n][m]));
                }
            }

            if (inter.debug & 32) {
                message("\n   ");
            }
        }

        if (inter.debug & 32) {
            message("\n");
        }
    }

//    if (NOT node3.gnd_node &&
//            NOT node2.gnd_node) {
//        qxt = qx3;
//        qyt = qy3;
//
//        qxt2 = qx2;
//        qyt2 = qy2;
//        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
//            diode->mismatching |= 4;
//        }
//
//        if (inter.trace & 64) {
//            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
//                mode_match(qxt, qxt2, &kx, &kmx);
//                mode_match(qyt, qyt2, &ky, &kmy);
//                message(" t32: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
//                        complex_form(kx), complex_form(ky), kmx, kmy);
//            }
//        }
//
//        if (inter.debug & 32) {
//            message("k32");
//        }
//
//        if (inter.set_tem_phase_zero & 1) {
//            phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr3, diode->knm_flags));
//        } else {
//            phase = 0.0;
//        }
//
//        int n, m;
//        for (n = 0; n < inter.num_fields; n++) {
//            for (m = 0; m < inter.num_fields; m++) {
//                get_tem_modes_from_field_index(&n1, &m1, n);
//                get_tem_modes_from_field_index(&n2, &m2, m);
//                if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
//                        n2 == NOT_FOUND || m2 == NOT_FOUND) {
//                    bug_error("notfound 1");
//                }
//                diode->k32[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2,
//                        0.0, 0.0, nr3, diode->knm_flags), -phase);
//                if (inter.debug & 32 && !ceq(complex_0, diode->k32[n][m])) {
//                    message(" [%d, %d]=%s, ", n, m, complex_form(diode->k32[n][m]));
//                }
//            }
//
//            if (inter.debug & 32) {
//                message("\n   ");
//            }
//        }
//
//        if (inter.debug & 32) {
//            message("\n");
//        }
//
//        // transmission2
//        qxt = cminus(cconj(qx2));
//        qyt = cminus(cconj(qy2));
//        qxt2 = cminus(cconj(qx3));
//        qyt2 = cminus(cconj(qy3));
//        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
//            diode->mismatching |= 8;
//        }
//
//        if (inter.trace & 64) {
//            if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
//                mode_match(qxt, qxt2, &kx, &kmx);
//                mode_match(qyt, qyt2, &ky, &kmy);
//                message(" t23: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
//                        complex_form(kx), complex_form(ky), kmx, kmy);
//            }
//        }
//
//        if (inter.debug & 32) {
//            message("k23");
//        }
//
//        if (inter.set_tem_phase_zero & 1) {
//            phase = zphase(k_nmnm(0, 0, 0, 0, qxt, qyt, qxt2, qyt2, 0.0, 0.0, nr1, diode->knm_flags));
//        } else {
//            phase = 0.0;
//        }
//
//        for (n = 0; n < inter.num_fields; n++) {
//            for (m = 0; m < inter.num_fields; m++) {
//                get_tem_modes_from_field_index(&n1, &m1, n);
//                get_tem_modes_from_field_index(&n2, &m2, m);
//                if (n1 == NOT_FOUND || m1 == NOT_FOUND ||
//                        n2 == NOT_FOUND || m2 == NOT_FOUND) {
//                    bug_error("notfound 2");
//                }
//                diode->k23[n][m] = z_by_phr(k_nmnm(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2,
//                        0.0, 0.0, nr1, diode->knm_flags), -phase);
//                if (inter.debug & 32 && !ceq(complex_0, diode->k23[n][m])) {
//                    message(" [%d, %d]=%s, ", n, m, complex_form(diode->k23[n][m]));
//                }
//            }
//
//            if (inter.debug & 32) {
//                message("\n   ");
//            }
//        }
//
//        if (inter.debug & 32) {
//            message("\n");
//        }
//    }

    int n, m;

    memset(diode->k12_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(diode->k21_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(diode->k23_sqrd_sum, 0, sizeof (double) * inter.num_fields);
    memset(diode->k32_sqrd_sum, 0, sizeof (double) * inter.num_fields);

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            diode->k12_sqrd_sum[n] += zabs(diode->k12[n][m]);
            diode->k21_sqrd_sum[n] += zabs(diode->k21[n][m]);
            diode->k23_sqrd_sum[n] += zabs(diode->k32[n][m]);
            diode->k32_sqrd_sum[n] += zabs(diode->k23[n][m]);
        }
    }

    return (diode->mismatching);
}

//! Compute the mode match between two beam segments 

/*!
 * i.e. between two chosen base systems
 *
 * trace 4: mode mismatch parameters for the initial setup
 *  - Kx and Ky are used internally, but the output of 'trace 4'
 *    computes the mode mismatch with min(z_r, z_r') in the denominator 
 *    (see manual).  This gives a better quantitative number for the 
 *    mode mismatch.
 *  - kmx, kmy are now computed as K**2/(1+K**2) which seems to be more 
 *    useful than the original formula from Bayer-Helms.
 *  - please see the section about these parameters in the manual!
 *
 * \param q1 Gaussian beam parameter
 * \param q2 Gaussian beam parameter
 * \param k array of coupling coefficients
 * \param km array of coupling coefficients
 *
 * \see Test_mode_match()
 */
void mode_match(complex_t q1, complex_t q2, complex_t *k, double *km) {
    double zr;
    complex_t kt;
    double kmt;

    zr = 1.0 / min(q1.im, q2.im);
    kt.re = 0.5 * (q1.im - q2.im) * zr;
    kt.im = 0.5 * (q1.re - q2.re) * zr;
    kmt = zabs(kt) / (1.0 + zabs(kt)); // 22.07.03
    *k = kt;
    *km = kmt;
}

//! Set k_mnmn for all components

void set_k_all_components(bool rebuilding) {
    int initonly, noinit;

    bool force = false; // force update of the knm matrices, used for debugging

    // mode mismatch coefficients: TODO make use of them somewhere or remove.
    int mm = 0;
    int mbs = 0;
    int ms = 0;
    int ml = 0;
    int mmd = 0;
    int md = 0;

    initonly = noinit = 0;

    if (inter.trace & 4) {
        message("--- initial mode mismatch:\n");
        if (!(inter.trace & 64)) {
            inter.trace |= 64;
            initonly = 1;
        }
    } else {
        if (inter.trace & 64) {
            noinit = 1;
            inter.trace &= 447;
        }
    }

    int i;

    for (i = 0; i < inter.num_mirrors; i++) {
        mirror_t *mirror = &(inter.mirror_list[i]);

        if (!rebuilding || mirror->rebuild) {
            mm = set_k_mirror(i);
        } else {
            // no known rebuilding, still need to check if the q values have
            // changed
            node_t n1 = inter.node_list[mirror->node1_index];
            node_t n2 = inter.node_list[mirror->node2_index];

            if (force ||
                    ((!n1.gnd_node) && (n1.q_changed)) ||
                    ((!n2.gnd_node) && (n2.q_changed))) {
                mm = set_k_mirror(i);
            }
        }
    }

    for (i = 0; i < inter.num_beamsplitters; i++) {
        beamsplitter_t *bs = &(inter.bs_list[i]);
        if (!rebuilding || bs->rebuild) {
            mbs = set_k_beamsplitter(i);
        } else {
            // no known rebuilding, still need to check if the q values have
            // changed
            node_t n1 = inter.node_list[bs->node1_index];
            node_t n2 = inter.node_list[bs->node2_index];
            node_t n3 = inter.node_list[bs->node3_index];
            node_t n4 = inter.node_list[bs->node4_index];

            if (force || ((!n1.gnd_node) && (n1.q_changed))
                    || ((!n2.gnd_node) && (n2.q_changed))
                    || ((!n3.gnd_node) && (n3.q_changed))
                    || ((!n4.gnd_node) && (n4.q_changed))) {
                mbs = set_k_beamsplitter(i);
            }
        }
    }

    for (i = 0; i < inter.num_spaces; i++) {
        space_t *s = &inter.space_list[i];
        if (!rebuilding || s->rebuild) {
            ms = set_k_space(i);
        } else {
            // no known rebuilding, still need to check if the q values have
            // changed
            node_t n1 = inter.node_list[s->node1_index];
            node_t n2 = inter.node_list[s->node2_index];

            if (force || ((!n1.gnd_node) && (n1.q_changed))
                    || ((!n2.gnd_node) && (n2.q_changed))) {
                ms = set_k_space(i);
            }
        }

        if (inter.trace & 4 && ms) {
            message("Warning: mode mismatch at space %s (%d)\n",
                    inter.space_list[i].name, ms);
        }
    }

    for (i = 0; i < inter.num_lenses; i++) {
        lens_t *l = &inter.lens_list[i];
        if (!rebuilding || l->rebuild) {
            ml = set_k_lens(i);
        } else {
            // no known rebuilding, still need to check if the q values have
            // changed
            node_t n1 = inter.node_list[l->node1_index];
            node_t n2 = inter.node_list[l->node2_index];

            if (force || ((!n1.gnd_node) && (n1.q_changed))
                    || ((!n2.gnd_node) && (n2.q_changed))) {
                ml = set_k_lens(i);
            }
        }
    }

    for (i = 0; i < inter.num_modulators; i++) {
        modulator_t *mod = &inter.modulator_list[i];

        if (!rebuilding || mod->rebuild) {
            mmd = set_k_modulator(i);
        } else {
            // no known rebuilding, still need to check if the q values have
            // changed
            node_t n1 = inter.node_list[mod->node1_index];
            node_t n2 = inter.node_list[mod->node2_index];

            if (force || ((!n1.gnd_node) && (n1.q_changed))
                    || ((!n2.gnd_node) && (n2.q_changed))) {
                mmd = set_k_modulator(i);
            }
        }
    }

    for (i = 0; i < inter.num_diodes; i++) {
        diode_t *d = &inter.diode_list[i];

        if (!rebuilding || d->rebuild) {
            md = set_k_diode(i);
        } else {
            // no known rebuilding, still need to check if the q values have
            // changed
            node_t n1 = inter.node_list[d->node1_index];
            node_t n2 = inter.node_list[d->node2_index];

            if (force || ((!n1.gnd_node) && (n1.q_changed))
                    || ((!n2.gnd_node) && (n2.q_changed))) {
                md = set_k_diode(i);
            }
        }
    }

    for (i = 0; i < inter.num_dbss; i++) {
        dbs_t *dbs = &(inter.dbs_list[i]);
        if (!rebuilding || dbs->rebuild) {
            mbs = set_k_dbs(i);
        } else {
            // no known rebuilding, still need to check if the q values have changed
            node_t n1 = inter.node_list[dbs->node1_index];
            node_t n2 = inter.node_list[dbs->node2_index];
            node_t n3 = inter.node_list[dbs->node3_index];
            node_t n4 = inter.node_list[dbs->node4_index];

            if (force || ((!n1.gnd_node) && (n1.q_changed))
                    || ((!n2.gnd_node) && (n2.q_changed))
                    || ((!n3.gnd_node) && (n3.q_changed))
                    || ((!n4.gnd_node) && (n4.q_changed))) {
                mbs = set_k_dbs(i);
            }
        }
    }

    if (initonly) {
        inter.trace &= 447;
    }

    if (noinit) {
        inter.trace |= 64;
    }

    if (inter.trace & 4) {
        message("\n");
    }

    // TODO remove this later, if variables stay unused. For the moment:
    // silence compiler warning
    (void) mm;
    (void) mbs;
    (void) ms;
    (void) ml;
    (void) mmd;
    (void) md;
}

//! Set up an ABCD mirror

/*!
 * \param mirror_index index of mirror in mirror list
 *
 * \see Test_set_ABCD_mirror()
 */
void set_ABCD_mirror(int mirror_index) {
    double n, n1, n2;
    mirror_t *mirror;
    node_t node1, node2;

    // sanity checks on input
    // mirror index should be in the range 0 <= mirror_index < inter.num_mirrors
    assert(mirror_index >= 0);
    assert(mirror_index < inter.num_mirrors);


    mirror = &(inter.mirror_list[mirror_index]);

    node1 = inter.node_list[mirror->node1_index];
    node2 = inter.node_list[mirror->node2_index];

    n1 = n2 = 0; // silence compiler warning

    // setting reflection matrix
    if (NOT node1.gnd_node) {
        n1 = *(node1.n);
        if (mirror->Rcx == 0) {
            mirror->qqr1t = Unity;
        } else {
            // n1 added 040803
            mirror->qqr1t = make_ABCD(1.0, 0.0, -2.0 * n1 / mirror->Rcx, 1.0);
        }

        if (mirror->Rcy == 0) {
            mirror->qqr1s = Unity;
        } else {
            // n1 added 040803
            mirror->qqr1s = make_ABCD(1.0, 0.0, -2.0 * n1 / mirror->Rcy, 1.0);
        }
    }

    if (NOT node2.gnd_node) {
        n2 = *(node2.n);
        if (mirror->Rcx == 0) {
            mirror->qqr2t = Unity;
        } else {
            mirror->qqr2t = make_ABCD(1.0, 0.0, 2.0 * n2 / mirror->Rcx, 1.0);
        }

        if (mirror->Rcy == 0) {
            mirror->qqr2s = Unity;
        } else {
            mirror->qqr2s = make_ABCD(1.0, 0.0, 2.0 * n2 / mirror->Rcy, 1.0);
        }
    }

    if (NOT node1.gnd_node &&
            NOT node2.gnd_node) {
        // setting transmission matrix
        n = n2 - n1;
        if (n == 0) {
            mirror->qqtt = Unity;
            mirror->qqts = Unity;
        } else {
            if (mirror->Rcx == 0) {
                mirror->qqtt = Unity;
            } else {
                mirror->qqtt = make_ABCD(1.0, 0.0, n / mirror->Rcx, 1.0);
            }

            if (mirror->Rcy == 0) {
                mirror->qqts = Unity;
            } else {
                mirror->qqts = make_ABCD(1.0, 0.0, n / mirror->Rcy, 1.0);
            }
        }
    }

    if (inter.debug & 256) {
        fprintf(stdout, "mirror : %s ----------------\n", mirror->name);
        fprintf(stdout, "  qqtt\n");
        dump_ABCD(stdout, mirror->qqtt);
        fprintf(stdout, "  qqtt\n");
        dump_ABCD(stdout, mirror->qqtt);
        fprintf(stdout, "  qqtr1t\n");
        dump_ABCD(stdout, mirror->qqr1t);
        fprintf(stdout, "  qqr1s\n");
        dump_ABCD(stdout, mirror->qqr1s);
        fprintf(stdout, "  qqr2t\n");
        dump_ABCD(stdout, mirror->qqr2t);
        fprintf(stdout, "  qqr2s\n");
        dump_ABCD(stdout, mirror->qqr2s);

        // and send the output to the log file
        fprintf(fp_log, "mirror : %s ----------------\n", mirror->name);
        fprintf(fp_log, "  qqtt\n");
        dump_ABCD(fp_log, mirror->qqtt);
        fprintf(fp_log, "  qqtt\n");
        dump_ABCD(fp_log, mirror->qqtt);
        fprintf(fp_log, "  qqtr1t\n");
        dump_ABCD(fp_log, mirror->qqr1t);
        fprintf(fp_log, "  qqr1s\n");
        dump_ABCD(fp_log, mirror->qqr1s);
        fprintf(fp_log, "  qqr2t\n");
        dump_ABCD(fp_log, mirror->qqr2t);
        fprintf(fp_log, "  qqr2s\n");
        dump_ABCD(fp_log, mirror->qqr2s);

    }

    // dump_ABCD(stdout, mirror->qqtt);
    // dump_ABCD(stdout, mirror->qqts);
}

//! Set up an ABCD matrix for a beam splitter

/*!
 * \param bs_index index of beam splitter in bs_list
 *
 * \see Test_set_ABCD_beamsplitter()
 */
void set_ABCD_beamsplitter(int bs_index) {
    int j;
    int node1_index, node2_index, node3_index, node4_index;
    node_t node1, node2, node3, node4;
    double n1, n2;
    double co1, co2, si1, si2, coco;
    double ir[2] = {0};

    beamsplitter_t *bs;

    // sanity checks on input
    // beam splitter index should be in the range: 
    // 0 <= bs_index < inter.num_beamsplitters
    assert(bs_index >= 0);
    assert(bs_index < inter.num_beamsplitters);

    bs = &(inter.bs_list[bs_index]);

    node1_index = bs->node1_index;
    node2_index = bs->node2_index;
    node3_index = bs->node3_index;
    node4_index = bs->node4_index;

    node1 = inter.node_list[node1_index];
    node2 = inter.node_list[node2_index];
    node3 = inter.node_list[node3_index];
    node4 = inter.node_list[node4_index];

    n1 = n2 = init.n0;

    j = 0;
    if (NOT node1.gnd_node) {
        //node1 = inter.node_list[node1_index];
        ir[j++] = *node1.n;
    }

    if (NOT node2.gnd_node) {
        //node2 = inter.node_list[node2_index];
        ir[j++] = *node2.n;
    }

    int k;
    for (k = 0; k < j; k++) {
        if (ir[k] != ir[0]) {
            gerror("index of refraction not consistent at beamsplitter '%s'\n",
                    bs->name);
        }
    }

    if (j) {
        n1 = ir[0];
    }

    j = 0;
    if (NOT node3.gnd_node) {
        //node3 = inter.node_list[node3_index];
        ir[j++] = *node3.n;
    }

    if (NOT node4.gnd_node) {
        //node4 = inter.node_list[node4_index];
        ir[j++] = *node4.n;
    }

    for (k = 0; k < j; k++) {
        if (ir[k] != ir[0]) {
            gerror("index of refraction not consistent at beamsplitter '%s'\n",
                    bs->name);
        }
    }

    if (j) {
        n2 = ir[0];
    }

    si1 = sin(bs->alpha_1 * RAD);
    si2 = n1 / n2*si1;
    if (fabs(si2) > 1) { // fabs 290402
        gerror("total reflection at beamsplitter %s\n", bs->name);
    }
    bs->alpha_2 = DEG * asin(si2);

    co1 = cos(bs->alpha_1 * RAD);
    co2 = cos(bs->alpha_2 * RAD);

    if (inter.debug & 256) {
        fprintf(stdout, "Setting ABCD for bs %s\n", bs->name);
        fprintf(stdout, "%s: alpha %g , alpha2 %g\n", bs->name, bs->alpha_1, bs->alpha_2);
        fprintf(stdout, "n1 %g n2 %g c1 %g c2 %g si %g s2 %g\n", n1, n2, co1, co2, si1, si2);
    }

    if (co1 == 0.0 || co2 == 0.0) {
        gerror("beamsplitter '%s': alpha(%s) out of range\n", bs->name,
                double_form(bs->alpha_1));
    }

    coco = n2 * co2 - n1*co1;

    // setting transmission matrix
    // x = tangential plane, y = sagittal plane

    if (n1 == n2) {
        bs->qqt1t = bs->qqt2t = Unity;
        bs->qqt1s = bs->qqt2s = Unity;
    } else {
        if (bs->Rcx == 0.0) {
            bs->qqt1t = make_ABCD(co2 / co1, 0.0, 0.0, co1 / co2);
            bs->qqt2t = make_ABCD(co1 / co2, 0.0, 0.0, co2 / co1);
        } else {
            bs->qqt1t =
                    make_ABCD(co2 / co1, 0.0, coco / (bs->Rcx * co1 * co2), co1 / co2);
            bs->qqt2t =
                    make_ABCD(co1 / co2, 0.0, coco / (bs->Rcx * co1 * co2), co2 / co1);
        }

        if (bs->Rcy == 0.0) {
            bs->qqt1s = bs->qqt2s = Unity;
        } else {
            bs->qqt1s = bs->qqt2s =
                    make_ABCD(1.0, 0.0, coco / (bs->Rcy), 1.0); // adf 31/10/13 removed wrong denomnator for C element, bug 128
        }
    }

    // setting reflection matrix
    if (bs->Rcx == 0.0) {
        bs->qqr1t = bs->qqr2t = Unity;
    } else {
        // n1 added 040803
        bs->qqr1t = make_ABCD(1.0, 0.0, -2.0 * n1 / (bs->Rcx * co1), 1.0);
        // n2 added 231003
        bs->qqr2t = make_ABCD(1.0, 0.0, 2.0 * n2 / (bs->Rcx * co2), 1.0);
    }

    if (bs->Rcy == 0.0) {
        bs->qqr1s = bs->qqr2s = Unity;
    } else {
        // n1 added 040803
        bs->qqr1s = make_ABCD(1.0, 0.0, -2.0 * n1 * co1 / bs->Rcy, 1.0);
        // n2 added 231003
        bs->qqr2s = make_ABCD(1.0, 0.0, 2.0 * n2 * co2 / bs->Rcy, 1.0);
    }

    if (inter.debug & 256) {
        fprintf(stdout, "bs : %s ----------------\n", bs->name);
        fprintf(stdout, "  qqt1t\n");
        dump_ABCD(stdout, bs->qqt1t);
        fprintf(stdout, "  qqt1s\n");
        dump_ABCD(stdout, bs->qqt1s);
        fprintf(stdout, "  qqt2t\n");
        dump_ABCD(stdout, bs->qqt2t);
        fprintf(stdout, "  qqt2s\n");
        dump_ABCD(stdout, bs->qqt2s);

        fprintf(stdout, "  qqr1t\n");
        dump_ABCD(stdout, bs->qqr1t);
        fprintf(stdout, "  qqr1s\n");
        dump_ABCD(stdout, bs->qqr1s);
        fprintf(stdout, "  qqr2t\n");
        dump_ABCD(stdout, bs->qqr2t);
        fprintf(stdout, "  qqr2s\n");
        dump_ABCD(stdout, bs->qqr2s);

        // and send the output to the log file
        fprintf(fp_log, "bs : %s ----------------\n", bs->name);
        fprintf(fp_log, "  qqt1t\n");
        dump_ABCD(fp_log, bs->qqt1t);
        fprintf(fp_log, "  qqt1s\n");
        dump_ABCD(fp_log, bs->qqt1s);
        fprintf(fp_log, "  qqt2t\n");
        dump_ABCD(fp_log, bs->qqt2t);
        fprintf(fp_log, "  qqt2s\n");
        dump_ABCD(fp_log, bs->qqt2s);

        fprintf(fp_log, "  qqr1t\n");
        dump_ABCD(fp_log, bs->qqr1t);
        fprintf(fp_log, "  qqr1s\n");
        dump_ABCD(fp_log, bs->qqr1s);
        fprintf(fp_log, "  qqr2t\n");
        dump_ABCD(fp_log, bs->qqr2t);
        fprintf(fp_log, "  qqr2s\n");
        dump_ABCD(fp_log, bs->qqr2s);
    }
}

//! Set up an ABCD matrix for a free space element

/*!
 * \param space_index index of space in space list
 *
 * \see Test_set_ABCD_space()
 */
void set_ABCD_space(int space_index) {
    space_t *space;
    node_t node1, node2;

    // sanity checks on input
    // space index should be in the range: 0 <= space_index < inter.num_spaces
    assert(space_index >= 0);
    assert(space_index < inter.num_spaces);

    space = &(inter.space_list[space_index]);

    node1 = inter.node_list[space->node1_index];
    node2 = inter.node_list[space->node2_index];

    if (NOT node1.gnd_node &&
            NOT node2.gnd_node) {
        space->qq = make_ABCD(1.0, space->L / space->n, 0.0, 1.0);
        //    space->qq = make_ABCD(1.0, space->L, 0.0, 1.0);

        if (inter.debug & 256) {
            fprintf(stdout, "space : %s ----------------\n", space->name);
            fprintf(stdout, "  qq\n");
            dump_ABCD(stdout, space->qq);

            // and send the output to the log file
            fprintf(fp_log, "space : %s ----------------\n", space->name);
            fprintf(fp_log, "  qq\n");
            dump_ABCD(fp_log, space->qq);
        }
    }
}

//! Set up an ABCD matrix for a lens 

/*!
 * \param lens_index index of lens in lens list
 *
 * \see Test_set_ABCD_lens()
 */
void set_ABCD_lens(int lens_index) {
    lens_t *lens;
    node_t node1, node2;

    // sanity checks on input
    // lens index should be in range: 0 <= lens_index < inter.num_lenses
    assert(lens_index >= 0);
    assert(lens_index < inter.num_lenses);

    lens = &(inter.lens_list[lens_index]);

    node1 = inter.node_list[lens->node1_index];
    node2 = inter.node_list[lens->node2_index];


    // if focal length=0, ignore lens
    double f = lens->f_of_D;
    // if we're dealing with optical power, invert it
    if (!lens->is_focal_length) {
        f = 1.0 / f;
    }

    if (f == 0.0) {
        warn("focal length is zero, lens '%s' ignored\n", lens->name);
        f = -1.0e23;
    }

    if (NOT node1.gnd_node &&
            NOT node2.gnd_node) {
        lens->qq = make_ABCD(1.0, 0.0, -1.0 / f, 1.0);
    }
}

//! Set up an ABCD matrix for a grating

/*!
 * \param grating_index index of grating in grating list
 *
 * \see Test_set_ABCD_grating()
 */
void set_ABCD_grating(int grating_index) {
    double ca;
    double reRs, reRt; // refers to 1/Rs and 1/Rt
    double M;
    grating_t *grating;

    // sanity checks on input
    // grating index should be in range: 0 <= grating_index < inter.num_gratings
    assert(grating_index >= 0);
    assert(grating_index < inter.num_gratings);

    grating = &(inter.grating_list[grating_index]);

    ca = cos(RAD * grating->alpha);

    // zeroth order is equal for all grating types:
    if (grating->Rcx == 0.0) {
        reRt = 0.0;
    } else {
        reRt = 1.0 / (grating->Rcx * ca);
    }
    if (grating->Rcy == 0.0) {
        reRs = 0.0;
    } else {
        reRs = ca / grating->Rcy;
    }
    M = 1;
    grating->qqt[0][1] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
    grating->qqt[1][0] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
    grating->qqs[0][1] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);
    grating->qqs[1][0] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);

    grating->qqt[0][0] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
    grating->qqt[1][1] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
    grating->qqs[0][0] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);
    grating->qqs[1][1] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);


    switch (grating->num_of_ports) {
        case 2:
            break;
        case 3:
            if (grating->Rcx == 0.0) {
                reRt = 0.0;
            } else {
                reRt = (1.0 + ca) / (2.0 * grating->Rcx * ca);
            }
            if (grating->Rcy == 0.0) {
                reRs = 0.0;
            } else {
                reRs = (1.0 + ca) / (2.0 * grating->Rcy);
            }
            M = 1 / ca;
            grating->qqt[0][2] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
            grating->qqs[0][2] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);
            M = ca;
            grating->qqt[2][0] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
            grating->qqs[2][0] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);

            M = 1 / ca;
            grating->qqt[1][2] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
            grating->qqs[1][2] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);
            M = ca;
            grating->qqt[2][1] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
            grating->qqs[2][1] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);

            if (grating->Rcx == 0.0) {
                reRt = 0.0;
            } else {
                reRt = 1.0 / grating->Rcx;
            }
            if (grating->Rcy == 0.0) {
                reRs = 0.0;
            } else {
                reRs = 1.0 / grating->Rcy;
            }
            M = 1.0;
            grating->qqt[2][2] = make_ABCD(M, 0.0, -2.0 * reRt, 1.0 / M);
            grating->qqs[2][2] = make_ABCD(1.0, 0.0, -2.0 * reRs, 1.0);


            break;
        case 4:

            break;
        default:
            bug_error("incorrect number of ports");
    }
}

//! Set ABCD matrices for all components

/*!
 *
 * \todo refactor for readability
 *
 * \see Test_set_ABCDs()
 */
void set_ABCDs(void) {
    // mirrors
    int mirror_index;
    for (mirror_index = 0; mirror_index < inter.num_mirrors; mirror_index++) {
        set_ABCD_mirror(mirror_index);
    }

    // beamsplitters
    int bs_index;
    for (bs_index = 0; bs_index < inter.num_beamsplitters; bs_index++) {
        set_ABCD_beamsplitter(bs_index);
    }

    //gratings
    int grating_index;
    for (grating_index = 0; grating_index < inter.num_gratings; grating_index++) {
        set_ABCD_grating(grating_index);
    }

    // modulators
    int modulator_index;
    for (modulator_index = 0;
            modulator_index < inter.num_modulators;
            modulator_index++) {
        modulator_t *modulator;
        modulator = &(inter.modulator_list[modulator_index]);
        modulator->qq = Unity;
    }

    // spaces
    int space_index;
    for (space_index = 0; space_index < inter.num_spaces; space_index++) {
        set_ABCD_space(space_index);
    }

    // lenses
    int lens_index;
    for (lens_index = 0; lens_index < inter.num_lenses; lens_index++) {
        set_ABCD_lens(lens_index);
    }

    // diodes
    int diode_index;
    for (diode_index = 0; diode_index < inter.num_diodes; diode_index++) {
        diode_t *diode;
        diode = &(inter.diode_list[diode_index]);
        diode->qq = Unity;
    }

    // direction BS
    int dbs_index;
    for (dbs_index = 0; dbs_index < inter.num_dbss; dbs_index++) {
        dbs_t *dbs;
        dbs = &(inter.dbs_list[dbs_index]);
        dbs->qq = Unity;
    }
}

//! Return number of equations for given order of used TEM modes

/*!
 * \param modes the number of TEM modes
 *
 * \see Test_num_eqns_for()
 */
int num_eqns_for(int modes) {
    // modes should be >= 0
    assert(modes >= 0);

    //sum: 1+2+3+4+...+modes+1
    return ((int) ((modes + 1) * (modes + 2) / 2.0));
}

//! Compute the cavity parameters

/*!
 *
 * \todo refactor for readability
 * \todo break up into smaller routines!!!
 *
 * \see Test_get_cavity()
 */
void compute_cavity_params(void) {
    ABCD_t eigen_x, eigen1_x, eigen2_x, temp_x;
    ABCD_t eigen_y, eigen1_y, eigen2_y, temp_y;

    double nr1, nr2;

    complex_t q_x, q_y, q_xt, q_yt;

    if (inter.trace & 2 || inter.trace & 128) {
        message("--- cavity tracing\n");
    }

    int cavity_index;
    for (cavity_index = 0; cavity_index < inter.num_cavities; cavity_index++) {
        cavity_t *cavity;
        cavity = &(inter.cavity_list[cavity_index]);
        cavity->stable = 0;
        int c1_index = 0;
        int c2_index = 0;
        // initialise matrices
        eigen_x = eigen1_x = eigen2_x = temp_x = Unity;
        eigen_y = eigen1_y = eigen2_y = temp_y = Unity;

        int complete;
        int complete2;
        if (get_component_type(cavity->component1_index) == BEAMSPLITTER) {
            complete = trace_cavity(&eigen1_x, &eigen1_y,
                    cavity->component1_index, cavity->node1_index,
                    cavity->component2_index, cavity->node2_index,
                    &c1_index, vlocal.trace_c1);
            int err_x = component_matrix(&temp_x,
                    cavity->component1_index, cavity->node2_index,
                    cavity->node1_index, TANGENTIAL);
            int err_y = component_matrix(&temp_y,
                    cavity->component1_index, cavity->node2_index,
                    cavity->node1_index, SAGITTAL);
            if (!err_x && !err_y) {
                eigen_x = multiply_ABCD(eigen1_x, inv_ABCD(temp_x));
                eigen_y = multiply_ABCD(eigen1_y, inv_ABCD(temp_y));
            } else {
                bug_error("errxy0");
            }
            complete = complete & (!err_x) & (!err_y);
        } else {
            complete = trace_cavity(&eigen1_x, &eigen1_y,
                    cavity->component1_index, cavity->node1_index,
                    cavity->component2_index, cavity->node2_index,
                    &c1_index, vlocal.trace_c1);
            if (inter.debug & 256) {
                fprintf(stdout, "eigen1 (x, y):\n");
                dump_ABCD(stdout, eigen1_x);
                dump_ABCD(stdout, eigen1_y);
            }
            complete2 = trace_cavity(&eigen2_x, &eigen2_y,
                    cavity->component2_index, cavity->node2_index,
                    cavity->component1_index, cavity->node1_index,
                    &c2_index, vlocal.trace_c2);

            if (inter.debug & 256) {
                fprintf(stdout, "eigen2 (x, y):\n");
                dump_ABCD(stdout, eigen2_x);
                dump_ABCD(stdout, eigen2_y);
            }

            int err_x = component_matrix(&temp_x,
                    cavity->component2_index, cavity->node2_index,
                    cavity->node2_index, TANGENTIAL);
            int err_y = component_matrix(&temp_y,
                    cavity->component2_index, cavity->node2_index,
                    cavity->node2_index, SAGITTAL);

            if (inter.debug & 256) {
                fprintf(stdout, "temp2 (x, y):\n");
                dump_ABCD(stdout, temp_x);
                dump_ABCD(stdout, temp_y);
            }

            if (!err_x && !err_y) {
                eigen_x = multiply_ABCD(multiply_ABCD(eigen1_x, inv_ABCD(temp_x)), eigen2_x);
                eigen_y = multiply_ABCD(multiply_ABCD(eigen1_y, inv_ABCD(temp_y)), eigen2_y);
            } else {
                bug_error("errxy1");
            }

            complete = complete & complete2 & (!err_x) & (!err_y);
            err_x = component_matrix(&temp_x,
                    cavity->component1_index, cavity->node1_index,
                    cavity->node1_index, TANGENTIAL);
            err_y = component_matrix(&temp_y,
                    cavity->component1_index, cavity->node1_index,
                    cavity->node1_index, SAGITTAL);

            if (inter.debug & 256) {
                fprintf(stdout, "temp1 (x, y):\n");
                dump_ABCD(stdout, temp_x);
                dump_ABCD(stdout, temp_y);
            }

            if (!err_x && !err_y) {
                eigen_x = multiply_ABCD(eigen_x, inv_ABCD(temp_x));
                eigen_y = multiply_ABCD(eigen_y, inv_ABCD(temp_y));
            } else {
                bug_error("errxy2");
            }
            complete = complete & (!err_x) & (!err_y);
        }

        int m = 0;
        if (complete) {
            // using inversed matrix to reconstruct right order 
            // AB^(-1)=B^(-1) A^(-1)
            eigen_x = inv_ABCD(eigen_x);
            eigen_y = inv_ABCD(eigen_y);

            cavity->Mx = eigen_x;
            cavity->My = eigen_y;

            int m_x = check_ABCD(eigen_x, &cavity->stability_x);
            int m_y = check_ABCD(eigen_y, &cavity->stability_y);

            if (inter.debug & 128) {
                fprintf(stdout, "%s eigenmatrix (x):\n ", cavity->name);
                dump_ABCD(stdout, eigen_x);
                fprintf(stdout, "eigenvalue mx=%.15g\n ", (eigen_x.A + eigen_x.D) / 2.0);
                fprintf(stdout, " (y):\n ");
                dump_ABCD(stdout, eigen_y);
                fprintf(stdout, "eigenvalue my=%.15g\n ", (eigen_y.A + eigen_y.D) / 2.0);

                // dump the same info to the log file
                fprintf(fp_log, "%s eigenmatrix (x):\n ", cavity->name);
                dump_ABCD(fp_log, eigen_x);
                fprintf(fp_log, "eigenvalue mx=%.15g\n ", (eigen_x.A + eigen_x.D) / 2.0);
                fprintf(fp_log, " (y):\n ");
                dump_ABCD(fp_log, eigen_y);
                fprintf(fp_log, "eigenvalue my=%.15g\n ", (eigen_y.A + eigen_y.D) / 2.0);
            }

            m = m_x | m_y;

            if (inter.trace & 2 || inter.trace & 128) {
                message("cavity %s:\n", cavity->name);
            }

            // Adding final node to list
            if (get_component_type(cavity->component1_index) == BEAMSPLITTER) {
                vlocal.trace_c1[c1_index++] = cavity->node2_index;
            }

            switch (m) {
                case 0: // cavity is stable!
                    cavity->stable = 1;
                    nr1 = *inter.node_list[cavity->node1_index].n;
                    q_x = q_cavity(eigen_x, nr1);
                    q_y = q_cavity(eigen_y, nr1);

                    if (inter.trace & 2) {
                        message(" cavity is stable! Eigenvalues:\n");
                        if (ceq(q_x, q_y)) {
                            message(" q=%s, w0=%sm z=%sm m=%g\n", complex_form(q_x),
                                    xdouble_form(w0_size(q_x, nr1)), xdouble_form(z_q(q_x)), cavity->stability_x);
                        } else {
                            message(" qx=%s, w0x=%sm zx=%sm mx=%g\n", complex_form(q_x),
                                    xdouble_form(w0_size(q_x, nr1)), xdouble_form(z_q(q_x)), cavity->stability_x);
                            message(" qy=%s, w0y=%sm zy=%sm my=%g\n", complex_form(q_y),
                                    xdouble_form(w0_size(q_y, nr1)), xdouble_form(z_q(q_y)), cavity->stability_y);
                        }
                    }

                    if (inter.trace & 128) {
                        message(" found the cavity nodes: ");
                        int j;
                        for (j = 0; j < c1_index; j++) {
                            message("%s(%d) ", get_node_name(vlocal.trace_c1[j]),
                                    vlocal.trace_c1[j]);
                        }

                        if (get_component_type(cavity->component1_index) == MIRROR) {
                            message("\n ... and back: ");
                            int j;
                            for (j = 0; j < c2_index; j++) {
                                message("%s(%d) ", get_node_name(vlocal.trace_c2[j]),
                                        vlocal.trace_c2[j]);
                            }
                        }
                        message("\n");
                    }
                    break;
                case 1:
                    if (inter.trace & 2) {
                        warn(" WARNING!\n"
                                " cavity is critical, cannot derive beam parameters\n");
                    } else {
                        warn("cavity %s is critical, skipping it\n", cavity->name);
                    }
                    break;
                default: // 2 or 3
                    if (inter.trace & 2) {
                        warn(" WARNING!\n"
                                " cavity is unstable, cannot derive beam parameters\n");
                    } else {
                        warn("cavity %s is unstable (m = %g), skipping it\n", cavity->name, cavity->stability_x);
                    }
                    break;
            }
        } else {
            gerror("could not trace through cavity %s\n", cavity->name);
        }

        // set q values for cavity
        if (get_component_type(cavity->component1_index) == MIRROR) {
            //setting turning node as last node for setting q values
            vlocal.trace_c1[c1_index++] = vlocal.trace_c2[0];
        }

        if (!m) { // set q values if cavity is stable (1)
            set_q(cavity->node1_index, cavity->component1_index, q_x, q_y);
            cavity->qx = q_x;
            cavity->qy = q_y;
            cavity->n = *inter.node_list[cavity->node1_index].n;
        }

        double cavity_power = 1.0;
        double cavity_length = 0.0;
        int type = 0;
        int j;
        for (j = 0; j < c1_index - 1; j++) {
            if (j + 2 > c1_index) { // wrong number of nodes
                gerror("cavity %s: cavity is broken\n", cavity->name);
            }
            int node1_index = vlocal.trace_c1[j];
            int node2_index = vlocal.trace_c1[j + 1];
            if (node1_index == node2_index) {
                gerror("cavity %s: cavity is broken (1)\n", cavity->name);
            }
            int component1_index;
            int component2_index;
            int component3_index;
            int component4_index;
            which_components(node1_index, &component1_index, &component2_index);
            which_components(node2_index, &component3_index, &component4_index);

            //      printf("c1 %s c2 %s c3 %s c4 %s\n",
            //             get_component_name(component1_index), get_component_name(component2_index), get_component_name(component3_index), get_component_name(component4_index)); 
            int component0_index = 0; // silence compiler warning
            if ((component1_index == component3_index ||
                    component1_index == component4_index)
                    && component1_index != NOT_FOUND) {
                component0_index = component1_index;
            } else if ((component2_index == component3_index ||
                    component2_index == component4_index)
                    && component2_index != NOT_FOUND) {
                component0_index = component2_index;
            } else { // cannot find component
                gerror("cavity %s: cavity is broken (2)\n", cavity->name);
            }

            // getting type and relative comp number for calculating
            // FSR and Finesse of cavity 
            int rcomp = component0_index;
            type = get_component_type_decriment_index(&rcomp);

            switch (type) {
                case MIRROR:
                    if ((node1_index == inter.mirror_list[rcomp].node1_index &&
                            node2_index == inter.mirror_list[rcomp].node2_index) ||
                            (node2_index == inter.mirror_list[rcomp].node1_index &&
                            node1_index == inter.mirror_list[rcomp].node2_index)) {
                        cavity_power *= inter.mirror_list[rcomp].T;
                    } else {
                        cavity_power *= inter.mirror_list[rcomp].R;
                    }
                    break;
                case BEAMSPLITTER:
                    if (node1_index == inter.bs_list[rcomp].node1_index &&
                            node2_index == inter.bs_list[rcomp].node2_index) {
                        cavity_power *= inter.bs_list[rcomp].R;
                    } else if (node1_index == inter.bs_list[rcomp].node1_index &&
                            node2_index == inter.bs_list[rcomp].node3_index) {
                        cavity_power *= inter.bs_list[rcomp].T;
                    } else if (node1_index == inter.bs_list[rcomp].node2_index &&
                            node2_index == inter.bs_list[rcomp].node1_index) {
                        cavity_power *= inter.bs_list[rcomp].R;
                    } else if (node1_index == inter.bs_list[rcomp].node2_index &&
                            node2_index == inter.bs_list[rcomp].node4_index) {
                        cavity_power *= inter.bs_list[rcomp].T;
                    } else if (node1_index == inter.bs_list[rcomp].node3_index &&
                            node2_index == inter.bs_list[rcomp].node4_index) {
                        cavity_power *= inter.bs_list[rcomp].R;
                    } else if (node1_index == inter.bs_list[rcomp].node3_index &&
                            node2_index == inter.bs_list[rcomp].node1_index) {
                        cavity_power *= inter.bs_list[rcomp].T;
                    } else if (node1_index == inter.bs_list[rcomp].node4_index &&
                            node2_index == inter.bs_list[rcomp].node3_index) {
                        cavity_power *= inter.bs_list[rcomp].R;
                    } else if (node1_index == inter.bs_list[rcomp].node4_index &&
                            node2_index == inter.bs_list[rcomp].node2_index) {
                        cavity_power *= inter.bs_list[rcomp].T;
                    } else {
                        bug_error("cavity FSR: wrong node at BS");
                    }
                    break;
                case DIODE:
                    if (node1_index == inter.diode_list[rcomp].node2_index) {
                        // e.g. 20db loss is a factor of 100 in power
                        cavity_power *= pow(10., -inter.diode_list[rcomp].S / 10.0);
                    }
                    break;
                case SPACE:
                    cavity_length += inter.space_list[rcomp].L * inter.space_list[rcomp].n;
                    if (inter.debug & 256) {
                        warn("current cav length %g (added %g*%g from space %s)\n", cavity_length, inter.space_list[rcomp].L, inter.space_list[rcomp].n, inter.space_list[rcomp].name);
                    }
                    break;
                default:
                    break;
            }

            if (inter.debug & 256) {
                message("get_cavity: comp %s \n", get_component_name(component0_index));
                message("cavity loss: %.10g \n", 1.0 - cavity_power);
            }

            if (!m) { // set q values if cavity is stable (2)
                if (!inter.node_list[node1_index].q_is_set) {
                    bug_error("q not set");
                }

                // q -> q(-z)=-q* if necessary (reverse beam direction)
                if (inter.node_list[node1_index].component_index == component0_index) {
                    if (inter.debug & 256) {
                        message("get_cavity: reverse direction\n");
                    }
                    q_x = cminus(cconj(inter.node_list[node1_index].qx));
                    q_y = cminus(cconj(inter.node_list[node1_index].qy));
                } else {
                    q_x = inter.node_list[node1_index].qx;
                    q_y = inter.node_list[node1_index].qy;
                }

                ABCD_t transform;
                int err = component_matrix(&transform, component0_index,
                        node1_index, node2_index, TANGENTIAL);

                if (!err) {
                    nr1 = *inter.node_list[node1_index].n;
                    nr2 = *inter.node_list[node2_index].n;
                    q_xt = q1_q2(transform, q_x, nr1, nr2);
                    err = component_matrix(&transform, component0_index,
                            node1_index, node2_index, SAGITTAL);
                    q_yt = q1_q2(transform, q_y, nr1, nr2);
                    set_q(node2_index, component0_index, q_xt, q_yt);
                } else if (inter.debug & 256) {
                    message("get_cavity: err -> %s %s\n", get_node_name(node1_index),
                            get_node_name(node2_index));
                }
            }
        }

        // final computation of FSR and Finesse of cavity
        int rcomp;

        if (get_component_type(cavity->component1_index) == BEAMSPLITTER) {
            rcomp = get_component_index(cavity->component1_index);
            if (rcomp > inter.num_beamsplitters) {
                bug_error("cavity FSR: wrong bs start comp");
            }
            cavity_power *= inter.bs_list[rcomp].R;
        } else {
            cavity_length *= 2.0;
            cavity_power *= cavity_power;
            rcomp = get_component_index(cavity->component1_index);
            if (rcomp > inter.num_mirrors) {
                bug_error("cavity FSR: wrong m start comp");
            }

            cavity_power *= inter.mirror_list[rcomp].R;
            rcomp = get_component_index(cavity->component2_index);
            if (rcomp > inter.num_mirrors) {
                bug_error("cavity FSR: wrong m stop comp");
            }
            cavity_power *= inter.mirror_list[rcomp].R;
        }

        cavity->finesse = finesse(1.0 - cavity_power);
        cavity->loss = 1.0 - cavity_power;
        cavity->length = cavity_length;
        cavity->FSR = init.clight / cavity_length;
        cavity->FWHM = init.clight / cavity_length / finesse(1.0 - cavity_power);
        cavity->pole = 0.5 * init.clight / cavity_length / finesse(1.0 - cavity_power);
        cavity->rt_gouy_x = 2 * acos(sign(cavity->Mx.B) * sqrt((cavity->stability_x + 1) / 2));
        cavity->rt_gouy_y = 2 * acos(sign(cavity->My.B) * sqrt((cavity->stability_y + 1) / 2));

        if (cavity->rt_gouy_x > PI) cavity->rt_gouy_x -= TWOPI;
        if (cavity->rt_gouy_y > PI) cavity->rt_gouy_y -= TWOPI;

        if (inter.trace & 2) {
            message(" finesse : %g, round-trip power loss: %.8g [\%/100]\n",
                    cavity->finesse, cavity->loss);
            message(" opt. length: %sm, FSR: %sHz, m: %s\n",
                    xdouble_form(cavity->length), xdouble_form(cavity->FSR), xdouble_form(cavity->stability_x));
            message(" FWHM: %sHz (pole: %sHz)\n",
                    xdouble_form(cavity->FWHM),
                    xdouble_form(cavity->pole));
            
            message(" (x) A: %.8g B: %.8g C: %.8g D: %.8g\n", eigen_x.A, eigen_x.B, eigen_x.C, eigen_x.D);
            message(" (y) A: %.8g B: %.8g C: %.8g D: %.8g\n", eigen_y.A, eigen_y.B, eigen_y.C, eigen_y.D);
                    
            if (cavity->stable) {
                if (cavity->stability_x == cavity->stability_y) {
                    message(" RT Gouy: %s deg (mode sep.: %sHz)\n",
                            xdouble_form(cavity->rt_gouy_x * DEG),
                            xdouble_form(cavity->rt_gouy_x / TWOPI * cavity->FSR));
                } else {
                    message(" (x) RT Gouy: %s deg, mode sep. %sHz\n",
                            xdouble_form(cavity->rt_gouy_x * DEG),
                            xdouble_form(cavity->rt_gouy_x / TWOPI * cavity->FSR));
                    message(" (y) RT Gouy: %s deg, mode sep. %sHz\n",
                            xdouble_form(cavity->rt_gouy_y * DEG),
                            xdouble_form(cavity->rt_gouy_y / TWOPI * cavity->FSR));
                }
            }


            /*
            message(" finesse : %g, round-trip power loss: %g\n",
                            finesse(1.0 - cavity_power), 1.0 - cavity_power);
            message(" opt. length: %sm, FSR: %sHz\n",
                            xdouble_form(cavity_length), xdouble_form(init.clight / cavity_length));
            message(" FWHM: %sHz (pole: %sHz)\n",
                            xdouble_form(init.clight / cavity_length / finesse(1.0 - cavity_power)),
                            xdouble_form(0.5* init.clight / cavity_length / finesse(1.0 - cavity_power)));
             */
        }
    }
    if (inter.trace & 2) {
        message("\n");
    }
}

//! Trace the cavity (find a path through the nodal net of interferometer)

/*!
 * \param Mlocal_x eigenmatrix of cavity in x-plane
 * \param Mlocal_y eigenmatrix of cavity in y-plane
 * \param component1_index index of first component in trace
 * \param node1_index first node index in trace
 * \param component2_index index of second component in trace
 * \param node2_index second node index in trace
 * \param index index of the current node
 * \param traced array of already found nodes
 *
 * \return an integer indicating if cavity tracing is finished or not
 *
 * \see Test_trace_cavity()
 */
int trace_cavity(ABCD_t *Mlocal_x, ABCD_t *Mlocal_y,
        int component1_index, int node1_index,
        int component2_index, int node2_index,
        int *index, int *traced) {
    int i;
    int index_local;
    int c1, c2, component;
    int node2t, comp2t;
    int node_indices[4] = {0};
    int num_nodes;
    int test;
    int finished;
    int err_x, err_y;

    ABCD_t transform_x, temp_x;
    ABCD_t transform_y, temp_y;

    // sanity checks on inputs
    // component indices should be in the range: 
    // 0 <= component_index < inter.num_components
    assert(component1_index >= 0);
    assert(component2_index >= 0);
    assert(component1_index < inter.num_components);
    assert(component2_index < inter.num_components);
    // node indices should be in the range:
    // 0 <= node_index < inter.num_nodes
    assert(node1_index >= 0);
    assert(node2_index >= 0);
    assert(node1_index < inter.num_nodes);
    assert(node2_index < inter.num_nodes);
    // index should be in the range:
    // 0 <= index < inter.num_nodes
    assert(*index >= 0);
    assert(*index < inter.num_nodes);


    if (inter.debug & 256) {
        message("trace_cavity(): ------- starting new trace\n");
    }

    finished = 0;

    traced[(*index)++] = node1_index;

    comp2t = component2_index;
    node2t = node2_index;

    if (inter.debug & 256) {
        message("trace_cavity(): start comp %s, end comp %s\n",
                get_component_name(component1_index), get_component_name(component2_index));
    }

    //!< \todo c1 and c2 need to be called component1_index and component2_index
    which_components(node1_index, &c1, &c2);

    if (c2 == component1_index) {
        component = c1;
    } else {
        component = c2;
    }

    if (inter.debug & 256) {
        message("trace_cavity(): moving to component %s\n",
                get_component_name(component));
    }

    if (component != NOT_FOUND) {
        which_nodes(component, node1_index, &num_nodes, node_indices);

        i = 0;
        while (i < num_nodes && NOT finished) {
            if (inter.debug & 256) {
                message("trace_cavity(): (%d) node %s\n", i,
                        get_node_name(node_indices[i]));
            }

            if (node_indices[i] == node2t) {
                err_x = component_matrix(&transform_x, component,
                        node1_index, node_indices[i], TANGENTIAL);
                err_y = component_matrix(&transform_y, component,
                        node1_index, node_indices[i], SAGITTAL);

                if (NOT err_x && NOT err_y) {
                    // using inversed matrix to reconstruct right order later
                    // AB^(-1)=B^(-1) A^(-1)
                    temp_x = multiply_ABCD(*Mlocal_x, inv_ABCD(transform_x));
                    temp_y = multiply_ABCD(*Mlocal_y, inv_ABCD(transform_y));
                    finished = 1;
                    *Mlocal_x = temp_x;
                    *Mlocal_y = temp_y;

                    if (inter.debug & 256) {
                        message("trace_cavity(): finished!\n");
                    }
                } else {
                    bug_error("errxy");
                }
            } else {
                test = 0;
                int j;
                for (j = 0; j < (*index); j++) {
                    if (node_indices[i] == traced[j]) {
                        test = 1;
                    }
                }
                if (NOT test) { // found new node
                    if (inter.debug & 256) {
                        message("trace_cavity(): new node  %s (index %d)\n",
                                get_node_name(node_indices[i]), node_indices[i]);
                    }

                    err_x = component_matrix(&transform_x, component, node1_index, node_indices[i], TANGENTIAL);
                    err_y = component_matrix(&transform_y, component, node1_index, node_indices[i], SAGITTAL);

                    if (NOT err_x && NOT err_y) {
                        temp_x = multiply_ABCD(*Mlocal_x, inv_ABCD(transform_x));
                        temp_y = multiply_ABCD(*Mlocal_y, inv_ABCD(transform_y));

                        if (node_indices[i] == node2t) {
                            finished = 1;
                        } else {
                            index_local = *index;
                            finished =
                                    trace_cavity(&temp_x, &temp_y, component,
                                    node_indices[i], comp2t, node2t, index, traced);

                            if (NOT finished) {
                                *index = index_local;
                            }
                        }

                        if (finished) {
                            *Mlocal_x = temp_x;
                            *Mlocal_y = temp_y;
                        }
                    } else {
                        bug_error("errxy3 %i %i", err_x, err_y);
                    }
                }
            }
            i++;
        }
    }

    return (finished);
}

//! Set up the trace beam

/*!
 * \param startnode the node at which to start the trace
 *
 * \todo startnode -> start_node_index
 *
 * \see Test_trace_beam()
 */
void trace_beam(int startnode) {
    int j, component1_index, component2_index;
    // sanity checks on input
    // start node should be in the range: 0 <= startnode < inter.num_nodes
    assert(startnode >= 0);
    assert(startnode < inter.num_nodes);

    n_index = 0;
    j = 0;

    if (inter.debug & 32) {
        message("-- Start tracing beam :\n");
    }

    which_components(startnode, &component1_index, &component2_index);

    if (component1_index != NOT_FOUND) {
        set_qbase(component1_index);
    }

    if (component2_index != NOT_FOUND) {
        set_qbase(component2_index);
    }

    if (component1_index != NOT_FOUND) {
        tracing(startnode, component1_index);
    }

    if (component2_index != NOT_FOUND) {
        tracing(startnode, component2_index);
    }

    if (inter.debug & 32) {
        message("-- ... tracing complete\n");
    }

    // with new gnd_node implementation, the dumps are now counted as normal
    // nodes, hence the inter.num_nodes variable will be greater than in the
    // previous implementation.  So, now need to check if the number of nodes
    // traced through PLUS the number of dumps is equal to the total number of
    // nodes, if not, then barf.
    if ((n_index + inter.num_dump_nodes) != inter.num_nodes) {
        fprintf(stderr,
                "Interferometer may be broken (%i/%i);"
                " no parameters were set for the\nfollowing nodes:\n", n_index + inter.num_dump_nodes, inter.num_nodes);

        int i;
        bool missing = false;

        for (i = 0; i < inter.num_nodes; i++) {
            if ((NOT inter.node_list[i].q_is_set) && (NOT inter.node_list[i].gnd_node)) {
                fprintf(stderr, "%s ", inter.node_list[i].name);
                missing = true;
                j++;

                if (j > 6) {
                    j = 0;
                    fprintf(stderr, "\n");
                }
            }
        }

        fprintf(stderr, "\n");

        if (missing) {
            gerror("Could not completely trace beam.\n");
        }
    }
}

//! Perform the full trace of the interferometer

/*!
 * \param startnode the node at which to start tracing
 * \param start_comp the component at which to start tracing
 *
 * \todo startnode -> start_node_index
 * \todo start_comp -> start_component_index
 *
 * \see Test_tracing()
 */
void tracing(int startnode, int start_comp) {
    int component1_index, component2_index, component_index;
    int node_indices[4] = {0};
    int num_nodes;
    int test;
    int testqset;

    // sanity checks on inputs
    // start node should be in the range: 0 <= startnode < inter.num_nodes
    assert(startnode >= 0);
    assert(startnode < inter.num_nodes);
    // start_comp should be in the range:
    // 0 <= start_comp < inter.num_components
    assert(start_comp >= 0);
    assert(start_comp < inter.num_components);

    which_components(startnode, &component1_index, &component2_index);

    //warn("%s\n", get_component_name(component1_index));
    //warn("%s\n", get_component_name(component2_index));

    if (component2_index == start_comp) {
        component_index = component1_index;
    } else {
        component_index = component2_index;
    }

    test = 0;
    int i;
    for (i = 0; i < n_index; i++) {
        if (vlocal.trace_n[0][i] == startnode) {
            test = 1;
        }
    }

    if (!test) {
        vlocal.trace_n[0][n_index] = startnode;
        vlocal.trace_n[1][n_index] = start_comp;
        vlocal.trace_n[2][n_index] = component_index;
        n_index++;
    }

    if (component_index != NOT_FOUND) {
        which_nodes(component_index, GND_NODE, &num_nodes, node_indices);

        if (num_nodes) { // new nodes
            testqset = 0;
            int i;
            for (i = 0; i < num_nodes; i++) {
                if (inter.node_list[node_indices[i]].q_is_set) {
                    testqset++;
                }
            }
            if (testqset == 0) {
                bug_error("missing beam parameter");
            }

            // setting new q values around new component
            set_qbase(component_index);

            for (i = 0; i < num_nodes; i++) {
                if (inter.node_list[node_indices[i]].gnd_node) {
                    test = 1;
                } else {
                    test = 0;
                }

                int j;
                for (j = 0; j < n_index; j++) {
                    if (node_indices[i] == vlocal.trace_n[0][j]) {
                        test++;
                        if (inter.debug & 256) {
                            message("tracing: node %s(%d) already found as number %d\n",
                                    get_node_name(node_indices[i]), node_indices[i], j);
                        }
                    }
                }
                if (NOT test) { // found new node
                    if (inter.debug & 256) {
                        message("tracing: New node %s(%d)\n",
                                get_node_name(node_indices[i]), node_indices[i]);
                    }

                    // recursively look for next nodes/components
                    tracing(node_indices[i], component_index);
                }
            }
        }
    }

}

//! Set up the component matrix

/*!
 * \param matrix the matrix to return
 * \param component_index component index for which the matrix should be set up
 * \param node1_index component node index
 * \param node2_index component node index
 * \param tan_sag flag: determines if the return matrix is in sagittal or
 * tangential plane
 *
 * \return the success status of the routine: 1 failure, 0 success.
 *
 * \see Test_component_matrix()
 */
int component_matrix(ABCD_t *matrix, int component_index,
        int node1_index, int node2_index, int tan_sag) {
    int comp_type;
    mirror_t *mirror;
    space_t *space;
    beamsplitter_t *bs;
    modulator_t *modulator;
    diode_t *diode;
    lens_t *lens;
    grating_t *grating;
    dbs_t *dbs;
    
    comp_type = get_component_type_decriment_index(&component_index);

    *matrix = Unity;

    // sanity checks on inputs
    // component_index should be in the range:
    // 0 <= component_index < inter.num_components
    assert(component_index >= 0);
    assert(component_index < inter.num_components);
    // node[1-2]_index should be in the range:
    // 0 <= node[1-2]_index < inter.num_nodes
    assert(node1_index >= 0);
    assert(node1_index < inter.num_nodes);
    assert(node2_index >= 0);
    assert(node2_index < inter.num_nodes);
    // tan_sag should be either TANGENTIAL or SAGITTAL
    assert(tan_sag == TANGENTIAL || tan_sag == SAGITTAL);

    switch (comp_type) {
        case MIRROR:
            mirror = &(inter.mirror_list[component_index]);
            if ((node1_index != mirror->node1_index &&
                    node1_index != mirror->node2_index)
                    ||
                    (node2_index != mirror->node1_index &&
                    node2_index != mirror->node2_index)) {
                bug_error("mirror no such node");
            }

            // transmission
            if (node1_index != node2_index) {
                if (tan_sag == TANGENTIAL) {
                    *matrix = (mirror->qqtt);
                } else {
                    *matrix = (mirror->qqts);
                }
            }// reflection, left to right
            else if (node1_index == mirror->node1_index) {
                if (tan_sag == TANGENTIAL) {
                    *matrix = (mirror->qqr1t);
                } else {
                    *matrix = (mirror->qqr1s);
                }
            }// reflection, right to left
            else {
                if (tan_sag == TANGENTIAL) {
                    *matrix = (mirror->qqr2t);
                } else {
                    *matrix = (mirror->qqr2s);
                }
            }
            //dump_ABCD(stdout, *matrix);
            break;
        case BEAMSPLITTER:
            bs = &(inter.bs_list[component_index]);
            if ((node1_index != bs->node1_index &&
                    node1_index != bs->node2_index &&
                    node1_index != bs->node3_index &&
                    node1_index != bs->node4_index)
                    ||
                    (node2_index != bs->node1_index &&
                    node2_index != bs->node2_index &&
                    node2_index != bs->node3_index &&
                    node2_index != bs->node4_index)) {
                bug_error("bs no such node");
            }

            if (node1_index == node2_index) {
                return (1);
            }

            // reflection1
            if ((node1_index == bs->node1_index &&
                    node2_index == bs->node2_index)
                    ||
                    (node1_index == bs->node2_index &&
                    node2_index == bs->node1_index)) {
                if (tan_sag == TANGENTIAL) {
                    *matrix = (bs->qqr1t);
                } else {
                    *matrix = (bs->qqr1s);
                }
            }// reflection2
            else if ((node1_index == bs->node3_index &&
                    node2_index == bs->node4_index)
                    ||
                    (node1_index == bs->node4_index &&
                    node2_index == bs->node3_index)) {
                if (tan_sag == TANGENTIAL) {
                    *matrix = (bs->qqr2t);
                } else {
                    *matrix = (bs->qqr2s);
                }
            }// transmission to
            else if ((node1_index == bs->node1_index &&
                    node2_index == bs->node3_index)
                    ||
                    (node1_index == bs->node2_index &&
                    node2_index == bs->node4_index)) {
                if (tan_sag == TANGENTIAL)
                    *matrix = (bs->qqt1t);
                else
                    *matrix = (bs->qqt1s);
            }// transmission back
            else if ((node1_index == bs->node3_index &&
                    node2_index == bs->node1_index)
                    ||
                    (node1_index == bs->node4_index &&
                    node2_index == bs->node2_index)) {
                if (tan_sag == TANGENTIAL) {
                    *matrix = (bs->qqt2t);
                } else {
                    *matrix = (bs->qqt2s);
                }
            } else {
                return (1);
            }
            break;
            
        case DBS:
            dbs = &(inter.dbs_list[component_index]);
            if ((node1_index != dbs->node1_index &&
                    node1_index != dbs->node2_index &&
                    node1_index != dbs->node3_index &&
                    node1_index != dbs->node4_index)
                    ||
                    (node2_index != dbs->node1_index &&
                    node2_index != dbs->node2_index &&
                    node2_index != dbs->node3_index &&
                    node2_index != dbs->node4_index)) {
                bug_error("dbs no such node");
            }

            bool allow = false;
            
            if (node1_index == dbs->node1_index &&
                    node2_index == dbs->node3_index) {
                allow = true;
            } else if (node1_index == dbs->node2_index &&
                    node2_index == dbs->node1_index) {
                allow = true;
            } else if (node1_index == dbs->node3_index &&
                    node2_index == dbs->node4_index) {
                allow = true;
            } else if (node1_index == dbs->node4_index &&
                    node2_index == dbs->node2_index) {
                allow = true;
            }

            if (!allow) {
                return 1;
            } else {
                *matrix = dbs->qq;
            }
            
            break;
            
            
        case SPACE:
            space = &(inter.space_list[component_index]);
            if ((node1_index != space->node1_index &&
                    node1_index != space->node2_index)
                    ||
                    (node2_index != space->node1_index &&
                    node2_index != space->node2_index)) {
                bug_error("space no such node");
            }

            if (node1_index == node2_index) {
                return (1);
            }
            *matrix = (space->qq);
            break;
        case MODULATOR:
            modulator = &(inter.modulator_list[component_index]);
            if ((node1_index != modulator->node1_index &&
                    node1_index != modulator->node2_index)
                    ||
                    (node2_index != modulator->node1_index &&
                    node2_index != modulator->node2_index)) {
                bug_error("modulator no such node");
            }

            if (node1_index == node2_index) {
                return (1);
            }
            *matrix = (modulator->qq);
            break;
        case LENS:
            lens = &(inter.lens_list[component_index]);
            if ((node1_index != lens->node1_index &&
                    node1_index != lens->node2_index)
                    ||
                    (node2_index != lens->node1_index &&
                    node2_index != lens->node2_index)) {
                bug_error("lens no such node");
            }

            if (node1_index == node2_index) {
                return (1);
            }
            *matrix = (lens->qq);
            break;
        case DIODE:
            diode = &(inter.diode_list[component_index]);

            if ((node1_index != diode->node1_index &&
                    node1_index != diode->node2_index &&
                    node1_index != diode->node3_index)
                    ||
                    (node2_index != diode->node1_index &&
                    node2_index != diode->node2_index &&
                    node2_index != diode->node3_index)
                    ) {
                bug_error("diode no such node");
            }

            if (node1_index == node2_index) {
                return (1);
            }

            // there is no connection between node 1 and node 3
            if ((node1_index == diode->node1_index && node2_index == diode->node3_index)
                    || (node1_index == diode->node3_index && node2_index == diode->node1_index))
                return (1);


            *matrix = (diode->qq);
            break;
        case GRATING:
            grating = &(inter.grating_list[component_index]);
            switch (grating->num_of_ports) {
                case 2:
                    if ((node1_index != grating->node1_index &&
                            node1_index != grating->node2_index)
                            ||
                            (node2_index != grating->node1_index &&
                            node2_index != grating->node2_index)) {
                        bug_error("gr2 no such node");
                    }

                    // reflection
                    if (node1_index == node2_index) {
                        if (tan_sag == TANGENTIAL) {
                            *matrix = (grating->qqt[0][0]);
                        } else {
                            *matrix = (grating->qqs[0][0]);
                        }
                    }// reflection, input to 0th order
                    else if (node1_index == grating->node1_index) {
                        if (tan_sag == TANGENTIAL) {
                            *matrix = (grating->qqt[0][1]);
                        } else {
                            *matrix = (grating->qqs[0][1]);
                        }
                    }// reflection, zeroth order into input
                    else {
                        if (tan_sag == TANGENTIAL) {
                            *matrix = (grating->qqt[1][0]);
                        } else {
                            *matrix = (grating->qqs[1][0]);
                        }
                    }
                    break;
                case 3:
                    if ((node1_index != grating->node1_index &&
                            node1_index != grating->node2_index &&
                            node1_index != grating->node3_index)
                            ||
                            (node2_index != grating->node1_index &&
                            node2_index != grating->node2_index &&
                            node2_index != grating->node3_index)) {
                        bug_error("gr3 no such node");
                    }

                    // reflection, input to 0th order
                    if (node1_index == grating->node1_index) {
                        if (node2_index == grating->node1_index) {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[0][0]);
                            } else {
                                *matrix = (grating->qqs[0][0]);
                            }
                        } else if (node2_index == grating->node2_index) {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[0][1]);
                            } else {
                                *matrix = (grating->qqs[0][1]);
                            }
                        } else {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[0][2]);
                            } else {
                                *matrix = (grating->qqs[0][2]);
                            }
                        }
                    } else if (node1_index == grating->node2_index) {
                        if (node2_index == grating->node1_index) {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[1][0]);
                            } else {
                                *matrix = (grating->qqs[1][0]);
                            }
                        } else if (node2_index == grating->node2_index) {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[1][1]);
                            } else {
                                *matrix = (grating->qqs[1][1]);
                            }
                        } else {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[1][2]);
                            } else {
                                *matrix = (grating->qqs[1][2]);
                            }
                        }
                    } else // node1_index = grating-> node3
                    {
                        if (node2_index == grating->node1_index) {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[2][0]);
                            } else {
                                *matrix = (grating->qqs[2][0]);
                            }
                        } else if (node2_index == grating->node2_index) {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[2][1]);
                            } else {
                                *matrix = (grating->qqs[2][1]);
                            }
                        } else // node 2 == grating->node3
                        {
                            if (tan_sag == TANGENTIAL) {
                                *matrix = (grating->qqt[2][2]);
                            } else {
                                *matrix = (grating->qqs[2][2]);
                            }
                        }
                    }
                    break;
                case 4:
                    break;
            }
            break;
        default:
            bug_error("no such type");
    }
    return (0);
}

//! Set the Gaussian beam parameter for the respective mode.

/*!
 * \param component_index index of component at which to set the Gaussian
 * beam parameter
 *
 * \see Test_set_qbase()
 */
void set_qbase(int component_index) {
    int err;
    int no, node_indices[4] = {0};
    ABCD_t transform;
    complex_t qx, qy, qxt, qyt;
    double nr1, nr2;

    node_t node_i, node_j;

    // sanity checks on input
    // component index should be in the range:
    // 0 <= component_index < inter.num_components
    assert(component_index >= 0);
    assert(component_index < inter.num_components);

    
    // ddb - this returns a list of all the nodes at a component
    //       for then checking if it is set.
    which_nodes(component_index, GND_NODE, &no, node_indices);
    
    int k, i, j;
    
    // ddb - What on earth is 'k' loop for?
    for (k = 0; k < 2; k++) {
        for (i = 0; i < no; i++) {
            node_i = inter.node_list[node_indices[i]];
            
            if (node_i.q_is_set) {
                for (j = 0; j < no; j++) {
                    node_j = inter.node_list[node_indices[j]];
                    
                    // set q if q is not yet set
                    if (NOT node_j.q_is_set && NOT node_j.gnd_node) { // || get_component_type(component)==SPACE)
                        err = component_matrix(&transform, component_index,
                                node_indices[i], node_indices[j], TANGENTIAL);
                        
                        if (!err) {
                            if (inter.debug & 256) {
                                fprintf(stdout, "set_qbase: q set: %s\n",
                                        get_node_name(node_indices[j]));
                                dump_ABCD(stdout, transform);

                                // dump info to log file
                                fprintf(fp_log, "set_qbase: q set: %s\n",
                                        get_node_name(node_indices[j]));
                                dump_ABCD(fp_log, transform);
                            }
                            
                            // q -> q(-z)=-q* if necessary (reverse beam direction)
                            // ddb - always get beam coming into the node
                            if (node_i.component_index == component_index) {
                                qx = cminus(cconj(node_i.qx));
                                qy = cminus(cconj(node_i.qy));
                            } else {
                                qx = node_i.qx;
                                qy = node_i.qy;
                            }

                            nr1 = *node_i.n;
                            nr2 = *node_j.n;
                            
                            qxt = q1_q2(transform, qx, nr1, nr2);
                            err = component_matrix(&transform, component_index,
                                    node_indices[i], node_indices[j], SAGITTAL);
                            
                            qyt = q1_q2(transform, qy, nr1, nr2);
                            
                            set_q(node_indices[j], component_index, qxt, qyt);
                        } else if (inter.debug & 256) {
                            message("set_qbase: err -> %s %s\n",
                                    get_node_name(node_indices[i]),
                                    get_node_name(node_indices[j]));
                        }
                    }
                }
            }
        }
    }
}

//! For two given nodes returns the component connecting these nodes

/*!
 * (if the component is a beam splitter, and consequently has four nodes,
 * one only needs to give two out of the four as this is for tracing a path
 * when one walks from node to node).
 * 
 * \param component_type the type of component
 * \param component_index the component index
 * \param node1_index first component node index
 * \param node2_index second component node index
 *
 * \see Test_get_component()
 */
void get_component(int *component_type, int *component_index,
        int node1_index, int node2_index) {
    int tmp_component_index, component1_index, component2_index;
    int component3_index, component4_index;

    // sanity checks on inputs
    // the component index should be in the range:
    // 0 <= component_index < inter.num_components
    assert(*component_index >= 0);
    assert(*component_index < inter.num_components);
    // the node indices should be in the range:
    // 0 <= node_index < inter.num_nodes
    assert(node1_index >= 0);
    assert(node2_index >= 0);
    assert(node1_index < inter.num_nodes);
    assert(node2_index < inter.num_nodes);

    which_components(node1_index, &component1_index, &component2_index);
    which_components(node2_index, &component3_index, &component4_index);

    if (component1_index == component3_index ||
            component1_index == component4_index) {
        tmp_component_index = component1_index;
    } else if (component2_index == component3_index ||
            component2_index == component4_index) {
        tmp_component_index = component2_index;
    } else {
        bug_error("nodes not at the same component");
    }

    *component_type = get_component_type_decriment_index(&tmp_component_index);
    *component_index = tmp_component_index;
}

//! For a given component number return number of nodes and node numbers

/*! 
 * The 'current_node' is used to determine which BS nodes are reachable 
 * for the incident beam. This feature is used for beam and cavity tracing. 
 * In other places you can set current_node to GND_NODE and all four BS 
 * nodes will be returned!
 *
 * \param component_index the component number
 * \param current_node the node we are currently at in the system
 * \param num_nodes the number of nodes
 * \param node_indices the node indices
 *
 * \see Test_which_nodes()
 */
void which_nodes(int component_index, int current_node,
        int *num_nodes, int *node_indices) {
    int k;
    int no;

    // sanity checks on inputs
    // component index should be in the range:
    assert(component_index >= 0);
    assert(component_index < inter.num_components);
    // current node should be in the range:
    assert(current_node == GND_NODE ||
            (current_node >= 0 && current_node < inter.num_nodes));

    k = component_index;

    no = 0;

    //if (inter.debug & 256)
    //  printf("which_nodes component_index: %d\n", component_index);

    if (component_index == NOT_FOUND) { // experimental: node only used once
        no = 0;
    } else if (component_index < inter.num_mirrors) {
        if (component_index < 0) {
            bug_error("component index less than zero");
        }

        mirror_t *mirror = &inter.mirror_list[component_index];
        node_t *node1 = &inter.node_list[mirror->node1_index];
        node_t *node2 = &inter.node_list[mirror->node2_index];

        if (NOT node1->gnd_node) {
            node_indices[no++] = mirror->node1_index;
        }

        if (NOT node2->gnd_node) {
            node_indices[no++] = mirror->node2_index;
        }
    }// experimental: BS returns only the two available nodes
        // depending on the node of the incident field
    else if ((component_index -= inter.num_mirrors) < inter.num_beamsplitters) {
        beamsplitter_t *beamsplitter = &inter.bs_list[component_index];

        node_t *node1 = &inter.node_list[beamsplitter->node1_index];
        node_t *node2 = &inter.node_list[beamsplitter->node2_index];
        node_t *node3 = &inter.node_list[beamsplitter->node3_index];
        node_t *node4 = &inter.node_list[beamsplitter->node4_index];

        if (current_node == GND_NODE ||
                inter.node_list[current_node].gnd_node) {
            if (NOT node1->gnd_node) {
                node_indices[no++] = beamsplitter->node1_index;
            }

            if (NOT node2->gnd_node) {
                node_indices[no++] = beamsplitter->node2_index;
            }

            if (NOT node3->gnd_node) {
                node_indices[no++] = beamsplitter->node3_index;
            }

            if (NOT node4->gnd_node) {
                node_indices[no++] = beamsplitter->node4_index;
            }
        } else {
            if (current_node == beamsplitter->node1_index) {
                if (NOT node2->gnd_node) {
                    node_indices[no++] = beamsplitter->node2_index;
                }

                if (NOT node3->gnd_node) {
                    node_indices[no++] = beamsplitter->node3_index;
                }
            }

            if (current_node == beamsplitter->node2_index) {
                if (NOT node1->gnd_node) {
                    node_indices[no++] = beamsplitter->node1_index;
                }

                if (NOT node4->gnd_node) {
                    node_indices[no++] = beamsplitter->node4_index;
                }
            }

            if (current_node == beamsplitter->node3_index) {
                if (NOT node4->gnd_node) {
                    node_indices[no++] = beamsplitter->node4_index;
                }

                if (NOT node1->gnd_node) {
                    node_indices[no++] = beamsplitter->node1_index;
                }
            }

            if (current_node == beamsplitter->node4_index) {
                if (NOT node3->gnd_node) {
                    node_indices[no++] = beamsplitter->node3_index;
                }

                if (NOT node2->gnd_node) {
                    node_indices[no++] = beamsplitter->node2_index;
                }
            }
        }
    } else if ((component_index -= inter.num_beamsplitters) < inter.num_gratings) {
        grating_t *grating = &inter.grating_list[component_index];

        node_t *node1 = &inter.node_list[grating->node1_index];
        node_t *node2 = &inter.node_list[grating->node2_index];
        node_t *node3 = &inter.node_list[grating->node3_index];
        node_t *node4 = &inter.node_list[grating->node4_index];

        if (NOT node1->gnd_node) {
            node_indices[no++] = grating->node1_index;
        }
        if (NOT node2->gnd_node) {
            node_indices[no++] = grating->node2_index;
        }
        switch (grating->num_of_ports) {
            case 3:
                if (NOT node3->gnd_node) {
                    node_indices[no++] = grating->node3_index;
                }
            case 4:
                if (NOT node4->gnd_node) {
                    node_indices[no++] = grating->node4_index;
                }
        }
    } else if ((component_index -= inter.num_gratings) < inter.num_spaces) {
        space_t *space = &inter.space_list[component_index];

        node_t *node1 = &inter.node_list[space->node1_index];
        node_t *node2 = &inter.node_list[space->node2_index];

        if (NOT node1->gnd_node) {
            node_indices[no++] = space->node1_index;
        }

        if (NOT node2->gnd_node) {
            node_indices[no++] = space->node2_index;
        }
    } else if ((component_index -= inter.num_spaces) < inter.num_lenses) {
        lens_t *lens = &inter.lens_list[component_index];

        node_t *node1 = &inter.node_list[lens->node1_index];
        node_t *node2 = &inter.node_list[lens->node2_index];

        if (NOT node1->gnd_node) {
            node_indices[no++] = lens->node1_index;
        }

        if (NOT node2->gnd_node) {
            node_indices[no++] = lens->node2_index;
        }
    } else if ((component_index -= inter.num_lenses) < inter.num_diodes) {
        diode_t *diode = &inter.diode_list[component_index];

        node_t *node1 = &inter.node_list[diode->node1_index];
        node_t *node2 = &inter.node_list[diode->node2_index];
        node_t *node3 = &inter.node_list[diode->node3_index];

        if (current_node == GND_NODE || inter.node_list[current_node].gnd_node) {
            if (NOT node1->gnd_node) node_indices[no++] = diode->node1_index;
            if (NOT node2->gnd_node) node_indices[no++] = diode->node2_index;
            if (NOT node3->gnd_node) node_indices[no++] = diode->node3_index;
        } else {
            // otherwise we need to return only the nodes that the current node
            // can reach
            if (current_node == diode->node1_index) {
                if (NOT node2->gnd_node) node_indices[no++] = diode->node2_index;
            } else if (current_node == diode->node2_index) {
                if (NOT node1->gnd_node) node_indices[no++] = diode->node1_index;
                if (NOT node3->gnd_node) node_indices[no++] = diode->node3_index;
            } else if (current_node == diode->node3_index) {
                if (NOT node2->gnd_node) node_indices[no++] = diode->node2_index;
            } else
                bug_error("unhandled (%i)\n", current_node);
        }
    } else if ((component_index -= inter.num_diodes) < inter.num_light_inputs) {
        light_in_t *light_in = &inter.light_in_list[component_index];

        node_t light_in_node = inter.node_list[light_in->node_index];

        if (NOT light_in_node.gnd_node) {
            node_indices[no++] = light_in->node_index;
        }
    } else if ((component_index -= inter.num_light_inputs) < inter.num_modulators) {
        modulator_t *modulator = &inter.modulator_list[component_index];

        node_t *node1 = &inter.node_list[modulator->node1_index];
        node_t *node2 = &inter.node_list[modulator->node2_index];

        if (NOT node1->gnd_node) {
            node_indices[no++] = modulator->node1_index;
        }

        if (NOT node2->gnd_node) {
            node_indices[no++] = modulator->node2_index;
        }
    } else if ((component_index -= inter.num_modulators) < inter.num_dbss) {
        // based on beamsplitter code as other 4 port device
        dbs_t *dbs = &inter.dbs_list[component_index];

        node_t *node1 = &inter.node_list[dbs->node1_index];
        node_t *node2 = &inter.node_list[dbs->node2_index];
        node_t *node3 = &inter.node_list[dbs->node3_index];
        node_t *node4 = &inter.node_list[dbs->node4_index];

        if (current_node == GND_NODE ||
                inter.node_list[current_node].gnd_node) {
            if (NOT node1->gnd_node) {
                node_indices[no++] = dbs->node1_index;
            }

            if (NOT node2->gnd_node) {
                node_indices[no++] = dbs->node2_index;
            }

            if (NOT node3->gnd_node) {
                node_indices[no++] = dbs->node3_index;
            }

            if (NOT node4->gnd_node) {
                node_indices[no++] = dbs->node4_index;
            }
        } else {
            if (current_node == dbs->node1_index) {
                if (NOT node3->gnd_node) {
                    node_indices[no++] = dbs->node3_index;
                }
            }

            if (current_node == dbs->node2_index) {
                if (NOT node1->gnd_node) {
                    node_indices[no++] = dbs->node1_index;
                }
            }

            if (current_node == dbs->node3_index) {
                if (NOT node4->gnd_node) {
                    node_indices[no++] = dbs->node4_index;
                }
            }

            if (current_node == dbs->node4_index) {
                if (NOT node2->gnd_node) {
                    node_indices[no++] = dbs->node2_index;
                }
            }
        }
    } else if ((component_index -= inter.num_dbss) < inter.num_signals) {
        bug_error("which_node 3");
        no = 2;
    } else if ((component_index -= inter.num_signals) < inter.num_outputs) {
        output_data_t *output_data = &inter.output_data_list[component_index];

        node_t output_data_node = inter.node_list[output_data->node_index];

        if (NOT output_data_node.gnd_node) {
            node_indices[no++] = output_data->node_index;
        }

    } else {
        bug_error("component index outside correct range");
    }

    *num_nodes = no;

    if (inter.debug & 16) {
        if (*num_nodes) {
            message("For component %s(%d) -> nodes:", get_component_name(k), k);
        } else {
            message("no new nodes found here\n");
        }

        int j;
        for (j = 0; j<*num_nodes; j++) {
            message(" %s(%d)", get_node_name(node_indices[j]), node_indices[j]);
        }
        message("\n");
    }
}

//! For given node number sets component numbers with connected components

/*!
 * For a given node number sets the two component numbers with the 
 * connected components to NOT_FOUND if only one component is connected.
 * The result is stored in a global array vlocal.component_trace_list
 *
 * \param node_index the node index to check
 * \param component1_index index of one component connected to node
 * \param component2_index index of other component connected to node
 *
 * \see Test_which_components()
 */
void which_components(int node_index,
        int *component1_index, int *component2_index) {
    // sanity checking on inputs
    // node_index should be in the range 0 <= node_index < inter.num_nodes
    assert(node_index >= 0);
    assert(node_index < inter.num_nodes);

    //  int comp[MAXCOMP] = { 0 };

    if (node_index < 0) {
        bug_error("node index less than zero");
    }

    int index = 0; //!< \todo is this the trace index???
    int object_index = 0;

    int mirror_index;
    for (mirror_index = 0; mirror_index < inter.num_mirrors; mirror_index++) {
        if (inter.mirror_list[mirror_index].node1_index == node_index ||
                inter.mirror_list[mirror_index].node2_index == node_index) {
            vlocal.component_trace_list[index++] = mirror_index;
        }
    }
    object_index += inter.num_mirrors;

    int beamsplitter_index;
    for (beamsplitter_index = 0;
            beamsplitter_index < inter.num_beamsplitters;
            beamsplitter_index++) {
        if (inter.bs_list[beamsplitter_index].node1_index == node_index ||
                inter.bs_list[beamsplitter_index].node2_index == node_index ||
                inter.bs_list[beamsplitter_index].node3_index == node_index ||
                inter.bs_list[beamsplitter_index].node4_index == node_index) {
            vlocal.component_trace_list[index++] = beamsplitter_index + object_index;
        }
    }
    object_index += inter.num_beamsplitters;

    int grating_index;
    for (grating_index = 0; grating_index < inter.num_gratings; grating_index++) {
        switch (inter.grating_list[grating_index].num_of_ports) {
            case 2:
                if (inter.grating_list[grating_index].node1_index == node_index ||
                        inter.grating_list[grating_index].node2_index == node_index) {
                    vlocal.component_trace_list[index++] = grating_index + object_index;
                }
                break;
            case 3:
                if (inter.grating_list[grating_index].node1_index == node_index ||
                        inter.grating_list[grating_index].node2_index == node_index ||
                        inter.grating_list[grating_index].node3_index == node_index) {
                    vlocal.component_trace_list[index++] = grating_index + object_index;
                }
                break;
            case 4:
                if (inter.grating_list[grating_index].node1_index == node_index ||
                        inter.grating_list[grating_index].node2_index == node_index ||
                        inter.grating_list[grating_index].node3_index == node_index ||
                        inter.grating_list[grating_index].node4_index == node_index) {
                    vlocal.component_trace_list[index++] = grating_index + object_index;
                }
                break;
            default:
                bug_error("aa, comptrace: grating - wrong number of ports");
        }
    }
    object_index += inter.num_gratings;

    int space_index;
    for (space_index = 0; space_index < inter.num_spaces; space_index++) {
        if (inter.space_list[space_index].node1_index == node_index ||
                inter.space_list[space_index].node2_index == node_index) {
            vlocal.component_trace_list[index++] = space_index + object_index;
        }
    }
    object_index += inter.num_spaces;

    int squeezer_index;
    for (squeezer_index = 0;
            squeezer_index < inter.num_squeezers;
            squeezer_index++) {
        if (inter.squeezer_list[squeezer_index].node1_index == node_index ||
                inter.squeezer_list[squeezer_index].node2_index == node_index) {
            vlocal.component_trace_list[index++] = squeezer_index + object_index;
        }
    }
    object_index += inter.num_squeezers;

    int lens_index;
    for (lens_index = 0; lens_index < inter.num_lenses; lens_index++) {
        if (inter.lens_list[lens_index].node1_index == node_index ||
                inter.lens_list[lens_index].node2_index == node_index) {
            vlocal.component_trace_list[index++] = lens_index + object_index;
        }
    }
    object_index += inter.num_lenses;

    int diode_index;
    for (diode_index = 0; diode_index < inter.num_diodes; diode_index++) {
        if (inter.diode_list[diode_index].node1_index == node_index ||
                inter.diode_list[diode_index].node2_index == node_index ||
                inter.diode_list[diode_index].node3_index == node_index) {
            vlocal.component_trace_list[index++] = diode_index + object_index;
        }
    }
    object_index += inter.num_diodes;

    int light_input_index;
    for (light_input_index = 0;
            light_input_index < inter.num_light_inputs;
            light_input_index++) {
        if (inter.light_in_list[light_input_index].node_index == node_index) {
            vlocal.component_trace_list[index++] = light_input_index + object_index;
        }
    }
    object_index += inter.num_light_inputs;

    int modulator_index;
    for (modulator_index = 0;
            modulator_index < inter.num_modulators;
            modulator_index++) {
        if (inter.modulator_list[modulator_index].node1_index == node_index ||
                inter.modulator_list[modulator_index].node2_index == node_index) {
            vlocal.component_trace_list[index++] = modulator_index + object_index;
        }
    }
    //  j+=inter.num_modulators+inter.num_signals;
    object_index += inter.num_modulators; // 080805

    int dbs_index;
    for (dbs_index = 0;
            dbs_index < inter.num_dbss;
            dbs_index++) {
        if (inter.dbs_list[dbs_index].node1_index == node_index ||
            inter.dbs_list[dbs_index].node2_index == node_index ||
            inter.dbs_list[dbs_index].node3_index == node_index ||
            inter.dbs_list[dbs_index].node4_index == node_index) {
            vlocal.component_trace_list[index++] = dbs_index + object_index;
        }
    }

    object_index += inter.num_dbss;

    if (index > 2 || index < 1) {
        if (index < 1) {
            fprintf(stderr, "at node %s !!", get_node_name(node_index));
            bug_error("no component at node");
        }
        gerror("node %s connected to %s components!\n", get_node_name(node_index),
                int_form(index));
    }

    *component1_index = vlocal.component_trace_list[0];
    if (index == 2) {
        *component2_index = vlocal.component_trace_list[1];
    } else {
        *component2_index = NOT_FOUND;
    }

    if (inter.debug & 16) {
        message("For node %s(%d) -> components: %s(%d) and %s(%d)\n",
                get_node_name(node_index), node_index,
                get_component_name(*component1_index), *component1_index,
                get_component_name(*component2_index), *component2_index);
    }
}

//! Returns a given node's name

/*!
 * \param node_index the node index
 *
 * \see Test_get_node_name()
 */
char *get_node_name(int node_index) {
    // sanity checks on input
    // node index should be in the range: 0 <= node_index < inter.num_nodes
    assert(node_index >= 0);
    assert(node_index < inter.num_nodes);

    if (node_index < 0) {
        message("node_index:%d\n", node_index);
        bug_error("node index less than zero");
    }

    if (node_index >= inter.num_nodes) {
        message("node_index:%d\n", node_index);
        bug_error("node index greater than number of nodes");
    }

    return duplicate_string(inter.node_list[node_index].name);
}

//! Sets Gaussian beam parameter (different situation to set_qbase)

/*!
 * \param node_index the node index to set
 * \param component_index the component index
 * \param qx Gaussian beam parameter in x-plane
 * \param qy Gaussian beam parameter in y-plane
 *
 * \see set_qbase
 * \see Test_set_q()
 */
void set_q(int node_index, int component_index, complex_t qx, complex_t qy) {
    int component1_index, component2_index;
    int type, no;
    int num_nodes, node_indices[2] = {0};
    double nr1, nr2;
    complex_t qxt, qyt;
    complex_t qxt2, qyt2;
    node_t *node;
    space_t space;

    // make sure that the arguments are sane:
    // the node_index should be greater than, or equal to, zero
    assert(node_index >= 0);
    // and less than the total number of nodes in the interferometer
    assert(node_index < inter.num_nodes);
    // the component index should be >= 0
    assert(component_index >= 0);

    // and less than the total number of components in the interferometer
#ifndef NDEBUG
    if (component_index >= inter.num_components) {
        message("set_q(): component_index >= inter.num_components\n"
                "component_index = %d, inter.num_components = %d\n",
                component_index, inter.num_components);
    }
#endif
    assert(component_index < inter.num_components);

    node = &(inter.node_list[node_index]);

    if ((!(node->q_is_set)) || (node->q_is_set && (!ceq(node->qx, qx) || !ceq(node->qy, qy)))) {
        if (node->q_is_set) {
            if (inter.trace & 4 || inter.trace & 32) {
                message("q value already set at node %s, overwritten!\n",
                        node_print(node_index));

                if (inter.debug & 256) {
                    message("(old qx %s qy %s , new qx %s qy %s)\n",
                            complex_form(node->qx), complex_form(node->qy),
                            complex_form(qx), complex_form(qy));
                }
            }
        }

        // set q value
        if (inter.debug & 256) {
            message("--- setting q for node %s\n", get_node_name(node_index));
        }

        node->qx = qx;
        node->qy = qy;
        node->component_index = component_index;
        node->q_is_set = true;

        // now check whether a space is connected and set second
        // q value if necessary
        which_components(node_index, &component1_index, &component2_index);

        // first component
        no = component1_index;
        type = get_component_type_decriment_index(&no);
        if (type == SPACE) {
            space = inter.space_list[no];
            which_nodes(component1_index, GND_NODE, &num_nodes, node_indices);
            if (num_nodes > 2) {
                bug_error("set_q too many nodes (1)");
            }

            if (num_nodes == 2) {
                if (node_indices[0] == node_index) {
                    node = &(inter.node_list[node_indices[1]]);
                } else if (node_indices[1] == node_index) {
                    node = &(inter.node_list[node_indices[0]]);
                } else {
                    bug_error("set_q wrong node (1)");
                }
            }

            nr1 = *inter.node_list[node_index].n;
            nr2 = *(node->n);
            // if first component is initial component then reverse q
            if (component1_index == component_index) {
                qxt = q1_q2(space.qq, cminus(cconj(qx)), nr1, nr2);
                qyt = q1_q2(space.qq, cminus(cconj(qy)), nr1, nr2);
            } else {
                qxt = q1_q2(space.qq, qx, nr1, nr2);
                qyt = q1_q2(space.qq, qy, nr1, nr2);
            }

            if (node->q_is_set) {
                // if set q comes not out of first comp then reverse q
                if (node->component_index != component1_index) {
                    qxt2 = cminus(cconj(node->qx));
                    qyt2 = cminus(cconj(node->qy));
                } else {
                    qxt2 = node->qx;
                    qyt2 = node->qy;
                }


                if (!ceq(qxt2, qxt) || !ceq(qyt2, qyt)) {
                    if (inter.trace & 32) {
                        message("   overwriting second q value at space %s\n", space.name);
                    }

                    node->qx = qxt;
                    node->qy = qyt;
                    node->component_index = component1_index;
                    node->q_is_set = true;

                    if (inter.debug & 256) {
                        message("   1 :qx %s, qy %s\n", complex_form(node->qx),
                                complex_form(node->qy));
                    }
                }
            } else {
                if (inter.trace & 32) {
                    message("   setting second q value at space %s\n", space.name);
                }

                node->qx = qxt;
                node->qy = qyt;
                node->component_index = component1_index;
                node->q_is_set = true;
            }
        }

        // second component
        no = component2_index;
        type = get_component_type_decriment_index(&no);
        if (type == SPACE) {
            space = inter.space_list[no];
            which_nodes(component2_index, GND_NODE, &num_nodes, node_indices);
            if (num_nodes > 2) {
                bug_error("set_q too many nodes (2)");
            }

            if (num_nodes == 2) {
                if (node_indices[0] == node_index) {
                    node = &(inter.node_list[node_indices[1]]);
                } else if (node_indices[1] == node_index) {
                    node = &(inter.node_list[node_indices[0]]);
                } else {
                    bug_error("set_q wrong node (2)");
                }
            }

            nr1 = *inter.node_list[node_index].n;
            nr2 = *(node->n);
            // if second component is initial component then reverse q
            if (component2_index == component_index) {
                qxt = q1_q2(space.qq, cminus(cconj(qx)), nr1, nr2);
                qyt = q1_q2(space.qq, cminus(cconj(qy)), nr1, nr2);
            } else {
                qxt = q1_q2(space.qq, qx, nr1, nr2);
                qyt = q1_q2(space.qq, qy, nr1, nr2);
            }

            if (node->q_is_set) {
                // if set q does not come out of second comp then reverse q
                if (node->component_index != component2_index) {
                    qxt2 = cminus(cconj(node->qx));
                    qyt2 = cminus(cconj(node->qy));
                } else {
                    qxt2 = node->qx;
                    qyt2 = node->qy;
                }

                if (!ceq(qxt2, qxt) || !ceq(qyt2, qyt)) {
                    if (inter.trace & 32) {
                        message("   overwriting second q value at space %s\n", space.name);
                    }

                    node->qx = qxt;
                    node->qy = qyt;
                    node->component_index = component2_index;
                    node->q_is_set = true;

                    if (inter.debug & 256) {
                        message("   1 :qx %s, qy %s\n", complex_form(node->qx),
                                complex_form(node->qy));
                    }
                }
            } else {
                if (inter.trace & 32) {
                    message("   setting second q value at space %s\n", space.name);
                }

                node->qx = qxt;
                node->qy = qyt;
                node->component_index = component2_index;
                node->q_is_set = true;

                /*
                 * if (inter.debug & 256)
                 * {
                 * dump_ABCD(stdout, space.qq);
                 * printf("   ... and qx %s qy %s\n", complex_form(qx), complex_form(qy));
                 * printf("   2 :qx %s, qy %s\n", complex_form(n->qx), complex_form(n->qy));
                 * }
                 */
            }
        }
    } else {
        if (inter.debug & 256) {
            message("--- q already set for node %s\n", get_node_name(node_index));
        }
    }
}





/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
