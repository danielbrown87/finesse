// $Id$

/*!
 * \file kat_calc.c
 * \brief The routines of the core calculations in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#include "kat.h"
#include "kat_inline.c"
#include "kat_calc.h"
#include "kat_io.h"
#include "kat_gnu.h"
#include "kat_optics.h"
#include "kat_quant.h"
#include "kat_spa.h"
#include "kat_aux.h"
#include "kat_init.h"
#include "kat_aa.h"
#include "kat_matrix_ccs.h"
#include "kat_dump.h"
#include <errno.h>

#include <gsl/gsl_cblas.h>
//#include <gsl/gsl_errno.h>
//#include <gsl/gsl_math.h>
//#include <gsl/gsl_min.h>
#include "brent.h"

#ifdef OSWIN
#undef UCHAR // formulc.h defines this already and so does windows.h, so dont do it again!
#include <windows.h>
// remove definitions that cause conflicts, as generally causes problems and original WIN32 defintions aren't used
#undef small 
#undef OUT
#undef max
#undef min
#include <shellapi.h>
#include <Shlwapi.h>
#endif


extern init_variables_t init;
extern interferometer_t inter;
extern options_t options;
extern memory_t mem;
extern local_var_t vlocal;

extern global_var_t vglobal;
extern time_t now;

extern ifo_matrix_vars_t M_ifo_car; // carrier and modulator field matrix
extern ifo_matrix_vars_t M_ifo_sig;  // signal fields and optic degrees of freedom

extern klu_sparse_var_t klu;

extern int carrier_frequencies_tuned;
extern int signal_frequencies_tuned;

extern const complex_t complex_i;
extern const complex_t complex_1;
extern const complex_t complex_m1;
extern const complex_t complex_0;

static double dx; //!< A step distance in the x direction
static double dy; //!< A step distance in the y direction

static double x_axis_point; //!< Current distance along x-axis
static double y_axis_point; //!< Current distance along y-axis

int current_point; //!< The current point used for progress output
int current_point_tot; //!< Overall current point used for progress output

bool printed_mismatches = false;

//! A flag indicating that result 'looks like' a transfer function to Finesse
extern int *transfer;

extern double *f_s; //!< Frequency value list [frequency]
extern complex_t ***a_s; //!< Amplitude list [field] [num_light_outputs + num_quad_outputs + 2*num_homodynes] [frequency]
extern int *t_s; //!< Type_of_Signal list [frequency]

bool update_fields; //!< Flag to say whether or not to update the fields

FILE *ofp; //!< The output file pointer
FILE *gfp; //!< The gnuplot output file pointer
FILE *mfp; //!< The matlab output file pointer
FILE *pfp; //!< The python output file pointer

int compute_data_msg_flags = 0;

/**
 * Use this function to update the xaxis values. This also does other internal
 * checks to ensure that the negative xaxis values are fsigs are updated correctly
 * 
 * @param axis
 * @param value
 */
void update_xaxis_values(axis_t *axis, double value){
    
    *(axis->xaxis) = value;
    axis->minus_xaxis = -1.0 * value;
    
    if(axis->xaxis == &inter.fsig)
        inter.mfsig = axis->minus_xaxis;
    else if(axis->xaxis == &inter.mfsig)
        inter.fsig = axis->minus_xaxis;
}

/**
 * Given an xaxis object this returns the min/max values and the step size.
 * round argument can be used to either round or not round the step size
 * using round_to_dp.
 * 
 * @param axis
 * @param dx
 * @param x_min
 * @param x_max
 * @param round
 */
void get_xaxis_values(axis_t *axis, double *dx, double *x_min, double *x_max, int round){
    assert(axis != NULL);
    assert(dx != NULL);
    assert(x_min != NULL);
    assert(x_max != NULL);
    
    if (axis->xtype == FLIN) {
        *x_min = axis->xmin;
        *x_max = axis->xmax;
    } else if (axis->xtype == FLOG) {
        *x_min = log(axis->xmin);
        *x_max = log(axis->xmax);
    } else {
        bug_error("axis min max error %s", axis->xname);
    }

    if (axis->xsteps) {
        *dx = (*x_max - *x_min) / (axis->xsteps);
        if(round) *dx = round_to_dp(*dx);
    } else {
        *dx = 0.0;
    }
}

/**
 * Computes the Laplace transformed (s-domain) transfer function H(s).
 * Transfer function is defined by poles and zeros as stored
 * in the mech_transfer_func_t object.
 * 
 * @param mtf pointer to mechanical transfer function
 * @param s complex s-domain value
 * @return H(s)
 */
complex_t calc_transfer_func(transfer_func_t *mtf, complex_t s){
    assert(mtf != NULL);
    
    complex_t rtn = z_by_xph(complex_1, mtf->gain, mtf->phase);
    double omega;
    
    int i;
    if(mtf->num_zeros > 0){
        
        switch(mtf->type){
            case GENERAL_TF:
                for(i=0; i<mtf->num_zeros; i++)
                    rtn = z_by_z(rtn, z_m_z(s, mtf->zeros[i]));
                
                break;
                
            case Q_F_TF:
                
                for(i=0; i<mtf->num_zeros; i++){
                    omega = TWOPI*mtf->zero_Q_f[i].f;
                    assert(mtf->zero_Q_f[i].Q !=0 );
                    
                    rtn = z_by_z(rtn, z_pl_z(z_pl_z(z_by_x(s, omega/mtf->zero_Q_f[i].Q), z_by_z(s,s)), co(omega*omega,0))); 
                }
                
                break;
            default:
                bug_error("unexpected transfer function type");
        }
    }
    
    if(mtf->num_poles > 0){
        complex_t tmp = complex_1;
        
        switch(mtf->type){
            case GENERAL_TF:
                for(i=0; i<mtf->num_poles; i++)
                    tmp = z_by_z(tmp, z_m_z(s, mtf->poles[i]));
                
                break;
                
            case Q_F_TF:
                for(i=0; i<mtf->num_poles; i++){
                    omega = TWOPI*mtf->pole_Q_f[i].f;
                    assert(mtf->pole_Q_f[i].Q !=0 );
                    
                    tmp = z_by_z(tmp, z_pl_z(z_pl_z(z_by_x(s, omega/mtf->pole_Q_f[i].Q), z_by_z(s,s)), co(omega*omega,0))); 
                }
                
                break;
            default:
                bug_error("unexpected transfer function type");
        }
        
        rtn = div_complex(rtn, tmp);
    }
    
    return rtn;
}

//! Compute all data, function sweeps x-axis calling all computation functions

/*!
 *
 * \see Test_compute_data()
 */
void compute_data(void) {
    // initialise variables local to this file
    // when x3axis is set this compute_data is called multiple times
    if(!inter.x3_axis_is_set){
        current_point = 0;
        current_point_tot = 0;
    }
    
    dx = 0.0;
    dy = 0.0;

    // initialise transfer function flag array
    int light_out_index;
    for (light_out_index = 0;
            light_out_index < inter.num_light_outputs;
            light_out_index++) {
        transfer[light_out_index] = 0;
    }

    double x_min = 0.0;
    double x_max = 0.0;
    double y_min = 0.0;
    double y_max = 0.0;

    // initialising x-axis ------------------------------------------------
    get_xaxis_values(&inter.x1, &dx, &x_min, &x_max, 1);

    // initialising second x-axis for 3D plot --------------------------
    int oneline = 0;
    if (inter.splot) {
        get_xaxis_values(&inter.x2, &dy, &y_min, &y_max, 1);
        
        oneline = inter.num_output_cols * (inter.x2.xsteps + 1);
    }
    
    // trying to compute one datapoint with original setup
    // this is useful for locks .... 
    // maybe this should be below if(inter.splot) below???

    // setting the first value for x and y axes so that
    // the data point actually corresponds to the first value in xaxis and
    // not to the static setup.
    update_xaxis_values(&inter.x1, inter.x1.xmin);
    
    if (inter.splot) {
        update_xaxis_values(&inter.x2, inter.x2.xmin);
    }

    update_fields = true;
    tune_parameters();
    
    data_point_new(true);
    
    if (inter.debug & 8192) {
        //dump_matrix(STANDARD, "stdout");
        dump_matrix_mirror_elements(stdout);
        dump_matrix_space_elements(stdout);
    }

    // initialise detector outputs, this is needed for functions and locks that read detector signals
    compute_functions();
    compute_puts();
    fill_detectors();

    if (inter.beam.x && inter.beam.y && !(compute_data_msg_flags & 1) ) {
        compute_data_msg_flags |= 1;
        message("Tuning only beam detector x1/x2 -> computing interferometer only once.\n");
    }

    if (!inter.beam.x && inter.beam.y && !(compute_data_msg_flags & 2)) {
        compute_data_msg_flags |= 2;
        message("Tuning only beam detector along x2 -> computing interferometer only along x1.\n");
    }

    if (inter.beam.x && !inter.beam.y && inter.splot && !(compute_data_msg_flags & 4)) {
        compute_data_msg_flags |= 4;
        message("Consider switching x1 and x2 axis for speed up!\n");
    }

    // start running along x-axis -------------------------------------
    int x_step_index;
    for (x_step_index = 0; x_step_index <= inter.x1.xsteps; x_step_index++) {
        if (inter.x1.xtype == FLOG) {
            x_axis_point = exp(x_min + x_step_index * dx);
            // in order to avoid rounding error we explictly use the max value as last step
            if (x_step_index == inter.x1.xsteps) {
                x_axis_point = exp(x_max);
            }
        } else {
            x_axis_point = x_min + x_step_index*dx;
            
            // in order to avoid rounding error we explictly use the max value as last step
            if (x_step_index == inter.x1.xsteps) {
                x_axis_point = x_max;
            }
        }


        if (inter.beam.x) {
            update_fields = false;
        } else {
            update_fields = true;
        }

        // setting next x value ---------------------------------------
        update_xaxis_values(&inter.x1, x_axis_point);
        
        if (inter.debug & 4) {
            message("xx = %.15g\n", x_axis_point);
        }

        // 3D plot ? --------------------------------------------------
        if (inter.splot) {

            // compute data point now if x2-axis is just beam analyser axis 
            // (but x1 is not)
            if (inter.beam.y) {
                if (!inter.beam.x) {
                    if (update_fields) {
                        tune_parameters();
                    } else {
                        compute_functions();
                        compute_puts();
                        fill_detectors();
                        //                      compute_functions();                                
                    }
                }
                update_fields = false;
            } else {
                update_fields = true;
            }

            // start running along second x-axis ----------------------
            int y_step_index;
            for (y_step_index = 0; y_step_index <= inter.x2.xsteps; y_step_index++) {
                set_progress_action_text("Calculating");
                //set_progress_message_text("");
                
                if (inter.x2.xtype == FLOG) {
                    y_axis_point = exp(y_min + y_step_index * dy);
                    // in order to avoid rounding error we explictly use the max value as last step
                    if (y_step_index == inter.x2.xsteps) {
                        y_axis_point = exp(y_max);
                    }
                } else {
                    y_axis_point = y_min + y_step_index*dy;
                    // in order to avoid rounding error we explictly use the max value as last step
                    if (y_step_index == inter.x2.xsteps) {
                        y_axis_point = y_max;
                    }
                }

                // setting next x2 value -------------------------------
                update_xaxis_values(&inter.x2, y_axis_point);
                
                if (inter.debug & 4) {
                    message("yy = %.15g\n", y_axis_point);
                }

                vlocal.respt = mem.restab + x_step_index * oneline + y_step_index * inter.num_output_cols;

                // writing x2 values to output array -------------------
                if (inter.x1.xtype == FLOG) {
                    *(vlocal.respt++) = x_axis_point / inter.x1.op;
                } else {
                    *(vlocal.respt++) = x_axis_point - inter.x1.op;
                }

                if (inter.x2.xtype == FLOG) {
                    *(vlocal.respt++) = y_axis_point / inter.x2.op;
                } else {
                    *(vlocal.respt++) = y_axis_point - inter.x2.op;
                }
                
                // *** DO THE CALCULATION for this x2 value ----------
                //     (anwrite_resultd write output)
                write_result();
            }
        } else {
            set_progress_action_text("Calculating");
            //set_progress_message_text("");
            
            vlocal.respt = mem.restab + x_step_index * inter.num_output_cols;

            // writing x values to output array -----------------------
            if (inter.x1.xtype == FLOG) {
                *(vlocal.respt++) = x_axis_point / inter.x1.op;
            } else {
                *(vlocal.respt++) = x_axis_point - inter.x1.op;
            }

            // *** DO THE CALCULATION for this x1 value ----------
            //     (and write output)
            write_result();
        }
        
        set_progress_action_text("Calculating");

        if ((inter.trace & 64 || inter.debug & 32) && inter.rebuild == 2) {
            message("x=%s:\n", double_form(x_axis_point));
        }
    }
}


//! Write the result of a calculation to the memory array

/*!
 *
 * \see Test_write_result()
 */
void write_result(void) {
    if (inter.num_diff_cmds) {
        compute_derivative(vlocal.zsolution, 0); // compute a derivative of a y value 
    } else {
        compute_result(vlocal.zsolution); // compute the y value 
    }

    current_point_tot++;
    current_point++;

    if (current_point > init.percentage_step / 100.0 * init.num_points_total) {
        if (!options.quiet) {
            print_progress(init.num_points_total, current_point_tot); // print progress percents 
        }
        current_point = 0;
    }

    create_data_array(vlocal.zsolution); // write result to memory array  
}

//! Compute the derivative

/*!
 * \param zsol complex array of which to take the derivative
 * \param this_derivative_index index of the current differentation
 *
 *
 * \see Test_compute_derivative()
 */
void compute_derivative(complex_t *zsol, int this_derivative_index) {
    // sanity check on input
    assert(this_derivative_index >= 0);

    // compute new data points at which the function
    // value is to be computed 
    double back_step =         *inter.deriv_list[this_derivative_index].xderiv - init.deriv_h;
    double back_half_step =    *inter.deriv_list[this_derivative_index].xderiv - init.deriv_h / 2.0;
    double forward_step =      *inter.deriv_list[this_derivative_index].xderiv + init.deriv_h;
    double forward_half_step = *inter.deriv_list[this_derivative_index].xderiv + init.deriv_h / 2.0;
    double current_point =     *inter.deriv_list[this_derivative_index].xderiv;

    int next_derivative_index = this_derivative_index + 1;

    // check if data points are within the allowed range
    if ((back_half_step >= inter.deriv_list[this_derivative_index].min ||
            !inter.deriv_list[this_derivative_index].lborder) &&
            (forward_half_step <= inter.deriv_list[this_derivative_index].min ||
            !inter.deriv_list[this_derivative_index].uborder)) {
        *inter.deriv_list[this_derivative_index].xderiv = back_half_step;
        // do more differentiations? 
        if (next_derivative_index < inter.num_diff_cmds) {
            compute_derivative(vlocal.zderiv1, next_derivative_index);
        } else {
            compute_result(vlocal.zderiv1);
        }

        *inter.deriv_list[this_derivative_index].xderiv = forward_half_step;
        if (next_derivative_index < inter.num_diff_cmds) {
            compute_derivative(vlocal.zderiv2, next_derivative_index);
        } else {
            compute_result(vlocal.zderiv2);
        }
    } else {
        if (back_half_step < inter.deriv_list[this_derivative_index].min &&
                inter.deriv_list[this_derivative_index].lborder) {
            *inter.deriv_list[this_derivative_index].xderiv = current_point;
            if (next_derivative_index < inter.num_diff_cmds) {
                compute_derivative(vlocal.zderiv1, next_derivative_index);
            } else {
                compute_result(vlocal.zderiv1);
            }

            *inter.deriv_list[this_derivative_index].xderiv = forward_step;
            if (next_derivative_index < inter.num_diff_cmds) {
                compute_derivative(vlocal.zderiv2, next_derivative_index);
            } else {
                compute_result(vlocal.zderiv2);
            }
        } else {
            *inter.deriv_list[this_derivative_index].xderiv = back_step;
            if (next_derivative_index < inter.num_diff_cmds) {
                compute_derivative(vlocal.zderiv1, next_derivative_index);
            } else {
                compute_result(vlocal.zderiv1);
            }

            *inter.deriv_list[this_derivative_index].xderiv = current_point;
            if (next_derivative_index < inter.num_diff_cmds) {
                compute_derivative(vlocal.zderiv2, next_derivative_index);
            } else {
                compute_result(vlocal.zderiv2);
            }
        }
    }

    *inter.deriv_list[this_derivative_index].xderiv = current_point;
    if (inter.debug & 2048) {
        message("x +-h= %g %g\n", back_step, forward_step);
    }

    int output_index;
    for (output_index = 0; output_index < inter.num_outputs; output_index++) {
        zsol[output_index] = z_by_x(z_m_z(vlocal.zderiv2[output_index],
                vlocal.zderiv1[output_index]),
                1.0 / init.deriv_h);
        if (inter.debug & 2048) {
            message("zsol=%s, zabs=%g\n",
                    complex_form15(zsol[output_index]), zabs(zsol[output_index]));
        }
    }
}

//! Compute the result of one given data set (one point along the axis)

/*!
 * Modifies the argument in place.
 *
 * \param z_tmp input complex array (result returned here also)
 *
 * \param z_tmp array of output signals
 *
 * \see Test_compute_result()
 */
void compute_result(complex_t *z_tmp) {

    if (update_fields) {
        // perform all locks, funcs, puts etc and then call solve the matrix and
        // compute the detector signals
        tune_parameters();
    } else {
        // use the previous solution and just re-compute the detector signals
        compute_functions();
        compute_puts();
        fill_detectors();
    }

    // for each defined output set the signal calculated via fill_detectors()
    int i;
    for (i = 0; i < inter.num_outputs; i++) {
        z_tmp[i] = inter.output_data_list[i].signal;
    }
}

void fill_signal_rhs() {
    // now we have computed the carrier fields everywhere we can compute the 
    // sideband sources for the RHS
    // loop over all signals
    int signal_index;

    for (signal_index = 0; signal_index < inter.num_signals; signal_index++) {
        signal_t *signal;
        signal = &(inter.signal_list[signal_index]);

        if(signal->type != NOSIGNAL){
            switch (signal->component_type) {
                case MIRROR:
                    fill_mirror_signal_rhs(signal, &M_ifo_car, &M_ifo_sig);
                    break;
                case BEAMSPLITTER:
                    fill_bs_signal_rhs(signal,&M_ifo_car, &M_ifo_sig);
                    break;
                case SPACE:
                    fill_space_signal_rhs(signal,&M_ifo_car, &M_ifo_sig);
                    break;
                case MODULATOR:
                    fill_modulator_signal_rhs(signal, &M_ifo_car, &M_ifo_sig);
                    break;
                case LIGHT_INPUT:
                    fill_light_signal_rhs(signal);
                    break;
                default:
                    bug_error("rhs_sig");
            }
        }
    }
}

//! Compute fsig amplitude for SIG_X SIG_Y signals

/*!
 * \param sig_node signal node
 * \param q Gaussian beam q parameter
 *
 * \todo sig_node -> signal_node
 **/
complex_t xyamp(node_t *sig_node, complex_t q) {
 
    complex_t txs;
    double nr, w0;

    nr = *(sig_node->n); 
    // TODO fix the amplitude below (i.e. add equation to manual)
    // the factor of two here is empirical but NOT UNDERSTOOD
    // should actually be * (-1) 
    w0 = 2.0 / w0_size(q, nr);
    
    //warn("!!!! %g %g %s\n", nr, w0_size(q, nr), complex_form(q));
    
    // ddb - added in refractive index here to match up with
    //       diff comparison for tilt signals
    txs = z_by_x(q, w0 / nr);
    //printf("node: %s, q: %s, amp: %s (abs: %g angle %g)\n",sig_node.name,complex_form(q),complex_form(txs),zabs(txs), zdeg(txs)); 
    return (txs);
}

//void calc_beamsplitter_signal_vars( beamsplitter_t *beamsplitter, double *factor, double *ph1, double f, double f_c){  
//}

complex_t calc_reflect_tilt_signal(signal_t *signal, complex_t **knm, int in, int out, complex_t X_bar){
    complex_t mult = complex_0;
                
    int l, tn=0, tm=0;
    
    get_tem_modes_from_field_index(&tn, &tm, in);
    
    // do the matrix multiplication, alignment knm matrix is very sparse though
    // as each mode will only couple into at most 2 other modes. This is due to
    // the assumption that the fsig amplitude is small (i.e. small angle) and the
    // coupling is linear
    if(signal->type == SIG_X) {
        l = get_field_index_from_tem(tn + 1, tm);

        if(l < inter.num_fields) { // ensure not coupling to mode higher than we are simulating
             mult = z_pl_z(mult, z_by_z( z_by_x(X_bar, sqrt(tn)), knm[l][out]));
        }
        
        if(tn > 0){
             l = get_field_index_from_tem(tn - 1, tm);
             mult = z_pl_z(mult, z_by_z( z_by_x(X_bar, sqrt(tn)), knm[l][out]));
        }

    } else if(signal->type == SIG_Y) {
        l = get_field_index_from_tem(tn, tm+1);

        if(l < inter.num_fields) // ensure not coupling to mode higher than we are simulating
             mult = z_pl_z(mult, z_by_z( z_by_x(X_bar, -1.0*sqrt(tm+1)), knm[l][out]));

        if(tn > 0){
             l = get_field_index_from_tem(tn, tm - 1);
             mult = z_pl_z(mult, z_by_z( z_by_x(X_bar, sqrt(tm)), knm[l][out]));
        }
    }
    
    return mult;
}

/**
 * Computes the scattering matrix element for a small dioptre modulation of a components focusing power.
 * Only considered on reflection, but probably also applicable on transmission of a lens. This is
 * computed by asuming some perturbing time oscillating C-matrix-term in the ABCD.
 * 
 * Essentially returns a matrix that scatters carrier modes to n+-2 modes of a signal sideband.
 * The direct coupling n->n pick up some phase shift, due to the modulating waist position.
 * 
 * Coupling coefficient returned is not reversed gouy'd.
 * 
 * qin: beam basis being modulated
 * amplitude, phase: amp and phs of signal, phs in degrees
 * P: Focal power of the object being modulated.
 * 
 * returns the coupling term for mode n -> n_
 */
complex_t calc_dioptre_modulation_knn(int n, int n_, complex_t qin, double P,
                                     double amplitude, double phase) {
    
    int dn = n - n_;
    complex_t knn_ = complex_0;

    if (dn == 2 || dn == -2 || dn == 0){
        double zr  = qin.im;
        double z   = qin.re;
        double fac = sqrt((n+1) * (n_+2));

        double eps_z  = zr * (2/(P*P*zr*zr + (P*z-1)*(P*z-1)) - 1) - (z*z)/(zr);
        double eps_zr = (2*z*(P*z-1) + 2*P*zr*zr) / (P*P*zr*zr + (P*z-1)*(P*z-1));

        complex_t eps_q = co(eps_z, eps_zr);

        if (dn == 2)
            knn_ = z_by_z( z_by_x(complex_i, -fac / 4.0), eps_q);
        else if(dn == -2)
            knn_ = z_by_z( z_by_x(complex_i, -fac / 4.0), cconj(eps_q));
        else
            knn_ = co(1, 0.25 * (2*n+1)*eps_z);
    } else {
        knn_ = complex_0;
    }

    return z_by_xph(knn_, amplitude, phase);
}

void _fill_dioptre_mod_knn(signal_t *signal, double Px, double Py, node_t* nodes_options[][2],
                            size_t num_node_opts, int comp_index, ifo_matrix_vars_t* M_sig) {

    int n_p2, n_m2, na, nb, nx, nd, d, i, j, k, n, m, sidx, f;
    complex_t qin = complex_0;
    complex_t qout = complex_0;
    char xy_opts[2] = {'x', 'y'};
    complex_t *srhs = (complex_t*)M_sig->rhs_values;
    double P;

    for(f=0; f<M_sig->num_frequencies; f++){
        frequency_t *f_sig = M_sig->frequencies[f];
        double phase = f_sig->order * signal->phase;
        double factor = signal->amplitude * EPSILON * 0.5; //Epsilon/2 for fsig

        for(i=0; i<1; i++){
            node_t *node_from = nodes_options[i][0];
            node_t *node_to   = nodes_options[i][1];
            
            if (!node_from->gnd_node && !node_to->gnd_node) {
                for(j=0; j<2; j++){
                    char x_or_y = xy_opts[j];
                    
                    if(x_or_y == 'x'){
                        get_node_q_to(node_from, comp_index, &qin, NULL);
                        get_node_q_from(node_to, comp_index, &qout, NULL);
                    } else {
                        get_node_q_to(node_from, comp_index, NULL, &qin);
                        get_node_q_from(node_to, comp_index, NULL, &qout);
                    }

                    for(k=0; k<inter.num_fields; k++){
                        get_tem_modes_from_field_index(&n, &m, k);

                        for(d=-2; d<=2; d+=4){ // for the n-2, n, n+2 options for the scattering
                            if(x_or_y == 'x'){
                                nx = n;    // index of mode we start in
                                nd = nx+d; // difference
                                na = nx+d; // x index in get_field_index_from_tem
                                nb = m;    // y index in get_field_index_from_tem
                                P  = Px;   // Focal power
                            } else {
                                nx = m;
                                nd = nx+d;
                                na = n;
                                nb = nx+d;
                                P  = Py;
                            }

                            int valid = (nd >= 0 && nd < inter.num_fields) ? get_field_index_from_tem(na, nb) : -1;
                            
                            if(valid >= 0 && valid < inter.num_fields) { // check if this is a valid mode
                                // if so, take this carrier mode and scatter it into a higher order mode
                                complex_t k_n_n_2 = calc_dioptre_modulation_knn(nx, nd, qin, P, factor, phase);
                                // Reverse some gouy phase as we've shifted from qin to qout 
                                k_n_n_2 = z_by_phr(k_n_n_2, -1.0 * (((nd + 0.5) * gouy(qout) - ((nx + 0.5) * gouy(qout)))));

                                // rhs index for node first port + index offset for second port field
                                sidx = M_sig->node_rhs_idx_1[node_to->list_index] + get_node_rhs_idx(2, f, get_field_index_from_tem(na, nb), M_sig->num_frequencies);
                                
                                if(f_sig->order == -1) // lower sideband is conjugated
                                    z_inc_zc(&srhs[sidx], z_by_z(signal->ar2[k], k_n_n_2));
                                else
                                    z_inc_z (&srhs[sidx], z_by_z(signal->ar2[k], k_n_n_2));
                            }
                        }
                    }
                }
            }
        }
    }
}

void fill_bs_signal_rhs(signal_t *signal, ifo_matrix_vars_t* M_car, ifo_matrix_vars_t* M_sig){
    assert(M_sig != NULL);
    assert(M_car != NULL);
    assert(signal != NULL);
    
    int f,i, tn,tm;
    beamsplitter_t *bs = &inter.bs_list[signal->component_index];
    
    node_t *node1 = &inter.node_list[bs->node1_index];
    node_t *node2 = &inter.node_list[bs->node2_index];
    node_t *node3 = &inter.node_list[bs->node3_index];
    node_t *node4 = &inter.node_list[bs->node4_index];

    for(i=0; i<bs->num_motions; i++){
        // If a signal is applied and we have some motions available we apply
        // the signal to the motion term, which then generates the sidebands
        // in the matrix.
        int xidx = bs->x_rhs_idx + i;
        
        switch(bs->motion_type[i]) {
            case Z:
                if(signal->type == SIG_PHS) {
                    assert(bs->long_tf != NULL);
                    double k = TWOPI/init.lambda;
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_1, EPSILON*signal->amplitude/(init.x_scale*k), signal->phase);
                } else if(signal->type == SIG_Z) {
                    assert(bs->long_tf != NULL);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_1, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                } else if(signal->type == SIG_FZ) {
                    assert(bs->long_tf != NULL);
                    // compute transfer function for positive signal frequency
                    complex_t G_Omega = z_by_x(calc_transfer_func(bs->long_tf, co(0, TWOPI *inter.fsig)), 1.0/bs->mass);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(G_Omega, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                }
                break;
            case ROTX:
                if(signal->type == SIG_X){
                    assert(bs->rot_x_tf != NULL);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_1, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                } else if(signal->type == SIG_FRX) {
                    assert(bs->rot_x_tf != NULL);
                    // compute transfer function for positive signal frequency
                    complex_t G_Omega = z_by_x(calc_transfer_func(bs->rot_x_tf, co(0, TWOPI *inter.fsig)), 1.0/bs->Ix);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(G_Omega, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                }
                break;
                
            case ROTY:
                if(signal->type == SIG_Y) {
                    assert(bs->rot_y_tf != NULL);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_1, EPSILON*signal->amplitude/init.x_scale, signal->phase); 
                } else if(signal->type == SIG_FRY) {
                    assert(bs->rot_y_tf != NULL);
                    // compute transfer function for positive signal frequency
                    complex_t G_Omega = z_by_x(calc_transfer_func(bs->rot_y_tf, co(0, TWOPI *inter.fsig)), 1.0/bs->Iy);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(G_Omega, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                }
                break;
            default:
                bug_error("not handled");
        }
    }
    
    if((signal->type == SIG_AMP || signal->type == SIG_PHS) && bs->mass == 0.0){
        for(f=0; f<M_sig->num_frequencies; f++){
            frequency_t *f_sig = M_sig->frequencies[f];
            double phase = (90.0 + f_sig->order * signal->phase);
            double _factor = signal->amplitude * EPSILON * 0.5; //Epsilon/2 for fsig
            double turn, _ph1;
            
            if(signal->type == SIG_AMP)
                phase -= 90;
            
            // TODO check refractive index
            _factor *= 2.0 * (1.0 + f_sig->carrier->f / inter.f0) * sqrt(bs->R);
            _ph1 = bs->phi * ((1.0 + f_sig->f / inter.f0) + (1.0 + f_sig->carrier->f / inter.f0));
            
            if(!node1->gnd_node && !node2->gnd_node){
                double factor =  _factor * cos(bs->alpha_1 * RAD);
                double ph1    =  _ph1 * cos(bs->alpha_1 * RAD);
                double phase1 = phase + ph1;

                // get the index for the signal field of frequency f and mode i and 
                // the carrier field which will be its source
                int sidx12 = M_sig->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(2, f, 0, M_sig->num_frequencies);
                int cidx12 = M_car->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, 0, M_car->num_frequencies);

                int sidx21 = M_sig->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(2, f, 0, M_sig->num_frequencies);
                int cidx21 = M_car->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, 0, M_car->num_frequencies);

                // idea is we compute: signal fields = factor*exp(i*phase)) * (knm * carrier fields)
                complex_t *sig_vec = &(((complex_t*)M_sig->rhs_values)[sidx12]);
                complex_t *car_vec = &(((complex_t*)M_car->rhs_values)[cidx12]);

                cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
                        &complex_1, &(bs->knm.k12[0][0]), inter.num_fields, car_vec, 1, &complex_0, sig_vec, 1);

                sig_vec = &(((complex_t*)M_sig->rhs_values)[sidx21]);
                car_vec = &(((complex_t*)M_car->rhs_values)[cidx21]);

                cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
                        &complex_1, &(bs->knm.k21[0][0]), inter.num_fields, car_vec, 1, &complex_0, sig_vec, 1);

                // now apply all the phase factors etc.
                for(i=0; i<inter.num_fields;i++){
                    get_tem_modes_from_field_index(&tn, &tm, i);
                    turn = turnit(tn);

                    sidx12 =  M_sig->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(2, f, i, M_sig->num_frequencies);
                    ((complex_t*)M_sig->rhs_values)[sidx12] = z_by_xph(((complex_t*)M_sig->rhs_values)[sidx12], turn * factor, phase1);

                    sidx21 =  M_sig->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(2, f, i, M_sig->num_frequencies);
                    ((complex_t*)M_sig->rhs_values)[sidx21] = z_by_xph(((complex_t*)M_sig->rhs_values)[sidx21], turn * factor, phase1);

                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) { 
                        ((complex_t*)M_sig->rhs_values)[sidx21].im *= -1;
                        ((complex_t*)M_sig->rhs_values)[sidx12].im *= -1;
                    }
                }
            }

            if(!node3->gnd_node && !node4->gnd_node){
                double factor =  _factor * cos(bs->alpha_2 * RAD);
                double ph1    =  _ph1 * cos(bs->alpha_2 * RAD);
                double phase2 = phase - ph1 + 180.0 * f_sig->order;

                // get the index for the signal field of frequency f and mode i and 
                // the carrier field which will be its source
                int sidx34 = M_sig->node_rhs_idx_1[node4->list_index] + get_node_rhs_idx(2, f, 0, M_sig->num_frequencies);
                int cidx34 = M_car->node_rhs_idx_1[node3->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, 0, M_car->num_frequencies);

                int sidx43 = M_sig->node_rhs_idx_1[node3->list_index] + get_node_rhs_idx(2, f, 0, M_sig->num_frequencies);
                int cidx43 = M_car->node_rhs_idx_1[node4->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, 0, M_car->num_frequencies);

                // idea is we compute: signal fields = factor*exp(i*phase)) * (knm * carrier fields)
                complex_t *sig_vec = &(((complex_t*)M_sig->rhs_values)[sidx34]);
                complex_t *car_vec = &(((complex_t*)M_car->rhs_values)[cidx34]);

                cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
                        &complex_1, &(bs->knm.k34[0][0]), inter.num_fields, car_vec, 1, &complex_0, sig_vec, 1);

                sig_vec = &(((complex_t*)M_sig->rhs_values)[sidx43]);
                car_vec = &(((complex_t*)M_car->rhs_values)[cidx43]);

                cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
                        &complex_1, &(bs->knm.k43[0][0]), inter.num_fields, car_vec, 1, &complex_0, sig_vec, 1);

                // now apply all the phase factors etc.
                for(i=0; i<inter.num_fields;i++){
                    get_tem_modes_from_field_index(&tn, &tm, i);
                    turn = turnit(tn);

                    sidx34 =  M_sig->node_rhs_idx_1[node4->list_index] + get_node_rhs_idx(2, f, i, M_sig->num_frequencies);
                    ((complex_t*)M_sig->rhs_values)[sidx34] = z_by_xph(((complex_t*)M_sig->rhs_values)[sidx34], turn * factor, phase2);

                    sidx43 =  M_sig->node_rhs_idx_1[node3->list_index] + get_node_rhs_idx(2, f, i, M_sig->num_frequencies);
                    ((complex_t*)M_sig->rhs_values)[sidx43] = z_by_xph(((complex_t*)M_sig->rhs_values)[sidx43], turn * factor, phase2);

                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1){
                        ((complex_t*)M_sig->rhs_values)[sidx43].im *= -1;
                        ((complex_t*)M_sig->rhs_values)[sidx34].im *= -1;
                    }
                }
            }
        }
    
    } else if((signal->type == SIG_X && bs->Ix == 0) || (signal->type == SIG_Y && bs->Iy == 0)) { 

        double ph2; // phase picked up on side 2
        
        // invert effective y tilt seen on side 2
        if (signal->type == SIG_Y) {
            ph2 = 180.0;
        } else {
            ph2 = 0.0;
        }
        
        for(f=0; f<M_sig->num_frequencies; f++){
            frequency_t *f_sig = M_sig->frequencies[f];
            double phase = f_sig->order * signal->phase;
            double factor = signal->amplitude * EPSILON * 0.5; //Epsilon/2 for fsig
            double turn = 1.0;
           
            complex_t txs1 = complex_0;
            complex_t txs2 = complex_0;
            complex_t txs3 = complex_0;
            complex_t txs4 = complex_0;

            complex_t q = complex_0;

            if ((!node1->gnd_node) && (!node2->gnd_node)) {
                if (signal->type == SIG_X) {
                    q = node1->qx;
                } else {
                    q = node1->qy;
                }

                if (node1->component_index == signal->component_index) {
                    q = cminus(cconj(q));
                }
                
                txs1 = xyamp(node1, q);

                if (signal->type == SIG_X) {
                    q = node2->qx;
                } else {
                    q = node2->qy;
                }
                
                if (node2->component_index == signal->component_index) {
                    q = cminus(cconj(q));
                }
                
                txs2 = xyamp(node2, q);
            }

            if (!node3->gnd_node && !node4->gnd_node) {
                if (signal->type == SIG_X) {
                    q = node3->qx;
                } else {
                    q = node3->qy;
                }
                if (node3->component_index == signal->component_index) {
                    q = cminus(cconj(q));
                }
                txs3 = xyamp(node3, q);

                if (signal->type == SIG_X) {
                    q = node4->qx;
                } else {
                    q = node4->qy;
                }
                if (node4->component_index == signal->component_index) {
                    q = cminus(cconj(q));
                }
                txs4 = xyamp(node4, q);
            }

            // the old Gouy phase problem again: I should now do a proper
            // rev_gouy with txs1 and txs2, but I think it will just 
            // produce a constant phase of 90 deg. So this is what I do instead.

            txs1 = co(0, zmod(txs1));
            txs2 = co(0, zmod(txs2));
            txs3 = co(0, zmod(txs3));
            txs4 = co(0, zmod(txs4));

            int n, i, j;
            int cidx, sidx;

            complex_t *crhs = (complex_t*)M_car->rhs_values;
            complex_t *srhs = (complex_t*)M_sig->rhs_values;

            // compute purely reflected carrier field, do this by taking the incoming beam and computing matrix multiplication
            // with the reflection elements of the matrix. Store reflected field in memory already allocated for this in the
            // signal structure
            int ni, mi, nj, mj;
                
            for(i=0; i < inter.num_fields; i++){
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                signal->ar1[i] = complex_0;
                signal->ar2[i] = complex_0;
                signal->ar3[i] = complex_0;
                signal->ar4[i] = complex_0;

                for(j=0; j < inter.num_fields; j++){
                    get_tem_modes_from_field_index(&nj, &mj, j);
                    
                    if(!node1->gnd_node && !node2->gnd_node) {
                        if(include_mode_coupling(&bs->a_cplng_21, ni,nj,mi,mj)){
                            cidx = M_car->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, j, M_car->num_frequencies);
                            signal->ar1[i] = z_pl_z(signal->ar1[i], z_by_z(*bs->a21f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                        }
                        
                        if(include_mode_coupling(&bs->a_cplng_12, ni,nj,mi,mj)){
                            cidx = M_car->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, j, M_car->num_frequencies);
                            signal->ar2[i] = z_pl_z(signal->ar2[i], z_by_z(*bs->a12f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                        }
                    }

                    if(!node3->gnd_node && !node4->gnd_node) {
                        if(include_mode_coupling(&bs->a_cplng_43, ni,nj,mi,mj)){
                            cidx = M_car->node_rhs_idx_1[node4->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, j, M_car->num_frequencies);
                            signal->ar3[i] = z_pl_z(signal->ar3[i], z_by_z(*bs->a43f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                        }
                        
                        if(include_mode_coupling(&bs->a_cplng_34, ni,nj,mi,mj)){
                            cidx = M_car->node_rhs_idx_1[node3->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, j, M_car->num_frequencies);
                            signal->ar4[i] = z_pl_z(signal->ar4[i], z_by_z(*bs->a34f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                        }
                    }
                }
            }

            for (n = 0; n < inter.num_fields; n++) {
                int tn, tm;
                int field;
                //double turn;
                get_tem_modes_from_field_index(&tn, &tm, n);
                // TODO fix this! The `turn' below should be fixed once this is better understood!
                //turn = turnit(tn);
                //turn = 1.0;

                complex_t a12 = complex_0;
                complex_t a21 = complex_0;
                complex_t a34 = complex_0;
                complex_t a43 = complex_0;

                if (tn && signal->type == SIG_X) {
                    field = get_field_index_from_tem(tn - 1, tm);
                    if(!node1->gnd_node && !node2->gnd_node){
                        a12 = z_by_x(z_by_z(signal->ar2[field], cconj(txs2)), sqrt(tn));
                        a21 = z_by_x(z_by_z(signal->ar1[field], cconj(txs1)), sqrt(tn));
                    }

                    if(!node3->gnd_node && !node4->gnd_node){
                        a34 = z_by_x(z_by_z(signal->ar4[field], cconj(txs4)), sqrt(tn));
                        a43 = z_by_x(z_by_z(signal->ar3[field], cconj(txs3)), sqrt(tn));
                    }
                } else if (tm && signal->type == SIG_Y) {
                    field = get_field_index_from_tem(tn, tm - 1);

                    if(!node1->gnd_node && !node2->gnd_node){
                        a12 = z_by_x(z_by_z(signal->ar2[field], cconj(txs2)), sqrt(tm));
                        a21 = z_by_x(z_by_z(signal->ar1[field], cconj(txs1)), sqrt(tm));
                    }

                    if(!node3->gnd_node && !node4->gnd_node){
                        a34 = z_by_x(z_by_z(signal->ar4[field], cconj(txs4)), sqrt(tm));
                        a43 = z_by_x(z_by_z(signal->ar3[field], cconj(txs3)), sqrt(tm));
                    }
                }

                int ttn, ttm;
                if (tn + tm < inter.tem) {
                    if (signal->type == SIG_X) {
                        ttn = tn + 1;
                        ttm = tm;
                        
                        field = get_field_index_from_tem(ttn, ttm);

                        if(!node1->gnd_node && !node2->gnd_node){
                            a12 = z_pl_z(a12,z_by_x(z_by_z(signal->ar2[field], txs2), -1.0 * sqrt(ttn)));
                            a21 = z_pl_z(a21,z_by_x(z_by_z(signal->ar1[field], txs1), -1.0 * sqrt(ttn)));
                        }

                        if(!node3->gnd_node && !node4->gnd_node){
                            a34 = z_pl_z(a34,z_by_x(z_by_z(signal->ar4[field], txs4), -1.0 * sqrt(ttn)));
                            a43 = z_pl_z(a43,z_by_x(z_by_z(signal->ar3[field], txs3), -1.0 * sqrt(ttn)));
                        }
                    } else {
                        ttn = tn;
                        ttm = tm + 1;
                        
                        field = get_field_index_from_tem(ttn, ttm);

                        if(!node1->gnd_node && !node2->gnd_node){
                            a12 = z_pl_z(a12,z_by_x(z_by_z(signal->ar2[field], txs2), -1.0 * sqrt(ttm)));
                            a21 = z_pl_z(a21,z_by_x(z_by_z(signal->ar1[field], txs1), -1.0 * sqrt(ttm)));
                        }

                        if(!node3->gnd_node && !node4->gnd_node){
                            a34 = z_pl_z(a34,z_by_x(z_by_z(signal->ar4[field], txs4), -1.0 * sqrt(ttm)));
                            a43 = z_pl_z(a43,z_by_x(z_by_z(signal->ar3[field], txs3), -1.0 * sqrt(ttm)));
                        }
                    }
                }
                
                double AoI_side_1 = 1;
                double AoI_side_2 = 1;
                
                // additional scaling from coordinate system misalignments at non-normal incidence
                // see section 4.11 (Misalignment angles at a beam splitter) in manual.
                // This only affects pitch signals
                if (signal->type == SIG_Y){
                    // the angle of incidence is different on each side depending on
                    // the refractive indices
                    AoI_side_1 *= cos(bs->alpha_1 * RAD);
                    AoI_side_2 *= cos(bs->alpha_2 * RAD);
                }

                if (!node1->gnd_node) {
                    sidx = M_sig->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(2, f, n, M_sig->num_frequencies);
                    srhs[sidx] = z_by_xph(a21, turn * factor * AoI_side_1, phase);
                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) srhs[sidx].im *= -1;
                }

                if (!node2->gnd_node) {
                    sidx = M_sig->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(2, f, n, M_sig->num_frequencies);
                    srhs[sidx] = z_by_xph(a12, turn * factor * AoI_side_1, phase);
                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) srhs[sidx].im *= -1;
                }

                if (!node3->gnd_node) {
                    sidx = M_sig->node_rhs_idx_1[node3->list_index] + get_node_rhs_idx(2, f, n, M_sig->num_frequencies);
                    srhs[sidx] = z_by_xph(a43, turn * factor * AoI_side_2, phase + ph2);
                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) srhs[sidx].im *= -1;
                }

                if (!node4->gnd_node) {
                    sidx = M_sig->node_rhs_idx_1[node4->list_index] + get_node_rhs_idx(2, f, n, M_sig->num_frequencies);
                    srhs[sidx] = z_by_xph(a34, turn * factor * AoI_side_2, phase + ph2);
                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) srhs[sidx].im *= -1;
                }
            }
        }
    } else if(signal->type == SIG_RC) { 
        int cidx;
        complex_t *crhs = (complex_t*)M_car->rhs_values;
        //complex_t *srhs = (complex_t*)M_sig->rhs_values;

        // compute purely reflected carrier field, do this by taking the incoming beam and computing matrix multiplication
        // with the reflection elements of the matrix. Store reflected field in memory already allocated for this in the
        // signal structure
        for(f=0; f<M_sig->num_frequencies; f++){
            frequency_t *f_sig = M_sig->frequencies[f];

            int i, j;
            int ni, mi, nj, mj;
            for(i=0; i < inter.num_fields; i++){
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                signal->ar1[i] = complex_0;
                signal->ar2[i] = complex_0;
                signal->ar3[i] = complex_0;
                signal->ar4[i] = complex_0;

                for(j=0; j < inter.num_fields; j++){
                    get_tem_modes_from_field_index(&nj, &mj, j);
                    
                    if(!node1->gnd_node && !node2->gnd_node) {
                        if(include_mode_coupling(&bs->a_cplng_21, ni,nj,mi,mj)){
                            cidx = M_car->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, j, M_car->num_frequencies);
                            signal->ar1[i] = z_pl_z(signal->ar1[i], z_by_z(*bs->a21f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                        }
                        
                        if(include_mode_coupling(&bs->a_cplng_12, ni,nj,mi,mj)){
                            cidx = M_car->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, j, M_car->num_frequencies);
                            signal->ar2[i] = z_pl_z(signal->ar2[i], z_by_z(*bs->a12f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                        }
                    }

                    if(!node3->gnd_node && !node4->gnd_node) {
                        if(include_mode_coupling(&bs->a_cplng_43, ni,nj,mi,mj)){
                            cidx = M_car->node_rhs_idx_1[node4->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, j, M_car->num_frequencies);
                            signal->ar3[i] = z_pl_z(signal->ar3[i], z_by_z(*bs->a43f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                        }
                        
                        if(include_mode_coupling(&bs->a_cplng_34, ni,nj,mi,mj)){
                            cidx = M_car->node_rhs_idx_1[node3->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, j, M_car->num_frequencies);
                            signal->ar4[i] = z_pl_z(signal->ar4[i], z_by_z(*bs->a34f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                        }
                    }
                }
            }
        }   

        // Different reflections to compute
        node_t *nodes[4][2] = {
            {node1, node2},
            {node2, node1},
            {node3, node4},
            {node4, node3}
        };

        /*
        There is a TODO item here. ddb
        This modulation calculation assumes that a reflected beam stays in the same reflected basis.
        This basis is then modulated from the change in focal power. However, if there is a mismatch
        from one node to another and we modulate the focal power, we need an additional mode basis
        change from the modulated directly reflected basis to the final basis. This is a bit annoying
        as we don't store this anywhere as a separate K matrix. Maybe we can or just recompute it here.
        Either way, for now just warn if it's happening so I can see in future what could be a wrong
        result.
        
        1+2+4+8 bitflag, see set_k_beamsplitter -> set_coupling_info call
        */
        if((bs->mismatching & (1+2+4+8)) && !node1->gnd_node)
            gerror("Mismatching at a beamsplitter with Rc fsig not supported yet\n");

        // TODO need to handle AoI pon other side
        double Px = cos(bs->alpha_1*RAD) * 2.0 / bs->Rcx;
        double Py = 2.0 / bs->Rcy;

        _fill_dioptre_mod_knn(signal, Px, Py, nodes, 4, bs->comp_index, M_sig);
    }
}

void fill_light_signal_rhs(signal_t *signal) {
    assert(signal);
    assert(inter.num_signal_freqs > 0);
    assert(signal->component_type == LIGHT_INPUT);

    //(void)signal; // mute compiler warning, probably should change function though
    
    // Now we must insert all the various sources to the RHS vector
    // for the carrier matrix
    int j;
    
    light_in_t *light_input;
    light_input = &(inter.light_in_list[signal->component_index]);
    
    node_t* n = &inter.node_list[light_input->node_index];
    
    int rhs_idx_sig = 0;
    
    if(light_input->node_port == 2) { // first port in the input
      rhs_idx_sig = M_ifo_sig.node_rhs_idx_2[n->list_index];
    } else if(light_input->node_port == 1) { // second port is the input
      rhs_idx_sig = M_ifo_sig.node_rhs_idx_1[n->list_index];
    } else
      bug_error("Node did not have direction set when filling in RHS vector");
    
    double sig_factor = 0.0;
    double sig_phase = 0.0;
    
    double sig_mod_phase = 0; // Phase of modulation
    
    sig_factor = signal->amplitude * EPSILON * 0.5;
    
    switch(signal->type){
    case SIG_AMP:
      sig_mod_phase = 0; // 0 for amplitude modulation
      sig_factor *= 0.5; // energy conserving amplitude modulation uses
      // m/4 rather than m/2
      sig_phase = 0.0;
      break;
      
    case SIG_PHS:
      sig_mod_phase = 90; // for phase/frequency modulation;
      sig_phase = 0.0;
      
      break;
    case SIG_FRQ:
      sig_mod_phase = 90; // for phase/frequency modulation
      // adf changed to -90, because of factor shpuld be 1/iOmega, 28.05.2016
      sig_phase = -90.0;
      
      if (inter.fsig == 0) {
        sig_factor = 1e20;
        warn("fsig=0 for frequency modulation is not useful!\n");
      } else {
        // extra 2pi to convert from W/rad/Hz to W/Hz, adf 28.05.2016
        sig_factor /= (2.0 * PI * 2.0 * PI * inter.fsig);
      }
      
      break;
    default:
      bug_error("wrong signal type for input");
    }
    
    for (j = 0; j < inter.num_fields; j++) {
      // the factor of 2 here comes from the fact we only model
      // the positive frequency components. This wasn't a problem until
      // radiation pressure was added as there were no effects where effects
      // were proportional to the power. With RP though we need to use the
      // proper scaling.
      complex_t input_amplitude = z_by_ph(co(sqrt(2*light_input->I0 / init.epsilon_0_c), 0.0), light_input->phase);
      input_amplitude = z_by_z(input_amplitude, light_input->power_coeff_list[j]);
      
      if(input_amplitude.im == 0.0 && input_amplitude.re == 0.0)
        continue;
      
      int k;
      
      // find the signal sidebands of the laser carrier
      for(k=0; k<inter.num_signal_freqs; k++){
        frequency_t *freq_sig = inter.signal_f_list[k];
        
        // if this signals carrier is this inputs frequency...
        if(freq_sig->carrier == light_input->f){
          int sidx = rhs_idx_sig + get_node_rhs_idx(1, freq_sig->index, j, M_ifo_sig.num_frequencies);
          
          ((complex_t*)M_ifo_sig.rhs_values)[sidx] = z_by_xph(input_amplitude, sig_factor, sig_mod_phase + freq_sig->order * (signal->phase+sig_phase));
          
          // check if we need to conjugate lower sideband
          if(freq_sig->order == -1)
            ((complex_t*)M_ifo_sig.rhs_values)[sidx].im *= -1;
        }
      }
    }
}

void calc_mirror_signal_vars(signal_t *signal, mirror_t *mirror, double *factor, double *phase, double *ph1, double f, double f_c){
    assert(signal != NULL);
    assert(mirror != NULL);
    assert(factor != NULL);
    assert(phase != NULL);
    assert(ph1 != NULL);
    
    // amplitude modulation (test)
    if (signal->type == SIG_AMP) {
        *phase = *phase - 90.0;
        // energy conserving amplitude modulation uses EPSILON/4
        *factor *= 0.5 * sqrt(mirror->R);
    } else if(signal->type == SIG_PHS) {
        *factor *= 2.0 * (1 + f_c / inter.f0) * sqrt(mirror->R);
    } else if(signal->type == SIG_Z) {
        double k = TWOPI / init.lambda;
        *factor *= 2.0 * k *(1 + f_c / inter.f0) * sqrt(mirror->R);
    }

    *ph1 = mirror->phi * ((1 + f / inter.f0) + (1 + f_c / inter.f0));
}

void fill_mirror_signal_rhs(signal_t *signal, ifo_matrix_vars_t* M_car, ifo_matrix_vars_t* M_sig){
    assert(M_sig != NULL);
    assert(M_car != NULL);
    assert(signal != NULL);
    
    int f,i, tn,tm;
    mirror_t *mirror = &inter.mirror_list[signal->component_index];
                
    node_t *node1 = &inter.node_list[mirror->node1_index];
    node_t *node2 = &inter.node_list[mirror->node2_index];
    
    for(i=0; i<mirror->num_motions; i++) {
        // If a signal is applied and we have some motions available we apply
        // the signal to the motion term, which then generates the sidebands
        // in the matrix.
        int xidx = mirror->x_rhs_idx + i;
        
        switch(mirror->motion_type[i]) {
            case Z:
                if(signal->type == SIG_PHS) {
                    assert(mirror->long_tf != NULL);
                    double k = TWOPI/init.lambda;
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_m1, EPSILON*signal->amplitude/(init.x_scale*k), signal->phase);
                } else if(signal->type == SIG_Z) {
                    assert(mirror->long_tf != NULL);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_1, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                } else if(signal->type == SIG_FZ) {
                    assert(mirror->long_tf != NULL);
                    // compute transfer function for positive signal frequency
                    complex_t G_Omega = z_by_x(calc_transfer_func(mirror->long_tf, co(0, TWOPI *inter.fsig)), 1.0/mirror->mass);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(G_Omega, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                }
                break;
                
            case ROTX:
                if(signal->type == SIG_X){
                    assert(mirror->rot_x_tf != NULL);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_1, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                } else if(signal->type == SIG_FRX) {
                    assert(mirror->rot_x_tf != NULL);
                    // compute transfer function for positive signal frequency
                    complex_t G_Omega = z_by_x(calc_transfer_func(mirror->rot_x_tf, co(0, TWOPI *inter.fsig)), 1.0/mirror->Ix);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(G_Omega, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                }
                break;
                
            case ROTY:
                if(signal->type == SIG_Y) {
                    assert(mirror->rot_y_tf != NULL);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_1, EPSILON*signal->amplitude/init.x_scale, signal->phase); 
                } else if(signal->type == SIG_FRY) {
                    assert(mirror->rot_y_tf != NULL);
                    // compute transfer function for positive signal frequency
                    complex_t G_Omega = z_by_x(calc_transfer_func(mirror->rot_y_tf, co(0, TWOPI *inter.fsig)), 1.0/mirror->Iy);
                    ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(G_Omega, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                }
                break;
                
            case SURFACE:
                if(i == (mirror->num_motions-1) + signal->surface_mode_index) {
                    if(signal->type == SIG_SURF) {
                        assert(mirror->surface_motions_tf != NULL);
                        assert(mirror->surface_motions_tf[signal->surface_mode_index] != NULL);
                        ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(complex_1, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                    } else if(signal->type == SIG_FSURF) {
                        assert(mirror->surface_motions_tf != NULL);
                        assert(mirror->surface_motions_tf[signal->surface_mode_index] != NULL);
                        // compute transfer function for positive signal frequency
                        complex_t G_Omega = calc_transfer_func(mirror->surface_motions_tf[signal->surface_mode_index], co(0, TWOPI *inter.fsig));
                        ((complex_t*)M_sig->rhs_values)[xidx] = z_by_xph(G_Omega, EPSILON*signal->amplitude/init.x_scale, signal->phase);
                    }
                }
                break;
            default:
                bug_error("not handled");
        }
    }
    
    // In the case we have an infinite mass mirror
    if((signal->type == SIG_AMP || signal->type == SIG_PHS || signal->type == SIG_Z) && mirror->mass == 0){
        for(f=0; f<M_sig->num_frequencies; f++){
            // for each signal frequency compute the generated 
            // sidebands on reflection and transmission
            
            frequency_t *f_sig = M_sig->frequencies[f];
            double phase = (90.0 + f_sig->order * signal->phase);
            double factor = signal->amplitude * EPSILON * 0.5; //Epsilon/2 for fsig
            double turn, ph1;

            calc_mirror_signal_vars(signal, mirror, &factor, &phase, &ph1, f_sig->f, f_sig->carrier->f);

            if(!node1->gnd_node){

                // get the index for the signal field of frequency f and mode i and 
                // the carrier field which will be its source
                int sidx = M_sig->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(2, f, 0, M_sig->num_frequencies);
                // get the incoming field at the mirror as we then have to compute the purely
                // reflected part of the field by multiplying with the mirror knm matrix. We can't use
                // the reflected carrier field as this will contain the non-reflected tranmitted carrier field too
                int cidx = M_car->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(1, f_sig->carrier->index, 0, M_car->num_frequencies);
                
                // idea is we compute: signal fields = factor*exp(i*phase)) * (knm * carrier fields)
                complex_t *sig_vec = &(((complex_t*)M_sig->rhs_values)[sidx]);
                complex_t *car_vec = &(((complex_t*)M_car->rhs_values)[cidx]);

                // multiply carrier vector by knm matrix and store result in signal vector
                cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
                        &complex_1, &(mirror->knm.k11[0][0]), inter.num_fields, car_vec, 1, &complex_0, sig_vec, 1);

                // now apply all the phase factors etc.
                for(i=0; i<inter.num_fields;i++){
                    get_tem_modes_from_field_index(&tn, &tm, i);
                    turn = turnit(tn);
                    sidx =  M_sig->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(2, f, i, M_sig->num_frequencies);

                    ((complex_t*)M_sig->rhs_values)[sidx] = z_by_xph(((complex_t*)M_sig->rhs_values)[sidx], turn * factor, phase + ph1);

                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) ((complex_t*)M_sig->rhs_values)[sidx].im *= -1;
                }
            }

            if(!node2->gnd_node){

                // get the index for the signal field of frequency f and mode i and 
                // the carrier field which will be its source
                int sidx = M_sig->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(1, f, 0, M_sig->num_frequencies);
                // get the incoming field at the mirror as we then have to compute the purely
                // reflected part of the field by multiplying with the mirror knm matrix. We can't use
                // the reflected carrier field as this will contain the non-reflected tranmitted field too
                int cidx = M_car->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(2, f_sig->carrier->index, 0, M_car->num_frequencies);

                complex_t *sig_vec = &(((complex_t*)M_sig->rhs_values)[sidx]);
                complex_t *car_vec = &(((complex_t*)M_car->rhs_values)[cidx]);

                cblas_zgemv (CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
                        &complex_1, &(mirror->knm.k22[0][0]), inter.num_fields, car_vec, 1, &complex_0, sig_vec, 1);

                // now apply all the phase factors etc.
                for(i=0; i<inter.num_fields;i++){
                    get_tem_modes_from_field_index(&tn, &tm, i);
                    turn = turnit(tn);
                    sidx =  M_sig->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(1, f, i, M_sig->num_frequencies);

                    ((complex_t*)M_sig->rhs_values)[sidx] = z_by_xph(((complex_t*)M_sig->rhs_values)[sidx], turn * factor, phase - ph1 + 180 * f_sig->order);

                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) ((complex_t*)M_sig->rhs_values)[sidx].im *= -1;
                }
            }
        }

    } else if((signal->type == SIG_X && mirror->Ix == 0) || (signal->type == SIG_Y && mirror->Iy == 0)) { 

        double ph2;
        // when the fsig is vertical the apparent sign at the back surface is reversed
        if (signal->type == SIG_Y)
            ph2 = 180.0;
        else
            ph2 = 0.0;
        
        double nr1 = 1.0, nr2 = 1.0;
        
        if(node1->n) nr1 = *node1->n;
        if(node2->n) nr2 = *node2->n;
        
        for(f=0; f<M_sig->num_frequencies; f++){
            frequency_t *f_sig = M_sig->frequencies[f];
            // reflection 
            double phase_r = f_sig->order * signal->phase; // amplitude modulated for alignment signals
            double factor_r = signal->amplitude * EPSILON / 2; //Epsilon/2 for fsig

            double factor_t = signal->amplitude * EPSILON / 4;
            // i factor for transmission is provided in the matrix multiplication
            // a12f and a21f later
            double phase_t  = f_sig->order * signal->phase;

            complex_t q1 = complex_0, q2 = complex_0;
            complex_t txs1 = complex_0, txs2 = complex_0;

            // Compute coupling coefficients as in 4.6.2 Alignment transfer function
            // in the manual

            if(!node1->gnd_node){
                if (signal->type == SIG_X) {
                    q1 = node1->qx;
                } else {
                    q1 = node1->qy;
                }

                if (node1->component_index == signal->component_index) {
                    q1 = cminus(cconj(q1));
                }

                // compute q/w_0
                txs1 = xyamp(node1, q1);
            }

            if(!node2->gnd_node){
                if (signal->type == SIG_X) {
                    q2 = node2->qx;
                } else {
                    q2 = node2->qy;
                }

                // new testing: reversed
                if (node2->component_index == signal->component_index) {
                    q2 = cminus(cconj(q2));
                }

                // compute q/w_0
                txs2 = xyamp(node2, q2);
            }
            
            //warn("!!! %s nr1=%f  nr2=%f\n", mirror->name, nr1, nr2 );

            // the old Gouy phase problem again: I should now do a proper
            // rev_gouy with txs1 and txs2, but I think it will just 
            // produce a constant phase of 90 deg. So this is what I do instead.

            txs1 = co(0, zmod(txs1));
            txs2 = co(0, zmod(txs2));
      
            int cidx, sidx, car_field;
            int i, j, field_index;
            
            // Store RHS vectors for convenience
            complex_t *crhs = (complex_t*)M_car->rhs_values;
            complex_t *srhs = (complex_t*)M_sig->rhs_values;

            // compute purely reflected and transmitted carrier field, do this by
            // taking the incoming beam and computing the matrix multiplication
            // with the reflection and transmission elements of the matrix. Store reflected field in
            // memory already allocated for this in the signal structure
            // In here will be the r, it, tuning factors for the generated sidebands
            int ni, mi, nj, mj;
            for(i=0; i < inter.num_fields; i++){
                signal->ar1[i] = complex_0;
                signal->ar2[i] = complex_0;
                signal->at1[i] = complex_0;
                signal->at2[i] = complex_0;
                
                get_tem_modes_from_field_index(&ni, &mi, i);

                for(j=0; j < inter.num_fields; j++){
                    get_tem_modes_from_field_index(&nj, &mj, j);
                
                    if(!node1->gnd_node && include_mode_coupling(&mirror->a_cplng_11, ni,nj,mi,mj)) {
                        // Get the RHS index of port 1 of the node (The incoming port) then
                        // add to it the field HOM j at frequency  f_sig->carrier->index.
                        cidx = M_car->node_rhs_idx_1[node1->list_index] + get_port_rhs_idx(f_sig->carrier->index, j);
                        // Apply component coupling matrix to the carrier field vector
                        signal->ar1[i] = z_pl_z(signal->ar1[i], z_by_z(*mirror->a11f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                    }

                    if(!node2->gnd_node && include_mode_coupling(&mirror->a_cplng_22, ni,nj,mi,mj)) {
                        // Get the RHS index of port 2 of the node (The incoming port) then
                        // add to it the field HOM j at frequency  f_sig->carrier->index.
                        cidx = M_car->node_rhs_idx_2[node2->list_index] + get_port_rhs_idx(f_sig->carrier->index, j);
                        signal->ar2[i] = z_pl_z(signal->ar2[i], z_by_z(*mirror->a22f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                    }
                    
                    // Similar to above but for transmitted carrier fields
                    if(!node1->gnd_node && !node2->gnd_node && include_mode_coupling(&mirror->a_cplng_12, ni,nj,mi,mj)) {
                        cidx = M_car->node_rhs_idx_1[node1->list_index] + get_port_rhs_idx(f_sig->carrier->index, j);
                        signal->at2[i] = z_pl_z(signal->at2[i], z_by_z(*mirror->a12f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                    }
                    
                    if(!node1->gnd_node && !node2->gnd_node && include_mode_coupling(&mirror->a_cplng_21, ni,nj,mi,mj)) {
                        cidx = M_car->node_rhs_idx_2[node2->list_index] + get_port_rhs_idx(f_sig->carrier->index, j);
                        signal->at1[i] = z_pl_z(signal->at1[i], z_by_z(*mirror->a21f[f_sig->carrier->index][f_sig->carrier->index][j][i], crhs[cidx]));
                    }
                }
            }

            for (field_index = 0; field_index < inter.num_fields; field_index++) {
                complex_t a11 = complex_0;
                complex_t a22 = complex_0;
                complex_t a12 = complex_0;
                complex_t a21 = complex_0;
                
                int tn, tm;
                double turn;

                get_tem_modes_from_field_index(&tn, &tm, field_index);
                //turn = turnit(tn);
                // ddb - I don't think we are using turn it here initially as we take the
                // outgoing field, which is already turned. The turning of the carrier
                // field is done in the carrier coupling coefficients
                turn = 1.0;
                //TODO: test and the finalize the above !
                
                // ddb - update: Because we also deal with the transmitted field too
                // which does have odd x modes reflected like reflection we have to
                // apply this turning later specifically.

                // the aim below is to select the carrier field index and from the carrier
                // matrix extract the value to use in the signal field computation
                if (tn && signal->type == SIG_X) {
                    car_field = get_field_index_from_tem(tn - 1, tm);

                    if(!node1->gnd_node) a11 = z_by_x(z_by_z(signal->ar1[car_field], cconj(txs1)), 1.0 * sqrt(tn));
                    if(!node2->gnd_node) a22 = z_by_x(z_by_z(signal->ar2[car_field], cconj(txs2)), 1.0 * sqrt(tn));
                    
                    if(!node1->gnd_node && !node2->gnd_node) {
                        a12 = z_by_x(z_by_z(signal->at2[car_field], cconj(txs2)), 1.0 * sqrt(tn));
                        a21 = z_by_x(z_by_z(signal->at1[car_field], cconj(txs1)), 1.0 * sqrt(tn));
                    }
                    
                } else if (tm && signal->type == SIG_Y) {
                    car_field = get_field_index_from_tem(tn, tm - 1);

                    if(!node1->gnd_node) a11 = z_by_x(z_by_z(signal->ar1[car_field], cconj(txs1)), 1.0 * sqrt(tm));
                    if(!node2->gnd_node) a22 = z_by_x(z_by_z(signal->ar2[car_field], cconj(txs2)), 1.0 * sqrt(tm));
                    
                    if(!node1->gnd_node && !node2->gnd_node) {
                        // minus sign difference here between direction couplings as pitch deflects
                        // beam in opposite direction
                        a12 = z_by_x(z_by_z(signal->at2[car_field], cconj(txs2)), -1.0 * sqrt(tm));
                        a21 = z_by_x(z_by_z(signal->at1[car_field], cconj(txs1)), 1.0 * sqrt(tm));
                    }
                }

                if (tn + tm < inter.tem) {
                    if (signal->type == SIG_X) {
                        car_field = get_field_index_from_tem(tn + 1, tm);

                        if(!node1->gnd_node) a11 = z_pl_z(a11, z_by_x(z_by_z(signal->ar1[car_field], txs1), -1.0 * sqrt(tn + 1)));
                        if(!node2->gnd_node) a22 = z_pl_z(a22, z_by_x(z_by_z(signal->ar2[car_field], txs2), -1.0 * sqrt(tn + 1)));
                        
                        if(!node1->gnd_node && !node2->gnd_node) {
                            a12 = z_pl_z(a12, z_by_x(z_by_z(signal->at2[car_field], txs2), -1.0 * sqrt(tn + 1)));
                            a21 = z_pl_z(a21, z_by_x(z_by_z(signal->at1[car_field], txs1), -1.0 * sqrt(tn + 1)));
                        }
                        
                    } else {
                        car_field = get_field_index_from_tem(tn, tm + 1);
                        if(!node1->gnd_node) a11 = z_pl_z(a11, z_by_x( z_by_z(signal->ar1[car_field], txs1), -1.0 * sqrt(tm + 1)));
                        if(!node2->gnd_node) a22 = z_pl_z(a22, z_by_x( z_by_z(signal->ar2[car_field], txs2), -1.0 * sqrt(tm + 1)));
                        if(!node1->gnd_node && !node2->gnd_node) {
                            // minus sign difference here between direction couplings as pitch deflects
                            // beam in opposite direction
                            a12 = z_pl_z(a12, z_by_x(z_by_z(signal->at2[car_field], txs2), 1.0 * sqrt(tm + 1)));
                            a21 = z_pl_z(a21, z_by_x(z_by_z(signal->at1[car_field], txs1), -1.0 * sqrt(tm + 1)));
                        }
                    }
                }

                if (!node1->gnd_node) {
                    // Get output at node 1
                    sidx = M_sig->node_rhs_idx_2[node1->list_index] + get_port_rhs_idx(f, field_index);
                    srhs[sidx] = z_by_xph(a11, turn * factor_r * nr1, phase_r);
                    
                    if (!node2->gnd_node) {
                        z_inc_z(&srhs[sidx], z_by_xph(a21, (nr1-nr2)*factor_t, phase_t));
                        //if(zabs(a21) != 0) warn("!!! 21 %s %s %s\n", mirror->name, complex_form(a21), complex_form(srhs[sidx]));
                    }
                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) srhs[sidx].im *= -1;
                }

                if (!node2->gnd_node) {
                    sidx = M_sig->node_rhs_idx_1[node2->list_index] + get_port_rhs_idx(f, field_index);
                    srhs[sidx] = z_by_xph(a22, turn * factor_r * nr2, phase_r + ph2);
                    
                    if (!node1->gnd_node) {
                        z_inc_z(&srhs[sidx], z_by_xph(a12, -(nr1-nr2)*factor_t, phase_t));
                        //if(zabs(a12) != 0) warn("!!! 12 %s %s %s\n", mirror->name, complex_form(a12), complex_form(srhs[sidx]));
                    }
                    // check if we need to conjugate the value for the lower sideband
                    if(f_sig->order == -1) srhs[sidx].im *= -1;
                }
            }
        }
    }    
}

void calc_space_signal_vars(space_t *space, double *factor, double *ph1, double f_c, int order){
    *factor *= sin(space->n * space->L * 2 * PI * inter.fsig / (2*init.clight)) * (f_c + inter.f0) / inter.fsig;
    *ph1 = space->n * space->L / init.clight * (-1.0 * f_c - order * inter.fsig * 0.5) * 360.0;
}

void fill_space_signal_rhs(signal_t *signal, ifo_matrix_vars_t* M_car, ifo_matrix_vars_t* M_sig) {
    
    node_t signal_node1 = inter.node_list[*(signal->node1_index)];
    node_t signal_node2 = inter.node_list[*(signal->node2_index)];
    int f;
    space_t *space = &inter.space_list[signal->component_index];
    
    if (signal->type == SIG_PHS || signal->type == SIG_SAGNAC) {
        double ph1;
        
        for(f=0; f < M_sig->num_frequencies; f++){
            frequency_t *f_sig = M_sig->frequencies[f];
            
            double phase = (90.0 + f_sig->order * signal->phase);
            double factor = signal->amplitude * EPSILON * 0.5;
            
            calc_space_signal_vars(space, &factor, &ph1, f_sig->carrier->f, f_sig->order);
            
            int i, sidx=0, cidx=0, iport1, oport1, iport2, oport2;
            iport1 = 0;//silence compiler warning
            iport2 = 0;
            oport1 = 0;
            oport2 = 0;
                
            // select the outgoing field and use the carrier there to generate the sidebands
            // !! NOTE !!
            // The space IN and OUT ports are actually opposite, as IN and OUT refer to 
            // what is going in and out of a COMPONENT not a space.
            if(signal_node1.direction == IN_OUT){
                iport1 = 2;
                oport1 = 1;
            }else if(signal_node1.direction == OUT_IN){
                iport1 = 1;
                oport1 = 2;
            }else
                bug_error("not expected");
            
            if(signal_node2.direction == IN_OUT){
                iport2 = 2;
                oport2 = 1;
            }else if(signal_node2.direction == OUT_IN){
                iport2 = 1;
                oport2 = 2;
            }else
                bug_error("not expected");

            complex_t *sig_vec = NULL, *car_vec = NULL;
            
            if (!signal_node1.gnd_node && !signal_node2.gnd_node) {
                sidx = M_sig->node_rhs_idx_1[signal_node2.list_index] + get_node_rhs_idx(oport2, f, 0, M_sig->num_frequencies);
                cidx = M_car->node_rhs_idx_1[signal_node1.list_index] + get_node_rhs_idx(iport1, f_sig->carrier->index, 0, M_car->num_frequencies);

                sig_vec = &(((complex_t*)M_sig->rhs_values)[sidx]);
                car_vec = &(((complex_t*)M_car->rhs_values)[cidx]);
                
                cblas_zgemv (CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
                    &complex_1, &(space->k12[0][0]), inter.num_fields, car_vec, 1, &complex_0, sig_vec, 1);
                
                for (i = 0; i < inter.num_fields; i++) {
                    sidx = M_sig->node_rhs_idx_1[signal_node2.list_index] + get_node_rhs_idx(oport2, f, i, M_sig->num_frequencies);
                    ((complex_t*)M_sig->rhs_values)[sidx] = z_by_xph(((complex_t*)M_sig->rhs_values)[sidx], factor, phase+ph1);
                    if(f_sig->order == -1) ((complex_t*)M_sig->rhs_values)[sidx].im *= -1;
                }
                
                sidx = M_sig->node_rhs_idx_1[signal_node1.list_index] + get_node_rhs_idx(oport1, f, 0, M_sig->num_frequencies);
                cidx = M_car->node_rhs_idx_1[signal_node2.list_index] + get_node_rhs_idx(iport2, f_sig->carrier->index, 0, M_car->num_frequencies);
                
                sig_vec = &(((complex_t*)M_sig->rhs_values)[sidx]);
                car_vec = &(((complex_t*)M_car->rhs_values)[cidx]);

                if(signal->type == SIG_SAGNAC) {
                    // Sagnac effect is doing opposite sign of phase for modulation
                    // when going in the opposite direction
                    ph1 += 180;
                }
                
                cblas_zgemv (CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
                    &complex_1, &(space->k21[0][0]), inter.num_fields, car_vec, 1, &complex_0, sig_vec, 1);
                
                for (i = 0; i < inter.num_fields; i++) {
                    sidx = M_sig->node_rhs_idx_1[signal_node1.list_index] + get_node_rhs_idx(oport1, f, i, M_sig->num_frequencies);
                    
                    ((complex_t*)M_sig->rhs_values)[sidx] = z_by_xph(((complex_t*)M_sig->rhs_values)[sidx], factor, phase +ph1);
                    if(f_sig->order == -1) ((complex_t*)M_sig->rhs_values)[sidx].im *= -1;
                }
            }
        }
    } else 
        bug_error("wrong signal type for space");   
}


void fill_modulator_signal_rhs(signal_t *signal, ifo_matrix_vars_t* M_car, ifo_matrix_vars_t* M_sig){
    
    node_t *node1 = &inter.node_list[*(signal->node1_index)];
    node_t *node2 = &inter.node_list[*(signal->node2_index)];
    
    // if either are a dump then there are no signal sidebands being generated around
    // any carriers. Dump node could be an input for vacuum quantum noise but signal sidebands
    // of noise would be negligible I would assume...
    if(node1->gnd_node || node2->gnd_node)
        return;
    
    int fin, fout, fs, i, cidx, sidx;
    
    complex_t *crhs = (complex_t*) M_car->rhs_values;
    complex_t *srhs = (complex_t*) M_sig->rhs_values;
    
    if (signal->type == SIG_PHS || signal->type == SIG_FRQ) {
        /* So the idea here is that any carrier being coupled by this modulator
         * will then have phase/frequency sidebands around it. So we first determine
         * which carriers are coupling then add the signal sideband there
         */
        for(fin=0; fin<M_car->num_frequencies; fin++){
            for(fout=0; fout<M_car->num_frequencies; fout++){
                // modulator noise won't affect direct coupling from same frequency
                if(M_car->mod_does_f_couple[signal->component_index][fin][fout] && fin!=fout){
                    frequency_t *fcar = M_car->frequencies[fout];
                    // Now we know this carrier has been created by this modulator
                    // therefore some noise will be present on it. Now we just need to find
                    // which signal frequencies belong to it
                    for(fs=0; fs<M_sig->num_frequencies; fs++){
                        frequency_t *fsig = M_sig->frequencies[fs];
                        
                        if(fsig->carrier == fcar){
                            // This should be equation 3.27 in the manual
                            
                            int car_mod_order = M_car->mod_f_couple_order[signal->component_index][fin][fout]; // The modulation order of this signal sidebands carrier
                            
                            double phase = 90.0 + fsig->order * signal->phase;
                            double factor = car_mod_order * signal->amplitude * EPSILON * 0.5;

                            for(i=0; i<inter.num_fields; i++){
                                // compute output at node 1 of a modulator, take outgoing field here and make sidebands around it
                                cidx = M_car->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(2, fout, i, M_car->num_frequencies);
                                sidx = M_sig->node_rhs_idx_1[node1->list_index] + get_node_rhs_idx(2, fs, i, M_sig->num_frequencies);
                                srhs[sidx] = z_pl_z(srhs[sidx], z_by_xph(crhs[cidx], factor, phase));

                                // now node2...
                                cidx = M_car->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(1, fout, i, M_car->num_frequencies);
                                sidx = M_sig->node_rhs_idx_1[node2->list_index] + get_node_rhs_idx(1, fs, i, M_sig->num_frequencies);
                                srhs[sidx] = z_pl_z(srhs[sidx], z_by_xph(crhs[cidx], factor, phase));
                            }
                        }
                    }
                    
                }
            }
        }
    } else if (signal->type == SIG_AMP) {
        bug_error("mod amp: this modulation type is not implemented yet");
    } else {
        bug_error("wrong signal type for mod");
    }
}

void set_light_input_ports(){
    
    int i;
    for(i=0; i<inter.num_light_inputs; i++){
        light_in_t *light_input;
        light_input = &(inter.light_in_list[i]);
        
        node_t* n = &inter.node_list[light_input->node_index];
        node_connections_t *nc = &inter.node_conn_list[light_input->node_index];
        
        // All laser light inputs should be at a loose node of a space or a component
        // that isn't connected to anything else.

        int is_loose = n->connect && (nc->comp_1 == NULL)^(nc->comp_2 == NULL);

        if(!is_loose)
            bug_error("Light input %s should be connected to a loose node", light_input->name);

        if((nc->comp_1 != NULL && nc->type_1 == SPACE) || (nc->comp_2 != NULL && nc->type_2 == SPACE)){
            // if the light input is connected to a space...
            if(n->direction == IN_OUT) { // first port in the input
                light_input->node_port = 2;
            } else if(n->direction == OUT_IN) { // second port is the input
                light_input->node_port = 1;
            } else
                bug_error("Node did not have direction set when filling in RHS vector");
        } else if((nc->comp_1 != NULL && nc->type_1 != SPACE) || (nc->comp_2 != NULL && nc->type_2 != SPACE)){
            // if the light input is connected to something else...
            if(n->direction == IN_OUT) { // first port in the input
                light_input->node_port = 1;
            } else if(n->direction == OUT_IN) { // second port is the input
                light_input->node_port = 2;
            }else
                bug_error("Node did not have direction set when filling in RHS vector");
        }
    }
}

complex_t compute_motion(int motion_idx, int comp_type, int comp_idx){
    int x_rhs_idx=-1;
    int num_motions=0;
    complex_t signal = complex_0;
    
    if(comp_type == MIRROR){
        mirror_t *m = &inter.mirror_list[comp_idx];
        x_rhs_idx = m->x_rhs_idx;
        num_motions = m->num_motions;
        //mout->motion_index = parse_motion_value(mout, m->mass, m->Ix, m->Iy, m->num_surface_motions); 
    } else if(comp_type == BEAMSPLITTER){
        beamsplitter_t *bs = &inter.bs_list[comp_idx];
        x_rhs_idx = inter.bs_list[comp_idx].x_rhs_idx;
        num_motions = bs->num_motions;
        // TODO add surface motion parsing when added
        //mout->motion_index = parse_motion_value(mout, bs->mass, bs->Ix, bs->Iy, 0);
    } else
        bug_error("Component type not compatiable with motion detector");

    // no mechanical motion will be happening if this is null
    if(motion_idx == NOT_FOUND || motion_idx >= num_motions || num_motions == 0) {
        signal = complex_0;
    } else {
        signal = ((complex_t*)M_ifo_sig.rhs_values)[x_rhs_idx+motion_idx];
        signal = z_by_x(signal, init.x_scale);
    }
    
    return signal;
}

// variable only used for debug output
int x_count = 0;

/**
 * Computes a data point for all light amplitudes for one point on the x-axis
 * This differs from the old data_point routine which handles frequencies 
 * sequentially instead of solving all frequencies at the same time.
 */
void data_point_new(int do_sig){
    //int tid = startTimer("DATAPOINT");
    
    // ensure that the fsig variables are always sensible
    assert((inter.fsig==0 && inter.mfsig == 0) || (inter.fsig == -inter.mfsig));
    
    // variable only used for debug output
    x_count++;
    
    // check global flag to see any of the frequencies are getting tuned
    if(carrier_frequencies_tuned || signal_frequencies_tuned){
        // refresh the used frequencies
        update_frequency_list();

        // update which frequencies now couple to which
        update_frequency_coupling(0);
        
        int f;
        frequency_t *f1 = NULL;

        // update result frequency list
        for(f=0; f<inter.num_carrier_freqs; f++){
            f1 = inter.carrier_f_list[f];
            f_s[f1->result_index] = f1->f;
        }
        
        for(f=0; f<inter.num_signal_freqs; f++){
            f1 = inter.signal_f_list[f];
            f_s[f1->result_index] = f1->f;
        }
    }
    
    // re fill the matrix with the latest values for the data point
    
    fill_ccs_matrix(&M_ifo_car);
    factor_matrix(STANDARD);
    clear_rhs(STANDARD);
    
    int using_sig_qcorr = inter.num_signal_freqs > 0 && do_sig;
    
    if(using_sig_qcorr){
        // clear the rhs vector as we will add to it in the next loop for the laser frequency modulation...
        clear_rhs(SIGNAL_QCORR);
    }
    
    // Now we must insert all the various sources to the RHS vector
    // for the carrier matrix
    int i,j;
    for(i=0; i<inter.num_light_inputs; i++){
        light_in_t *light_input;
        light_input = &(inter.light_in_list[i]);
        
        node_t* n = &inter.node_list[light_input->node_index];

        int rhs_idx_car = 0;
        
        if(light_input->node_port == 2) { // first port in the input
            rhs_idx_car = M_ifo_car.node_rhs_idx_2[n->list_index];
        } else if(light_input->node_port == 1) { // second port is the input
            rhs_idx_car = M_ifo_car.node_rhs_idx_1[n->list_index];
        } else
            bug_error("Node did not have direction set when filling in RHS vector");
        
        for (j = 0; j < inter.num_fields; j++) {
            // the factor of 2 here comes from the fact we only model
            // the positive frequency components. This wasn't a problem until
            // radiation pressure was added as there were no effects where effects
            // were proportional to the power. With RP though we need to use the
            // proper scaling.
            // !! Must match up with fill_light_signal_rhs too!!
            complex_t input_amplitude = z_by_ph(co(sqrt(2*light_input->I0 / init.epsilon_0_c), 0.0), light_input->phase);
            input_amplitude = z_by_z(input_amplitude, light_input->power_coeff_list[j]);
            
            if(input_amplitude.im == 0.0 && input_amplitude.re == 0.0)
                continue;
            
            int idx = rhs_idx_car + get_node_rhs_idx(1, light_input->f->index, j, M_ifo_car.num_frequencies);
            set_rhs(idx, input_amplitude, STANDARD);
        }
    }
    
    // solve the carrier matrix
    solve_matrix(STANDARD);
    store_carrier_solutions();
    
    if(inter.printmatrix){
        char fname[100] = {0};
        sprintf(fname, "klu_full_matrix_car_%i.dat", x_count);
        dump_matrix_ccs(&(M_ifo_car.M), fname); 
    }

    if(inter.powers){
        if (x_count==1){ // only once
          dump_powers(stdout, &M_ifo_car);
        }
    }
    
    if(using_sig_qcorr){
        // now we have computed the carrier fields everywhere we can compute the 
        // sideband sources for the RHS
        // loop over all signals
        fill_signal_rhs();
        
        // if there are some signals we need to fill the matrix and solve
        fill_ccs_matrix(&M_ifo_sig);
        factor_matrix(SIGNAL_QCORR);
        solve_matrix(SIGNAL_QCORR);
        store_signal_solutions();

        if(inter.printmatrix){
            char fname[100] = {0};
            sprintf(fname, "klu_full_matrix_sig_lbl.dat");
            dump_matrix_ccs_labels(&M_ifo_sig, fname);

            sprintf(fname, "klu_full_matrix_sig%i.dat", x_count);
            dump_matrix_ccs(&(M_ifo_sig.M), fname); 
        }
        
        // finally if we are computing some quantum noise we need to set that
        // matrix up, as it differs when using modulators as they mix frequencies
        // coherently in signal matrix          
        if(inter.num_qnoise_inputs > 0 && (inter.num_modulators > 0 || inter.num_slinks > 0)){
            // if there were no signals, we wouldn't have filled the signal
            // matrix yet
            fill_ccs_matrix(&M_ifo_sig);
            
            fill_matrix_modulator_elements_quantum(&M_ifo_sig);
            factor_matrix(SIGNAL_QCORR);
            
            if(inter.printmatrix){
                char fname[100] = {0};
                sprintf(fname, "klu_full_matrix_qn%i.dat", x_count);
                dump_matrix_ccs(&(M_ifo_sig.M), fname); 
            }
        }
        
        // actual quantum noise computations are done on a per detector basis
        // in fill_detector. We must ensure that the signal matrix is not changed
        // from here onwards.
    }  
    
    //endTimer(tid);
}


//! Fill detectors with output signals computed from field amplitudes

/*!
 * \todo can this routine be made clearer?
 *
 * \see Test_fill_detectors()
 */
void fill_detectors(void) {
    complex_t z;
    motion_out_t *mout;
    beampar_out_t bpout;
    cavity_par_out_t cpout;
    complex_t *q;
    int detector_index;

    int output_index;
    output_data_t *output_data;
    
    for (output_index = 0; output_index < inter.num_outputs; output_index++) {
        output_data = &(inter.output_data_list[output_index]);
        detector_index = output_data->detector_index;
        
        if (output_data->detector_type == PD0 ||
                output_data->detector_type == PD1 ||
                output_data->detector_type == AD ||
                output_data->detector_type == QNOISE ||
                output_data->detector_type == SHOT ||
                output_data->detector_type == QSHOT ||
                output_data->detector_type == CONV ||
                output_data->detector_type == BEAM) {

            output_data->signal = complex_0;
            const int num_demods = inter.light_out_list[detector_index].num_demods;
            complex_t tmp;
                
            // calculate detector sigs from a_s[][]
            switch (output_data->detector_type) {
                case AD:
                    tmp = amplitude_detector(detector_index);
                    
                    // Need to scale the amplitude correctly for outputting.
                    // We already 
                    switch(init.amplitude_scaling){
                        case SQRT_2_EPS_C:
                        case SQRT_2:
                            output_data->signal = tmp;
                            break;
                        case ONE:
                            output_data->signal = z_by_x(tmp, 1.0/SQRTTWO);
                            break;
                    }
                    
                    break;
                case QNOISE:     
                    if(inter.fsig == 0 && !inter.warned_quantum_no_fsig){
                        warn("Quantum noise computations require a signal frequency to be specified using 'fsig'\n");
                        warn("Use 'fsig name frequency [phase] [amplitude]' if no signal needs to be injected.\n");
                        inter.warned_quantum_no_fsig = true;
                    }
                    
                    if(inter.light_out_list[detector_index].sensitivity == OFF){
                        output_data->signal.re = compute_qnoised_output(detector_index);
                        output_data->signal.im = 0;
                    } else {
                        // must compute signal first to fill in max phase computation
                        double signal_power = zmod(photo_detector2(detector_index, num_demods));
                        double noise_power  = compute_qnoised_output(detector_index);
                        
                        if(output_data->quantum_scaling == PSD_HF || output_data->quantum_scaling == PSD)
                            signal_power *= signal_power; 
                        
                        output_data->signal.im = 0;
                        
                        if(inter.light_out_list[detector_index].sensitivity == ON)
                            output_data->signal.re = noise_power/signal_power;
                        else
                            output_data->signal.re = signal_power/noise_power;
                    }
                    
                    if(options.print_qnoise_matrix)
                        print_covariance_matrix(&inter.light_out_list[detector_index]);
                    
                    break;
                case PD0:
                    output_data->signal = photo_detector0(detector_index);
                    break;
                case PD1:
                    output_data->signal = photo_detector2(detector_index, num_demods);
                    break;
                case SHOT:
                    z = photo_detector0(detector_index);
                    output_data->signal.re = schottky_shotnoise(output_data, z.re);
                    output_data->signal.im = 0.0;
                    break;
                    
                case QSHOT:
                    if(inter.light_out_list[detector_index].sensitivity == OFF){
                        output_data->signal.re = compute_qshot_output(detector_index);
                        output_data->signal.im = 0;
                    } else {
                        // must compute signal first to fill in max phase computation
                        double signal_power = zmod(photo_detector2(detector_index, num_demods));
                        double noise_power  = compute_qshot_output(detector_index);
                        
                        if(output_data->quantum_scaling == PSD_HF || output_data->quantum_scaling == PSD)
                            signal_power *= signal_power; 
                        
                        output_data->signal.im = 0;
                        
                        if(inter.light_out_list[detector_index].sensitivity == ON)
                            output_data->signal.re = noise_power/signal_power;
                        else
                            output_data->signal.re = signal_power/noise_power;
                    }
                    break;
                case BEAM:
                    output_data->signal = beam_shape(detector_index);
                    break;
                case CONV:
                    output_data->signal = beam_convolution(detector_index);
                    break;
                default:
                    message("i=%d, type=%d\n", output_index, output_data->detector_type);
                    bug_error("unknown/incorrect detector type");
            }

            if (inter.debug & 4) {
                message("dp1 z_tmp[%d]=%s\n",
                        output_index, complex_form15(output_data->signal));
            }

            // handle default sensitivity computation for PD
            if(inter.light_out_list[detector_index].sensitivity 
                    && output_data->detector_type != QNOISE 
                    && output_data->detector_type != QSHOT ){
                
                complex_t power_DC = photo_detector0(detector_index);
                double noise_power = schottky_shotnoise(output_data, power_DC.re);
                double signal_SD = zmod(output_data->signal); // amplitude spectral density
                
                // if output PSD we need to square the signal ASD
                if(output_data->quantum_scaling == PSD_HF || output_data->quantum_scaling == PSD)
                    signal_SD *= signal_SD; 
                
                if (inter.light_out_list[detector_index].sensitivity == ON) {
                    output_data->signal.re = noise_power / signal_SD;
                    output_data->signal.im = 0.0;    
                } else if (inter.light_out_list[detector_index].sensitivity == NORM) {
                    output_data->signal.re =  signal_SD / noise_power;
                    output_data->signal.im = 0.0;
                    
                } else if(inter.light_out_list[detector_index].sensitivity != OFF) {
                    bug_error("unexpected sensitivity value");
                }
            }
        
        } else if(output_data->detector_type == HOMODYNE){
            output_data->signal = complex_0;
            
            if(inter.fsig == 0 && !inter.warned_quantum_no_fsig){
                warn("Quantum noise computations require a signal frequency to be specified using 'fsig'\n");
                warn("Use 'fsig name frequency [phase] [amplitude]' if no signal needs to be injected.\n");
                inter.warned_quantum_no_fsig = true;    
            } else {
                homodyne_t *hom = &inter.homodyne_list[output_data->detector_index];

                assert(hom != NULL);
                double noise = 0;
                complex_t signal1, signal2;
                
                if(hom->is_quantum){
                    noise = compute_homodyne_output(&inter.homodyne_list[output_data->detector_index]);
                    output_data->signal.re = noise;
                }
                
                if(hom->sensitivity || !hom->is_quantum) {
                    inter.light_out_list[hom->light_out_1].f[1] = inter.fsig;
                    signal1 = photo_detector2(hom->light_out_1, 1);
                    
                    inter.light_out_list[hom->light_out_2].f[1] = inter.fsig;
                    signal2 = photo_detector2(hom->light_out_2, 1);
                    
                    output_data->signal = z_pl_z(signal1, z_by_ph(signal2, hom->phase));
                }
                
                if(hom->sensitivity == ON){
                    output_data->signal.re = fabs(noise / zmod(output_data->signal));
                } else if(hom->sensitivity == NORM) {
                    output_data->signal.re = fabs(output_data->signal.re / noise);
                }
            }
        
        } else if(output_data->detector_type == MHOMODYNE){
            output_data->signal = complex_0;
            
            if(inter.fsig == 0 && !inter.warned_quantum_no_fsig){
                warn("Quantum noise computations require a signal frequency to be specified using 'fsig'\n");
                warn("Use 'fsig name frequency [phase] [amplitude]' if no signal needs to be injected.\n");
                inter.warned_quantum_no_fsig = true;    
            } else {
                mhomodyne_t *hom = &inter.mhomodyne_list[output_data->detector_index];

                assert(hom != NULL);
                double noise = 0;
                complex_t signal1, signal2;
                
                if(hom->is_quantum){
                    noise = compute_mhomodyne_output(&inter.mhomodyne_list[output_data->detector_index]);
                    output_data->signal.re = noise;
                }
                
                if(hom->sensitivity || !hom->is_quantum) {
                    int i;
                    
                    for (i=0; i<hom->num_node_pairs; i++) {
                        inter.light_out_list[hom->pairs[i].light_out_1].f[1] = inter.fsig;
                        signal1 = photo_detector2(hom->pairs[i].light_out_1, 1);

                        inter.light_out_list[hom->pairs[i].light_out_2].f[1] = inter.fsig;
                        signal2 = photo_detector2(hom->pairs[i].light_out_2, 1);

                        z_inc_z(&output_data->signal, z_by_x(z_m_z(signal1, signal2), inter.function_list[0].result));
                    }
                }
                
                if(hom->sensitivity == ON){
                    output_data->signal.re = fabs(noise / zmod(output_data->signal));
                } else if(hom->sensitivity == NORM) {
                    output_data->signal.re = fabs(output_data->signal.re / noise);
                }
            }
        
        } else if(output_data->detector_type == FORCERP){
            
//            if(inter.num_signals > 0) {
//                // zero the input array
//                memset(M_ifo_sig.rhs_values, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
//
//                force_out_t *fd = &(inter.force_out_list[output_data->detector_index]);
//
//                int rhs_idx = -1;
//
//                if(fd->comp_type == MIRROR) {
//                    mirror_t *m = &inter.mirror_list[fd->comp_index];
//                } else if(fd->comp_type == BEAMSPLITTER) {
//                    beamsplitter_t *bs = &inter.bs_list[fd->comp_index];
//                    rhs_idx = bs->x_rhs_idx + fd->motion_index;
//                } else {
//                    bug_error("Unhandled component type");
//                }
//               
//                output_data->signal = compute_motion(fd->motion_index,
//                                                    fd->component_type,
//                                                    fd->component_index);
//
//                
//            } else {
//                output_data->signal = complex_0;
//            } 
            
        } else if(output_data->detector_type == RGAIN){
            if(inter.num_signals > 0){
                // zero the input array
                memset(M_ifo_sig.rhs_values, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);

                openloopgain_out_t *olg = &(inter.openloopgain_list[output_data->detector_index]);

                int rhs_idx = -1;

                if(olg->comp_type == MIRROR) {
                    mirror_t *m = &inter.mirror_list[olg->comp_index];
                    rhs_idx = m->x_rhs_idx + olg->motion_index;
                } else if(olg->comp_type == BEAMSPLITTER) {
                    beamsplitter_t *bs = &inter.bs_list[olg->comp_index];
                    rhs_idx = bs->x_rhs_idx + olg->motion_index;
                } else {
                    bug_error("Unhandled component type");
                }
                
                //fill_signal_rhs();
                
                // fill vector to select out the diagonal transfer function for the motion
                // which should be Delta A/A as in Evans paper
                complex_t *crhs = (complex_t*)M_ifo_sig.rhs_values;
                crhs[rhs_idx] = complex_1;
                
                //warn("%s\n", complex_form(crhs[rhs_idx]));
                
                solve_ccs_matrix(&M_ifo_sig, M_ifo_sig.rhs_values, false, false);

                output_data->signal.re = z_m_z(complex_1, div_complex(complex_1, crhs[rhs_idx])).re;
                output_data->signal.im = 0;
            } else {
                output_data->signal = complex_0;
            }
        } else if(output_data->detector_type == SD || output_data->detector_type == SD2){
            if(inter.fsig == 0 && !inter.warned_quantum_no_fsig){
                warn("Quantum noise computations require a signal frequency to be specified using 'fsig'\n");
                warn("Use 'fsig name frequency [phase] [amplitude]' if no signal needs to be injected.\n");
                inter.warned_quantum_no_fsig = true;
            }
            
            double r, phi, size;
            light_out_t *out = &inter.light_out_list[output_data->detector_index];
            
            frequency_t *f = get_carrier_frequency(out->f[1]);
            
            if(f == NULL){
                output_data->signal = complex_0;
            } else {
                compute_squeezing_factor(out, &r, &phi, &size);
                
                if(output_data->detector_type == SD)
                    output_data->signal = z_by_xphr(complex_1, r, phi);
                else if(output_data->detector_type == SD2)
                    output_data->signal = z_by_xphr(complex_1, size, phi);
            }
            
        } else if(output_data->detector_type == QD){
            if(inter.fsig == 0 && !inter.warned_quantum_no_fsig){
                warn("Quantum noise computations require a signal frequency to be specified using 'fsig'\n");
                warn("Use 'fsig name frequency [phase] [amplitude]' if no signal needs to be injected.\n");
                inter.warned_quantum_no_fsig = true;
            }
            
            light_out_t *qd = &inter.light_out_list[detector_index];
            
            frequency_t *f = get_carrier_frequency(qd->freq);
            
            if(f == NULL){
                output_data->signal = complex_0;
            } else {
                double quadrature;
                compute_quadrature(qd, &quadrature);
                        
                output_data->signal.re = quadrature;
                output_data->signal.im = 0;
            }
            
        } else if(output_data->detector_type == XD){
            mout = &inter.motion_out_list[detector_index];
            
            if(mout->component_type == MIRROR){
                mirror_t *m = &inter.mirror_list[mout->component_index];
                mout->motion_index = parse_motion_value(mout, m->mass, m->Ix, m->Iy, m->num_surface_motions); 
                
                output_data->signal = compute_motion(mout->motion_index,
                                                     mout->component_type,
                                                     mout->component_index);
                
            } else if(mout->component_type == BEAMSPLITTER){
                beamsplitter_t *bs = &inter.bs_list[mout->component_index];
                mout->motion_index = parse_motion_value(mout, bs->mass, bs->Ix, bs->Iy, 0);
                
                output_data->signal = compute_motion(mout->motion_index,
                                                     mout->component_type,
                                                     mout->component_index);
            } else
                bug_error("Component type not compatiable with motion detector");
            
        } else if (output_data->detector_type == CP) {
            cpout = inter.cavity_par_out_list[detector_index];

            cavity_t *cav;

            cav = &inter.cavity_list[cpout.cavity];

            if (cpout.x) {
                q = &cav->qx;
            } else {
                q = &cav->qy;
            }

            double n = cav->n;

            switch (cpout.action) {
                case CPW0:
                    output_data->signal.re = w0_size(*q, n);
                    output_data->signal.im = 0.0;
                    break;
                case CPW:
                    output_data->signal.re = w_size(*q, n);
                    output_data->signal.im = 0.0;
                    break;
                case CPZ:
                    output_data->signal.re = z_q(*q);
                    output_data->signal.im = 0.0;
                    break;
                case CPZR:
                    output_data->signal.re = z_r(*q);
                    output_data->signal.im = 0.0;
                    break;
                case CPF:
                    output_data->signal.re = cav->finesse;
                    output_data->signal.im = 0.0;
                    break;
                case CPS:
                    if (cpout.x)
                        output_data->signal.re = cav->stability_x;
                    else
                        output_data->signal.re = cav->stability_y;
                    
                    output_data->signal.im = 0.0;
                    break;
                case CPL:
                    output_data->signal.re = cav->loss;
                    output_data->signal.im = 0.0;
                    break;
                case CPOL:
                    output_data->signal.re = cav->length;
                    output_data->signal.im = 0.0;
                    break;
                case CPFSR:
                    output_data->signal.re = cav->FSR;
                    output_data->signal.im = 0.0;
                    break;
                case CPFWHM:
                    output_data->signal.re = cav->FWHM;
                    output_data->signal.im = 0.0;
                    break;
                case CPPOLE:
                    output_data->signal.re = cav->pole;
                    output_data->signal.im = 0.0;
                    break;
                case CPRC:
                    output_data->signal.re = roc(*q);
                    output_data->signal.im = 0.0;
                    break;
                case CPQ:
                    output_data->signal = *q;
                    break;
                case CPGOUY:
                    if (cpout.x)
                        output_data->signal = co(cav->rt_gouy_x * DEG, 0);
                    else
                        output_data->signal = co(cav->rt_gouy_y * DEG, 0);
                    break;
                case CPFSEP:
                    if (cpout.x)
                        output_data->signal = co(cav->rt_gouy_x/TWOPI * cav->FSR, 0);
                    else
                        output_data->signal = co(cav->rt_gouy_y/TWOPI * cav->FSR, 0);
                    break;
                case CPA:
                    if (cpout.x)
                        output_data->signal = co(cav->Mx.A, 0);
                    else
                        output_data->signal = co(cav->My.A, 0);
                    break;
                case CPB:
                    if (cpout.x)
                        output_data->signal = co(cav->Mx.B, 0);
                    else
                        output_data->signal = co(cav->My.B, 0);
                    break;
                case CPC:
                    if (cpout.x)
                        output_data->signal = co(cav->Mx.C, 0);
                    else
                        output_data->signal = co(cav->My.C, 0);
                    break;
                case CPD:
                    if (cpout.x)
                        output_data->signal = co(cav->Mx.D, 0);
                    else
                        output_data->signal = co(cav->My.D, 0);
                    break;
                default:
                    message("action %d\n", cpout.action);
                    bug_error("No such parameter (detector outputs cp 1)");
                    break;
            }
        } else if (output_data->detector_type == BP) {
            bpout = inter.beampar_out_list[detector_index];

            if (bpout.x) {
                q = output_data->qx;
            } else {
                q = output_data->qy;
            }

            switch (bpout.action) {
                case BPW0:
                    output_data->signal.re =
                            w0_size(*q, *inter.node_list[output_data->node_index].n);
                    output_data->signal.im = 0.0;
                    break;
                case BPW:
                    output_data->signal.re =
                            w_size(*q, *inter.node_list[output_data->node_index].n);
                    output_data->signal.im = 0.0;
                    break;
                case BPG:
                    output_data->signal.re = gouy(*q);
                    output_data->signal.im = 0.0;
                    break;
                case BPZ:
                    output_data->signal.re = z_q(*q);
                    output_data->signal.im = 0.0;
                    break;
                case BPZR:
                    output_data->signal.re = z_r(*q);
                    output_data->signal.im = 0.0;
                    break;
                case BPRC:
                    output_data->signal.re = roc(*q);
                    output_data->signal.im = 0.0;
                    break;
                case BPQ:
                    output_data->signal = *q;
                    break;
                default:
                    message("action %d\n", bpout.action);
                    bug_error("No such parameter (detector outputs 1)");
                    break;
            }
        } else if (output_data->detector_type == PG) {
            bpout = inter.beampar_out_list[detector_index];

            output_data->signal.re = 0;
            output_data->signal.im = 0;

            if (bpout.x) {
                int k;
                for (k = 0; k < bpout.num_spaces; k++) {
                    output_data->signal.re +=
                            inter.space_list[bpout.space_list[k]].gouy_x;
                }
            } else {
                int k;
                for (k = 0; k < bpout.num_spaces; k++) {
                    output_data->signal.re +=
                            inter.space_list[bpout.space_list[k]].gouy_y;
                }
            }
        } else if (output_data->detector_type == MINIMIZER) {
            output_data->signal.re = inter.minimizer->x_result;
            output_data->signal.im = 0.0;
        } else if (output_data->detector_type == FEEDBACK) {
            output_data->signal.re = inter.lock_list[detector_index].value;
            output_data->signal.im = 0.0;
        }/*      else  if (output_data->detector_type==UFUNCTION)
            {
            j=output_data->detector; 
            do_function(j); // to have first value set correctly
            output_data->signal.re=inter.function_list[j].result;         
            output_data->signal.im=0.0;
            } 
    */
        else if (output_data->detector_type != UFUNCTION) {
            fprintf(stderr, "No. %d, name %s, type %d.   \n",
                    output_index, output_data->name, output_data->detector_type);
            bug_error("No such output (detector outputs 3)");
        }

        if (output_data->detector_type != UFUNCTION) {
            if (inter.debug & 4) {
                message("dp2 z_tmp[%d]=%s\n",
                        output_index, complex_form15(output_data->signal));
            }

            output_data->re = output_data->signal.re;
            output_data->im = output_data->signal.im;
            output_data->abs = sqrt(zabs(output_data->signal));
            output_data->deg = zdeg(output_data->signal);

            if (inter.debug & 2048) {
                message("%d: %s.re: %g\n",
                        output_index, output_data->name, output_data->re);
            }
        }
    }

    if (inter.num_func_cmds) {
        compute_functions();
        for (output_index = 0; output_index < inter.num_outputs; output_index++) {
            output_data = &(inter.output_data_list[output_index]);
            if (output_data->detector_type == UFUNCTION) {
                if (inter.debug & 4) {
                    message("(func) dp2 z_tmp[%d]=%s\n",
                            output_index, complex_form15(output_data->signal));
                }

                detector_index = output_data->detector_index;
                output_data->signal.re = inter.function_list[detector_index].result;
                output_data->signal.im = 0.0;
                output_data->re = output_data->signal.re;
                output_data->im = output_data->signal.im;
                output_data->abs = sqrt(zabs(output_data->signal));
                output_data->deg = zdeg(output_data->signal);
                if (inter.debug & 2048) {
                    message("%d: %s.re: %g\n",
                            output_index, output_data->name, output_data->signal.re);
                }
            }
        }
    }
}

//! Calculate the beam shape

/*!
 *
 *
 * \see Test_beam_shape() TODO: function rewritten, test needs to be adapted
 */
complex_t beam_shape(int detector_index) {
    int tn, tm;
    complex_t at1, at2;
    double phase;

    light_out_t bout = inter.light_out_list[detector_index];
    output_data_t out = inter.output_data_list[bout.output_idx];
    complex_t qx = *(out.qx);
    complex_t qy = *(out.qy);
    double nr = *inter.node_list[out.node_index].n;
    double x = w0_size(qx, nr) * bout.x;
    double y = w0_size(qy, nr) * bout.y;

    complex_t z_tmp = complex_0;

    if (bout.freq_is_set) {
        int freq_index;
        for (freq_index = 0; freq_index < inter.num_frequencies; freq_index++) {
            if (eq(f_s[freq_index], bout.freq)) {
                at1 = complex_0;
                int field_index;
                for (field_index = 0; field_index < inter.num_fields; field_index++) {
                    get_tem_modes_from_field_index(&tn, &tm, field_index);
                    if (inter.set_tem_phase_zero & 2) {
                        phase = tn * gouy(qx) + tm * gouy(qy);
                    } else {
                        phase = (tn + 0.5) * gouy(qx) + (tm + 0.5) * gouy(qy);
                    }
                    //          message("x=%f, y=%f",x,y);
                    at1 = z_pl_z(at1,
                            z_by_z(u_nm(tn, tm, qx, qy, x, y, nr),
                            z_by_x(z_by_phr
                            (a_s[field_index][detector_index][freq_index], -1.0 * phase),
                            bout.masking_factor[field_index]/SQRTTWO)));
                }
                z_tmp = z_pl_z(z_tmp, at1);
            }
        }
    } else {
        int i, j;
        for (i = 0; i < inter.num_frequencies; i++) {
            if (t_s[i] != SIGNAL_FIELD && ((NOT bout.freq_is_set) || eq(f_s[i], bout.freq))) {
                for (j = 0; j < inter.num_frequencies; j++) {
                    if (t_s[j] != SIGNAL_FIELD && eq(f_s[i], f_s[j])) {
                        at1 = at2 = complex_0;
                        int field_index;
                        for (field_index = 0; field_index < inter.num_fields; field_index++) {
                            get_tem_modes_from_field_index(&tn, &tm, field_index);
                            if (inter.set_tem_phase_zero & 2) {
                                phase = tn * gouy(qx) + tm * gouy(qy);
                            } else {
                                phase = (tn + 0.5) * gouy(qx) + (tm + 0.5) * gouy(qy);
                            }

                            complex_t u_tmp = u_nm(tn, tm, qx, qy, x, y, nr);
                            at1 = z_pl_z(at1,
                                    z_by_z(u_tmp,
                                    z_by_x(z_by_phr
                                    (a_s[field_index][detector_index][i], -1.0 * phase),
                                    bout.masking_factor[field_index]/SQRTTWO)));
                            at2 = z_pl_z(at2,
                                    z_by_z(u_tmp,
                                    z_by_x(z_by_phr
                                    (a_s[field_index][detector_index][j], -1.0 * phase),
                                    bout.masking_factor[field_index]/SQRTTWO)));
                        }
                        z_tmp = z_pl_z(z_tmp, z_by_zc(at1, at2));
                    }
                }
            }
        }
    }
    return z_tmp;
}


//====================================================================================================================
//! Calculate the beam convolution

/*!
 *
 *
 * 
 */
complex_t beam_convolution(int detector_index) {
    int tn, tm;
    complex_t at1, at2;
    double phase;

    convolution_out_t conv_out = inter.convolution_out_list[detector_index];
    //light_out_t bout = inter.light_out_list[detector_index];
    output_data_t out = inter.output_data_list[conv_out.output_index];
    complex_t qx = *(out.qx);
    complex_t qy = *(out.qy);
    double nr = *inter.node_list[out.node_index].n;
    //  double x = w0_size(qx, nr) * bout.x;
    //double y = w0_size(qy, nr) * bout.y;

    double x = 0;
    double y = 0;

    complex_t model_qx;
    complex_t model_qy;

    // TODO move this into setup
    // adf check the use of model_qx/qy and Gouy phase settings, 240713
    if (conv_out.x_is_set) {
        model_qx = q_w0z(conv_out.w0x, conv_out.z0x, nr);
    } else {
        model_qx = qx;
    }
    if (conv_out.y_is_set) {
        model_qy = q_w0z(conv_out.w0y, conv_out.z0y, nr);
    } else {
        model_qy = qy;
    }

    complex_t z_tmp = complex_0;

    if (conv_out.freq_is_set) {
        int freq_index;
        for (freq_index = 0; freq_index < inter.num_frequencies; freq_index++) {
            if (eq(f_s[freq_index], conv_out.freq)) {
                at1 = complex_0;
                int field_index;
                for (field_index = 0; field_index < inter.num_fields; field_index++) {
                    get_tem_modes_from_field_index(&tn, &tm, field_index);
                    if (inter.set_tem_phase_zero & 2) {
                        phase = tn * gouy(model_qx) + tm * gouy(model_qy);
                    } else {
                        phase = (tn + 0.5) * gouy(model_qx) + (tm + 0.5) * gouy(model_qy);
                    }
                    // message("x=%f, y=%f",x,y);
                    at1 = z_pl_z(at1,
                            z_by_z(u_nm(tn, tm, model_qx, model_qy, x, y, nr),
                            z_by_x(z_by_phr
                            (a_s[field_index][detector_index][freq_index], -1.0 * phase),
                            conv_out.masking_factor[field_index])));
                }
                z_tmp = z_pl_z(z_tmp, at1);
            }
        }
    } else {
        int i, j;
        for (i = 0; i < inter.num_frequencies; i++) {
            if (t_s[i] != SIGNAL_FIELD && ((NOT conv_out.freq_is_set) || eq(f_s[i], conv_out.freq))) {
                for (j = 0; j < inter.num_frequencies; j++) {
                    if (t_s[j] != SIGNAL_FIELD && eq(f_s[i], f_s[j])) {
                        at1 = at2 = complex_0;
                        int field_index;
                        for (field_index = 0; field_index < inter.num_fields; field_index++) {
                            get_tem_modes_from_field_index(&tn, &tm, field_index);
                            if (inter.set_tem_phase_zero & 2) {
                                phase = tn * gouy(model_qx) + tm * gouy(model_qy);
                            } else {
                                phase = (tn + 0.5) * gouy(model_qx) + (tm + 0.5) * gouy(model_qy);
                            }

                            complex_t u_tmp = u_nm(tn, tm, model_qx, model_qy, x, y, nr);
                            at1 = z_pl_z(at1,
                                    z_by_z(u_tmp,
                                    z_by_x(z_by_phr
                                    (a_s[field_index][detector_index][i], -1.0 * phase),
                                    conv_out.masking_factor[field_index])));
                            at2 = z_pl_z(at2,
                                    z_by_z(u_tmp,
                                    z_by_x(z_by_phr
                                    (a_s[field_index][detector_index][j], -1.0 * phase),
                                    conv_out.masking_factor[field_index])));
                        }
                        z_tmp = z_pl_z(z_tmp, z_by_zc(at1, at2));
                    }
                }
            }
        }
    }
    return z_tmp;
}
//====================================================================================================================

//! Calculate the amplitude at detector

/*!
 * \param detector_index detector index
 *
 * \see Test_amplitude_detector()
 */
complex_t amplitude_detector(int detector_index) {
    // sanity check on input
    assert(detector_index >= 0);
    assert(detector_index < inter.num_light_outputs);

    light_out_t *light_output;
    light_output = &(inter.light_out_list[detector_index]);
    output_data_t output_data = inter.output_data_list[light_output->output_idx];

    complex_t z_tmp = complex_0;
    double amp = 0.0;

    // make sure that we only compute fields that are below maxtem (or 00 for maxtem off)
    if ((light_output->field_is_set && inter.tem_is_set && (light_output->field>=inter.num_fields))
            || (light_output->field_is_set && !inter.tem_is_set && (light_output->field!=0))) {
        if (inter.debug & 8) {
            message("ad %s: non-exisiting mode, detector ignored\n",light_output->name);            
        }
        return(complex_0);       
    }
    else if (light_output->field_is_set || !inter.tem_is_set) {
        int field_index = light_output->field;
        int freq_index;
        for (freq_index = 0; freq_index < inter.num_frequencies; freq_index++) {
            if (eq(f_s[freq_index], light_output->f[1])) {
                z_tmp = z_pl_z(z_tmp, a_s[field_index][detector_index][freq_index]);
                
                if (inter.debug & 8) {
                    message("ad a[%d][%d]=%s\n", field_index, freq_index, complex_form15(a_s[field_index][detector_index][freq_index]));
                }
            }
        }
        double phase = 0.0;
        if (inter.set_tem_phase_zero & 4) {
            complex_t qx = *(output_data.qx);
            complex_t qy = *(output_data.qy);

            int tn, tm;
            get_tem_modes_from_field_index(&tn, &tm, field_index);

            if (inter.set_tem_phase_zero & 2) {
                phase = tn * gouy(qx) + tm * gouy(qy);
            } else {
                phase = (tn + 0.5) * gouy(qx) + (tm + 0.5) * gouy(qy);
            }
        } else {
            phase = 0.0;
        }

        return (z_by_phr(z_tmp, -1.0 * phase));
    } else {
        // adf 21.04.2012, swapped order of for loops so that same fields in one mode are added as amplitudes!
        int field_index;
        amp=0.0;
        complex_t z_fake=complex_0; // dummy variable to compute a fake phase 
        for (field_index = 0; field_index < inter.num_fields; field_index++) {
            z_tmp=complex_0;
            int freq_index;
            for (freq_index = 0; freq_index < inter.num_frequencies; freq_index++) {
                if (eq(f_s[freq_index], light_output->f[1])) {
                    z_tmp = z_pl_z(z_tmp, a_s[field_index][detector_index][freq_index]);           
                    if (inter.debug & 8) {
                        message("ad a[%d][%d]=%s, amp=%g\n", field_index, freq_index,
                                complex_form15(a_s[field_index][detector_index][freq_index]), amp);
                    }
                }
            }
            z_fake = z_pl_z(z_fake,z_tmp);
            amp += zabs(z_tmp);
            if (inter.debug & 8) {
                message("ad a[%d][%d] z_tmp=%s, amp=%g\n", field_index, freq_index,
                        complex_form15(z_tmp), amp);
            }
        }
        return (get_complex_from_real_and_phase(sqrt(amp), zphase(z_fake)));
    }
}

//! Calculate the output of the photodetector with no demodulation (DC output)

/*!
 * For a discussion of how this photodetector works, see the DC detector
 * subsection of the Photodetectors and demodulation section of the user
 * manual.  This calculation performs an extended version of
 * this equation:
 *
 * \f{equation}
 * x = \sum_i \sum_j a_i a_j^* \quad \mathrm{with} 
 * \quad \{ i,j | i,j \in \{ 0, \ldots, N\}
 * \wedge \omega_i = \omega_j \}
 * \f}
 *
 * If the user has defined a photodetector to use, then this equation is
 * used:
 *
 * \f{equation}
 * P_{DC} = \epsilon_0 \sum_n \sum_m \sum_i \sqrt{c_n c_m} 
 * \sum_j B_k a_i a_j^* \quad \mathrm{with} \quad \omega_i = \omega_j
 * \f}
 *
 * where \f$n\f$ and \f$m\f$ represent the field indices and are related to
 * the TEM mode orders \f$n_1\f$, \f$m_1\f$ and \f$n_2\f$, \f$m_2\f$, 
 * \f$c_n\f$ and \f$c_m\f$ are the masking factors for a given field (and
 * therefore TEM mode order),
 * \f$\epsilon_0\f$ is the permittivity of free space, \f$B_k\f$ is the
 * beat factor for the given photodetector and for the given TEM mode
 * indices, and the sums over
 * \f$i\f$ and \f$j\f$ range over the number of frequencies in the system.
 *
 * In the case that the standard photodetector is used, then this is the
 * equation used:
 *
 * \f{equation}
 * P_{DC} = \epsilon_0 \sum_n \sum_i c_n \sum_j a_i a_j^* \quad
 * \mathrm{with} \quad \omega_i = \omega_j
 * \f}
 *
 * \param k detector index
 *
 * \todo if (light_output->detector_type > -1) branch untested
 * \todo k -> detector_index
 *
 * \see Test_photo_detector0()
 */
complex_t photo_detector0(int k) {
    int n1, m1, n2, m2;
    complex_t z_tmp;
    complex_t z_tmp2;
    double pdfactor;

    // sanity checks on input
    // detector index should be in the range: 0 <= k < inter.num_light_outputs
    assert(k >= 0);
    assert(k < inter.num_light_outputs);

    light_out_t *light_output;
    light_output = &(inter.light_out_list[k]);

    // initialise a temporary complex variable.  This will be returned from
    // the routine
    z_tmp2 = complex_0;

    // if the photodetector type is defined in the ini file...
    if (light_output->detector_type > -1) {

        int field_index_outer;
        for (field_index_outer = 0; field_index_outer < inter.num_fields; field_index_outer++) {
            // get the tem mode numbers for the given field index
            get_tem_modes_from_field_index(&n1, &m1, field_index_outer);

            int m;
            for (m = 0; m < inter.num_fields; m++) {
                // get the tem mode numbers for the given field index
                get_tem_modes_from_field_index(&n2, &m2, m);

                // get beat factor for this photodetector, and for the current
                // tem mode numbers
                pdfactor = init.pdt[light_output->detector_type].cross[n1][m1][n2][m2];
                 if (inter.debug & 8) {
                   message("beat factor (n1m1n2m2 %d %d %d %d) = %f\n",n1,m1,n2,m2,pdfactor);
                 }
                // initialise a temporary complex variable
                z_tmp = complex_0;

                // for every amplitude (that is not a signal sideband; i.e. fsig) 
                // at a given frequency, beat the amplitude with every other 
                // non-signal amplitude at the same frequency
                int freq_index_outer;
                for (freq_index_outer = 0;
                        freq_index_outer < inter.num_frequencies;
                        freq_index_outer++) {

                    if (t_s[freq_index_outer] != SIGNAL_FIELD) {
                        int freq_index_inner;
                        for (freq_index_inner = 0;
                                freq_index_inner < inter.num_frequencies;
                                freq_index_inner++) {

                            // not a signal, and the same frequency
                            if (t_s[freq_index_inner] != SIGNAL_FIELD &&
                                    eq(f_s[freq_index_outer], f_s[freq_index_inner])) {
                                // print debugging output if requested
                                if (inter.debug & 8) {
                                    message("pd0 [%d, %d]=%s\n",
                                            freq_index_outer, freq_index_inner,
                                            complex_form15(z_by_zc(a_s[field_index_outer][k][freq_index_outer],
                                            a_s[m][k][freq_index_inner])));
                                }

                                // if the beat factor is non-zero, beat the complex
                                // amplitudes with one another
                                // if the beat factor is zero, there is no point in doing
                                // the calculation, as the answer would be zero, so z_tmp
                                // should be left at complex_0 which it was initialised to
                                // hence the check
                                if (pdfactor != 0.0) {
                                    // \sum_j (a_{n,i} a_{m,j}^*) pdfactor
                                    z_tmp = z_pl_z(z_tmp,
                                            z_by_x(z_by_zc
                                            (a_s[field_index_outer][k][freq_index_outer],
                                            a_s[m][k][freq_index_inner]),
                                            pdfactor));
                                }
                            }
                        } // j frequency index
                    }
                    // multiply the first sum (that over j) by the masking factors
                    // (c[n]) for the given photodetector (light output)
                    // \sqrt{c_n c_m} \sum_j a_{n,i} a_{m,j}^* pdfactor
                    z_tmp = z_by_x(z_tmp, sqrt(light_output->masking_factor[field_index_outer] * light_output->masking_factor[m]));
                } // i frequency index

                // complete the summation over the fields
                // \sum_i \sqrt{c_n c_m} \sum_j a_{n,i} a_{m,j}^* pdfactor
                z_tmp2 = z_pl_z(z_tmp2, z_tmp);
            } // m tem mode index
        } // n tem mode index
        // E = \sum_n \sum_m \sum_i \sqrt{c_n c_m} \sum_j a_{n,i} a_{m,j}^* pdfactor
    }// otherwise if we are using the pre-defined DC photodetector
    else {
        // loop over all fields
        int field_index;
        for (field_index = 0; field_index < inter.num_fields; field_index++) {

            // initialise the temporary summation variable
            z_tmp = complex_0;

            // loop over all frequencies
            int freq_index_outer;
            for (freq_index_outer = 0;
                    freq_index_outer < inter.num_frequencies;
                    freq_index_outer++) {

                // if the type of signal is not a signal sideband continue
                if (t_s[freq_index_outer] != SIGNAL_FIELD) {

                    // loop over all frequencies
                    int freq_index_inner;
                    for (freq_index_inner = 0;
                            freq_index_inner < inter.num_frequencies;
                            freq_index_inner++) {

                        // if the current type of signal is not a signal sideband and
                        // the frequencies of the amplitudes to multiply are the same
                        // continue
                        if (t_s[freq_index_inner] != SIGNAL_FIELD &&
                                eq(f_s[freq_index_outer], f_s[freq_index_inner])) {

                            // output debugging information about a_i a_j^*
                            if (inter.debug & 8) {
                                message("pd0 [%d, %d]=%s\n",
                                        freq_index_outer, freq_index_inner,
                                        complex_form15(z_by_zc(a_s[field_index][k][freq_index_outer],
                                        a_s[field_index][k][freq_index_inner])));
                            }

                            // \f$ A_i = \sum_j a_i a_j^* \f$
                            z_tmp = z_pl_z(z_tmp, z_by_zc(a_s[field_index][k][freq_index_outer],
                                    a_s[field_index][k][freq_index_inner]));
                        }
                    } // end inner frequency sum
                }
                // apply the beam masks
                // \f$ A_n = c_n A_i = c_n \sum_j a_i a_j^* \f$
                z_tmp = z_by_x(z_tmp, light_output->masking_factor[field_index]);

            } // end outer frequency sum

            // E = \sum_n \sum_i c_n \sum_j a_i a_j^* 
            z_tmp2 = z_pl_z(z_tmp2, z_tmp);

        } // fields
    }

    z_tmp2 = z_by_x(z_tmp2, 0.5*init.epsilon_0_c); // Watt Ihr Volt !!!!!!!!!!!

    // output debugging information about the photodetector output
    if (inter.debug & 8) {
        message("   sum=%s\n", complex_form15(z_tmp2));
    }

    // ... and return the DC power
    return z_tmp2;
}

//! Calculate the output of a general photodetector

/*!
 * \param detector_index photodetector index
 * \param num_demod number of demodulations
 *
 * \see Test_photo_detector2()
 */
complex_t photo_detector2(int detector_index, int num_demod) {
    complex_t z[MAX_MIX + 1] = {complex_0};
    complex_t z_tmp;

    // sanity checks on input
    assert(detector_index >= 0);
    assert(detector_index < inter.num_light_outputs);

    assert(num_demod >= 0);
    assert(num_demod <= MAX_DEMOD);

    light_out_t *light_output;
    light_output = &(inter.light_out_list[detector_index]);

    double fmix[MAX_MIX + 1][MAX_DEMOD + 1] = {
        { 0},
        { 0}
    };
    
    int num_mixes = create_mix_table(num_demod, fmix);
    
    // calculate output for every freq mix
    int mix_index;
    for (mix_index = 1; mix_index <= num_mixes; mix_index++) {
        double f_ref = 0.0;

        // calculate the current mix frequency from the mix table and the
        // current light output's demodulation frequency
        //! \todo could make this loop into get_reference_freq()
        int demod_index;
        for (demod_index = 1; demod_index <= num_demod; demod_index++) {
            f_ref += fmix[mix_index][demod_index] * light_output->f[demod_index];
        }

        z[mix_index] = get_mixed_amplitude(detector_index, mix_index, f_ref, light_output);

        // report on the sum for the given mix frequency if required
        if (inter.debug & 8) {
            message("pd2(5) Sum: z%d %s\n", mix_index, complex_form15(z[mix_index]));
        }
    }

    z_tmp = set_demodulation_phase(num_demod, z, fmix, light_output, num_mixes);

    complex_t demod_signal = get_demodulation_signal(num_demod, detector_index, light_output, z_tmp);

    return demod_signal;
}



//! Rescales output signal in sol; writes array with pointer arithmetic

/*!
 * \param sol the solution output signal
 *
 * \todo not all OTYPEs are tested
 *
 * \see Test_create_data_array()
 */
void create_data_array(complex_t *sol) {
    int num_axes;
    complex_t z, x;
    double y, dout;
    output_data_t *out;

    dout = 0.0; // silence compiler warning;

    y = 0.0;
    if (inter.yaxis[1]) {
        num_axes = 1;
    } else {
        num_axes = 0;
    }

    int j, k;
    for (j = 0; j < inter.num_outputs; j++) {
        out = &(inter.output_data_list[j]);
        z = sol[j];

        for (k = 0; k <= num_axes; k++) {
            if (out->output_type == COMPLEX) {
                // scale output value by user defined factor
                x = z;
                x = z_by_x(x, out->user_defined_scale);

                switch (inter.yaxis[k]) {
                    case OTYPE_RE:
                        dout = x.re;
                        break;
                    case OTYPE_IM:
                        dout = x.im;
                        break;
                    case OTYPE_ABS:
                        dout = zmod(x);
                        break;
                    case OTYPE_DEG:
                        dout = zdeg(x);
                        break;
                    case OTYPE_DEGP:
                        dout = zdegp(x);
                        break;
                    case OTYPE_DEGM:
                        dout = zdegm(x);
                        break;
                    case OTYPE_DB:
                        dout = 0.5 * zdb(x);
                        break;
                    default:
                        bug_error("inter.yaxis[k] 1 ");
                        break;
                }
            } else {
                // scale output value by user defined factor
                y = z.re;
                y = y * out->user_defined_scale;

                switch (inter.yaxis[k]) {
                    case OTYPE_RE:
                    case OTYPE_ABS:
                        dout = y;
                        break;
                    case OTYPE_DB:
                        dout = xdb(y);
                        break;
                    case OTYPE_IM:
                    case OTYPE_DEG:
                    case OTYPE_DEGM:
                        dout = 0.0;
                        break;
                    default:
                        bug_error("inter.yaxis[k] 2 ");
                        break;
                }
            }
            
            if (options.servermode) {
                // output data through socket
                // to be removed here!!!!!
                message("Sending data\n");
            } else {
                // write data to temprorary memory array
                // and compute max/min if -max is set.
                //
                // it looks like respt is a pointer to output data, and that dout is
                // the actual value to put into that location, for later writing to
                // file.
                *(vlocal.respt++) = dout;

                if (out->Omax[k] < *(vlocal.respt - 1)) {
                    out->Omax[k] = *(vlocal.respt - 1);
                    if (inter.x1.xtype == FLOG) {
                        out->Omax_x[k] = x_axis_point / inter.x1.op;
                    } else {
                        out->Omax_x[k] = x_axis_point - inter.x1.op;
                    }

                    if (inter.splot) {
                        if (inter.x2.xtype == FLOG) {
                            out->Omax_y[k] = y_axis_point / inter.x2.op;
                        } else {
                            out->Omax_y[k] = y_axis_point - inter.x2.op;
                        }
                    }
                }

                if (out->Omin[k] > *(vlocal.respt - 1)) {
                    out->Omin[k] = *(vlocal.respt - 1);
                    if (inter.x1.xtype == FLOG) {
                        out->Omin_x[k] = x_axis_point / inter.x1.op;
                    } else {
                        out->Omin_x[k] = x_axis_point - inter.x1.op;
                    }

                    if (inter.splot) {
                        if (inter.x2.xtype == FLOG) {
                            out->Omin_y[k] = y_axis_point / inter.x2.op;
                        } else {
                            out->Omin_y[k] = y_axis_point - inter.x2.op;
                        }
                    }
                }
            }
        }
    }
}

//! Evaluate a function

/*!
 * \param function_index function index
 *
 * \see Test_do_function()
 */
void do_function(int function_index) {
    func_command_t *ftmp;
    char *err;

    // sanity check on input
    // function index should be in the range:
    // 0 <= function_index < inter.num_func_cmds
    assert(function_index >= 0);
    assert(function_index < inter.num_func_cmds);

    ftmp = &inter.function_list[function_index];
    ftmp->result = myfval(ftmp->formula, ftmp->tokens, ftmp->input);
    err = (char *) fget_error();

    if ((err != NULL) || isnan(ftmp->result) || isinf(ftmp->result)) {
        fputs("\n*** Error: ", stderr);
        if (err != NULL) {
          fprintf(stderr, "computation of function %s failed, error code : %s",
                  ftmp->name, err);
        }
        else {
          fprintf(stderr, "computation of function %s returned NaN or Inf",
                  ftmp->name);
        }
        putc('\n', stderr);
        my_finish(1);
    }

    if (inter.debug & 2048) {
        message("func %d: %s = %g = %s ( ",
                function_index, ftmp->name + 1, ftmp->result,
                ftmp->fstring);
        int j;
        for (j = 0; j < (int) strlen(ftmp->tokens); j++) {
            message("%c=%g ", *(ftmp->tokens + j), **(ftmp->input + j));
        }
        message(")\n");
    }
}

//! Automatic gain control

/*!
 * \param lock_index lock index
 * \param counter the current iteration number
 *
 * \see Test_autogain()
 */
void autogain(int lock_index, int counter) {
    double input, finput;
    double slope;

    // sanity check on input
    // lock index should be in the range: 0 <= lock_index < inter.num_locks
    assert(lock_index >= 0);
    assert(lock_index < inter.num_locks);

    // counter should be greater than or equal to zero
    assert(counter >= 0);

    if (inter.lock_list[lock_index].once != -1) {

        input = (*(inter.lock_list[lock_index].input) + inter.lock_list[lock_index].offset) /
                inter.lock_list[lock_index].accuracy;
        finput = fabs(input) + 1.0e-9;

        slope = fabs(input -
                ((2 * inter.lock_list[lock_index].last1 -
                inter.lock_list[lock_index].last2) /
                inter.lock_list[lock_index].accuracy));

        if (inter.debug & 2048) {
            message("%4d  lock %2d: break %4d, slope %10.3g "
                    "(input/acc %10.3g, slope/in %10.3g thresholdhigh-low %g - %g)\n",
                    counter, lock_index, inter.breakcount[lock_index], slope, input,
                    slope / finput,
                    init.lockthresholdhigh, init.lockthresholdlow);
        }

        if (slope / finput < init.lockthresholdlow) { // oscillation?
            inter.breakcount[lock_index]--;
        } else if (slope / finput > init.lockthresholdhigh) { // progress too slow?
            inter.breakcount[lock_index]++;
        }
        //              else            // gain OK!

        //  inter.breakcount[lock_index]=0;
        if (inter.breakcount[lock_index] > init.locktest2) {
            inter.breakcount[lock_index] = 0;
            inter.lock_list[lock_index].gain = inter.lock_list[lock_index].gain /
                    init.gainfactor;
            if (init.autogain == 2) {
                message("%4d v lock %s: new gain %g\n",
                        counter, inter.lock_list[lock_index].name, inter.lock_list[lock_index].gain);
            }
        } else if ((-1 * inter.breakcount[lock_index]) > init.locktest2) {
            inter.breakcount[lock_index] = 0;
            inter.lock_list[lock_index].gain = inter.lock_list[lock_index].gain *
                    init.gainfactor;
            if (init.autogain == 2) {
                message("%4d ^ lock %s: new gain %g\n",
                        counter, inter.lock_list[lock_index].name, inter.lock_list[lock_index].gain);
            }
        }
    }
}

//! Do the locks

/*!
 * \param lock_index lock index
 *
 * \see Test_do_locks()
 */
void do_locks(int lock_index) {
    // sanity check on input
    // lock index should be in the range: 0 <= lock_index < inter.num_locks
    assert(lock_index >= 0);
    assert(lock_index < inter.num_locks);

    double total_input = *(inter.lock_list[lock_index].input) + inter.lock_list[lock_index].offset;
    
    if (inter.lock_list[lock_index].once != -1) {
        inter.lock_list[lock_index].last2 = inter.lock_list[lock_index].last1;
        inter.lock_list[lock_index].last1 = total_input;
        
        if (is_not_locked(lock_index)){
            inter.lock_list[lock_index].value += inter.lock_list[lock_index].gain * total_input;
            
            if (inter.debug & 2048) {
                debug_msg("lock no. %d, input=%g, value = %g\n", lock_index, total_input, inter.lock_list[lock_index].value);
            }
        }  
    }
}

//! Determine if interferometer isn't locked

/*!
 * \param lock_index lock index
 *
 * \see Test_is_not_locked()
 */
int is_not_locked(int lock_index) {
    double input, finput;

    // sanity check on input
    // lock index should be in the range: 0 <= lock_index < inter.num_locks
    assert(lock_index >= 0);
    assert(lock_index < inter.num_locks);

    // assumed 'locked' for loops stat have stopped (lock* command)
    if (inter.lock_list[lock_index].once == -1){
        return 0;
    }

    input = (*(inter.lock_list[lock_index].input) + inter.lock_list[lock_index].offset) / inter.lock_list[lock_index].accuracy;
    finput = fabs(input) + 1.0e-9;
    
    if (finput > 1.0) {
        return 1;
    }

    return 0;
}

//! Compute the locks for the interferometer

/*! 
 * if mode < 0, computes all locks sequentially
 * if mode >= 0 computes only lock[mode]
 *
 * \param mode the mode in which to run the routine
 *
 * \see Test_compute_locks()
 */
void compute_locks(int mode) {
    int function_index, lock_index;

    lock_index = 0;
    function_index = 0;

    int i;
    for (i = 0; i < inter.num_locks_and_funcs; i++) {
        if (inter.is_locked[i]) {
            if (mode < 0 || mode == lock_index) {
                do_locks(lock_index);
            }
            lock_index++;

            if (inter.debug & 2048) {
                message("commonn %d lock %d : value %g\n", i, lock_index,
                        inter.lock_list[lock_index].value);
            }
        } else {
            do_function(function_index++);
        }
    }
}

//! Compute the functions

/*!
 *
 *
 * \see Test_compute_functions()
 */
void compute_functions(void) {
    int i;
    // along with computing actual functions also compute any fsig's that have
    // a transfer function assigned to it
    for (i=0; i<inter.num_signals; i++){
        signal_t *sig = &inter.signal_list[i];
        
        if(sig->tf != NULL){
            complex_t value = calc_transfer_func(sig->tf, co(0, TWOPI*inter.fsig));
            
            sig->amplitude = zabs(value);
            sig->phase     = zphase(value);
        }
    }
    
    for (i = 0; i < inter.num_func_cmds; i++) {
        do_function(i);
    }
}

//! Process all put commands

/*!
 *
 *
 * \see Test_compute_puts()
 */
void compute_puts(void) {
    double x;
    put_command_t puttmp;

    int i;
    for (i = 0; i < inter.num_put_cmds; i++) {
        puttmp = inter.put_list[i];

        if (puttmp.mode) {
            x = inter.put_list[i].startvalue + (*(inter.put_list[i].source));
        } else {
            x = *(inter.put_list[i].source);
        }

        if ((puttmp.lborder == 1 && x < puttmp.min) ||
            (puttmp.uborder == 1 && x > puttmp.max) ||
            (puttmp.lborder == 2 && x <= puttmp.min)||
            (puttmp.uborder == 2 && x >= puttmp.max)) {
            gerror("put: parameter %s %s out of range\n", puttmp.component_name, puttmp.parameter_name);
        }

        if (inter.debug & 2048) {
            message("put %d : %g\n", i, x);
        }

        *(inter.put_list[i].target) = x;
        
        // if we are updating the fsig, make sure we always update the negative
        // counterpart too
        if(inter.put_list[i].target == &(inter.fsig))
            inter.mfsig = -inter.fsig;
    }
}

double fn_minimizer(double x) {
    minimizer_t *mini = inter.minimizer;
    
    // First set the value to try
    *mini->variable = x;
    
    if(mini->variable == &inter.fsig) {
        inter.mfsig = -inter.fsig;
    }
    
    //--------------------------------
    // Do the usual calculations
    //--------------------------------
    compute_functions();
    compute_puts();
    rebuild_all();

    data_point_new(true);

    fill_detectors();
    //--------------------------------
    
    double ret = 0; // value to return as f(x)
    
    // Now get the output
    if (strcasecmp("re", mini->target_parameter) == 0) {
        ret = mini->target_output->re;
    } else if (strcasecmp("im", mini->target_parameter) == 0) {
        ret = mini->target_output->im;
    } else if (strcasecmp("abs", mini->target_parameter) == 0) {
        ret = mini->target_output->abs;
    } else if (strcasecmp("deg", mini->target_parameter) == 0) {
        ret = mini->target_output->deg;
    } else {
        bug_error("not handled\n");
    }
    
    // If we want to find a maximum we just take the negative of the value
    ret = (mini->find_max) ? -ret: ret;
    
    //warn("f(%.15g) = %.15g @ xaxis = %.15g\n", x, ret, *inter.x1.xaxis);
    
    return ret;
}

void minimize(){
    if(inter.minimizer == NULL) return;
    
    double m = *inter.minimizer->variable;
    double a = m+inter.minimizer->A;
    double b = m+inter.minimizer->B;

    local_min(a, b, inter.minimizer->abserr, 1e-15, &fn_minimizer, &m);
    //glomin (a, b, m, 10, 2e-16, inter.minimizer->abserr, inter.minimizer->abserr, &fn_minimizer, &m);
            
    inter.minimizer->x_result = m;
    
    inter.minimizer->result_output->signal.re = m;
    inter.minimizer->result_output->signal.im = 0;
}

//! Tune the parameters

/*!
 * This function changes all the parameters when one moves one step along
 * the axis.  In principle this changes only the "x" parameter, but with all
 * the functions and locks, several others could be affected.  So,
 * tune_parameters() does all this in order to generate a new proper state
 * of the interferometer before the next data point can be computed.
 * The function also calls data_point() which solves the interferometer matrix
 * and fill_detectors to compute the output (detector) signals. 
 *
 * \see Test_tune_parameters()
 */
void tune_parameters(void) {
    int counter, subcounter, gaincounter;
    int notlocked;
    int index1, index2;

    counter = subcounter = 0;
    gaincounter = 0;
    notlocked = 1;

    int i;
    for (i = 0; i < inter.num_locks; i++) {
        inter.breakcount[i] = 0;
    }
    
    set_progress_action_text("Calculating");
    print_progress(init.num_points_total, current_point_tot); // print progress percents 
    
    if (inter.lock && init.autostop != 2) {
        while (notlocked && counter < init.locksteps) {
            index1 = index2 = 0;
            
            while (index1 < inter.num_locks) {
                while (index2 <= index1) {
                    if (!(init.sequential)) {
                        index1 = index2 = inter.num_locks;
                        compute_locks(-1);
                        if (init.autogain && gaincounter > init.locktest1) {
                            gaincounter = 0;
                            int i;
                            for (i = 0; i < inter.num_locks; i++) {
                                autogain(i, counter);
                            }
                        }
                    } else {
                        if (!(init.sequential & 2)) {
                            index2 = index1;
                        }
                        compute_locks(index2);
                        if (init.autogain && gaincounter > init.locktest1) {
                            autogain(index2, counter);
                            if (index2 == inter.num_locks) {
                                gaincounter = 0;
                            }
                        }
                    }
                    
                    gaincounter++;
                    
                    compute_functions();
                    compute_puts();
                    rebuild_all();
                    
                    set_progress_action_text("Calculating");
                    print_progress(init.num_points_total, current_point_tot); // print progress percents 
                    
                    data_point_new(false);
                    
                    fill_detectors();
                    //  compute_functions();

                    index2++;
                }
                index1++;
            }

            // are we locked?
            notlocked = 0;
            int i;
            
            char done[100] = {0};
            assert(inter.num_locks < 100);
            
            for (i = 0; i < inter.num_locks; i++) {
                int j = is_not_locked(i);

                if (!inter.debug){
                  if(!j)
                    sprintf(done, "%s\xE2\x9C\x93", done);
                  else
                    sprintf(done, "%sX", done);
                }
                notlocked += j;
            }
            
            char msg[LINE_LEN] = {0};
            //sprintf(msg,"Locked %i of %i (Locking steps tried %i/%i (Max))", inter.num_locks-notlocked, inter.num_locks, counter, init.locksteps);
            if (!inter.debug){ // print mesage only when not printing debug output. 
              sprintf(msg, "- Locked %s (Locking steps tried %i/%i)", done, counter, init.locksteps);
              set_progress_message_text(msg);
              print_progress(init.num_points_total, current_point_tot); // print progress percents 
            }
            
            if (inter.showiterate > 0) {
                if (subcounter >= inter.showiterate) {
                    if (counter == subcounter) {
                        message("\n");
                    }
                    message("steps %d, lock ", counter);
                    int i;
                    for (i = 0; i < inter.num_locks; i++) {
                        message("%d: in=%g out=%g  ", i,
                                *(inter.lock_list[i].input) + inter.lock_list[i].offset, inter.lock_list[i].value);
                    }
                    message("\n");
                    subcounter = 0;
                }
            }
            counter++;
            subcounter++;
        }
    
        data_point_new(true);
        fill_detectors();
        
        inter.startlock++;
        if (inter.startlock > 1 && init.sequential & 4) {
            init.sequential = 0;
        }

        if (inter.debug & 2048) {
            message("Locked!\n");
        }

        if (counter >= init.locksteps) {
            message("Stopped lock iteration after %d steps!\n", init.locksteps);
            if (init.autostop) {
                message("Stop locking now.\n");
                init.autostop = 2;
            }
        }

        if (inter.showiterate == -1) {
            //message("\n");
            message("%d Steps to search for initial operating point, given by:\n",
                    counter);
            int i;
            for (i = 0; i < inter.num_locks; i++) {
                message("%d %s: error signal =%11.4g, "
                        "gain = %11.4g, feedback =%19.12g\n",
                        i, inter.lock_list[i].name, *(inter.lock_list[i].input) + inter.lock_list[i].offset,
                        inter.lock_list[i].gain, inter.lock_list[i].value);
            }
            inter.showiterate = 0;
        }

        int i;
        for (i = 0; i < inter.num_locks; i++) {
            if (inter.lock_list[i].once == 1) {
                inter.lock_list[i].once = -1;
                message("Stop locking of loop %d now.\n", i);
            }
        }
        
    } else if (inter.do_minimize) {
        //warn("Doing minimisation\n");
        set_progress_action_text("Calculating");
        set_progress_message_text("Optimising");
        print_progress(init.num_points_total, current_point_tot); // print progress percents 
        
        minimize();
        
    } else {
        set_progress_action_text("Calculating");
        set_progress_message_text("");
        print_progress(init.num_points_total, current_point_tot); // print progress percents 
        
        compute_functions();
        compute_puts();
        rebuild_all();
        
        data_point_new(true);
        
        fill_detectors();
    }
    
    bool print_each_step = inter.mismatches_options & PRINT_EACH_STEP;
    
    if ((!printed_mismatches && !print_each_step) || print_each_step){
        output_mismatch();
        printed_mismatches = true;
    }
}

//! Rebuild all components in system

/*!
 * For example when the radius of curvature of a mirror is changed, then the
 * ABCD matrix and the coupling coefficients need to be recomputed.  This is
 * what this function does.
 *
 * \see Test_rebuild_all()
 */
void rebuild_all(void) {
    //int tid = startTimer("REBUILD_ALL");
    int _tid;
    
    if (inter.rebuild & 1) {
        int i;
        _tid = startTimer("REBUILD_GRATING");
        for (i = 0; i < inter.num_gratings; i++) {
            if (inter.grating_list[i].rebuild) {
                rebuild_grating(&inter.grating_list[i]);
            }
        }
        endTimer(_tid);
    }

    if (inter.rebuild & 2 && inter.tem_is_set) {
        // mirrors
        int i;
        _tid = startTimer("REBUILD_ABCD");
        
        //#pragma omp parallel
        {
            //#pragma omp for private(i) schedule(guided)
            for (i = 0; i < inter.num_mirrors; i++) {
                if (inter.mirror_list[i].rebuild) {
                    if (inter.mirror_list[i].rebuild > 1) {
                        set_ABCD_mirror(i);
                    }
                }
            }

            // beamsplitters
            //#pragma omp for private(i) schedule(guided)
            for (i = 0; i < inter.num_beamsplitters; i++) {
                if (inter.bs_list[i].rebuild) {
                    if (inter.bs_list[i].rebuild > 1) {
                        set_ABCD_beamsplitter(i);
                    }
                }
            }

            // spaces
            //#pragma omp for private(i) schedule(guided)
            for (i = 0; i < inter.num_spaces; i++) {
                if (inter.space_list[i].rebuild) {
                    if (inter.space_list[i].rebuild > 1) {
                        set_ABCD_space(i);
                    }
                }
            }

            // lenses
            //#pragma omp for private(i) schedule(guided)
            for (i = 0; i < inter.num_lenses; i++) {
                if (inter.lens_list[i].rebuild) {
                    if (inter.lens_list[i].rebuild > 1) {
                        set_ABCD_lens(i);
                    }
                }
            }
        }
        endTimer(_tid);
        
        // retrace all
        if (inter.retrace) {
            _tid = startTimer("REBUILD_RETRACE");
            
            for (i = 0; i < inter.num_nodes; i++) {
                inter.node_list[i]._prev_qx = inter.node_list[i].qx;
                inter.node_list[i]._prev_qy = inter.node_list[i].qy;
            }
            
            retrace();
            
            for (i = 0; i < inter.num_nodes; i++) {
                if(!ceq(inter.node_list[i]._prev_qx, inter.node_list[i].qx) ||
                   !ceq(inter.node_list[i]._prev_qy, inter.node_list[i].qy)) {
                    
                    inter.node_list[i].q_changed = true;
                } else {
                    inter.node_list[i].q_changed = false;
                }
            }
            
            endTimer(_tid);
        }
    }
    
    // This check is needed when we are tuning something that varies a
    // cavity parameter but doesn't require a full retrace. If a retrace
    // happens though we don't need to redo this step
    if (inter.num_cavities && inter.num_cavity_param_outputs && !(inter.rebuild & 2) && inter.tem_is_set) {
        compute_cavity_params();
    }
    
    if (inter.rebuild && inter.tem_is_set) {
        _tid = startTimer("REBUILD_KNM");
        set_k_all_components(true);
        endTimer(_tid);
    }
    
    //endTimer(tid);
}

//! Write the data file

/*!
 * \param fp output data file
 */
void write_data_header(FILE *ofp) {
    // if option --noheader has not been used
    // write three comment lines into data file:

    if (!options.noheader) {
        // first line: Finesse version and build 
        fprintf(ofp, "%% ");
        print_version(ofp);

        // second line: 2D/3D plot, y2axis set or not
        if (inter.splot) {
            fprintf(ofp, "%% 3D plot");
        } else {
            fprintf(ofp, "%% 2D plot");
        }
        fprintf(ofp, ", y1axis: %s", inter.y1name);
        if (inter.yaxis[1]) {
            fprintf(ofp, ", y2axis: %s", inter.y2name);
        }
        fprintf(ofp, "\n");



        // third line names of the data column names 
        // x1axis:
        fprintf(ofp, "%% %s", inter.x1.xname);

        if (inter.splot) {
            // x2axis:
            fprintf(ofp, ", %s", inter.x2.xname);
        }

        int i;
        for (i = 0; i < inter.num_outputs; i++) {
            fprintf(ofp, ", %s", inter.output_data_list[i].name);
            // if y2axis exists, print output name twice with axis names attached
            if (inter.yaxis[1]) {
                fprintf(ofp, " %s", inter.y1name);
                fprintf(ofp, ", %s", inter.output_data_list[i].name);
                fprintf(ofp, " %s", inter.y2name);
            }
        }
        fprintf(ofp, "\n\n");
    }
}

//! Write the data file

/*!
 *
 *
 * \see Test_write_data_file()write_data_file
 */
    void write_data_file(void) {
    const char *stmp = options.out_format;

    // open output data file 
    if (!inter.x3_axis_is_set || inter.video) {
        if ((ofp = fopen(vglobal.output_fname, "w")) == NULL) {
            gerror("cannot open '%s'\n", vglobal.output_fname);
        }
    }

    set_progress_action_text("Saving data");
    set_progress_message_text("");
    int tot_points, curr_points = 0;
    
    if(inter.splot)
        tot_points = inter.x1.xsteps * inter.x2.xsteps;
    else
        tot_points = inter.x1.xsteps;
        
    // write some useful information in the output file header
    write_data_header(ofp);

    // loop across x axis values
    int i;
    for (i = 0; i <= inter.x1.xsteps; i++) {
        if(!inter.x3_axis_is_set)
            print_progress(tot_points, curr_points);
        
        if (inter.splot) {
            // loop across y axis values if appropriate
            int ii;
            for (ii = 0; ii <= inter.x2.xsteps; ii++) {
                vlocal.respt = mem.restab + inter.num_output_cols * (i * (inter.x2.xsteps + 1) + ii);

                if (inter.x3_axis_is_set && !inter.video) {
                    fprintf(ofp, stmp, *inter.x3.xaxis);
                }

                // loop across the number of output data columns and write the data
                int j;
                for (j = 0; j < inter.num_output_cols; j++) {
                    fprintf(ofp, stmp, *(vlocal.respt++));
                }
                putc('\n', ofp);
                curr_points++;
            }
            putc('\n', ofp);
        } else {
            vlocal.respt = mem.restab + i * inter.num_output_cols;
            // loop across the number of output data columns and write the data
            int j;
            for (j = 0; j < inter.num_output_cols; j++) {
                fprintf(ofp, stmp, *(vlocal.respt++));
            }
            putc('\n', ofp);
            
            curr_points++;
        }
    }
    fflush(ofp);

    // close the output data file
    if (!inter.x3_axis_is_set || inter.video) {
        fclose(ofp);
    }
}

//! Compute data points on request in server mode

/*!
 *
 * \see Test_x0_out()
 */
void x0_out(void) {

    // set parameters from command
    data_point_new(true);

    fill_detectors();

    update_fields = true;

    if (inter.num_diff_cmds) {
        compute_derivative(vlocal.zsolution, 0); // compute a derivative of a y value 
    } else {
        compute_result(vlocal.zsolution); // compute the y value 
    }

}

//! Generate results and plot for 1D or 2D simultions

/*!
 *
 * \see Test_x12_out()
 */
void x12_out(void) {
    char command[LINE_LEN] = {0};
    
    compute_data();

    fflush(stdout);
    fflush(stderr);
    
    write_data_file();

    clear_progress();
    
    int i;
    for (i = 0; i < inter.num_light_outputs; i++) {
        // print warning if it was not clear to KAT whether this was 
        // an error signal or a transfer function.
        // I.e. the sidebands from the signal frequency added at 
        // some point with an error signal (same mixing frequency).
        if (inter.debug & 16) {
            message("transfer[%d]=%d\n", i, transfer[i]);
        }

        if (transfer[i] & 1 && transfer[i] & 2) {
            warn("Output %s : some data points represent neither "
                    "a pure transfer function\n   nor a pure error signal! "
                    "Interpret carefully!\n", inter.light_out_list[i].name);
        }
    }

    if (!options.quiet) {
        message("\n");
    }

    if (options.maxmin) {
        max_min();
    }

    if (inter.time) {
        print_time();
    }

    // replace forward slashes by backslashes if on Windows
    char tmp_out_filename[FNAME_LEN];
    strcpy(tmp_out_filename, vglobal.output_fname);

    if (OS == __WIN_32__ || OS == __WIN_64__) {
        char *find_slash;
        while ((find_slash = strchr(tmp_out_filename, '/')) != NULL) {
            *find_slash = '\\';
        }
    }

    if (!options.perl1 && !inter.noscripts && !inter.noxaxis) {
        if (!options.quiet) {
            message("writing matlab/python/gnuplot batch files...\n");
        }
        // writing all batch files regardless of termnal setting in 
        // the *.kat file. This probably should be adjustable with an
        // option in the kat.ini file?
        open_file_to_write_ascii(vglobal.gnuplot_fname, &gfp);
        gnufile(0, gfp, tmp_out_filename, false);
        fclose(gfp);
#ifdef OSWIN
        if(strlen(vglobal.gnuplot_test_fname) > 0){
            open_file_to_write_ascii(vglobal.gnuplot_test_fname, &gfp);
            gnufile(0, gfp, tmp_out_filename, true);
            fclose(gfp);
        }
#endif
        open_file_to_write_ascii(vglobal.matlab_fname, &mfp);
        matlabfile(mfp, tmp_out_filename);
        fclose(mfp);
        
        open_file_to_write_ascii(vglobal.python_fname,&pfp);
        pythonfile(pfp, tmp_out_filename);
        fclose(pfp);
    }

    if (inter.num_gnuterm_cmds != NO_GNUTERM && !options.test && !inter.noxaxis) {
#if defined(OSWIN) && !defined(__CYGWIN__)
        
        wchar_t wargs[LINE_LEN] = {0};
        wchar_t w_command[LINE_LEN] = {0};
        wchar_t drive[MAX_PATH];
        wchar_t dir[MAX_PATH];
        wchar_t chosen_cmd[MAX_PATH];
        wchar_t ext[MAX_PATH];
        wchar_t wtmp[MAX_PATH] = {0};
        wchar_t wtmp2[MAX_PATH] = {0};
        wchar_t wgnuplot[MAX_PATH] = {0};
        wchar_t gnuplot[MAX_PATH] = {0};
        char test_cmd[LINE_LEN] = {0};
        
        strcpy(command, replace_str(init.gnucommand, "$s", ""));        
        
        // replace forward slashes by backslashes if on Windows
        char *find_slash;
        
        while ((find_slash = strchr(command, '/')) != NULL) {
            *find_slash = '\\';
        }
        
        mbstowcs(w_command, command, LINE_LEN);
        
        int nargs;
        LPWSTR *argList = CommandLineToArgvW((LPCWSTR)w_command, &nargs);
        
        if(argList == NULL || nargs < 2){
            gerror("Could not parse GNUPLOT command: %s", command);
        }
        
        _wsplitpath(argList[0], drive, dir, chosen_cmd, ext);
        
        bool want_gnuplot = (_wcsnicmp(chosen_cmd, L"gnuplot", MAX_PATH) == 0);
        bool want_wgnuplot = (_wcsnicmp(chosen_cmd, L"wgnuplot", MAX_PATH) == 0);
        
        if(!want_gnuplot && !want_wgnuplot){ 
            gerror("GNUCOMMAND specified an executable other than wgnuplot or gnuplot, please change to one of these.");
        }
            
        _wmakepath(wtmp, drive, dir, L"wgnuplot", ext);
        
        bool wgnuplot_exists = false, gnuplot_exists = false;
        DWORD wgnu_err, gnu_err;
        
        // if no drive or dir is present, the user has specified just
        // an executable name, probably something that is in the path
        if(wcslen(drive) == 0 && wcslen(dir) == 0){
            swprintf(wtmp2, MAX_PATH, L"where %ws >null 2>&1", wtmp);
            wgnuplot_exists = !_wsystem(wtmp2);
        }
        
        if (!wgnuplot_exists) {
            wgnuplot_exists = PathFileExistsW(wtmp);
            wgnu_err = GetLastError();
        }
        
        swprintf(wgnuplot, MAX_PATH, L"\"%s\"", wtmp);
        
        _wmakepath(wtmp, drive, dir, L"gnuplot", ext);
        
        // if no drive or dir is present, the user has specified just
        // an executable name, probably something that is in the path
        if(wcslen(drive) == 0 && wcslen(dir) == 0){
            swprintf(wtmp2, MAX_PATH, L"where %ws >null 2>&1", wtmp);
            gnuplot_exists = !_wsystem(wtmp2);
        } 
        
        if (!gnuplot_exists) {
            gnuplot_exists = PathFileExistsW(wtmp);
            gnu_err = GetLastError();
        }
        
        swprintf(gnuplot, MAX_PATH, L"\"%s\"", wtmp);
        
        int n;
        
        for(n=1; n<nargs; n++){
            wcscat(wargs, argList[n]);
            wcscat(wargs, L" ");
        }
        
        mbstowcs(wtmp, vglobal.gnuplot_fname, LINE_LEN);
        wcscat(wargs, L" ");
        wcscat(wargs, wtmp);
        
        message("calling gnuplot...\n");
        fflush(stdout);
        
        wcstombs(test_cmd, gnuplot, LINE_LEN);
        strcat(test_cmd, " ");
        strcat(test_cmd, vglobal.gnuplot_test_fname);
        
        if (inter.debug & 1){
            if(want_wgnuplot){
                message("(testing with '%s')\n", test_cmd);
                char tmp[LINE_LEN] = {0};
                wcstombs(tmp, wgnuplot, LINE_LEN);
                char tmp2[LINE_LEN] = {0};
                wcstombs(tmp2, wargs, LINE_LEN);
                message("(running with '%s %s')\n", tmp, tmp2);
            } else {
                char tmp[LINE_LEN] = {0};
                wcstombs(tmp, gnuplot, LINE_LEN);
                char tmp2[LINE_LEN] = {0};
                wcstombs(tmp2, wargs, LINE_LEN);
                message("(running with '%s %s')\n", tmp, tmp2);
            }
            
            fflush(stdout);
        }
        
        if(want_gnuplot && !gnuplot_exists){
            warn("Skipping plotting as the executable set in GNUCOMMAND wasn't found:\n");
            warn("GNUCOMMAND = %s\n", init.gnucommand);
            warn("Check GNUCOMMAND in kat.ini and make sure it is correct. error code = %ul\n", gnu_err);
        } else if(want_gnuplot) {
            
            STARTUPINFOW si;
            PROCESS_INFORMATION pi;

            ZeroMemory( &si, sizeof(si) );
            si.cb = sizeof(si);

            ZeroMemory( &pi, sizeof(pi) );

            wcscat(gnuplot, L" ");
            wcscat(gnuplot, wargs);

            CreateProcessW(NULL, gnuplot, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
        }
        
        if(want_wgnuplot && !wgnuplot_exists){
            warn("Skipping plotting as the executable set in GNUCOMMAND wasn't found:\n");
            warn("GNUCOMMAND = %s\n", init.gnucommand);
            warn("Check GNUCOMMAND in kat.ini and make sure it is correct. error code = %ul\n", wgnu_err);
        } else if(want_wgnuplot) {
            if(gnuplot_exists){
                // firstly run without plotting anything
                system_call(test_cmd);
            } else {
                warn("Couldn't find gnuplot.exe to test %s, plotting errors aren't shown using wnugplot.exe.s", vglobal.gnuplot_fname);
            }
            
            STARTUPINFOW si;
            PROCESS_INFORMATION pi;

            ZeroMemory( &si, sizeof(si) );
            si.cb = sizeof(si);
            ZeroMemory( &pi, sizeof(pi) );

            wcscat(wgnuplot, L" ");
            wcscat(wgnuplot, wargs);

            CreateProcessW(NULL, wgnuplot, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
        } 
        
        LocalFree(argList);
        
#else
        char tmpstr[LINE_LEN]={0};
        strcpy(tmpstr, vglobal.gnuplot_fname);
        replace_string(tmpstr, " ", "\\ ", 1, 2);
        strcpy(command, replace_str(init.gnucommand, "$s", tmpstr));        
        //strcpy(command, replace_str(init.gnucommand, "$s", vglobal.gnuplot_fname));        
        message("calling gnuplot...\n");
        
        if (inter.debug & 1)
            message("(using '%s')\n", command);
        
        system_call(command);
#endif
    }


    // creating python file and calling python!
    if (inter.num_pyterm_cmds != NO_PYTERM && !options.test) {
#if defined(OSWIN) && !defined(__CYGWIN__)
        
        wchar_t wargs[LINE_LEN] = {0};
        wchar_t tmp[LINE_LEN] = {0};
        
        strcpy(command,replace_str(init.pycommand, "$s", vglobal.python_fname));
        
        // replace forward slashes by backslashes if on Windows
        if (OS == __WIN_32__ || OS == __WIN_64__) {
            char *find_slash;
            while ((find_slash = strchr(command, '/')) != NULL) {
                *find_slash = '\\';
            }
        }
        
        wchar_t w_command[LINE_LEN] = {0};
        
        mbstowcs(w_command, command, LINE_LEN);
        
        int nargs;
        
        LPWSTR *argList = CommandLineToArgvW((LPCWSTR)w_command, &nargs);
        
        if(argList == NULL || nargs < 2){
            gerror("Could not parse GNUPLOT command: %s", command);
        }
        
        int n;
        
        for(n=1; n<nargs; n++){
            swprintf(tmp, LINE_LEN, L"%s %s", wargs, argList[n]);
            wcsncpy(wargs, tmp, LINE_LEN);
        }
        
        message("calling gnuplot...\n");
        fflush(stdout);
        
        if (inter.debug & 1){
            message("(using '%s')\n", command);
            char tmp2[LINE_LEN] = {0};
            wcstombs(tmp2, argList[0], LINE_LEN);
            message("%s ", tmp2);
            wcstombs(tmp2, wargs, LINE_LEN);
            message("%s\n", tmp2);        
            fflush(stdout);
        }
        
        ShellExecuteW(NULL, L"open", argList[0], wargs, L"" , SW_SHOW);
        LocalFree(argList);
        
#else
        char tmpstr[LINE_LEN]={0};
        strcpy(tmpstr, vglobal.python_fname);
        replace_string(tmpstr, " ", "\\ ", 1, 2);
        strcpy(command, replace_str(init.pycommand, "$s", tmpstr));        
        //strcpy(command,replace_str(init.pycommand, "$s", vglobal.python_fname));

        message("calling python...\n");
        
        if (inter.debug & 1) {
            message("(using '%s')\n", command);
        }
        
        system_call(command);
#endif
    }


}

//! Function to generate a number of simulation results and plots (x3axis)


/*
 * \see x12_out()
 *
 * \todo currently works only with gnuplot terminal
 *
 * \see Test_x3_out(void)
 */
void x3_out(void) {
    double xz0, xz1, dxz;
    char command[LINE_LEN] = {0};
    char text[LINE_LEN] = {0};
    char gtext[LINE_LEN] = {0};

    xz0 = xz1 = dxz = 0.0;

    if (inter.video) {
        if (inter.num_gnuterm_cmds != NO_GNUTERM) {
            if (inter.num_gnuterm_cmds != 1) {
                gerror("set exactly one gnuterm for video mode.\n");
            }

            if (NOT init.gnuterm[inter.gnuterm[0]].output_is_file) {
                gerror("set gnuterm with file output for video mode.\n");
            }

            strcpy(command, init.gnucommand);
            strcat(command, " ");
            strcat(command, vglobal.gnuplot_fname);
        }
    } else {
        if (inter.num_gnuterm_cmds != NO_GNUTERM) {
            warn("Gnuplot terminals are switched off for x3axis output.\n");
            inter.num_gnuterm_cmds = NO_GNUTERM;
        }

        open_file_to_write_ascii(vglobal.output_fname, &ofp);
    }

    get_xaxis_values(&inter.x3, &dxz, &xz0, &xz1, 0);

    int i;
    double fxz;
    
    set_progress_message_text("");
        
    for (i = 0, fxz = xz0; i <= inter.x3.xsteps; fxz += dxz, i++) {
        
        if (inter.x3.xtype == FLOG) {
            update_xaxis_values(&inter.x3, exp(fxz));
        } else {
            update_xaxis_values(&inter.x3, fxz);
        }

        if (inter.num_gnuterm_cmds != NO_GNUTERM) {
            if (1 + (int) floor(log10(inter.x3.xsteps)) > 3) {
                sprintf(text, "_%.4d.", i);
            } else if (1 + (int) floor(log10(inter.x3.xsteps)) > 2) {
                sprintf(text, "_%.3d.", i);
            } else if (1 + (int) floor(log10(inter.x3.xsteps)) > 1) {
                sprintf(text, "_%.2d.", i);
            } else {
                sprintf(text, "_%d.", i);
            }
            strcpy(gtext, inter.basename);
            strcat(gtext, text);
            strcat(gtext, init.gnuterm[inter.gnuterm[0]].suffix);

            inter.gnutermfn[0] = gtext;
            set_progress_message_text("name : %s - %d of %d...", inter.gnutermfn[0], i, inter.x3.xsteps);
        }
        
        compute_data();

        write_data_file();

        int j;
        for (j = 0; j < inter.num_light_outputs; j++) {
            // print warning if it was not clear to KAT whether this was 
            // an error signal or a transfer function.
            // I.e. the sidebands from the signal frequency added at 
            // some point with an error signal (same mixing frequency).
            if (inter.debug & 16) {
                message("transfer[%d]=%d\n", j, transfer[j]);
            }

            if (transfer[j] & 1 && transfer[j] & 2) {
                warn("Output %s : "
                        "some data points represent neither a pure transfer function\n"
                        "   nor a pure error signal! Interpret carefully!\n",
                        inter.light_out_list[i].name);
            }
        }

        if (options.maxmin) {
            max_min();
            int j;
            for (j = 0; j < mem.num_outputs; j++) {
                inter.output_data_list[j].Omax[0] = -1e20;
                inter.output_data_list[j].Omax[0] = -1e20;
                inter.output_data_list[j].Omin[0] = 1e20;
                inter.output_data_list[j].Omin[0] = 1e20;
            }
        }

        if (inter.num_gnuterm_cmds != NO_GNUTERM) {
            
            open_file_to_write_ascii(vglobal.gnuplot_fname, &gfp);
            gnufile(i, gfp, vglobal.output_fname, false);
            fclose(gfp);
        }
        if (inter.num_gnuterm_cmds != NO_GNUTERM && !options.test) {
            system_call(command);
        }
    }

    clear_progress();
    
    fflush(stdout);
    if (inter.time) {
        print_time();
    }
    
    fclose(ofp);
}


void store_carrier_solutions() {

    // store the values for the given output, field and frequency
    int out, f, mode,idx;       
    // for each frequency of the output node requested get all the field 
    // amplitudes and store them
    for (mode = 0; mode < inter.num_fields; mode++) {

        // store the carrier frequency results
        for (f = 0; f < M_ifo_car.num_frequencies; f++){
            idx = M_ifo_car.frequencies[f]->result_index;
            
            for (out = 0; out < inter.num_light_outputs; out++) { 
                uint64_t oidx = M_ifo_car.output_rhs_idx[out] + get_node_rhs_idx(1, f, mode, M_ifo_car.num_frequencies);
                a_s[mode][out][idx] = (((complex_t*)M_ifo_car.rhs_values)[oidx]);
            }
            
            for (out = inter.num_light_outputs; out < inter.num_light_outputs + inter.num_quad_outputs; out++) { 
                uint64_t oidx = M_ifo_car.output_rhs_idx[out] + get_node_rhs_idx(1, f, mode, M_ifo_car.num_frequencies);
                a_s[mode][out][idx] = (((complex_t*)M_ifo_car.rhs_values)[oidx]);
            }
        }
    }
}

void store_signal_solutions() {

    // store the values for the given output, field and frequency
    int out, f, mode,idx;
    // for each frequency of the output node requested get all the field 
    // amplitudes and store them
    for (mode = 0; mode < inter.num_fields; mode++) {

        for (f = 0; f < M_ifo_sig.num_frequencies; f++){
            idx = M_ifo_sig.frequencies[f]->result_index;
            
            for (out = 0; out < inter.num_light_outputs; out++) {        
                uint64_t oidx = M_ifo_sig.output_rhs_idx[out] + get_node_rhs_idx(1, f, mode, M_ifo_sig.num_frequencies);
                a_s[mode][out][idx] = (((complex_t*)M_ifo_sig.rhs_values)[oidx]);
                // if value is a conjugate frequency, un-conjugate it...
                if(M_ifo_sig.frequencies[f]->order == -1){
                    a_s[mode][out][idx].im *= -1;
                }
            }
            
            for (out = inter.num_light_outputs; out < inter.num_light_outputs + inter.num_quad_outputs; out++) { 
                uint64_t oidx = M_ifo_sig.output_rhs_idx[out] + get_node_rhs_idx(1, f, mode, M_ifo_sig.num_frequencies);
                a_s[mode][out][idx] = (((complex_t*)M_ifo_sig.rhs_values)[oidx]);
                // if value is a conjugate frequency, un-conjugate it...
                if(M_ifo_sig.frequencies[f]->order == -1){
                    a_s[mode][out][idx].im *= -1;
                }
            }
        }
    }
}

//! Calculates the classical power at the given detector

/*!
 * \param detector_index The detector index at which to measure the power
 *
 * \return power The calculated power
 *
 * \see Test_get_classical_power()
 */
double get_classical_power(int detector_index) {
    // field variables
    complex_t current_field;
    complex_t total_field = complex_0;

    // loop over all frequencies
    int freq_index;
    for (freq_index = 0; freq_index < inter.num_frequencies; freq_index++) {
        // loop over all the fields
        int field_index;
        for (field_index = 0; field_index < inter.num_fields; field_index++) {
            // get the current value of the classical solution
            current_field = a_s[field_index][detector_index][freq_index];

            total_field = z_pl_z(total_field, current_field);
        }
    }

    double power = zabs(total_field);

    return power;
}

//! Creates the frequency mixing table for demodulating photodetector signals

/*!
 * \param num_demod the number of demodulations
 * \param fmix the array of mix frequencies (also returned from the routine)
 *
 * \return num_mixes: the number of mix frequency permutations???
 *
 * \todo fmix -> freq_mix_table???
 *
 * \see Test_create_mix_table()
 */
int create_mix_table(int num_demod, double fmix[MAX_MIX + 1][MAX_DEMOD + 1]) {
    // sanity check inputs
    assert(num_demod >= 0);
    assert(num_demod < MAX_DEMOD);

    // initialise the mix frequency variables
    //  why not just go fmix_tmp = { 1.0 }; here???
    //  Is the zeroth element of the fmix_tmp and fmix arrays ever used???
    double fmix_tmp[MAX_DEMOD + 1] = {0};
    int demod_index;
    for (demod_index = 1; demod_index <= num_demod; demod_index++) {
        fmix_tmp[demod_index] = 1.0;
        fmix[1][demod_index] = 1.0;
    }

    if (inter.debug & 8) {
        message("(1)\n");
    }

    int num_mixes = 1;
    // generate the possible mix frequency permutations and store in mix table
    int demod_index_outer;
    for (demod_index_outer = 1;
            demod_index_outer < num_demod;
            demod_index_outer++) {

        // mix-frequency permutations
        int mix_permutation_index;
        for (mix_permutation_index = demod_index_outer;
                mix_permutation_index >= 1;
                mix_permutation_index--) {
            num_mixes++;

            // if num_mixes is larger than the maximum number of mixes allowed, barf
            if (num_mixes > MAX_MIX) {
                bug_error("maxmix");
            }

            // flip the sign of all previously defined mix frequency permutations
            fmix_tmp[mix_permutation_index] *= -1.0;

            // store the current set of permutations into fmix
            int demod_index_inner;
            for (demod_index_inner = 1;
                    demod_index_inner <= num_demod;
                    demod_index_inner++) {
                fmix[num_mixes][demod_index_inner] = fmix_tmp[demod_index_inner];
            }
        }
    }

    // output debugging information of the mix table if so requested
    if (inter.debug & 8) {
        message("mix table for %d demodulations:\n", num_demod);
        int mix_index;
        for (mix_index = 1; mix_index <= num_mixes; mix_index++) {
            message("%d : ", mix_index);
            int demod_index;
            for (demod_index = 1; demod_index <= num_demod; demod_index++) {
                message(" %s", double_form(fmix[mix_index][demod_index]));
            }
            message("\n");
        }
    }

    return num_mixes;
}

//! Calculates the internal sum of the photodetector signal calculation

/*!
 * \param mix_index index of the current mix frequency
 * \param field_index_outer index for outer field loop
 * \param field_index_inner index for inner field loop
 * \param detector_index index of the photodetector
 * \param f_ref the reference frequency
 * \param pdfactor photodetector amplication factor
 * \param light_output light output (i.e. detector) structure
 *
 * \return the amplitude sum
 *
 * \see Test_get_amplitude_sum()
 */
complex_t get_amplitude_sum(
        int mix_index,
        int field_index_outer,
        int field_index_inner,
        int detector_index,
        double f_ref,
        double pdfactor,
        light_out_t *light_output) {
    // sanity check inputs
    assert(detector_index >= 0);
    assert(detector_index < inter.num_light_outputs);

    complex_t amplitude_sum = complex_0;

    int freq_index_outer;
    
    for (freq_index_outer = 0; freq_index_outer < inter.num_frequencies; freq_index_outer++) {
        int freq_index_inner;
        for (freq_index_inner = 0; freq_index_inner < inter.num_frequencies; freq_index_inner++) {
            // if the difference between the signal frequencies is equal
            // to the current demodulation frequency, and either of the 
            // types of signal is NOT a gravitational wave signal 
            // (i.e. SIGNAL_FIELD) then continue to calculate some kind of
            // transfer parameter and the internal sum contributing to the
            // photodetector output signal
            double factor;
            if (teq(f_s[freq_index_outer], f_s[freq_index_inner], f_ref) && (t_s[freq_index_outer] != SIGNAL_FIELD || t_s[freq_index_inner] != SIGNAL_FIELD)) {
                
                if (t_s[freq_index_outer] == SIGNAL_FIELD || t_s[freq_index_inner] == SIGNAL_FIELD) {

                    // we have a signal; beef up its contribution to the sum
                    factor = 1.0 / EPSILON;
                    if (!ceq(a_s[field_index_outer][detector_index][freq_index_outer], complex_0) &&
                            !ceq(a_s[field_index_inner][detector_index][freq_index_inner], complex_0)) {

                        // output debugging information (about what exactly???)
                        if (inter.debug & 16) {
                            if ((transfer[detector_index] & 1 && !(transfer[detector_index] & 2))) {
                                message("err->trans (out %d) %.15g\n", detector_index, f_ref);
                            }
                        }

                        // The demodulated signal contains a transfer function
                        transfer[detector_index] = transfer[detector_index] | 2;
                    }
                } else {
                    factor = 1;
                    if (!ceq(a_s[field_index_outer][detector_index][freq_index_outer], complex_0) &&
                            !ceq(a_s[field_index_inner][detector_index][freq_index_inner], complex_0)) {

                        // output debugging information (about what exactly???)
                        if (inter.debug & 16) {
                            if (!(transfer[detector_index] & 1) &&
                                    transfer[detector_index] & 2) {
                                message("trans->err (out %d) %.15g\n", detector_index, f_ref);
                            }
                        }

                        transfer[detector_index] = transfer[detector_index] | 1;
                    }
                }

                // if the beat factor is nonzero, calculate the current term
                // in the sum.  If the beat factor is zero, then the current
                // term is zero, and so no point in running the calculation.
                if (pdfactor != 0.0) {
                    
                    complex_t tmp =   z_by_x(z_by_zc
                            (a_s[field_index_outer][detector_index][freq_index_outer],
                            a_s[field_index_inner][detector_index][freq_index_inner]),
                            factor * pdfactor);
                    amplitude_sum = z_pl_z(amplitude_sum, tmp);
                }

                // apply the beam masks
                amplitude_sum = z_by_x(amplitude_sum,
                        sqrt(light_output->masking_factor[field_index_outer] * light_output->masking_factor[field_index_inner]));

                // output debugging information about the photodetector and
                // the amplitude and phase of the two amplitudes if asked by
                // the user
                if (inter.debug & 8) {
                    message("pd2(1) z%d[%d, %d] (f=%s=%s - %s) : %s\n",
                            mix_index, freq_index_outer, freq_index_inner,
                            freq_form(f_ref), freq_form(f_s[freq_index_outer]),
                            freq_form(f_s[freq_index_inner]),
                            complex_form15(z_by_zc(a_s[field_index_outer][detector_index][freq_index_outer],
                                                          a_s[field_index_inner][detector_index][freq_index_inner])));
                    message("factor %.10g, pdfactor %.10g\n",factor, pdfactor);
                    message("a1: amp=%s ph=%s\n",
                            xdouble_form(zmod(a_s[field_index_outer][detector_index][freq_index_outer])),
                            xdouble_form(zdeg(a_s[field_index_outer][detector_index][freq_index_outer])));
                    message("a2: amp=%s ph=%s\n",
                            xdouble_form(zmod(a_s[field_index_outer][detector_index][freq_index_inner])),
                            xdouble_form(zdeg(a_s[field_index_outer][detector_index][freq_index_inner])));
                }
            }
        } // inner frequency loop
    } // outer frequency loop

    return amplitude_sum;
}

//! Calculates the demodulation phase and applies it to the field amplitudes

/*!
 * \param num_demod the number of demodulations
 * \param z complex amplitude
 * \param fmix the demodulation mix table
 * \param light_output the output light (i.e. detector) structure
 * \param num_mixes number of mixes being used in demodulation
 *
 * \todo passing the arrays z, fmix between the routines works, but
 * there should be a cleaner and safer way to do this to make sure that we
 * don't overwrite fmix or z unintentionally (for example).
 *
 * \todo fmix -> freq_mix_table???
 *
 * \return the resultant amplitude
 *
 * \see Test_set_demodulation_phase()
 */
complex_t set_demodulation_phase(
        int num_demod,
        complex_t z[MAX_MIX + 1],
        double fmix[MAX_MIX + 1][MAX_DEMOD + 1],
        light_out_t *light_output,
        int num_mixes) {
    // sanity check inputs
    assert(num_demod >= 0);
    assert(num_demod < MAX_DEMOD);

    // for each demodulation, set the demodulation phase
    double phi[MAX_MIX + 1] = {0.0};

    int demod_index;
    for (demod_index = 1; demod_index < num_demod; demod_index++) {
        complex_t z1 = complex_0;
        complex_t z2 = complex_0;
        int mix_index;
        switch (light_output->demod_phase_mode[demod_index]) {
                // use the default phase if not user-defined
            case MAX_PHASE:
                // add up the amplitude calculated above, depending upon the 
                // sign of the mixing frequency
                for (mix_index = 1; mix_index <= num_mixes; mix_index++) {
                    if (fmix[mix_index][demod_index] < 0.0) {
                        z1 = z_pl_z(z1, z[mix_index]);
                    } else {
                        z2 = z_pl_z(z2, z[mix_index]);
                    }
                }
                // use the extremal values of amplitude to calculate the maximum phase
                double phimax = 0.5 * (zdeg(z2) - zdeg(z1));
                // store max phase values for later reference
                light_output->phi[demod_index] = phimax;
                
                // calculate the phase to apply to the amplitude later
                for (mix_index = 1; mix_index <= num_mixes; mix_index++) {
                    phi[mix_index] += -phimax * fmix[mix_index][demod_index];
                    
                    if (inter.debug & 8) {
                        message("phimax(%d) :% g\n", mix_index, phi[mix_index]);
                    }
                }
                break;
                // the phase is user-defined
            case USER_PHASE:
                // calculate the phase to apply to the amplitude later
                for (mix_index = 1; mix_index <= num_mixes; mix_index++) {
                    phi[mix_index] += -light_output->phi[demod_index] * fmix[mix_index][demod_index];
                    if (inter.debug & 8) {
                        message("phi(%d) :% g\n", mix_index, phi[mix_index]);
                    }
                }
                break;
                // barf otherwise
            default:
                bug_error("light_output->phase i");
                break;
        }
    }

    // apply the phase to the amplitude
    complex_t z_tmp = complex_0; // initialise z_tmp
    int mix_index;
    for (mix_index = 1; mix_index <= num_mixes; mix_index++) {
        z_tmp = z_pl_z(z_tmp, z_by_ph(z[mix_index], phi[mix_index]));
    }

    if (inter.debug & 8) {
        message("pd2(7) z_tmp(p 1->i) %s\n", complex_form15(z_tmp));
    }

    return z_tmp;

}

//! Calculate and return the demodulation signal

/*!
 * \param num_demod the number of demodulations
 * \param detector_index the current detector
 * \param light_output the light output structure
 * \param demod_amplitude the demodulated amplitude
 *
 * \return the demodulation signal in Watts 
 *
 * \see Test_get_demodulation_signal()
 */
complex_t get_demodulation_signal(
        int num_demod,
        int detector_index,
        light_out_t *light_output,
        complex_t demod_amplitude) {
    // sanity check on inputs
    assert(num_demod >= 0);
    assert(num_demod < MAX_DEMOD);

    assert(detector_index >= 0);
    assert(detector_index < inter.num_light_outputs);

    switch (light_output->demod_phase_mode[num_demod]) {
        case MAX_PHASE:
            demod_amplitude.re = zmod(demod_amplitude);
            demod_amplitude.im = 0;
            break;
        case USER_PHASE:
            demod_amplitude.re =
                    (demod_amplitude.re * cos(light_output->phi[num_demod] * RAD) +
                    demod_amplitude.im * sin(light_output->phi[num_demod] * RAD));
            demod_amplitude.im = 0;
            break;
        default:
            break;
    }

    // 0.5 for each demodulation 050505
    demod_amplitude = z_by_x(demod_amplitude, npow(0.5, num_demod - 1));

    if (transfer[detector_index] == 2) {
        if (inter.debug & 8) {
            message("pd2(8.5) compensating signal mixer.\n");
        }
        // compensate 0.5 from mixer at fsig  050505
        demod_amplitude = z_by_x(demod_amplitude, 2.0);
    }

    if (inter.debug & 8) {
        message("pd2(8) z_tmp(p num_demod) %s\n", complex_form15(demod_amplitude));
    }

    // calculate the power from the amplitude
    demod_amplitude = z_by_x(demod_amplitude, 0.5*init.epsilon_0_c); // Watt Ihr Volt !!!!!!!!!!!!

    return demod_amplitude;
}

//! Calculates the field amplitude for the current mixing frequency

/*!
 * \param detector_index index of the current detector
 * \param mix_index the current mix index
 * \param f_ref the current value of the reference frequency
 * \param light_output the light output structure
 *
 * \todo much simplification can go on here.  for instance: do we really
 * need to pass mix_index and the detector_index around???
 *
 * \return the mixed amplitude
 *
 * \see Test_get_mixed_amplitude()
 */
complex_t get_mixed_amplitude(
        int detector_index,
        int mix_index,
        double f_ref,
        light_out_t *light_output) {
    // initialise a temporary summation variable
    complex_t z = complex_0;

    // if we have a user-defined photodectector, load its beat and masking
    // definition and calculate the equations appropriately (I can be more
    // clear in this explanation, although what happens below does follow
    // closely to what happens in photo_detector0() reasonably closely)
    int n1, m1, n2, m2; // tem mode indices
    if (light_output->detector_type > -1) {
        int field_index_outer;
        for (field_index_outer = 0;
                field_index_outer < inter.num_fields;
                field_index_outer++) {
            // get the TEM mode indices for the given field index
            get_tem_modes_from_field_index(&n1, &m1, field_index_outer);

            int field_index_inner;
            for (field_index_inner = 0;
                    field_index_inner < inter.num_fields;
                    field_index_inner++) {
                // get the TEM mode indices for the given field index
                get_tem_modes_from_field_index(&n2, &m2, field_index_inner);

                // determine the photodetector beat factor
                double pdfactor = init.pdt[light_output->detector_type].cross[n1][m1][n2][m2];

                // initialise the temporary variable for summation
                complex_t amplitude_sum = get_amplitude_sum(mix_index, field_index_outer, field_index_inner, detector_index, f_ref, pdfactor, light_output);

                // append to the outer sum
                z = z_pl_z(z, amplitude_sum);

            } // inner field index loop
        } // outer field index loop
    }// otherwise, use the predefined photodetector to calculate the output
    else {
        int field_index;
        for (field_index = 0; field_index < inter.num_fields; field_index++) {

            // initialise the summation variable
            complex_t amplitude_sum = get_amplitude_sum(mix_index, field_index, field_index, detector_index, f_ref, 1.0, light_output);

            // complete the summation, and multiply by the relevant beam
            // masking parameter
            //masking 220402
            z = z_pl_z(z, z_by_x(amplitude_sum, light_output->masking_factor[field_index]));
        }
    }

    // icm bug 0805 ##############
    // use -sin as second quadrature because that's more common
    // i.e. use first quadrature PLUS 90 degree for second quad

    //     z[m]=cconj(z[m]);

    return z;
}

//! Calculate and return the quantum demodulation signal

/*!
 * \param num_demod the number of demodulations
 * \param detector_index the current detector
 * \param light_output the light output structure
 * \param demod_amplitude the demodulated amplitude
 *
 * \todo Check that this works for *both* qshot and qnoised!!!
 * Am calling just ...qshot_demodulation... temporarily
 *
 * \return the demodulation signal in Watts (huh???) (correct units ???)
 *
 * \todo is this used anymore by qshot???  It is currently untested
 *
 * \see Test_get_qshot_noise_spectral_density()
 */
complex_t get_qshot_noise_spectral_density(
        int num_demod,
        int detector_index,
        light_out_t *light_output,
        complex_t demod_amplitude) {
    // sanity check on inputs
    assert(num_demod >= 0);
    assert(num_demod < MAX_DEMOD);

    assert(detector_index >= 0);
    assert(detector_index < inter.num_light_outputs);

    complex_t noise_spectral_density = complex_0;

    switch (light_output->demod_phase_mode[num_demod]) {
        case MAX_PHASE:
            noise_spectral_density.re = zmod(demod_amplitude);
            noise_spectral_density.im = 0;
            break;
        case USER_PHASE:
            noise_spectral_density.re =
                    (demod_amplitude.re * demod_amplitude.re) +
                    (demod_amplitude.im * demod_amplitude.im);
            noise_spectral_density.im = 0;
            break;
        default:
            bug_error("Unexpected phase demod value\n");
            break;
    }

    // 0.5 for each demodulation 050505
    noise_spectral_density = z_by_x(noise_spectral_density, npow(0.5, num_demod - 1));

    if (transfer[detector_index] == 2) {
        if (inter.debug & 8) {
            message("pd2(8.5) compensating signal mixer.\n");
        }
        // compensate 0.5 from mixer at fsig  050505
        noise_spectral_density = z_by_x(noise_spectral_density, 2.0);
    }

    if (inter.debug & 8) {
        message("pd2(8) z_tmp(p num_demod) %s\n", complex_form15(noise_spectral_density));
    }

    // calculate the power from the amplitude
    noise_spectral_density = z_by_x(noise_spectral_density, init.epsilon_0_c); // Watt Ihr Volt !!!!!!!!!!!!

    return noise_spectral_density;
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
