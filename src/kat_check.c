/*!
 * \file kat_check.c
 * \brief Value checking routines
 *
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#include "kat.h"
#include "kat_check.h"
#include "kat_io.h"

extern interferometer_t inter;

#ifndef NDEBUG
//! Check the mirror indices before assigning to the matrix elements

/*!
 * \param mirror Mirror object to be checked
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \see Test__check_mirror_indices()
 */
void _check_mirror_indices(mirror_t *mirror, indices_t *indices, int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;

    // grab the mirror's node indices
    int node1_index = mirror->node1_index;
    int node2_index = mirror->node2_index;

    // get the nodes at these indices
    node_t node1 = inter.node_list[node1_index];
    node_t node2 = inter.node_list[node2_index];

    if (matrix_type == STANDARD) {
        // make sure the index values are sensible
        assert(base_index_1 >= 0);
        assert(base_index_1 < inter.num_eqns);
        assert(base_index_2 >= 0);
        assert(base_index_2 < inter.num_eqns);

        assert(row_index_1 > -1);
        assert(row_index_1 < inter.num_eqns);
        assert(row_index_2 > -1);
        assert(row_index_2 < inter.num_eqns);

        assert(col_index_1 > -1);
        assert(col_index_1 < inter.num_eqns);
        assert(col_index_2 > -1);
        assert(col_index_2 < inter.num_eqns);

        if (NOT node1.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_1);
        }

        if (NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_2 != row_index_2);
        }

        if (NOT node1.gnd_node &&
                NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_2);

            // don't assign to diagonal elements
            assert(col_index_2 != row_index_1);
        }
    } else if (matrix_type == SIGNAL_QCORR) {

        /* row_index_1 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_1 != row_index_1);

        /* row_index_2 : col_index_2 */
        // don't assign to diagonal elements
        assert(col_index_2 != row_index_2);

        /* row_index_1 : col_index_2 */
        /* row_index_2 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_2 != row_index_1);
        assert(col_index_1 != row_index_2);
    } else {
        bug_error("Incorrect noise type");
    }

}

//! Check the beam splitter indices before assigning to the matrix elements

/*!
 * \param bs Beam splitter object to be checked
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \see Test__check_beamsplitter_indices()
 */
void _check_beamsplitter_indices(beamsplitter_t *bs, indices_t *indices,
        int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;
    int base_index_3 = indices->base_index_3;
    int base_index_4 = indices->base_index_4;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;
    int row_index_3 = indices->row_index_3;
    int row_index_4 = indices->row_index_4;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;
    int col_index_3 = indices->col_index_3;
    int col_index_4 = indices->col_index_4;

    // get the node indices, and then the nodes
    int node1_index = bs->node1_index;
    int node2_index = bs->node2_index;
    int node3_index = bs->node3_index;
    int node4_index = bs->node4_index;

    node_t node1 = inter.node_list[node1_index];
    node_t node2 = inter.node_list[node2_index];
    node_t node3 = inter.node_list[node3_index];
    node_t node4 = inter.node_list[node4_index];

    if (matrix_type == STANDARD) {
        // just to be really careful, check that the base indices are in the
        // right range (namely 0 <= base_index_[1-4] < inter.num_eqns)
        assert(base_index_1 >= 0);
        assert(base_index_1 < inter.num_eqns);
        assert(base_index_2 >= 0);
        assert(base_index_2 < inter.num_eqns);
        assert(base_index_3 >= 0);
        assert(base_index_3 < inter.num_eqns);
        assert(base_index_4 >= 0);
        assert(base_index_4 < inter.num_eqns);

        // make sure the values are sensible
        assert(row_index_1 > -1);
        assert(row_index_1 < inter.num_eqns);
        assert(row_index_2 > -1);
        assert(row_index_2 < inter.num_eqns);
        assert(row_index_3 > -1);
        assert(row_index_3 < inter.num_eqns);
        assert(row_index_4 > -1);
        assert(row_index_4 < inter.num_eqns);

        assert(col_index_1 > -1);
        assert(col_index_1 < inter.num_eqns);
        assert(col_index_2 > -1);
        assert(col_index_2 < inter.num_eqns);
        assert(col_index_3 > -1);
        assert(col_index_3 < inter.num_eqns);
        assert(col_index_4 > -1);
        assert(col_index_4 < inter.num_eqns);

        if (NOT node1.gnd_node &&
                NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_2);
            assert(col_index_2 != row_index_1);
        }

        if (NOT node1.gnd_node &&
                NOT node3.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_3);
            assert(col_index_3 != row_index_1);
        }

        if (NOT node2.gnd_node &&
                NOT node4.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_2 != row_index_4);
            assert(col_index_4 != row_index_2);
        }

        if (NOT node3.gnd_node &&
                NOT node4.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_3 != row_index_4);
            assert(col_index_4 != row_index_3);
        }
    } else if (matrix_type == SIGNAL_QCORR) {
      

        // make sure the values are sensible
        assert(row_index_1 > -1);
        assert(row_index_2 > -1);
        assert(row_index_3 > -1);
        assert(row_index_4 > -1);

        assert(col_index_1 > -1);
        assert(col_index_2 > -1);
        assert(col_index_3 > -1);
        assert(col_index_4 > -1);


        /* row_index_1 : col_index_3 */
        /* row_index_3 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_1 != row_index_3);
        assert(col_index_3 != row_index_1);

        /* row_index_2 : col_index_4 */
        /* row_index_4 : col_index_2 */
        // don't assign to diagonal elements
        assert(col_index_2 != row_index_4);
        assert(col_index_4 != row_index_2);

        /* row_index_3 : col_index_4 */
        /* row_index_4 : col_index_3 */
        // don't assign to diagonal elements
        assert(col_index_3 != row_index_4);
        assert(col_index_4 != row_index_3);

        /* row_index_1 : col_index_2 */
        /* row_index_2 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_1 != row_index_2);
        assert(col_index_2 != row_index_1);
    } else {
        bug_error("Incorrect noise type");
    }

}

//! Check the modulator indices before assigning to the matrix elements

/*!
 * \param modulator Modulator object to be checked
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \todo QCORR branch is untested
 *
 * \see Test__check_modulator_indices()
 */
void _check_modulator_indices(modulator_t *modulator, indices_t *indices,
        int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;

    // get the node indices, then the nodes
    int node1_index = modulator->node1_index;
    int node2_index = modulator->node2_index;

    node_t node1 = inter.node_list[node1_index];
    node_t node2 = inter.node_list[node2_index];

    if (matrix_type == STANDARD) {
        // just to be really careful, check that the base indices are in the
        assert(base_index_1 >= 0);
        assert(base_index_1 < inter.num_eqns);
        assert(base_index_2 >= 0);
        assert(base_index_2 < inter.num_eqns);

        // make sure the values are sensible
        assert(row_index_1 > -1);
        assert(row_index_1 < inter.num_eqns);
        assert(row_index_2 > -1);
        assert(row_index_2 < inter.num_eqns);

        assert(col_index_1 > -1);
        assert(col_index_1 < inter.num_eqns);
        assert(col_index_2 > -1);
        assert(col_index_2 < inter.num_eqns);

        if (NOT node1.gnd_node &&
                NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_2);
            assert(col_index_2 != row_index_1);
        }
    } else if (matrix_type == SIGNAL_QCORR) {

        /* row_index_1 : col_index_2 */
        /* row_index_2 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_1 != row_index_2);
        assert(col_index_2 != row_index_1);
    } else {
        bug_error("Incorrect noise type");
    }
}


//! Check the sagnac indices before assigning to the matrix elements

/*!
 * \param sagnac sagnac object to check
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \todo QCORR branch is untested
 *
 * \see Test__check_space_indices()
 */
void _check_sagnac_indices(sagnac_t *sagnac, indices_t *indices, int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;

    // get the node indices, then the nodes
    int node1_index = sagnac->node1_index;
    int node2_index = sagnac->node2_index;

    node_t node1 = inter.node_list[node1_index];
    node_t node2 = inter.node_list[node2_index];

    if (matrix_type == STANDARD) {
        // just to be really careful, check that the base indices are in the
        assert(base_index_1 >= 0);
        assert(base_index_1 < inter.num_eqns);
        assert(base_index_2 >= 0);
        assert(base_index_2 < inter.num_eqns);

        // make sure the values are sensible
        assert(row_index_1 > -1);
        assert(row_index_1 < inter.num_eqns);
        assert(row_index_2 > -1);
        assert(row_index_2 < inter.num_eqns);

        assert(col_index_1 > -1);
        assert(col_index_1 < inter.num_eqns);
        assert(col_index_2 > -1);
        assert(col_index_2 < inter.num_eqns);

        if (NOT node1.gnd_node &&
                NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_2);
            assert(col_index_2 != row_index_1);
        }
    } else {
        bug_error("Incorrect noise type");
    }
}

//! Check the space indices before assigning to the matrix elements

/*!
 * \param space Space object to check
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \todo QCORR branch is untested
 *
 * \see Test__check_space_indices()
 */
void _check_space_indices(space_t *space, indices_t *indices, int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;

    // get the node indices, then the nodes
    int node1_index = space->node1_index;
    int node2_index = space->node2_index;

    node_t node1 = inter.node_list[node1_index];
    node_t node2 = inter.node_list[node2_index];

    if (matrix_type == STANDARD) {
        // just to be really careful, check that the base indices are in the
        assert(base_index_1 >= 0);
        assert(base_index_1 < inter.num_eqns);
        assert(base_index_2 >= 0);
        assert(base_index_2 < inter.num_eqns);

        // make sure the values are sensible
        assert(row_index_1 > -1);
        assert(row_index_1 < inter.num_eqns);
        assert(row_index_2 > -1);
        assert(row_index_2 < inter.num_eqns);

        assert(col_index_1 > -1);
        assert(col_index_1 < inter.num_eqns);
        assert(col_index_2 > -1);
        assert(col_index_2 < inter.num_eqns);

        if (NOT node1.gnd_node &&
                NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_2);
            assert(col_index_2 != row_index_1);
        }
    } else if (matrix_type == SIGNAL_QCORR) {

        /* row_index_1 : col_index_2 */
        /* row_index_2 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_1 != row_index_2);
        assert(col_index_2 != row_index_1);
    } else {
        bug_error("Incorrect noise type");
    }
}

//! Check the squeezer indices before assigning to the matrix elements

/*!
 * \param squeezer Squeezer object to check
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \todo untested
 *
 * \see Test__check_squeezer_indices()
 */
void _check_squeezer_indices(squeezer_t *squeezer, indices_t *indices,
        int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;

    // get the node indices, then the nodes
    int node1_index = squeezer->node1_index;
    int node2_index = squeezer->node2_index;

    node_t node1 = inter.node_list[node1_index];
    node_t node2 = inter.node_list[node2_index];

    if (matrix_type == STANDARD) {
        // just to be really careful, check that the base indices are in the
        assert(base_index_1 >= 0);
        assert(base_index_1 < inter.num_eqns);
        assert(base_index_2 >= 0);
        assert(base_index_2 < inter.num_eqns);

        // make sure the values are sensible
        assert(row_index_1 > -1);
        assert(row_index_1 < inter.num_eqns);
        assert(row_index_2 > -1);
        assert(row_index_2 < inter.num_eqns);

        assert(col_index_1 > -1);
        assert(col_index_1 < inter.num_eqns);
        assert(col_index_2 > -1);
        assert(col_index_2 < inter.num_eqns);

        if (NOT node1.gnd_node &&
                NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_2);
            assert(col_index_2 != row_index_1);
        }
    } else if (matrix_type == SIGNAL_QCORR) {

        /* row_index_1 : col_index_2 */
        /* row_index_2 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_1 != row_index_2);
        assert(col_index_2 != row_index_1);
    } else {
        bug_error("Incorrect noise type");
    }
}

//! Check the lens indices before assigning to the matrix elements

/*!
 * \param lens Lens object to check
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \todo QCORR branch is untested
 *
 * \see Test__check_lens_indices()
 */
void _check_lens_indices(lens_t *lens, indices_t *indices, int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;

    // get the node indices, then the nodes
    int node1_index = lens->node1_index;
    int node2_index = lens->node2_index;

    node_t node1 = inter.node_list[node1_index];
    node_t node2 = inter.node_list[node2_index];

    if (matrix_type == STANDARD) {
        // just to be really careful, check that the base indices are in the
        assert(base_index_1 >= 0);
        assert(base_index_1 < inter.num_eqns);
        assert(base_index_2 >= 0);
        assert(base_index_2 < inter.num_eqns);

        // make sure the values are sensible
        assert(row_index_1 > -1);
        assert(row_index_1 < inter.num_eqns);
        assert(row_index_2 > -1);
        assert(row_index_2 < inter.num_eqns);

        assert(col_index_1 > -1);
        assert(col_index_1 < inter.num_eqns);
        assert(col_index_2 > -1);
        assert(col_index_2 < inter.num_eqns);

        if (NOT node1.gnd_node &&
                NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_2);
            assert(col_index_2 != row_index_1);
        }
    } else if (matrix_type == SIGNAL_QCORR) {

        /* row_index_1 : col_index_2 */
        /* row_index_2 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_1 != row_index_2);
        assert(col_index_2 != row_index_1);
    } else {
        bug_error("Incorrect noise type");
    }
}

//! Check the diode indices before assigning to the matrix elements

/*!
 * \param diode Diode object to check
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \todo QCORR branch is untested
 *
 * \see Test__check_diode_indices()
 */
void _check_diode_indices(diode_t *diode, indices_t *indices, int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;

    // get the node indices, then the nodes
    int node1_index = diode->node1_index;
    int node2_index = diode->node2_index;

    node_t node1 = inter.node_list[node1_index];
    node_t node2 = inter.node_list[node2_index];

    if (matrix_type == STANDARD) {
        // just to be really careful, check that the base indices are in the
        assert(base_index_1 >= 0);
        assert(base_index_1 < inter.num_eqns);
        assert(base_index_2 >= 0);
        assert(base_index_2 < inter.num_eqns);

        // make sure the values are sensible
        assert(row_index_1 > -1);
        assert(row_index_1 < inter.num_eqns);
        assert(row_index_2 > -1);
        assert(row_index_2 < inter.num_eqns);

        assert(col_index_1 > -1);
        assert(col_index_1 < inter.num_eqns);
        assert(col_index_2 > -1);
        assert(col_index_2 < inter.num_eqns);

        if (NOT node1.gnd_node &&
                NOT node2.gnd_node) {
            // don't assign to diagonal elements
            assert(col_index_1 != row_index_2);
            assert(col_index_2 != row_index_1);
        }
    } else if (matrix_type == SIGNAL_QCORR) {

        /* row_index_1 : col_index_2 */
        /* row_index_2 : col_index_1 */
        // don't assign to diagonal elements
        assert(col_index_1 != row_index_2);
        assert(col_index_2 != row_index_1);
    } else {
        bug_error("Incorrect noise type");
    }
}

//! Check the grating indices before assigning to the matrix elements

/*!
 * \param grating The grating whose indices are to be checked
 * \param indices Structure containing the indices to check
 * \param matrix_type The type of matrix: STANDARD or QCORR
 *
 * \todo QCORR branches are untested
 *
 * \see Test__check_grating_indices()
 */
void _check_grating_indices(grating_t *grating, indices_t *indices,
        int matrix_type) {
    int base_index_1 = indices->base_index_1;
    int base_index_2 = indices->base_index_2;
    int base_index_3 = indices->base_index_3;
    int base_index_4 = indices->base_index_4;

    int row_index_1 = indices->row_index_1;
    int row_index_2 = indices->row_index_2;
    int row_index_3 = indices->row_index_3;
    int row_index_4 = indices->row_index_4;

    int col_index_1 = indices->col_index_1;
    int col_index_2 = indices->col_index_2;
    int col_index_3 = indices->col_index_3;
    int col_index_4 = indices->col_index_4;

    int num_of_ports = grating->num_of_ports;

    switch (num_of_ports) {
        case 2:
            if (matrix_type == STANDARD) {
                // check that we reset the non-used indices
                assert(base_index_3 == 0);
                assert(base_index_4 == 0);
                assert(row_index_3 == 0);
                assert(row_index_4 == 0);
                assert(col_index_3 == 0);
                assert(col_index_4 == 0);

                // just to be really careful, check that the base indices are in the
                // right range (namely 0 <= base_index_[1-4] < inter.num_eqns)
                assert(base_index_1 >= 0);
                assert(base_index_1 < inter.num_eqns);
                assert(base_index_2 >= 0);
                assert(base_index_2 < inter.num_eqns);

                // make sure the values are sensible
                assert(row_index_1 > -1);
                assert(row_index_1 < inter.num_eqns);
                assert(row_index_2 > -1);
                assert(row_index_2 < inter.num_eqns);

                assert(col_index_1 > -1);
                assert(col_index_1 < inter.num_eqns);
                assert(col_index_2 > -1);
                assert(col_index_2 < inter.num_eqns);

                // don't assign to diagonal elements
                assert(col_index_2 != row_index_2);

                // don't assign to diagonal elements
                assert(col_index_1 != row_index_2);
                assert(col_index_2 != row_index_1);

                // don't assign to diagonal elements
                assert(col_index_1 != row_index_1);
            } else if (matrix_type == SIGNAL_QCORR) {
                // check that we reset the non-used indices
                assert(base_index_3 == 0);
                assert(base_index_4 == 0);
                assert(row_index_3 == 0);
                assert(row_index_4 == 0);
                assert(col_index_3 == 0);
                assert(col_index_4 == 0);

                /* row_index_1 : col_index_1 */
                // don't assign to diagonal elements
                assert(col_index_1 != row_index_1);

                /* row_index_2 : col_index_2 */
                // don't assign to diagonal elements
                assert(col_index_2 != row_index_2);

                /* row_index_1 : col_index_2 */
                /* row_index_2 : col_index_1 */
                // don't assign to diagonal elements
                assert(col_index_1 != row_index_2);
                assert(col_index_2 != row_index_1);
            } else {
                bug_error("Incorrect noise type");
            }
            break;
        case 3:
            if (matrix_type == STANDARD) {
                // check that we reset the non-used indices
                assert(base_index_4 == 0);
                assert(row_index_4 == 0);
                assert(col_index_4 == 0);

                // just to be really careful, check that the base indices are in the
                // right range (namely 0 <= base_index_[1-4] < inter.num_eqns)
                assert(base_index_1 >= 0);
                assert(base_index_1 < inter.num_eqns);
                assert(base_index_2 >= 0);
                assert(base_index_2 < inter.num_eqns);
                assert(base_index_3 >= 0);
                assert(base_index_3 < inter.num_eqns);

                // make sure the values are sensible
                assert(row_index_1 > -1);
                assert(row_index_1 < inter.num_eqns);
                assert(row_index_2 > -1);
                assert(row_index_2 < inter.num_eqns);
                assert(row_index_3 > -1);
                assert(row_index_3 < inter.num_eqns);

                assert(col_index_1 > -1);
                assert(col_index_1 < inter.num_eqns);
                assert(col_index_2 > -1);
                assert(col_index_2 < inter.num_eqns);
                assert(col_index_3 > -1);
                assert(col_index_3 < inter.num_eqns);

                // don't assign to diagonal elements
                assert(col_index_1 != row_index_2);
                assert(col_index_2 != row_index_1);

                // don't assign to diagonal elements
                assert(col_index_1 != row_index_3);
                assert(col_index_3 != row_index_1);

                // don't assign to diagonal elements
                assert(col_index_2 != row_index_3);
                assert(col_index_3 != row_index_2);

                // don't assign to diagonal elements
                assert(col_index_1 != row_index_1);

                // don't assign to diagonal elements
                assert(col_index_2 != row_index_2);

                // don't assign to diagonal elements
                assert(col_index_3 != row_index_3);
            } else if (matrix_type == SIGNAL_QCORR) {
                // check that we reset the non-used indices
                assert(base_index_4 == 0);
                assert(row_index_4 == 0);
                assert(col_index_4 == 0);

                /* row_index_1 : col_index_2 */
                /* row_index_2 : col_index_1 */
                // don't assign to diagonal elements
                assert(col_index_1 != row_index_2);
                assert(col_index_2 != row_index_1);

                /* row_index_1 : col_index_3 */
                /* row_index_3 : col_index_1 */
                // don't assign to diagonal elements
                assert(col_index_1 != row_index_3);
                assert(col_index_3 != row_index_1);

                /* row_index_2 : col_index_3 */
                /* row_index_3 : col_index_2 */
                // don't assign to diagonal elements
                assert(col_index_2 != row_index_3);
                assert(col_index_3 != row_index_2);

                /* row_index_1 : col_index_1 */
                // don't assign to diagonal elements
                assert(col_index_1 != row_index_1);

                /* row_index_2 : col_index_2 */
                // don't assign to diagonal elements
                assert(col_index_2 != row_index_2);

                /* row_index_3 : col_index_3 */
                // don't assign to diagonal elements
                assert(col_index_3 != row_index_3);
            } else {
                bug_error("Incorrect noise type");
            }
            break;
        case 4:
            if (matrix_type == STANDARD) {
                // just to be really careful, check that the base indices are in the
                // right range (namely 0 <= base_index_[1-4] < inter.num_eqns)
                assert(base_index_1 >= 0);
                assert(base_index_1 < inter.num_eqns);
                assert(base_index_2 >= 0);
                assert(base_index_2 < inter.num_eqns);
                assert(base_index_3 >= 0);
                assert(base_index_3 < inter.num_eqns);
                assert(base_index_4 >= 0);
                assert(base_index_4 < inter.num_eqns);

                // make sure the values are sensible
                assert(row_index_1 > -1);
                assert(row_index_1 < inter.num_eqns);
                assert(row_index_2 > -1);
                assert(row_index_2 < inter.num_eqns);
                assert(row_index_3 > -1);
                assert(row_index_3 < inter.num_eqns);
                assert(row_index_4 > -1);
                assert(row_index_4 < inter.num_eqns);

                assert(col_index_1 > -1);
                assert(col_index_1 < inter.num_eqns);
                assert(col_index_2 > -1);
                assert(col_index_2 < inter.num_eqns);
                assert(col_index_3 > -1);
                assert(col_index_3 < inter.num_eqns);
                assert(col_index_4 > -1);
                assert(col_index_4 < inter.num_eqns);

                // don't assign to diagonal elements
                assert(col_index_1 != row_index_2);
                assert(col_index_2 != row_index_1);

                // don't assign to diagonal elements
                assert(col_index_1 != row_index_3);
                assert(col_index_3 != row_index_1);

                // don't assign to diagonal elements
                assert(col_index_2 != row_index_4);
                assert(col_index_4 != row_index_2);

                // don't assign to diagonal elements
                assert(col_index_3 != row_index_4);
                assert(col_index_4 != row_index_3);
            } else if (matrix_type == SIGNAL_QCORR) {
             

                /* row_index_1 : col_index_2 */
                /* row_index_2 : col_index_1 */
                // make sure that we're not assigning to the diagonal
                assert(col_index_1 != row_index_2);
                assert(col_index_2 != row_index_1);

                /* row_index_1 : col_index_3 */
                /* row_index_3 : col_index_1 */
                // make sure that we're not assigning to the diagonal
                assert(col_index_1 != row_index_3);
                assert(col_index_3 != row_index_1);

                /* row_index_2 : col_index_4 */
                /* row_index_4 : col_index_2 */
                // make sure that we're not assigning to the diagonal
                assert(col_index_2 != row_index_4);
                assert(col_index_4 != row_index_2);

                /* row_index_3 : col_index_4 */
                /* row_index_4 : col_index_3 */
                // make sure that we're not assigning to the diagonal
                assert(col_index_3 != row_index_4);
                assert(col_index_4 != row_index_3);
            } else {
                bug_error("Incorrect noise type");
            }
            break;
        default:
            bug_error("Incorrect number of ports");
            break;
    }
}
#endif

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
