// $Id$

/*!
 * \file kat_check.h
 * \brief Header file for object checking routines
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


/*! \todo move these definitions and code into a kat_check.h???  Is this
 * reducing the complexity?  Would this reduce coupling between other parts
 * of the code?
 */

#ifndef KAT_CHECK_H
#define KAT_CHECK_H

int check_ABCD(ABCD_t s, double *stability);
void check_cavity(cavity_t *cavity);
void check_gauss(gauss_t *gauss);
void check_lens(lens_t *lens);
void check_sagnac(sagnac_t *lens);
void check_mirror(mirror_t *mirror);
void check_grating(grating_t *grating);
void check_diode(diode_t *diode);
void check_dbs(dbs_t *diode);
void check_beamsplitter(beamsplitter_t *bs);
void check_space(space_t *space);
void check_modulator(modulator_t *modulator);

//! Wrapper macro for _check_mirror_indices()
#ifndef NDEBUG
#define check_mirror_indices(mirror, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
_check_mirror_indices(mirror, &indices, noise_type);
#else
#define check_mirror_indices(mirror, noise_type)
#endif
void _check_mirror_indices(mirror_t *mirror, indices_t *indices,
        int noise_type);

//! Wrapper macro for _check_beamsplitter_indices()
#ifndef NDEBUG
#define check_beamsplitter_indices(beamsplitter, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.base_index_3 = base_index_3;\
    indices.base_index_4 = base_index_4;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.row_index_3 = row_index_3;\
    indices.row_index_4 = row_index_4;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
    indices.col_index_3 = col_index_3;\
    indices.col_index_4 = col_index_4;\
    _check_beamsplitter_indices(beamsplitter, &indices, noise_type);
#else
#define check_beamsplitter_indices(beamsplitter, noise_type)
#endif
void _check_beamsplitter_indices(beamsplitter_t *bs, indices_t *indices,
        int noise_type);

//! Wrapper macro for _check_modulator_indices()
#ifndef NDEBUG
#define check_modulator_indices(modulator, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
    _check_modulator_indices(modulator, &indices, noise_type);
#else
#define check_modulator_indices(modulator, noise_type)
#endif
void _check_modulator_indices(modulator_t *modulator, indices_t *indices,
        int noise_type);

//! Wrapper macro for _check_space_indices()
#ifndef NDEBUG
#define check_sagnac_indices(sagnac, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
    _check_sagnac_indices(sagnac, &indices, noise_type);
#else
#define check_sagnac_indices(sagnac, noise_type)
#endif
void _check_sagnac_indices(sagnac_t *sagnac, indices_t *indices, int noise_type);

//! Wrapper macro for _check_space_indices()
#ifndef NDEBUG
#define check_space_indices(space, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
    _check_space_indices(space, &indices, noise_type);
#else
#define check_space_indices(space, noise_type)
#endif
void _check_space_indices(space_t *space, indices_t *indices, int noise_type);

//! Wrapper macro for _check_squeezer_indices()
#ifndef NDEBUG
#define check_squeezer_indices(squeezer, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
    _check_squeezer_indices(squeezer, &indices, noise_type);
#else
#define check_squeezer_indices(squeezer, noise_type)
#endif
void _check_squeezer_indices(squeezer_t *squeezer, indices_t *indices,
        int noise_type);

//! Wrapper macro for _check_lens_indices()
#ifndef NDEBUG
#define check_lens_indices(lens, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
    _check_lens_indices(lens, &indices, noise_type);
#else
#define check_lens_indices(lens, noise_type)
#endif
void _check_lens_indices(lens_t *lens, indices_t *indices, int noise_type);

//! Wrapper macro for _check_diode_indices()
#ifndef NDEBUG
#define check_diode_indices(diode, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
    _check_diode_indices(diode, &indices, noise_type);
#else
#define check_diode_indices(diode, noise_type)
#endif
void _check_diode_indices(diode_t *diode, indices_t *indices, int noise_type);

//! Wrapper macro for _check_grating_indices()
#ifndef NDEBUG
#define check_grating_indices(grating, noise_type) \
    indices_t indices;\
    indices.base_index_1 = base_index_1;\
    indices.base_index_2 = base_index_2;\
    indices.base_index_3 = base_index_3;\
    indices.base_index_4 = base_index_4;\
    indices.row_index_1 = row_index_1;\
    indices.row_index_2 = row_index_2;\
    indices.row_index_3 = row_index_3;\
    indices.row_index_4 = row_index_4;\
    indices.col_index_1 = col_index_1;\
    indices.col_index_2 = col_index_2;\
    indices.col_index_3 = col_index_3;\
    indices.col_index_4 = col_index_4;\
    _check_grating_indices(grating, &indices, noise_type);
#else
#define check_grating_indices(grating, noise_type)
#endif
void _check_grating_indices(grating_t *grating, indices_t *indices,
        int noise_type);

#endif // KAT_CHECK_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
