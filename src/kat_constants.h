// $Id$

/*!
 * \file kat_constants.h
 * \brief Header file for constants used in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_CONSTANTS_H
#define KAT_CONSTANTS_H

#include "kat_config.h"

//#define DEVELOP //!< Indicates development version, this switches brand new code on

//! Macro to swap two variables
#define SWAP(a, b) {swap=(a);(a)=(b);(b)=swap;}

#define NOT !             //!< alias for the ! (not) operation

#define MESSAGE 0         //!< user message output type
#define WARNING 1         //!< user warning output type

#define UNIX 1            //!< Unix flag
#define MACOS 2            //!< MacOS flag
#define __WIN_32__ 3             //!< Windows 32 bit mingw setup flag
#define __WIN_64__ 4             //!< Windows 64 bit mingw setup flag

#define PROG_STRING "\r%s %s - %i%% %s" 
#define PROG_STRING5 "\r * Integrating %s: %3d%% estimated time %10s"  //!< String for progress and timing information
#define PROG_COMPLETE "\r * Integrating %s: done\n"

#define PROG_END_NEWLINE "\n"
#define PROG_CLEAR "                                                                                                                              "
#define INIT_FILE "kat.ini"  //!< Name of the initialisation file

#define DEFAULT_OUT_FORMAT "% #.15g "

//! Scale factor of signal amplitudes for transfer function computation
#define EPSILON 1.0      
#define TIMER_NAME_LEN 64    //!< length of timer names
#define FNAME_LEN 256        //!< File name length
#define PLOTFILE_LEN 4096    //!< Length of the buffers used for plot file computation
#define LINE_LEN 1024         //!< Line length
#define BUFF_LEN 1024         //!< Stream buffer length
#define ERR_MSG_LEN 1024      //!< Error message length
//! Max length of tokens when reading command strings from kat files
#define MAX_TOKEN_LEN 512      
#define REST_STRING_LEN 82   //!< Length of "rest" of string when reading cmds
#define SMALL 1e-9           //!< The definition of a small number
#define ZERO 1e-15           //!< The definition of zero
#define BIG 1e100            //!< The definition of a big number
#define ALLOC_LEN 1024       //!< Allocation length
#define FORM_LEN 64          //!< Length of home-made num2str output
#define MIN_INT_OP 200       //!< Minimum number of integration operations
#define MAX_MAP_LINE 65536   //!< Maximum length of a 
#ifndef PI
#define PI      3.14159265358979323846264338327 //!< Definition of pi
#endif
#define TWOPI   6.28318530717958647692528676654 //!< Definition of 2*pi
#define DEG     57.2957795130823208767981548142 //!< Convert from rad to deg
#define RAD  0.01745329251994329576923690768485 //!< Convert from deg to rad
#define SQRTTWO 1.41421356237309514547462185874 //!< Definition of sqrt(2)
#define SQRTPI  1.77245385090551602729816748334
#define R2DB 8.685889638065037 // 20 log_10(e^1) Converts the squeezing factor to db
//define sparse matrix solvers
// ddb - values now in solver_t enum
//#define KLU 1
//#define KLU_FULL 2
//#define SUPERLU 3
//#define NICSLU 4

#define ENV_KATINI 1          //!< use kat.ini from environmental variable
#define DEFAULT_KATINI 2      //!< use default kat.ini 

#define IN_OUT 1 /** If node is IN_OUT then port 1 is the input and port 2 is the output for the attached non-spcae component */
#define OUT_IN 2 /** If node is OUT_IN then port 2 is the input and port 1 is the output for the attached non-spcae component */
#define EITHER_DIR 3 /** If component attached to this node is not fussed which directions the nodes should be this is set as the direction. Note that this direction must at a later date be defined as IN_OUT or OUT_IN when it comes to building the matrix. */

// mechanical transfer function types
#define GENERAL_TF 1  /** User specified poles and zeros with complex numbers */
#define Q_F_TF 2 /** User specified poles and zeros using Q and frequency values */

//#define LOG10  2.3025850929940456840
#define C_LIGHT   299792458.0   //!< Definition of the speed of light
#define H_PLANCK 6.62607004e-34 //!< Definition of Planck's constant
#define C_ELEC   1.6022e-19     //!< Definition of electric charge - e

#define LAMBDA_YAG 1064.0e-9  //!< Wavelength of Nb:YAG laser

//#define WHITESPACE "\0\1\2\3\4\5\6\7\b\t\n\11\12\r\14\15\16\17\18\19\20\21\22\23\24\25\26\27\28\29\30\31\32\127"
#define WHITESPACE " \t\r"  //!< Definition of whitespace characters

#define MAX_INCLUDES 5
#define MAX_TOKEN 25        //!< Maximum number of tokens
#define MAX_BLOCKS 100
#define NOT_SET -1          //!< Returned if Gauss beam param not set for node

/* component photodector */
#define OFF 0              //!< Photodetector off
#define ON 1               //!< Photodetector on
#define NORM 2             //!< Photodetector normal

#define MAX_MHOMODYNE 4    //!< maximum number of pairs of homodynes
#define MAX_ORDER 6        //!< maximum order of EOM sidebands
#define MAX_DPARAM 20      //!< maximum number of differentiation commands
#define MAX_DEMOD 5        //!< maximum number of demodulations
#define MAX_QUANTUM_MIX 32 //!< max number of mixes for quantum noise (2^5)
#define MAX_MIX 16         //!< this MUST be 2^(MAX_DEMOD-1)
#define MAX_GNUTERM 20     //!< maximum number of gnuplot terminals
#define MAX_PYTERM 20      //!< maximum number of python terminals
#define MAX_PD_TYPE 20     //!< maximum number of photodetector types
#define MAX_MAPS 20        //!< maximum number of maps per mirror
#define MAX_VARIABLES 30   //!< maximum number of dummy parameters
#define MAX_TIMERS 10      //!< maximum number of timers that can be going at once
#define MAX_POLES_ZEROS 100//!< maximum number of poles or zeros for a transfer function
#define MAX_DOF_ITEMS 5    //!< maximum number of elements that can be included in one degree of freedom
#define NOT_FOUND -32768   //!< not found error code
#define GND_NODE -1        //!< unconnected node (beam dump)
#define NODE_UNUSED -2     //!< node not used code
#define NO_GNUTERM -1      //!< no gnuplot terminal code
#define NO_PYTERM -1       //!< no gnuplot terminal code

#define PLOT_PYTHON  1     //!< default plotting with python
#define PLOT_GNUPLOT 2     //!< default plotting with gnuplot

#define NO_FREQ -1         //!< Returned when no frequency specified
#define FLIN 1             //!< Set if axis is linear
#define FLOG 2             //!< Set if axis is logarithmic

#define OTYPE_DB 1         //!< Output in decibels
#define OTYPE_DEG 2        //!< Output in degrees (-pi:pi)
#define OTYPE_DEGP 3       //!< Output in degrees (0:2*pi)
#define OTYPE_DEGM 4       //!< Output in degrees (-0:-2*pi)
#define OTYPE_RE 5         //!< Output of real part
#define OTYPE_IM 6         //!< Output of imaginary part
#define OTYPE_ABS 7        //!< Output of absolute value
#define NO_TYPE 8          //!< No output type defined

#define MODTYPE_AM 10      //!< Amplitude modulation type
#define MODTYPE_PM 20      //!< Phase modulation type
#define MODTYPE_YAW 30     //!< Tilt modulation yaw type
#define MODTYPE_PITCH 40     //!< Tilt modulation pitch type

/* photo detector types */
#define AD 1               //!< Amplitude detector
#define PD0 2              //!< DC photodector (no modulation)
#define PD1 4              //!< Generic photodetector
#define SHOT 16            //!< Shot noise detector
#define BEAM 32            //!< Beam shape detector
#define BP 64              //!< Beam parameter detector
#define PG 128             //!< ???
#define FEEDBACK 256       //!< Lock output
#define UFUNCTION 512      //!< ???
#define QNOISE 1024        //!< Quantum noise detector
#define QSHOT 2048         //!< Quantum shotnoise detector (like shot detector)
#define CP 4096            //!< Cavity parameter detector
#define CONV 8192          //!< Convolution detector
#define XD 16384           //!< optic motion detector
#define QD 32768           //!< quadrature detector
#define SD 65536           //!< squeezing detector
#define RGAIN 131072       //!< open loop gain detector
#define HOMODYNE 262144    //!< homodyne detector
#define SD2 524288         //!< squeezing detector2
#define FORCERP 1048576    //!< Radiation pressure force detector
#define MINIMIZER 2097152  //!< Minimizer output
#define MHOMODYNE 4194304  //!< multi-homodyne detector

/* detection modes for a beam parameter detector "bp" */
#define BPW  1  //!< Detect waist beam parameter (w)
#define BPW0 2  //!< Detect spot size beam parameter (w0)
#define BPG  3  //!< Detect Gouy phase
#define BPZ  4  //!< Detect distance from waist beam parameter (z)
#define BPZR 5  //!< Detect Rayleigh range (z_R)
#define BPQ  6  //!< Detect q beam parameter (aka complex radius of curvature)
#define BPRC 7  //!< Detect radius of curvature Rc


/* detection modes for a cavity parameter detector "cp" */
#define CPW0 1  //!< Detect waist size beam parameter (w0)
#define CPW  2  //!< Detect spot size beam parameter (w)
#define CPZ  3  //!< Detect distance from waist beam parameter (z)
#define CPQ  4  //!< Detect waist beam parameter (q)
#define CPF  5  //!< Detect finesse
#define CPL  6  //!< Detect round-trip loss
#define CPOL 7  //!< Detect optical path length (round-trip)
#define CPFSR 8  //!< Detect free spectral range
#define CPFWHM 9  //!< Detect full width at half maximum
#define CPPOLE 10  //!< Detect cavity pole
#define CPRC 11  //!< Detect radius of curvature Rc
#define CPS 12  //!< Detect stability factor
#define CPZR  13  //!< Detect raleigh range
#define CPGOUY 14 //!< Round trip gouy phase
#define CPA 15 //!< A element of roundtrip ABCD matrix
#define CPB 16 //!< B element of roundtrip ABCD matrix
#define CPC 17 //!< C element of roundtrip ABCD matrix
#define CPD 18 //!< D element of roundtrip ABCD matrix
#define CPFSEP 19 //!< Frequency separation

#define REAL 1            //!< Real valued number
#define COMPLEX 2         //!< Complex valued number

#define MAX_PHASE 1             //!< Photodetector phase given by 'max' keyword
// #define MIN 2               //!< Not set
#define USER_PHASE 4            //!< Photodetector phase given as number

/* components */
#define MIRROR 1          //!< Mirror component type
#define BEAMSPLITTER 2    //!< Beam splitter component type
#define SPACE 3           //!< Free space component type
#define LENS 4            //!< Lens component type
#define DIODE 5           //!< Diode component type
#define LIGHT_INPUT 6     //!< Input light field component type
#define MODULATOR 7       //!< Modulator component type
#define FSIG 8            //!< Input for transfer function computation
#define OUT 9             //!< Output light field component type
#define GRATING 10        //!< Grating component type 
#define SQUEEZER 11       //!< Squeezer component type
#define BEAMPARAM 12      //!< not a component!! but needed for setting with xaxis or put
#define VARIABLE 13       //!< dummy component, used for storing a parameter
#define SAGNAC 14         //!< Sagnac component type
#define BLOCK 15          //!< Block component type
#define PUT 16
#define TF 17             /** Transfer function */
#define QFEEDBACK 18
#define MOTIONLINK 19
#define SPACECONNECTION 20
#define DBS 21           //!< Isolator component type
#define LOCK_CMD 22

#define X1 -12            //!< x axis tag
#define X2 -13            //!< y axis tag
#define X3 -14            //!< z axis tag
#define MX1 -15           //!< minus x axis tag
#define MX2 -16           //!< minus y axis tag
#define MX3 -17           //!< minus z axis tag
#define _FS -18           //!< minus y axis tag
#define _MFS -19           //!< minus z axis tag
#define XSET 1            //!< A value was set via the 'set' command
#define FUNC 2            //!< Output of a function
#define LOCK 3            //!< Output of a lock iteration

/* type of frequency component */
#define CARRIER_FIELD 1   //!< Carrier frequency component
#define MODULATOR_FIELD 2 //!< Modulator frequency component
#define SIGNAL_FIELD 3    //!< Signal frequency component
#define CARRIER_SIGNAL 4  //!< Signal modulation of the carrier
#define MODULATOR_SIGNAL 5//!< Signal modulation of modulations
#define USER_FIELD 6    //!< user frequency field component

/* sidebands */
#define UPPER 1           //!< Only have upper side band
#define LOWER 2           //!< Only have lower side band
#define BOTH 3            //!< Have both side bands

/* output scaling */
#define SCALE_METER 1        //!< Output scale in metres
#define SCALE_RAD2DEG 2      //!< Output scale in degrees
#define SCALE_USER 3         //!< Output scale is user-defined
#define SCALE_AMPERE 4       //!< Output scale in amperes
#define SCALE_QSHOT 5        //!< Output scale for qshot command
#define SCALE_QSHOT_METER 6  //!< Output scale for qshot command
#define SCALE_PSD 7
#define SCALE_ASD 8
#define SCALE_PSD_HF 9
#define SCALE_ASD_HF 10
#define SCALE_DEG2RAD 11

/* mirror, bs, grating and laser attributes */
#define NO_TYPE_SET 0     //!< Default type value
#define MASS 1            //!< Mass of the mirror/beam splitter
#define ROC 2             //!< Radius of curvature of mirror
#define XROC 4            //!< Radius of curvature of mirror in x direction
#define YROC 8            //!< Radius of curvature of mirror in y direction
#define XANGLE 16         //!< Misalignment angle in x plane/direction
#define YANGLE 32         //!< Misalignment angle in y plane/direction
#define GOUY 64           //!< Gouy phase
#define GOUYX 128         //!< Gouy phase in x plane
#define GOUYY 256         //!< Gouy phase in y plane
#define ALPHA 512         //!< Angle of incidence
#define ETA 1024          //!< Grating coupling efficiency
#define LASER_NOISE 2048  //!< Laser noise attribute
#define APERTURE 4096     //!< Aperture attribute
#define MAP_DEG 8192      //!< Aperture attribute
#define MECH_TF 16384
#define XOFF 32768
#define YOFF 65536
#define MOIX 131072
#define MOIY 262144
#define MECH_RX_TF 524288
#define MECH_RY_TF 1048576
#define HOM_ANGLE 2097152
#define BACKSCATTER 4194304
#define OFFSET 8388608

/* phase amplitude map atributes */
#define NO_MAP_SET 0         //!< Default map value
#define PHASE_MAP_REFL 1     //!< PhaseMap in reflection
#define PHASE_MAP_TRANS 2    //!< PhaseMap in transmission
#define PHASE_FUNC_REFL 4    //!< PhaseFunction in reflection
#define PHASE_FUNC_TRANS 8   //!< PhaseFunction in transmission
#define AMP_MAP_REFL 16      //!< AmplitudeMap in reflection
#define AMP_MAP_TRANS 32     //!< AmplitudeMap in transmission
#define AMP_FUNC_REFL 64     //!< AmplitudeFunction in reflection
#define AMP_FUNC_TRANS 128   //!< AmplitudeFunction in transmission


/* shotnoise modes */
#define STANDARD 1        //!< Shotnoise is standard kind
#define SIGNAL_QCORR 2           //!< Shotnoise includes quantum corrections

/* quantum noise and radiation pressure modes */
#define QN_AND_RP_OFF 0   //!< Quantum noise and radiation pressure off
#define QN_ONLY 1         //!< Only quantum noise simulated
#define RP_ONLY 2         //!< Only radiation pressure simulated
#define QN_AND_RP 3       //!< Quantum noise and radiation pressure simulated

/* base/unit vacuum noise level */
#define UNIT_VACUUM 1.0   //!< Base/unit vacuum noise, i.e. E=hf

/* HG mode */
#define SAGITTAL 1        //!< Hermite-Gauss mode is sagittal
#define TANGENTIAL 2      //!< Hermite-Gauss mode is tangential

/* methods for computing coupling coefficients for Hermite-Gauss modes */
#define KNM_BAYER 0    //!< Bayer method for computing HG coupling coeffs
#define KNM_FALTUNG 1  //!< Faltung method for computing HG coupling coeffs
#define KNM_DEBUG 2    //!< Debug mode for computing HG coupling coeffs

/* signal types */
#define SIG_AMP 1         //!< Amplitude signal
#define SIG_PHS 2         //!< Phase signal
#define SIG_FRQ 3         //!< Frequency signal
#define SIG_X   4         //!< Signal is a deviation in x axis
#define SIG_Y   5         //!< Signal is a deviation in y axis
#define SIG_SURF 6        /** Signal applied to a surface mode */
#define SIG_FZ   7         //!< Signal is a deviation in x axis
#define SIG_FRX   8         //!< Signal is a deviation in x axis
#define SIG_FRY   9         //!< Signal is a deviation in y axis
#define SIG_FSURF 10
#define SIG_Z     11
#define NOSIGNAL     12         /** No signal applied, just a name given to signal frequency */
#define SIG_SAGNAC 13
#define SIG_RC 14

/* phasemap types */
#define PM_REFL  1        //!< Reflection   phasemap from file 
#define PM_TRANS 2        //!< Transmission phasemap from file
#define PF_REFL  4        //!< Reflection phase function   (map from function)  
#define PF_TRANS 8        //!< Transmission phase function (map from function)   
#define AM_REFL  16       //!< Reflection   amplitudemap from file 
#define AM_TRANS 32       //!< Transmission amplitudemap from file
#define AF_REFL  64       //!< Reflection amplitude function  (map from function)  
#define AF_TRANS 128      //!< Transmission amplitude function (map from function)   

/* mirror and beam splitter modes */
//! Reflectance and transmittance used to define the mirror or beamsplitter
#define REFLECTANCE_TRANSMITTANCE 0  
//! Transmittance and loss used to define the mirror or beamsplitter
#define TRANSMITTANCE_LOSS        1
//! Reflectance and loss used to define the mirror or beamsplitter
#define REFLECTANCE_LOSS          2

#define TRANSMISSION 0
#define REFLECTION 1

// type of surface maps
#define PHASE_MAP 1
#define ABSORPTION_MAP 2 
#define REFLECTIVITY_MAP 3

// defines number of coupling coefficients matrices there are: maps, bayer-helm and apertures
// Affects both mirrors and beamsplitters
#define NUM_KNM_TYPES 4

/* modes for sparese matrix get_..._elements functions */
#define COUNT 0           //!< COUNT mode, prescane elements for CCS storage
#define GETELEMENT 1      //!< GETELEMENT mode, get pointers to matrix elements

#define BETA_SCALE 2.0    //!< Scaling for misalignment angles

#define DEFAULT_KNM_CHANGE_Q 2

#define DEFAULT_KNM_ORDER_FIRST 2
#define DEFAULT_KNM_ORDER_SECOND 1
#define DEFAULT_KNM_ORDER_THIRD 3
#define DEFAULT_KNM_ORDER_FOURTH 4

#endif // KAT_CONSTANTS_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
