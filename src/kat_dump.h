// $Id$

/*!
 * \file kat_dump.h
 * \brief Header file for data-dumping-related routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

void dump_constants(FILE *fp);
void dump_all_nodes(FILE *fp);
void dump_pdtype(FILE *fp);
void dump_ks(FILE *fp);
void dump_beamshape(FILE *fp, int order, complex_t **coeff,
        complex_t qx, complex_t qy, double nr,
        double xwide, double ywide, int xsteps, int ysteps);
void dump_powers(FILE *fp, ifo_matrix_vars_t *matrix);
void dump_trace(FILE *fp);
void dump_init(FILE *fp);
void dump_mirror(FILE *fp, mirror_t mirror);
void dump_space(FILE *fp, space_t space);
void dump_sagnac(FILE *fp, sagnac_t sagnac);
void dump_diode(FILE *fp, diode_t diode);
void dump_beamsplitter(FILE *fp, beamsplitter_t *bs);
void dump_all_cavities(FILE *fp);
void dump_cavity(FILE *fp, cavity_t cavity);
void dump_ABCD(FILE *fp, ABCD_t s);
void dump_all_gratings(FILE *fp);
void dump_all_mirrors(FILE *fp);
void dump_all_lenses(FILE *fp);
void dump_all_spaces(FILE *fp);
void dump_all_diodes(FILE *fp);
void dump_all_put_cmds(FILE *fp);
void dump_all_set_cmds(FILE *fp);
void dump_all_func_cmds(FILE *fp);
void dump_all_lock_cmds(FILE *fp);
void dump_all_beamsplitters(FILE *fp);
void dump_all_gauss_cmds(FILE *fp);
void dump_xaxis(FILE *fp);
void dump_deriv(FILE *fp);
void dump_yaxis(FILE *fp);
void dump_all_light_inputs(FILE *fp);
void dump_all_light_outputs(FILE *fp);
void dump_all_gnuplot_cmds(FILE *fp);
void dump_input(FILE *fp);
void dump_all_modulators(FILE *fp);
void dump_all_frequencies(FILE *fp);
void dump_all_signals(FILE *fp);
void debug(FILE *fp);

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
