/*!
 * \file kat_fortran.h
 * \brief Header file for legacy Fortran routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_FORTRAN_H
#define KAT_FORTRAN_H

//scb -- Phasefunction related
complex_t integrate_pf(int n1, int n2, int m1, int m2, complex_t qx1,
        complex_t qy1, complex_t qx2, complex_t qy2,
        double nr);
void function_pf(int *din, double point[3], int *dout, double value[3]);


void function_(int *din, double point[3], int *dout, double value[3]);
complex_t faltung_adapt(int n1, int n2, int m1, int m2,
        complex_t qx1, complex_t qy1, complex_t qx2, complex_t qy2,
        double gamma_x, double gamma_y, double nr, const unsigned int knm_flags);

#endif // KAT_FORTRAN_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
