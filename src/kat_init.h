// $Id$

/*!
 * \file kat_init.h
 * \brief Header file for initialisation routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_INIT_H
#define KAT_INIT_H

void initialise_simulation_variables(void);
void prepare_maps(void);
void read_init_pdtype(void);
void read_env(void);
void read_init(void);
void read_init_double(char *s, int l, double *dp);
void read_init_integer(char *s, int l, int *ip);
void read_init_string(char *s, int l, char *res);
void read_init_gnuterm(char *sn, int l, FILE *fp);
void read_init_pyterm(char *sn, int l, FILE *fp);
void ri_pdtype(char *sn, int l, FILE *fp);
void check_for_star(char *s);
void read_init_dummy(FILE *fp, int mode);
void ri_gnuplot(FILE *fp);
void ri_python(FILE *fp);
void ri_matlab(FILE *fp);
void ri_matlabplot(FILE *fp);
void init_def(void);
void init_dump(void);
void setup_system(void);
void check_photodetector_demod_phases(void);
void clear_output_data_list(void);
void clear_lock_list(void);
void check_and_rebuild_gratings(void);
void check_derivative_stepsize(void);
void set_hermite_gauss_mode(void);
void set_default_terminal(void);
void check_axis_for_beam_analyser(void);
void set_default_yaxis_values(void);
void check_yaxis_settings(void);
void set_output_scaling_factors(void);
void check_for_plottable_outputs(void);
void check_refractive_index_at_beamsplitters(void);
void setup_hermite_gauss_extension(void);
void trace_hermite_gauss_beam(void);
void set_gouy_phase_for_spaces(void);
void set_progress_printing_parameters(void);
void init_ks(void);
void retrace(void);
void alloc_merged_map_data(surface_merged_map_t *map0);

#endif // KAT_INIT_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
