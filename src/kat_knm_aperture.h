/* 
 * File:   kat_knm_aperture.h
 * Author: Daniel
 *
 * Created on 27 February 2013, 01:09
 */

#ifndef KAT_KNM_APERTURE_H
#define	KAT_KNM_APERTURE_H

void alloc_knm_accel_mirror_aperture_mem(long *bytes);

void fill_mirror_knm_aperture_matrix_analytic(mirror_t *mirror, double nr1);
void fill_mirror_knm_square_aperture(mirror_t *mirror, double nr1, double nr2);

#endif	/* KAT_KNM_APERTURE_H */

