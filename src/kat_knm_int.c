/**
 * kat_knm_int.c
 * 
 * Contains the code that does all the necessary integrating and interpolating for
 * maps. Mirror and beamsplitter specific code should be put in kat_knm_mirror/bs
 */

#include "kat.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_fortran.h"
#include "kat_aux.h"
#include "kat_dump.h"
#include "kat_mem.h"
#include "kat_check.h"
#include "kat_optics.h"
#include "kat_knm_mirror.h"
#include "kat_knm_bs.h"
#include "kat_knm_int.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#if INCLUDE_CUBA == 1
#include "cuba.h"
#endif
#include <stdlib.h>
#include <pthread.h>

extern init_variables_t init;
extern interferometer_t inter;
extern options_t options;
extern local_var_t vlocal;
extern FILE * fp_log;
extern FILE * ipfile;
extern FILE * idfile;

extern const complex_t complex_i; //!< sqrt(-1) or 0 + i
extern const complex_t complex_1; //!< 1 but in complex space: 1 + 0i
extern const complex_t complex_m1; //!< -1 but in complex space: -1 + 0i
extern const complex_t complex_0; //!< 0 but in complex space: 0 + 0i

pthread_mutex_t mxinterpfile = PTHREAD_MUTEX_INITIALIZER;

const int MAX_NEWTON_WEIGHT = 15;

const double newton_weights_0[] = {1};
const double newton_weights_1[] = {5.000000000000000e-01, 5.000000000000000e-01};
const double newton_weights_2[] = {3.333333333333333e-01, 1.333333333333333e+00, 3.333333333333333e-01};
const double newton_weights_3[] = {3.750000000000000e-01, 1.125000000000000e+00, 1.125000000000000e+00, 3.750000000000000e-01};
const double newton_weights_4[] = {3.111111111111111e-01, 1.422222222222222e+00, 5.333333333333333e-01, 1.422222222222222e+00, 3.111111111111111e-01};
const double newton_weights_5[] = {3.298611111111111e-01, 1.302083333333333e+00, 8.680555555555556e-01, 8.680555555555556e-01, 1.302083333333333e+00, 3.298611111111111e-01};
const double newton_weights_6[] = {2.928571428571429e-01, 1.542857142857143e+00, 1.928571428571429e-01, 1.942857142857143e+00, 1.928571428571429e-01, 1.542857142857143e+00, 2.928571428571429e-01};
const double newton_weights_7[] = {3.042245370370371e-01, 1.449016203703704e+00, 5.359375000000000e-01, 1.210821759259259e+00, 1.210821759259259e+00, 5.359375000000000e-01, 1.449016203703704e+00, 3.042245370370371e-01};
const double newton_weights_8[] = {2.790828924162257e-01, 1.661516754850088e+00, -2.618694885361552e-01, 2.961834215167548e+00, -1.281128747795415e+00, 2.961834215167548e+00, -2.618694885361552e-01, 1.661516754850088e+00, 2.790828924162257e-01};
const double newton_weights_9[] = {2.869754464285714e-01, 1.581127232142857e+00, 1.084821428571429e-01, 1.943035714285714e+00, 5.803794642857143e-01, 5.803794642857143e-01, 1.943035714285714e+00, 1.084821428571429e-01, 1.581127232142857e+00, 2.869754464285714e-01};
const double newton_weights_10[] = {2.683414836192614e-01, 1.775359414248303e+00, -8.104357062690396e-01, 4.549462882796216e+00, -4.351551226551226e+00, 7.137646304312971e+00, -4.351551226551226e+00, 4.549462882796216e+00, -8.104357062690396e-01, 1.775359414248303e+00, 2.683414836192614e-01};
const double newton_weights_11[] = {2.742655400315991e-01, 1.703408943727954e+00, -4.088615497317754e-01, 3.186240802744709e+00, -1.211958980930335e+00, 1.956905244157848e+00, 1.956905244157848e+00, -1.211958980930335e+00, 3.186240802744709e+00, -4.088615497317754e-01, 1.703408943727954e+00, 2.742655400315991e-01};
const double newton_weights_12[] = {2.596738499595642e-01, 1.884433281004710e+00, -1.443863565006422e+00, 6.797986775129632e+00, -9.798067646639074e+00, 1.665311602683031e+01, -1.670655744255744e+01, 1.665311602683031e+01, -9.798067646639074e+00, 6.797986775129632e+00, -1.443863565006422e+00, 1.884433281004710e+00, 2.596738499595642e-01};
const double newton_weights_13[] = {2.643513483666065e-01, 1.818389108455210e+00, -1.010254313749384e+00, 5.042650005170477e+00, -4.900009612317529e+00, 6.677790334230020e+00, -1.392916870155399e+00, -1.392916870155399e+00, 6.677790334230020e+00, -4.900009612317529e+00, 5.042650005170477e+00, -1.010254313749384e+00, 1.818389108455210e+00, 2.643513483666065e-01};
const double newton_weights_14[] = {2.524825970211773e-01, 1.989229125698261e+00, -2.156354858732945e+00, 9.796484746163758e+00, -1.853596647905136e+01, 3.533708856210091e+01, -4.701010285307971e+01, 5.465427831975980e+01, -4.701010285307971e+01, 3.533708856210091e+01, -1.853596647905136e+01, 9.796484746163758e+00, -2.156354858732945e+00, 1.989229125698261e+00, 2.524825970211773e-01};
const double newton_weights_15[] = {2.563094965743806e-01, 1.927610680160754e+00, -1.690843575892274e+00, 7.605640623152681e+00, -1.134439672265444e+01, 1.787040524258828e+01, -1.452007817243157e+01, 7.395352428488877e+00, 7.395352428509341e+00, -1.452007817245885e+01, 1.787040524258828e+01, -1.134439672267490e+01, 7.605640623152681e+00, -1.690843575892274e+00, 1.927610680160861e+00, 2.563094965743873e-01};

// Weights for Newton-Cotes integration. Use this array to select which order to use
const double* newton_weights[] = {newton_weights_0, newton_weights_1, newton_weights_2, newton_weights_3, newton_weights_4, newton_weights_5, newton_weights_6, newton_weights_7, newton_weights_8, newton_weights_9, newton_weights_10, newton_weights_11, newton_weights_12, newton_weights_13, newton_weights_14, newton_weights_15};

typedef struct cuba_int_params {
    void *p;
    KNM_COMPONENT_t knmcmp;
} cuba_int_params_t;


/**
 * Produces a composite rule for Newton-Cotes based integration. 
 * 
 * @param W Array to put composite rule
 * @param length Length of W
 * @param order Newton-Cotes integration order (0 <= order <= MAX_NEWTON_WEIGHT)
 */
void create_newton_cotes_composite_weights(double *W, size_t length, int order) {
    assert(W != NULL);
    assert(order >= 0 && order <= MAX_NEWTON_WEIGHT);
    
    const double *weights = newton_weights[order];
    
    int i=0, j=0, k=0;
    
    if (order == 0) {
			for (i=0; i<(int)length; i++) {
            W[i] = weights[0];
        }
    } else {
        // Whilst there is still room to an extra integration segment...
			while((i+1) < (int)length) {
            // Check if current order will fit into the result weights array,
            // if it doesn't reduce the order until it fits
				while((int)length - (i+1) < order) {
                order--;
                weights = newton_weights[order];
                if(order < 0) bug_error("Unexpected result");
            }

            // Now this order will fit add it to the composite rule
            for(j=i, k=0; j < i+order+1; j++, k++) {
                W[j] += weights[k];
            }

            // Move along to next segment of integration
            i += order;
        }
    }
}

/**
 * Merged map interpolation involves interpolating both the amplitude and phase
 * of a merged map. Depending on which coupling coefficient you want to 
 * calculate you need to choose the knum argument correctly: this determines
 * whether to use reflection or transmission parts.
 * 
 * This function applies the refractive index to the phase maps.
 * 
 * This method also deals with providing the A and phi in what should be the
 * outgoing beams (from a mirror surface) coordinate system. So the mirror map
 * is applied to a mirror looking at it's surface along the beam axis. Positive
 * values correspond to a surface that is closer, negative is further away.
 * So the beam is integrated in the outgoing modes coordinate system, this
 * causes many headaches and requires careful thinking. The best way (I find) is
 * to consider a tilted surface as is considered next.
 * 
 * So below x,y,z are the maps coordinates, x',y',z' are the outgoing modes 
 * coordinates, this only applies to phase maps remember,
 * 
 *       x'          \                            
 *        ^           \<---map (tilt in this case)  y' o--> z' 
 *    K11 |            \                               | 
 *        |             \                              v  K12
 *   z'<--o y'  n1 z <---o y   n2                      x'
 *                       |\                         
 *        x'             | \                        y' o--> z' 
 *   K21  ^              |  \                          |
 *        |              |   \                         v  K22
 *   z'<--o y'           v                             x'
 *                       x
 * 
 * So from the above diagram we should see that K11 and K21 has x' = -x and 
 * z' = z whereas K22 and K12 has x' = x and z' = -z (the z'=-z corresponds to 
 * a phase sign flip). n1->n2 and n2->n1 should see 0 phase if refractive index 
 * is the same on both sides. You have to think consider when nr2 != nr1 to 
 * realise why it also works for K12 and K21.
 * 
 * The above should hold true so long as the applied phase map is not purely a
 * transmission map. A transmission only phase map should combine all the various
 * complex layers in a mirror that might exist: coatings, substrate, refractive
 * index variations, etc.; such a map does not portray a surface height variation.
 * This requires us to treat transmission phase maps differently. Now the phase
 * should not be sign flipped for K12 and K22; you can justify this by assuming
 * a transmission phase map means the surface is flat across the mirror, thus no
 * difference in travel in nr1 or nr2 needs to be considered.
 * 
 * If an incident angle is specified the effect is to essentially stretch the map
 * coordinate system in the x direction by a 1/cos(incident_angle) factor. Thus if
 * 90 degrees is specfied the beam will not see any of the map etc.
 * 
 * @param[in] map	Merged map that will be used for interpolation
 * @param[in] knum	specifies which knm to calculate: 0=k11, 1=k12, 2=k21, 3=k22
 * @param[in] px	x coordinate to interpolate
 * @param[in] py	y coordinate to interpolate
 * @param[out] A	interpolated amplitude
 * @param[out] phi	interpolated phase
 * @param[in] nr1   refractive index of node 1 side
 * @param[in] nr2   refractive index of node 2 side
 * @param[in] angle rotation of map [degrees]
 * @param[in] incident_angle incident angle of the beam [degrees]
 */
void interpolate_merged_map(surface_merged_map_t *map, KNM_COMPONENT_t knmcmp, int knum, const double px
        , const double py, double *const A, double *const phi, double nr1
        , double nr2, double angle) {

    assert(map != NULL);
       
    int N = map->cols;
    int M = map->rows;
    double *x = map->x;
    double *y = map->y;
    double **z_abs = NULL, **z_phs = NULL;
    double x0 = map->x0;
    double y0 = map->y0;
    double dx = map->xstep;
    double dy = map->ystep;

    int INTERP_SIZE = map->interpolation_size;
    interpolation_method_t INTERP_METHOD = map->interpolation_method;
    
    assert(INTERP_SIZE % 2 == 1); //Check interpolation grid size is an odd number
    assert(INTERP_SIZE >= 3); //Ensure that 3 or more points are used for
    //cubic spline

    switch(knmcmp){
        case MIRROR_CMP:
            switch (knum) {
                case MR11:
                case MR22:
                    z_abs = map->r_abs;
                    z_phs = map->r_phs;
                    break;
                case MR12:
                case MR21:
                    z_abs = map->t_abs;
                    z_phs = map->t_phs;
                    break;
            }
            break;
        case BEAMSPLITTER_CMP:
            switch (knum) {
                case BS12:
                case BS21:
                case BS34:
                case BS43:
                    z_abs = map->r_abs;
                    z_phs = map->r_phs;
                    break;
                case BS13:
                case BS31:
                case BS24:
                case BS42:
                    z_abs = map->t_abs;
                    z_phs = map->t_phs;
                    break;
            }
            break;
        default:
            bug_error("unhandled");
    }

    int INTERP_ULIM = (INTERP_SIZE - 1) / 2;
    int INTERP_LLIM = -(int) (INTERP_SIZE - 1) / 2;

    gsl_spline *splinex = NULL, *spliney = NULL;

    if (INTERP_METHOD != NEAREST) {
        splinex = gsl_spline_alloc((INTERP_METHOD == 1) ? gsl_interp_cspline : gsl_interp_linear, INTERP_SIZE);
        spliney = gsl_spline_alloc((INTERP_METHOD == 1) ? gsl_interp_cspline : gsl_interp_linear, INTERP_SIZE);
    }

    // xx and yy are the transformed x and y values that are used to interpolate
    // with but the transformation will depend on the direction and component.
    double xx = px;
    double yy = py;

    // From diagram above we need to invert x'
    // this is because beams going away from the n1 node  have coord system rotated 180 degrees
    if (knmcmp == MIRROR_CMP && ((knum == MR21) || (knum == MR11)))
        xx *= -1;
        
    // similar again for beamsplitter. Though I think this needs thinking about
    // more perhaps, i.e. what happens with the coord system when tilted at 45 degrees?
    // Though I believe with 1/cos(incident_angle) factor below it should be the same...
    if (knmcmp == BEAMSPLITTER_CMP &&((knum == BS12) || (knum == BS21) || (knum == BS31) || (knum == BS42)))
        xx *= -1;
    
    // TODO the direction of the rotation needs testing more rigorously 
    // not sure where the -1 should be + or - depends on the angle direction
    // convention used
    if (angle != 0.0)
        rotate_vector(&xx, &yy, -angle);

    // apply x direction scaling factor for incident angle
    //if(incident_angle != 0)
    //    xx = xx / cos(incident_angle*RAD);
   
    // using definition of round(x) = floor(x+0.5)
    int u = floor(xx / dx + x0 + 0.5)-1;
    int v = floor(yy / dy + y0 + 0.5)-1;

    if (u < 0 || u > N - 1 || v < 0 || v > M - 1) {
        //if we are out of the bounds of the map then we need to return either
        //an unaffected beam or an apertured one. Assume for now unaffected beam. 
        *A = 0;
        *phi = 0;
    } else {

        if (u + INTERP_LLIM < 0) u = -INTERP_LLIM;
        if (u + INTERP_ULIM >= N) u = N - 1 - INTERP_ULIM;
        if (v + INTERP_LLIM < 0) v = -INTERP_LLIM;
        if (v + INTERP_ULIM >= M) v = M - 1 - INTERP_ULIM;
    
        if (INTERP_METHOD == NEAREST) {
            // if we are doing nearest neighbour interpolation then we just
            // use the u,v values above
            // update: we should be using the matlab convention of z[rows][cols]
            // which means z[y][x]
            *A = z_abs[v][u];
            *phi = z_phs[v][u];

        } else {

            double interpA[INTERP_SIZE];
            double interpPhi[INTERP_SIZE];
            int n, __u, __v, a;

            for (a = INTERP_LLIM, n = 0; a <= INTERP_ULIM; a++, n++) {
                //For some reason I cant see, the z variable indexes need to be swapped around
                // z[x][y] (which it should be) -> z[y][x]
                // Update: the reason for this is because of the matlab cconvetion is used
                // to store the map data z[rows][cols]

                __v = v + a;
                __u = u + INTERP_LLIM;

                if ((xx < x[0]) || (xx > x[N - 1])) {
                    interpA[n] = 1;
                    interpPhi[n] = 0;
                } else {
                    gsl_spline_init(splinex, &x[__u], &z_abs[__v][__u], INTERP_SIZE);
                    interpA[n] = gsl_spline_eval(splinex, xx, NULL);

                    gsl_spline_init(splinex, &x[__u], &z_phs[__v][__u], INTERP_SIZE);
                    interpPhi[n] = gsl_spline_eval(splinex, xx, NULL);
                }
            }

            if ((yy < y[0]) || (yy > y[M - 1])) {
                *A = 1;
                *phi = 0;
            } else {
                gsl_spline_init(spliney, &y[v + INTERP_LLIM], &interpA[0], INTERP_SIZE);
                *A = gsl_spline_eval(spliney, yy, NULL);
                
                gsl_spline_init(spliney, &y[v + INTERP_LLIM], &interpPhi[0], INTERP_SIZE);
                *phi = gsl_spline_eval(spliney, yy, NULL);
            }
        }
    }

    // as the refractive index  of the medium either side of a mirror could
    // change in the simulation we must apply the refractive index terms of the
    // phase here    
    switch(knmcmp){
        case MIRROR_CMP:
            switch (knum) {
                case MR11:
                    *phi *= nr1;
                    break;
                case MR12:
                case MR21:
                    // if the phase map is transmission only we consider the surface profile
                    // to be flat thus no difference because of nr1 and nr2 should be seen
                    if (!map->phaseIsOnlyTransmission)
                        *phi *= (nr2 - nr1);
                    break;
                case MR22:
                    *phi *= nr2;
                    break;
            }
            break;
            
        case BEAMSPLITTER_CMP:
            switch (knum) {
                case BS12:
                case BS21:
                    *phi *= nr1;
                    break;
                case BS13:
                case BS31:
                case BS24:
                case BS42:
                    // if the phase map is transmission only we consider the surface profile
                    // to be flat thus no difference because of nr1 and nr2 should be seen
                    if (!map->phaseIsOnlyTransmission)
                        *phi *= (nr2 - nr1);
                    break;
                case BS34:
                case BS43:
                    *phi *= nr2;
                    break;
            }
            break;
    }

    //if we are only applying a transmission phase then the phase should be same
    //whether we go from n1->n2 or n2->n1 as the T phase represents a "thickness"
    //of the component not a surface displacement so phase sign flip not needed.
    if (!map->phaseIsOnlyTransmission) {
        // From the diagram above we see that z' needs to be inverted
        // or that we just invert the sign of the phase, same thing
        if ( knmcmp == MIRROR_CMP && ((knum == MR12) || (knum == MR22)))
            (*phi) *= -1;
        
        // same with beamsplitter, anything travelling in the 3/4 direction
        if ( knmcmp == BEAMSPLITTER_CMP &&((knum == BS13) || (knum == BS34) || (knum == BS43) || (knum == BS24)))
            (*phi) *= -1;
    }
    
    // When at a non-normal incidence we also need to increase the depth of a 
    // phase map. As the larger angle of incidence causes a longer path length
    // difference
    //if (knmcmp == BEAMSPLITTER_CMP && incident_angle != 0) {
    //    (*phi) *= cos(incident_angle*RAD);
    //}
     
    if (INTERP_METHOD != NEAREST) {
        gsl_spline_free(splinex);
        gsl_spline_free(spliney);
    }

    // if a interpolation output data file is open then we should output some 
    // data to it.
    if (ipfile != NULL) {
        //mutex needed here to stop threads fighting over writing to the file
        pthread_mutex_lock(&mxinterpfile);

        switch(knmcmp){
            case MIRROR_CMP:
                switch (knum) {
                    case MR11: fprintf(ipfile, "11 ");
                        break;
                    case MR12: fprintf(ipfile, "12 ");
                        break;
                    case MR21: fprintf(ipfile, "21 ");
                        break;
                    case MR22: fprintf(ipfile, "22 ");
                        break;
                }
                break;
            
            case BEAMSPLITTER_CMP:
                switch (knum) {
                    case BS12: fprintf(ipfile, "12 ");break;
                    case BS21: fprintf(ipfile, "21 ");break;
                    case BS34: fprintf(ipfile, "34 ");break;
                    case BS43: fprintf(ipfile, "43 ");break;
                    case BS13: fprintf(ipfile, "13 ");break;
                    case BS31: fprintf(ipfile, "31 ");break;
                    case BS24: fprintf(ipfile, "24 ");break;
                    case BS42: fprintf(ipfile, "42 ");break;
                }
                break;
        }
        
        fprintf(ipfile, "%.15e %.15e %.15e %.15e\n", xx, yy, *A, *phi);
        fflush(ipfile); // need this flush as large files seem to get corrupted otherwise
        pthread_mutex_unlock(&mxinterpfile);
    }
}

void integrand_get_variables(void *p, KNM_COMPONENT_t knmcmp,
        mr_knm_map_int_params_t **pmr,
        bs_knm_map_int_params_t **pbs,
        surface_merged_map_t **map,
        double **pmax, double **pmin, double *nr1, double *nr2,
        int *min_k, int *max_k,
        unsigned int *knm_calc_flags,
        bool *polar_limits_used, bool *using_map,
        int *n1, int *m1, int *n2, int *m2, double *ap_xoff, double *ap_yoff) {

    assert(p != NULL);
    
    if (knmcmp == MIRROR_CMP) {
        *pbs = NULL;
        *pmr = ((mr_knm_map_int_params_t*) p);
        *map = (*pmr)->merged_map;
        *pmax = (*pmr)->xmax;
        *pmin = (*pmr)->xmin;
        *min_k = MR11;
        *max_k = MR22;
        *knm_calc_flags = (*pmr)->knm_calc_flags;
        *polar_limits_used = (*pmr)->polar_limits_used;
        *using_map = (*pmr)->usingMap;
        *n1 = (*pmr)->n1;
        *n2 = (*pmr)->n2;
        *m1 = (*pmr)->m1;
        *m2 = (*pmr)->m2;
        *nr1 = (*pmr)->nr1;
        *nr2 = (*pmr)->nr2;
        *ap_xoff = (*pmr)->mirror->x_off;
        *ap_yoff = (*pmr)->mirror->y_off;
    } else if (knmcmp == BEAMSPLITTER_CMP) {
        *pmr = NULL;
        *pbs = ((bs_knm_map_int_params_t*) p);
        *map = (*pbs)->merged_map;
        *pmax = (*pbs)->xmax;
        *pmin = (*pbs)->xmin;
        *min_k = BS12;
        *max_k = BS42;
        *knm_calc_flags = (*pbs)->knm_calc_flags;
        *polar_limits_used = (*pbs)->polar_limits_used;
        *using_map = (*pbs)->usingMap;
        *n1 = (*pbs)->n1;
        *n2 = (*pbs)->n2;
        *m1 = (*pbs)->m1;
        *m2 = (*pbs)->m2;
        *nr1 = (*pbs)->nr1;
        *nr2 = (*pbs)->nr2;
        *ap_xoff = (*pbs)->bs->x_off;
        *ap_yoff = (*pbs)->bs->y_off;
    } else
        bug_error("Could not handle component in integrand_get_variables");
    
    assert(pbs != NULL || pmr != NULL);
    
    /*
    message("pmr: %p\n",*pmr);
    message("map: %p\n",*map);
    message("pmin: %g - %g\n", (*pmin)[0],(*pmin)[1]);
    message("pmax: %g - %g\n", (*pmax)[0],(*pmax)[1]);
    message("knm_flag: %i\n",*knm_calc_flags);
    message("polar: %i\n",*polar_limits_used);
    message("using_map: %i\n",*using_map);
    message("n1: %i\n",*n1);
    message("n2: %i\n",*n2);
    message("m1: %i\n",*m1);
    message("m2: %i\n",*m2);
    message("nr1: %g\n",*nr1);
    message("nr2: %g\n",*nr2);
    */
}


void calc_bs_AOI(bs_knm_map_int_params_t *pbs, KNM_BS_NODE_DIRECTION_t k, double *cos_alpha){
    
    if(pbs->bs->alpha_1 != 0.0) {
        
        // if we are computing a reflection from the n1/n2 side we are in nr1
        // and the angle of incidence is the same as defined. For the side n3/n4
        // though the angle of incidence is defined by Snells law and the refractive
        // index difference
        if(eq(pbs->nr1,pbs->nr2)){
            *cos_alpha = cos(pbs->bs->alpha_1*RAD);
            
        } else {
            switch ((KNM_BS_NODE_DIRECTION_t) k) {
                // these are the directions that end up on the n1/n2 side
                case BS12:
                case BS21:
                case BS31:
                case BS42:
                    *cos_alpha = cos(pbs->bs->alpha_1*RAD);
                    break;
                // these are the directions that end up on the n3/n4 side
                case BS13:
                case BS24:
                case BS34:
                case BS43:
                    *cos_alpha = cos(asin(sin(pbs->bs->alpha_1*RAD) * pbs->nr1 / pbs->nr2));
                    break;
            }
        }
    } 
}

#if INCLUDE_CUBA == 1
/**
 * calcates ingtegrand for the cuba serial routine, the knm calculated is given
 * in params.p->knm_num
 * 
 * @param ndim
 * @param xx
 * @param ncomp
 * @param ff
 * @param userdata
 * @return 
 */
static int integrand_cuba_serial(const int *ndim, const double xx[], const int *ncomp, double ff[], void *userdata) {
    assert(*ndim == 2);
    assert(*ncomp == 2);
    
    // get rid of compiler warnings
    (void) ndim;
    (void) ncomp;

    cuba_int_params_t params = *((cuba_int_params_t*) userdata);

    mr_knm_map_int_params_t *pmr=NULL;
    bs_knm_map_int_params_t *pbs=NULL;

    surface_merged_map_t *map=NULL;
    double *pmax=NULL, *pmin=NULL, nr1=0, nr2=0;
    int min_k=0, max_k=0;
    unsigned int knm_calc_flags=0;
    bool polar_limits_used=0, using_map=0;
    int n1=0, m1=0, n2=0, m2=0;
    double ap_xoff=0, ap_yoff=0;

    integrand_get_variables(params.p, params.knmcmp, &pmr, &pbs, &map,
            &pmax, &pmin, &nr1, &nr2, &min_k, &max_k,
            &knm_calc_flags, &polar_limits_used, &using_map,
            &n1, &m1, &n2, &m2, &ap_xoff, &ap_yoff);

#if DDEBUG
    if (params.knmcmp == MIRROR_CMP) {
        if (p.knum == MR11 && !CALC_MR_KNM(pmr, 11)) bug_error("Should not be computing K11\n");
        if (p.knum == MR12 && !CALC_MR_KNM(pmr, 12)) bug_error("Should not be computing K12\n");
        if (p.knum == MR21 && !CALC_MR_KNM(pmr, 21)) bug_error("Should not be computing K21\n");
        if (p.knum == MR22 && !CALC_MR_KNM(pmr, 22)) bug_error("Should not be computing K22\n");
    } else if (params.knmcmp == BEAMSPLITTER_CMP) {
        if (p.knum == BS12 && !CALC_BS_KNM(pbs, 12)) bug_error("Should not be computing K12\n");
        if (p.knum == BS21 && !CALC_BS_KNM(pbs, 21)) bug_error("Should not be computing K21\n");
        if (p.knum == BS34 && !CALC_BS_KNM(pbs, 34)) bug_error("Should not be computing K34\n");
        if (p.knum == BS43 && !CALC_BS_KNM(pbs, 43)) bug_error("Should not be computing K43\n");
        if (p.knum == BS13 && !CALC_BS_KNM(pbs, 13)) bug_error("Should not be computing K13\n");
        if (p.knum == BS31 && !CALC_BS_KNM(pbs, 31)) bug_error("Should not be computing K31\n");
        if (p.knum == BS24 && !CALC_BS_KNM(pbs, 24)) bug_error("Should not be computing K24\n");
        if (p.knum == BS42 && !CALC_BS_KNM(pbs, 42)) bug_error("Should not be computing K42\n");
    }
#endif

    double x, y;
    double r = (pmin[0] + xx[0] * (pmax[0] - pmin[0]));
    double t = (pmin[1] + xx[1] * (pmax[1] - pmin[1]));
    double cos_alpha = 1.0;
    
    if(params.knmcmp == BEAMSPLITTER_CMP) calc_bs_AOI(pbs, pbs->knum, &cos_alpha);
    
    // if polar coordinates have been used then we need to convert
    // r and theta - which are x and y respectively from above
    // - into Cartesian coordinates
    if (polar_limits_used) {
        x = r * cos(t);
        y = r * sin(t);
    } else {
        x = r;
        y = t;
    }

    complex_t u1 = complex_0, u2=complex_0, f=complex_0;
    double A = 1.0;
    double phi = 0.0;
    
    if (using_map){
        
        if (params.knmcmp == MIRROR_CMP)
            interpolate_merged_map(pmr->merged_map, params.knmcmp, pmr->knum, x, y, &A, &phi, nr1, nr2, pmr->mirror->angle);
        else if (params.knmcmp == BEAMSPLITTER_CMP) {
            interpolate_merged_map(pbs->merged_map, params.knmcmp, pbs->knum, x, y, &A, &phi, nr1, nr2, pbs->bs->map_rotation);
                   
            // When at a non-normal incidence we also need to increase the depth of a 
            // phase map. As the larger angle of incidence causes a longer path length
            // difference
            if (pbs->bs->alpha_1 != 0) {
                phi *= cos_alpha;
            }
        } else
            gerror("knmcmp parameter not recognised");
    
    } 

    u_nm_accel_t *a1=NULL, *a2=NULL;
    complex_t *zc=NULL;

    // retrieve the correct accelerators and constant
    if (params.knmcmp == MIRROR_CMP) {
        get_mr_acc_zc_values(pmr->knum, pmr, &a1, &a2, &zc);
        
        if(a1 == NULL || a2 == NULL || zc == NULL) return 1;
        
        u1 = u_nm_fast_x(x-ap_xoff, y-ap_yoff, n1, m1, a1);
        u2 = u_nm_fast_x(x-ap_xoff, y-ap_yoff, n2, m2, a2);
        
    } else if (params.knmcmp == BEAMSPLITTER_CMP){
        get_bs_acc_zc_values(pbs->knum, pbs, &a1, &a2, &zc);
        
        if(a1 == NULL || a2 == NULL || zc == NULL) return 1;
        
        if(cos_alpha != 1.0){
            // if we have a tilted beamsplitter the beam will be elliptical in the x direction.
            //    x' = x / cos(alpha)
            // But here x_i which is where we evaluate the integrand at is actually x' for the beam.
            // So we must compute  the beam shape at the point x =  x' * cos(alpha)
            u1 = u_nm_fast_x(x*cos_alpha - ap_xoff, y - ap_yoff, n1, m1, a1);
            u2 = u_nm_fast_x(x*cos_alpha - ap_xoff, y - ap_yoff, n2, m2, a2);
        } else {
            u1 = u_nm_fast_x(x-ap_xoff, y-ap_yoff, n1, m1, a1);
            u2 = u_nm_fast_x(x-ap_xoff, y-ap_yoff, n2, m2, a2);
        }
    }
    
    f = z_by_z(z_by_zc(z_by_xphr(u1, A, phi), u2), *zc);

    ff[0] = f.re* cos_alpha;
    ff[1] = f.im* cos_alpha;

    // need to apply extra r term if integrating in polar coordinates
    if (polar_limits_used) {
        ff[0] *= r;
        ff[1] *= r;
    }

    return 0;
}

static int integrand_cuba_para(const int *ndim, const double xx[], const int *ncomp, double ff[], void *userdata) {
    assert(*ndim == 2);
    assert(*ncomp > 0);

		(void)ndim; //suppress compiler warning
		(void)ncomp; //suppress compiler warning
    cuba_int_params_t params = *((cuba_int_params_t*) userdata);
    
    mr_knm_map_int_params_t *pmr=NULL;
    bs_knm_map_int_params_t *pbs=NULL;

    surface_merged_map_t *map=NULL;
    double *pmax=NULL, *pmin=NULL, nr1=0, nr2=0;
    unsigned int knm_calc_flags=0;
    double ap_xoff=0, ap_yoff=0;
    bool polar_limits_used=0, using_map=0;
    int n1=0, m1=0, n2=0, m2=0, max_k=0, min_k=0;

    // depending on what type of component we are integrating fill the various 
    // variables
    integrand_get_variables(params.p, params.knmcmp, &pmr, &pbs, &map,
            &pmax, &pmin, &nr1, &nr2, &min_k, &max_k,
            &knm_calc_flags, &polar_limits_used, &using_map,
            &n1, &m1, &n2, &m2, &ap_xoff, &ap_yoff);

    double x, y;
    double r = (pmin[0] + xx[0] * (pmax[0] - pmin[0]));
    double t = (pmin[1] + xx[1] * (pmax[1] - pmin[1]));

    // if polar coordinates have been used then we need to convert
    // r and theta - which are x and y respectively from above
    // - into Cartesian coordinates
    if (polar_limits_used) {
        x = r * cos(t);
        y = r * sin(t);
    } else {
        x = r;
        y = t;
    }
    
    double A, phi;
    complex_t u1, u2;
    int k=0, i = 0;
    double cos_alpha = 1.0;
    
    // now loop through each component, check whether we should calculate
    // the knm, then interpolate etc.
    for (k = min_k; k <= max_k; k++) {
        // the calc flags are powers of 2 and stored in a flag
        // use bit shift here to get the powers of 2 in the loop
        unsigned int kk = ((unsigned int) (1 << (k - 1)));
        // if we should be calculating this k continue
        if ((knm_calc_flags & kk) == kk) {
            if(params.knmcmp == BEAMSPLITTER_CMP) calc_bs_AOI(pbs, (KNM_BS_NODE_DIRECTION_t)k, &cos_alpha);
            
            if (using_map) {
                if (params.knmcmp == MIRROR_CMP){
                    interpolate_merged_map(map, params.knmcmp, (KNM_MIRROR_NODE_DIRECTION_t) k, x, y, &A, &phi, nr1, nr2, pmr->mirror->angle);
                } else if(params.knmcmp == BEAMSPLITTER_CMP) {
                    
                    interpolate_merged_map(map, params.knmcmp, (KNM_BS_NODE_DIRECTION_t) k, x, y, &A, &phi, nr1, nr2, pbs->bs->map_rotation);
                    
                    // When at a non-normal incidence we also need to increase the depth of a 
                    // phase map. As the larger angle of incidence causes a longer path length
                    // difference
                    if (pbs->bs->alpha_1 != 0) {
                        phi *= cos_alpha;
                    }
                } else 
                    gerror("knmcmp parameter not recognised");
                
            } else {
                A = 1;
                phi = 0;
            }

            u_nm_accel_t *a1 = NULL, *a2 = NULL;
            complex_t *zc = NULL;

            // now we get the accelerators and constant for the integral
            // computations
            if (params.knmcmp == MIRROR_CMP) {
                pmr->knum = k; // this needs setting as it is read by get_mr_acc_zc_values to know which accelerator to get
                get_mr_acc_zc_values(k, pmr, &a1, &a2, &zc);
                
                if(a1 == NULL || a2 == NULL || zc == NULL) return 1;
                
                u1 = u_nm_fast_x(x-ap_xoff, y-ap_yoff, n1, m1, a1);
                u2 = u_nm_fast_x(x-ap_xoff, y-ap_yoff, n2, m2, a2);
            
            } else if (params.knmcmp == BEAMSPLITTER_CMP) {
                pbs->knum = k;
                get_bs_acc_zc_values(k, pbs, &a1, &a2, &zc);
                
                if(a1 == NULL || a2 == NULL || zc == NULL) return 1;
                
                if(cos_alpha != 1.0){
                    // if we have a tilted beamsplitter the beam will be elliptical in the x direction.
                    //    x' = x / cos(alpha)
                    // But here x_i which is where we evaluate the integrand at is actually x' for the beam.
                    // So we must compute  the beam shape at the point x =  x' * cos(alpha)
                    u1 = u_nm_fast_x(x*cos_alpha - ap_xoff, y - ap_yoff, n1, m1, a1);
                    u2 = u_nm_fast_x(x*cos_alpha - ap_xoff, y - ap_yoff, n2, m2, a2);
                } else {
                    u1 = u_nm_fast_x(x-ap_xoff, y-ap_yoff, n1, m1, a1);
                    u2 = u_nm_fast_x(x-ap_xoff, y-ap_yoff, n2, m2, a2);
                }
            }
            
            complex_t result = z_by_z(z_by_zc(z_by_xphr(u1, A, phi), u2), *zc);

            ff[i++] = result.re * cos_alpha;
            ff[i++] = result.im * cos_alpha;
            
            if (polar_limits_used) {
                ff[i - 2] *= r;
                ff[i - 1] *= r;
            }
        }
    }
    
    return 0;
}

#endif

/**
 * Allocates memory for workspace when using ROMHOM
 * @param ws
 */
void allocate_romhom_workspace(rom_map_t *rom_map, int component_type) {
    int i;
    roq_weights_t* w[4] = {&rom_map->roq11, &rom_map->roq22, &rom_map->roq12, &rom_map->roq21};
    roq_weights_t *rom;
    knm_workspace_t *ws;
    
    switch(component_type){
        case MIRROR:
            
            for(i=0; i<4; i++){
                rom = w[i];
                
                if (!rom->enabled) continue;
                ws = &rom->knm_ws;
                
                assert(rom->num_xnodes == rom->num_ynodes);
            
                allocate_unn_cache(&ws->ux_cache_11, rom->num_xnodes);
                allocate_unn_cache(&ws->ux_cache_12, rom->num_xnodes);
                allocate_unn_cache(&ws->ux_cache_21, rom->num_xnodes);
                allocate_unn_cache(&ws->ux_cache_22, rom->num_xnodes);

                allocate_unn_cache(&ws->uy_cache_11, rom->num_ynodes);
                allocate_unn_cache(&ws->uy_cache_12, rom->num_ynodes);
                allocate_unn_cache(&ws->uy_cache_21, rom->num_ynodes);
                allocate_unn_cache(&ws->uy_cache_22, rom->num_ynodes);

                allocate_dmatrix(&(ws->d_u_xy), rom->num_xnodes, NULL);

                ws->ux_cache_11.nodes = rom->x_nodes;
                ws->ux_cache_11.num_nodes = rom->num_xnodes;
                ws->ux_cache_12.nodes = rom->x_nodes;
                ws->ux_cache_12.num_nodes = rom->num_xnodes;
                ws->ux_cache_21.nodes = rom->x_nodes;
                ws->ux_cache_21.num_nodes = rom->num_xnodes;
                ws->ux_cache_22.nodes = rom->x_nodes;
                ws->ux_cache_22.num_nodes = rom->num_xnodes;

                ws->uy_cache_11.nodes = rom->y_nodes;
                ws->uy_cache_11.num_nodes = rom->num_ynodes;
                ws->uy_cache_12.nodes = rom->y_nodes;
                ws->uy_cache_12.num_nodes = rom->num_ynodes;
                ws->uy_cache_21.nodes = rom->y_nodes;
                ws->uy_cache_21.num_nodes = rom->num_ynodes;
                ws->uy_cache_22.nodes = rom->y_nodes;
                ws->uy_cache_22.num_nodes = rom->num_ynodes;
            }
            
            
            break;
        default:
            bug_error("Couldn't handle component type");
    }
}

void allocate_newton_cotes_workspace(knm_workspace_t *ws, surface_merged_map_t *map, int component_type) {
    switch(component_type){
        case MIRROR:
            
            allocate_unn_cache(&ws->ux_cache_11, map->cols);
            allocate_unn_cache(&ws->ux_cache_12, map->cols);
            allocate_unn_cache(&ws->ux_cache_21, map->cols);
            allocate_unn_cache(&ws->ux_cache_22, map->cols);
            
            allocate_unn_cache(&ws->uy_cache_11, map->rows);
            allocate_unn_cache(&ws->uy_cache_12, map->rows);
            allocate_unn_cache(&ws->uy_cache_21, map->rows);
            allocate_unn_cache(&ws->uy_cache_22, map->rows);
            
            allocate_dmatrix2(&(ws->d_u_xy), map->cols, map->rows, NULL);
            allocate_dmatrix2(&(ws->W_xy), map->cols, map->rows, NULL);
            allocate_zmatrix2(&(ws->z_u_xy), map->cols, map->rows, NULL);
            
            ws->ux_cache_11.nodes = map->x;
            ws->ux_cache_11.num_nodes = map->cols;
            ws->ux_cache_12.nodes = map->x;
            ws->ux_cache_12.num_nodes = map->cols;
            ws->ux_cache_21.nodes = map->x;
            ws->ux_cache_21.num_nodes = map->cols;
            ws->ux_cache_22.nodes = map->x;
            ws->ux_cache_22.num_nodes = map->cols;
            
            ws->uy_cache_11.nodes = map->y;
            ws->uy_cache_11.num_nodes = map->rows;
            ws->uy_cache_12.nodes = map->y;
            ws->uy_cache_12.num_nodes = map->rows;
            ws->uy_cache_21.nodes = map->y;
            ws->uy_cache_21.num_nodes = map->rows;
            ws->uy_cache_22.nodes = map->y;
            ws->uy_cache_22.num_nodes = map->rows;
            
            break;
        default:
            bug_error("Couldn't handle component type");
    }
}

void allocate_unn_cache(unn_cache_t *c, size_t num_nodes) {
    assert(c != NULL);
    assert(num_nodes > 0);
    assert(inter.num_fields > 0);
    
    int i;
    
    c->values = (double**) calloc((inter.tem*inter.tem + 3*inter.tem)/2 + 1, sizeof(double*));
    
    for(i=0; i<inter.num_fields; i++){
        c->values[i] = (double*) calloc(num_nodes, sizeof(double));
    }
    
    c->zvalues = (complex_t**) calloc((inter.tem*inter.tem + 3*inter.tem)/2 + 1, sizeof(complex_t*));
    
    for(i=0; i<inter.num_fields; i++){
        c->zvalues[i] = (complex_t*) calloc(num_nodes, sizeof(complex_t));
    }
}

void free_unn_cache(unn_cache_t *c) {
    assert(c != NULL);
    assert(c->values != NULL);
    
    int i;
    
    for(i=0; i<inter.num_fields; i++){
        free(c->values[i]);
    }
    
    free(c->values);
}

size_t unn_cache_index(int n1, int n2, bool *conjugate){
    if(n1==n2){
        *conjugate = false;
        return (n1*n1 + 3*n1)/2;
    } else if(n1 > n2) {
        *conjugate = false;
        n1 -= 1;
        return (n1*n1 + 3*n1)/2 + 1 + n2;
    }  else if(n1 < n2) {
        *conjugate = true;
        n2 -= 1;
        return (n2*n2 + 3*n2)/2 + 1 + n1;
    } else 
        bug_error("unhandled");
    
    return 0;
}

/**
 * Fills cache for ROM and Newton-Cotes. Currently assumes 
 * mode matched
 * 
 * @param p
 * @param knmcmp
 * @param results
 */
void fill_unn_cache(unn_cache_t *c, u_n_accel_t *acc1, u_n_accel_t *acc2, bool mode_matched){
    
    assert(c != NULL);
    assert(c->nodes != NULL);
    assert(c->num_nodes > 0);
    assert(c->values != NULL);
    
    assert(acc1 != NULL);
    assert(acc2 != NULL);
    
    c->acc1 = acc1;
    c->acc2 = acc2;
    
    int j;
    int n, _n;    
    
    for (n=0; n<=inter.tem; n++){
        for (_n=0; _n<=inter.tem; _n++) {
            bool conjugate = false;
            size_t idx = unn_cache_index(n, _n, &conjugate);
            
            if(!conjugate) {
							for(j=0; j<(int)c->num_nodes; j++) {
                    complex_t a = z_by_zc(u_fast_x(n, c->nodes[j], acc1), u_fast_x(_n, c->nodes[j], acc2));
                    
                    if(mode_matched) {
                        c->values[idx][j] = a.re;
                    } else
                        c->zvalues[idx][j] = a;
                }
            }
        }
    }
}

complex_t einsum_d(size_t nx, size_t ny, dmatrix u, zmatrix w, bool conjugate_w) {
    int i, j;
    
    complex_t z = complex_0;
    
    if(!conjugate_w){
			for(i=0; i<(int)nx; i++){
            for(j=0; j<(int)ny; j++){
                z_inc_zc(&z, z_by_x(w[i][j], u[i][j]));
            }
        }
    } else {
        for(i=0; i<(int)nx; i++){
            for(j=0; j<(int)ny; j++){
                z_inc_z(&z,  z_by_x(w[i][j], u[i][j]));
            }
        }
    }
    
    return z;
}

complex_t einsum_z(size_t nx, size_t ny, zmatrix u, zmatrix w, bool conjugate_w) {
    int i, j;
    
    complex_t z = complex_0;
    
    if(conjugate_w){
        for(i=0; i<(int)nx; i++){
            for(j=0; j<(int)ny; j++){
                z_inc_zc(&z, z_by_z(u[i][j], w[i][j]));
            }
        }
    } else {
        for(i=0; i<(int)nx; i++){
            for(j=0; j<(int)ny; j++){
                z_inc_z(&z, z_by_z(u[i][j], w[i][j]));
            }
        }
    }
    
    return z;
}

complex_t do_romhom_real_int(roq_weights_t *rom, double** u_xy, unn_cache_t *cx, unn_cache_t *cy, int n1, int m1, int n2, int m2) {
    int n1_mod_2 = n1 % 2;
    int n2_mod_2 = n2 % 2;
    int m1_mod_2 = m1 % 2;
    int m2_mod_2 = m2 % 2;
    
    bool conjugatex = false;
    bool conjugatey = false;
    
    int idx = unn_cache_index(n1, n2, &conjugatex);
    int idy = unn_cache_index(m1, m2, &conjugatey);
    
    int p,q;
    
    complex_t k = complex_0;
    bool conjugate_weights = false;
    
    // first need to do outer product between x and y U vectors
    // As this is computing the mode matched case, e.g. a real
    // integrand, the conjugates do not matter
    for(p=0; p<(int)cx->num_nodes; p++) {
        for(q=0; q<(int)cy->num_nodes; q++) {
            u_xy[p][q] = cx->values[idx][p] * cy->values[idy][q];
        }    
    }
    
    size_t nx = rom->num_xnodes;
    size_t ny = rom->num_ynodes;
    
    if (n1_mod_2 == n2_mod_2 && m1_mod_2 == m2_mod_2){
       k  = einsum_d(nx, ny, u_xy, rom->w_Q1Q2Q3Q4, conjugate_weights);
    } else if (n1_mod_2 != n2_mod_2) {
        if (m1_mod_2 == m2_mod_2){
            k  = z_m_z(einsum_d(nx, ny, u_xy, rom->w_Q1Q4, conjugate_weights), einsum_d(nx, ny, u_xy, rom->w_Q2Q3, conjugate_weights));
        } else {
            k  = z_m_z(einsum_d(nx, ny, u_xy, rom->w_Q2Q4, conjugate_weights), einsum_d(nx, ny, u_xy, rom->w_Q1Q3, conjugate_weights));
        }
    } else if(m1_mod_2 != m2_mod_2) {
        if (n1_mod_2 == n2_mod_2){
            k  = z_m_z(einsum_d(nx, ny, u_xy, rom->w_Q3Q4, conjugate_weights), einsum_d(nx, ny, u_xy, rom->w_Q1Q2, conjugate_weights));
        } else {
            k  = z_m_z(einsum_d(nx, ny, u_xy, rom->w_Q2Q4, conjugate_weights), einsum_d(nx, ny, u_xy, rom->w_Q1Q3, conjugate_weights));
        }
    } else
        bug_error("not handled");
    
    return k;
}

#if INCLUDE_CUBA == 1
/**
 * IMPORTANT: results should be a predefined array to store the knms in.
 * 4 complex_t when using a mirror and 8 when using a beamsplitter.
 * 
 * @param p pointer to component specific integration parameters
 * @param knmcmp Component identifier for the component who is having its knm calculated
 * @param results array of complex_t
 */
void do_para_cuhre_map_int(void *p, KNM_COMPONENT_t knmcmp, complex_t *results, double abs_err, double rel_err) {

    int NCOMP = 0;
    int nregions=0, neval=0, fail=0;
    double integral[16]={0}, error[16]={0}, prob[16]={0};
    
    mr_knm_map_int_params_t *pmr = NULL;
    bs_knm_map_int_params_t *pbs = NULL;

    surface_merged_map_t *map=NULL;
    double *pmax=NULL, *pmin=NULL, nr1=0, nr2=0;
    int min_k=0, max_k=0;
    unsigned int knm_calc_flags=0;
    double ap_xoff, ap_yoff;
    bool polar_limits_used=0, using_map=0;
    int n1=0, m1=0, n2=0, m2=0;

    integrand_get_variables(p, knmcmp, &pmr, &pbs, &map,
            &pmax, &pmin, &nr1, &nr2, &min_k, &max_k,
            &knm_calc_flags, &polar_limits_used, &using_map,
            &n1, &m1, &n2, &m2, &ap_xoff, &ap_yoff);
        
    // determine how many integrals we need to calculate
    if (knmcmp == MIRROR_CMP) {
        if (CALC_MR_KNM(pmr, 11)) NCOMP += 2;
        if (CALC_MR_KNM(pmr, 12)) NCOMP += 2;
        if (CALC_MR_KNM(pmr, 21)) NCOMP += 2;
        if (CALC_MR_KNM(pmr, 22)) NCOMP += 2;
        
        get_mirror_int_limit(pmr, 0); //calculate the map integration limits
    } else if (knmcmp == BEAMSPLITTER_CMP) {
        if (CALC_BS_KNM(pbs, 12)) NCOMP += 2;
        if (CALC_BS_KNM(pbs, 21)) NCOMP += 2;
        if (CALC_BS_KNM(pbs, 34)) NCOMP += 2;
        if (CALC_BS_KNM(pbs, 43)) NCOMP += 2;
        if (CALC_BS_KNM(pbs, 13)) NCOMP += 2;
        if (CALC_BS_KNM(pbs, 31)) NCOMP += 2;
        if (CALC_BS_KNM(pbs, 24)) NCOMP += 2;
        if (CALC_BS_KNM(pbs, 42)) NCOMP += 2;
        
        get_bs_int_limit(pbs, 0); //calculate the map integration limits
    } else
        bug_error("Could not resolve knm component argument");

    if (NCOMP != 0) {
        
        // this tells the integrating routine what component it is working with
        // so it knows how to correctly cast p to the right param variable
        cuba_int_params_t ps;
        ps.p = p;
        ps.knmcmp = knmcmp;

        Cuhre(2, NCOMP, integrand_cuba_para, (void*) &ps,
                rel_err, abs_err, 0,
                0, init.maxintcuba, 13, NULL,
                &nregions, &neval, &fail, integral, error, prob);
        
        if (inter.debug && neval >= init.maxintcuba) {
            warn("maxintcuba reached for %d %d->%d %d (evals: %d) Errors: \n", n1, m1, n2,m2,neval);

            int i = 0;

            if (knmcmp == MIRROR_CMP) {
                if (CALC_MR_KNM(pmr, 11)) {
                    debug_msg("K11: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_MR_KNM(pmr, 12)) {
                    debug_msg("K12: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_MR_KNM(pmr, 21)) {
                    debug_msg("K21: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_MR_KNM(pmr, 22)) {
                    debug_msg("K22: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
            } else if (knmcmp == BEAMSPLITTER_CMP) {
                if (CALC_BS_KNM(pbs, 12)) {
                    debug_msg("K12: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_BS_KNM(pbs, 21)) {
                    debug_msg("K21: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_BS_KNM(pbs, 34)) {
                    debug_msg("K34: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_BS_KNM(pbs, 43)) {
                    debug_msg("K43: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_BS_KNM(pbs, 13)) {
                    debug_msg("K13: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_BS_KNM(pbs, 31)) {
                    debug_msg("K31: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_BS_KNM(pbs, 24)) {
                    debug_msg("K24: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
                if (CALC_BS_KNM(pbs, 42)) {
                    debug_msg("K42: %.3g+-i%.3g ", error[i], error[i + 1]);
                    i += 2;
                }
            }

            debug_msg("\n");
        }

        if (inter.debug && map != NULL && map->show_knm_neval && !options.quiet)
            debug_msg("   - %i%i->%i%i calculated with %i evaluations\n", n1, m1, n2, m2, neval);
    }

    //This J variable is needed for the INT_VAL macro
    double J = (pmax[0] - pmin[0]) * (pmax[1] - pmin[1]);
    int j = 0, i=0;
    
    for (j = min_k; j <= max_k; j++) {
        unsigned int k = (unsigned int) (1 << (j - 1));

        if ((knm_calc_flags & k) == k) {
            results[j - 1] = co(INT_VAL(i), INT_VAL(i+1));
            i += 2;
        }
    }
}

void do_serial_cuhre_map_int(void *p, KNM_COMPONENT_t knmcmp, complex_t *results, double abs_err, double rel_err) {
    const int NCOMP = 2;
    int nregions, neval, fail;
    double integral[NCOMP], error[NCOMP], prob[NCOMP];
    int tneval = 0;

    mr_knm_map_int_params_t *pmr=NULL;
    bs_knm_map_int_params_t *pbs=NULL;

    surface_merged_map_t *map=NULL;
    double *pmax=NULL, *pmin=NULL, nr1=0, nr2=0;
    int min_k=0, max_k=0;
    unsigned int knm_calc_flags=0;
    double ap_xoff, ap_yoff;
    bool polar_limits_used=0, using_map=0;
    int n1=0, m1=0, n2=0, m2=0;

    integrand_get_variables(p, knmcmp, &pmr, &pbs, &map,
            &pmax, &pmin, &nr1, &nr2, &min_k, &max_k,
            &knm_calc_flags, &polar_limits_used, &using_map,
            &n1, &m1, &n2, &m2, &ap_xoff, &ap_yoff);

    int j;

    for (j = min_k; j <= max_k; j++) {
        unsigned int k = (unsigned int) (1 << (j - 1));

        if ((knm_calc_flags & k) == k) {
            // We are meant to be calculating KNM
            cuba_int_params_t ps;
            ps.p = p;
            ps.knmcmp = knmcmp;

            //calculate the map integration limits
            if (knmcmp == MIRROR_CMP) {
                pmr->knum = j;
                get_mirror_int_limit(pmr, j);
            } else if (knmcmp == BEAMSPLITTER_CMP){
                pbs->knum = j;
                get_bs_int_limit(pbs, j);
            } else
                bug_error("Could not handle a knmcmp value of %i", knmcmp);
            
            Cuhre(2, NCOMP, integrand_cuba_serial, (void*) &ps,
                    rel_err, abs_err, 0,
                    0, init.maxintcuba, 13, NULL,
                    &nregions, &neval, &fail, integral, error, prob);

            tneval += neval;

            if (inter.debug && (neval > init.maxintcuba)) {
                char kstr[4];

                switch (j) {
                    case MR11:
                        strcpy(kstr, "k11");
                        break;
                    case MR12:
                        strcpy(kstr, "k12");
                        break;
                    case MR21:
                        strcpy(kstr, "k21");
                        break;
                    case MR22:
                        strcpy(kstr, "k22");
                        break;
                }

                warn("maxintcuba reached for %d %d->%d %d (evals: %d) Errors: %s: %.3g+-i%.3g\n", n1, m1
                        , n2, m2, neval
                        , kstr, error[0], error[1]);
            }

            double J = (pmax[0] - pmin[0]) * (pmax[1] - pmin[1]);
            results[j - 1] = co(integral[0] * J, integral[1] * J);
        }
    }

    if (inter.debug && map->show_knm_neval && !options.quiet)
        message("   - %i%i->%i%i calculating with %i evaluations\n", n1, m1, n2, m2, tneval);
}
#endif

void do_newton_cotes_int(void *userdata, KNM_COMPONENT_t knmcmp, complex_t *results, bitflag mismatching) {
    mr_knm_map_int_params_t *pmr=NULL;
    bs_knm_map_int_params_t *pbs=NULL;

    surface_merged_map_t *map=NULL;
    double *pmax=NULL, *pmin=NULL, nr1=0, nr2=0;
    int min_k=0, max_k=0;
    unsigned int knm_calc_flags=0;
    bool polar_limits_used=0, using_map=0;
    int n1=0, m1=0, n2=0, m2=0;
    double ap_xoff=0.0, ap_yoff=0.0;
    unn_cache_t *cx = NULL;
    unn_cache_t *cy = NULL;
    
    integrand_get_variables(userdata, knmcmp, &pmr, &pbs, &map,
                            &pmax, &pmin, &nr1, &nr2, &min_k, &max_k,
                            &knm_calc_flags, &polar_limits_used, &using_map,
                            &n1, &m1, &n2, &m2, &ap_xoff, &ap_yoff);
    
    assert(pbs != NULL || pmr != NULL);
    assert(polar_limits_used == false);
    
    int i=0, j=0, k=0;
    
    knm_workspace_t *ws = &map->knm_ws;
    dmatrix W_xy = ws->W_xy;
    
    size_t ndx = map->rows;
    size_t ndy = map->cols;

    double dx = map->xstep;
    double dy = map->ystep;
    double da = dx*dy;
    
    bool conjugatex = false; // true if we should use conjugate of cache
    bool conjugatey = false; // true if we should use conjugate of cache
    
    int idx = unn_cache_index(n1, n2, &conjugatex);
    int idy = unn_cache_index(m1, m2, &conjugatey);
    
    int p=0, q=0;
    zmatrix u_xy = NULL;
    
    int intops = 0;
    
    double **A = NULL, **phi = NULL;
    double map_rotation = 0;
	//double incident_angle = 0;
    //double cos_alpha = 1;

    if (knmcmp == MIRROR_CMP) {
		//map_rotation = pmr->mirror->angle;   
        u_xy = ws->z_u_xy;
    } else if (knmcmp == BEAMSPLITTER_CMP) {
        map_rotation = pbs->bs->map_rotation;
        
        // The scaling for beamsplitters went tilted has not been implemented yet
        // for the Newton-Cotes integration method. The beam or map will need
        // scaling as is done in the function do_riemann_sum_new_int, for example.
        if (pbs->bs->alpha_1 != 0) {
            warn("Newton-Cotes solver does not support non-normal beamsplitters with maps");
        }
        
        //incident_angle = pbs->bs->alpha;
        //cos_alpha = cos(incident_angle * RAD);
    } else 
        gerror("knmcmp parameter not recognised");
    
    if(map_rotation != 0.0){
        gerror("Newton-Cotes sum does not support map rotations \n");
    }
    
    // now loop through each knm direction, check whether we should calculate
    // the knm, then interpolate etc.
    for (k = min_k; k <= max_k; k++) {

        // the calc flags are powers of 2 and stored in a flag
        // use bit shift here to get the powers of 2 in the loop
        unsigned int kk = ((unsigned int) (1 << (k - 1)));

        // if we should be calculating this k continue
        if ((knm_calc_flags & kk) == kk) {
            if (knmcmp == MIRROR_CMP) {
                switch(kk) {
                    case MR11Calc:
                        cx = &ws->ux_cache_11;
                        cy = &ws->uy_cache_11;
                        break;
                    case MR12Calc:
                        cx = &ws->ux_cache_12;
                        cy = &ws->uy_cache_12;
                        break;
                    case MR21Calc:
                        cx = &ws->ux_cache_21;
                        cy = &ws->uy_cache_21;
                        break;
                    case MR22Calc:
                        cx = &ws->ux_cache_22;
                        cy = &ws->uy_cache_22;
                        break;
                }
            } else if (knmcmp == BEAMSPLITTER_CMP) {
                bug_error("not implemented yet");
            } else {
                gerror("knmcmp parameter not recognised");
            }
            
            // first need to do outer product between x and y U vectors
            // and apply the integration weights
            if((mismatching && kk) == kk) {
                // if there is a mode_mismatch here we have to use the complex values to store a result
                for(p=0; p<(int)ndx; p++) {
                    for(q=0; q<(int)ndy; q++) {
                        u_xy[q][p] = z_by_x(z_by_z(cx->zvalues[idx][p], cy->zvalues[idy][q]), W_xy[q][p]);
                    }    
                }
            } else {
                for(p=0; p < (int)ndx; p++) {
                    for(q=0; q < (int)ndy; q++) {
                        u_xy[q][p].re = cx->values[idx][p] * cy->values[idy][q] * W_xy[q][p];
                        u_xy[q][p].im = 0;
                    }    
                }
            }

            if(using_map) {
                if (knmcmp == MIRROR_CMP) {
                    if(kk == MR11Calc) {
                        A = map->r_abs;
                        phi = map->r_phs;
                    } else if(kk == MR12Calc || kk == MR21Calc) {
                        A = map->t_abs;
                        phi = map->t_phs;
                    } else if(kk == MR22Calc) {
                        A = map->r_abs;
                        phi = map->r_phs;
                    }
                } else if (knmcmp == BEAMSPLITTER_CMP) {
                    bug_error("not implemented yet");
                } else 
                    gerror("knmcmp parameter not recognised");
                
                if(kk == MR22Calc) {
                    // for reflection from side 2 we need a minus sign on the phase
                    // as the phase map is inverted in height. Also we see the
                    // map from the other side so need to reflect about y.
                    for(i=0; i<(int)ndx; i++) {
                        for(j=0; j<(int)ndy; j++) {
                            u_xy[j][i] =  z_by_xphr(u_xy[j][i], A[ndy-1-j][ndx-1-i], phi[ndy-1-j][ndx-1-i]);
                        }
                    }
                } else {
                    for(i=0; i<(int)ndx; i++) {
                        for(j=0; j<(int)ndy; j++) {
                            u_xy[j][i] =  z_by_xphr(u_xy[j][i], A[ndy-1-j][i], -phi[ndy-1-j][i]);
                        }
                    }
                }
            }
            
            // As the zmatrix is allocated in one large block when
            // using allocate_zmatrix(...) we can access it like a 1D array 
            complex_t *tmp = u_xy[0];
            results[k-1] = complex_0;
            
            for(i=0; i<(int)(ndx*ndy); i++) {
                z_inc_z(results + k - 1, tmp[i]);
            }
            
            intops++;
        }
    }
    
    if (inter.debug && map->show_knm_neval && !options.quiet)
        message("   - %i%i->%i%i calculating with %i evaluations\n", n1, m1, n2, m2, intops);

    // finally multiply by the area
    i = 0;
    
    for (k = min_k; k <= max_k; k++) {
        unsigned int kk = ((unsigned int) (1 << (k - 1)));
        
        u_nm_accel_t *a1=NULL, *a2=NULL;
        complex_t *zc=NULL;

        // now we get the accelerators and constant for the integral
        // computations
        if (knmcmp == MIRROR_CMP) {
            get_mr_acc_zc_values(k, pmr, &a1, &a2, &zc);
        } else if (knmcmp == BEAMSPLITTER_CMP) {
            get_bs_acc_zc_values(k, pbs, &a1, &a2, &zc);
        } else
            bug_error("could not handle knmcmp value %i", knmcmp);
        
        if ((knm_calc_flags & kk) == kk) {
            results[i] = z_by_z(z_by_x(results[i], da), *zc);
        }
        
        i++;
    }
}

void do_riemann_sum_new_int(void *userdata, KNM_COMPONENT_t knmcmp, complex_t *results) {

    mr_knm_map_int_params_t *pmr=NULL;
    bs_knm_map_int_params_t *pbs=NULL;

    surface_merged_map_t *map=NULL;
    double *pmax=NULL, *pmin=NULL, nr1=0, nr2=0;
    int min_k=0, max_k=0;
    unsigned int knm_calc_flags=0;
    bool polar_limits_used=0, using_map=0;
    int n1=0, m1=0, n2=0, m2=0;
    double ap_xoff=0.0, ap_yoff=0.0;

    integrand_get_variables(userdata, knmcmp, &pmr, &pbs, &map,
            &pmax, &pmin, &nr1, &nr2, &min_k, &max_k,
            &knm_calc_flags, &polar_limits_used, &using_map,
            &n1, &m1, &n2, &m2, &ap_xoff, &ap_yoff);
    
    assert(pbs != NULL || pmr != NULL);
    
    double x=0, y=0;
    complex_t u1 = complex_0, u2 = complex_0;

    int i=0, j=0, k=0;
    
    //calculate the map integration limits
    if (knmcmp == MIRROR_CMP) {
        get_mirror_int_limit(pmr, 0);
    } else {
        get_bs_int_limit(pbs, 0);
    }
    
    double dfac = 1;
    int ndx = (int) ((pmax[0] - pmin[0]) / (dfac*map->xstep));
    int ndy = (int) ((pmax[1] - pmin[1]) / (dfac*map->ystep));

    double dx = (pmax[0] - pmin[0]) / (double) ndx;
    double dy = (pmax[1] - pmin[1]) / (double) ndy;
    double da = dx*dy;

    if (ndx == 0 || ndy == 0) {
        gerror("Riemann sum: integration limits are smaller than the step size. Please use a map with a higher resolution.\n");
    }

    if (false & inter.debug && !options.quiet) {
        debug_msg(" Riemann sum statistics: dx=%e dy=%e ndx=%d ndy=%d da=%e\n", dx, dy, ndx, ndy, da);
        debug_msg("                         x range: %e %e  y range: %e %e\n", pmin[0], pmax[0], pmin[1], pmax[1]);
        debug_msg("                         x step: %e  y step: %e\n", map->xstep, map->ystep);
    }

    int intops = 0;
    double A=0, phi=0;
    double cos_alpha = 1;
    double map_rotation = 0, incident_angle = 0;
    
    if (knmcmp == MIRROR_CMP) {
        map_rotation = pmr->mirror->angle;   
    } else if (knmcmp == BEAMSPLITTER_CMP) {
        map_rotation = pbs->bs->map_rotation;
        incident_angle = pbs->bs->alpha_1;
        cos_alpha = cos(incident_angle * RAD);
    } else 
        gerror("knmcmp parameter not recognised");
    
    /*#pragma omp parallel for collapse(3) default(none) \
                 private(i,j,A,phi,x,y,k,u1,u2) \
                 firstprivate(pmr, pbs) \
                 shared(results,dx,dy,ndx,ndy,min_k,max_k, pmin,knm_calc_flags, using_map, knmcmp, map, intops, idfile, n1,n2,m1,m2,map_rotation, incident_angle,nr1,nr2)
    */
    for (i = 0; i <= ndx; i++) {
        for (j = 0; j <= ndy; j++) {
            // now loop through each component, check whether we should calculate
            // the knm, then interpolate etc.
            for (k = min_k; k <= max_k; k++) {
                x = pmin[0] + i * dx;       
                y = pmin[1] + j * dy;
                
                // the calc flags are powers of 2 and stored in a flag
                // use bit shift here to get the powers of 2 in the loop
                unsigned int kk = ((unsigned int) (1 << (k - 1)));

                // if we should be calculating this k continue
                if ((knm_calc_flags & kk) == kk) {

                    if (using_map) {

                        if (knmcmp == MIRROR_CMP) {
                            interpolate_merged_map(map, knmcmp, (KNM_MIRROR_NODE_DIRECTION_t) k, x, y, &A, &phi, nr1,nr2,map_rotation);
                        } else if (knmcmp == BEAMSPLITTER_CMP) {
                            interpolate_merged_map(map, knmcmp, (KNM_BS_NODE_DIRECTION_t) k, x, y, &A, &phi, nr1,nr2,map_rotation);
                            // When at a non-normal incidence we also need to increase the depth of a 
                            // phase map. As the larger angle of incidence causes a longer path length
                            // difference
                            if (cos_alpha != 0) {
                                phi *= cos_alpha;
                            }
                            
                        } else 
                            gerror("knmcmp parameter not recognised");

                    } else {
                        A = 1;
                        phi = 0;
                    }

                    u_nm_accel_t *a1=NULL, *a2=NULL;
                    complex_t *zc=NULL;

                    // now we get the accelerators and constant for the integral
                    // computations
                    if (knmcmp == MIRROR_CMP) {
                        get_mr_acc_zc_values(k, pmr, &a1, &a2, &zc);
                    } else if (knmcmp == BEAMSPLITTER_CMP) {
                        get_bs_acc_zc_values(k, pbs, &a1, &a2, &zc);
                    } else
                        bug_error("could not handle knmcmp value %i", knmcmp);

                    u1 = u_nm_fast_x(x*cos_alpha - ap_xoff, y - ap_yoff, n1, m1, a1);
                    u2 = u_nm_fast_x(x*cos_alpha - ap_xoff, y - ap_yoff, n2, m2, a2);

                    complex_t tmp = z_by_z(z_by_zc(z_by_xphr(u1, A, phi), u2), *zc);

                    results[k - 1].re += tmp.re * cos_alpha;
                    results[k - 1].im += tmp.im * cos_alpha;

                    if(map->save_integration_points){                        
                        u1 = z_by_z(u1,z_by_z(a1->acc_n->prefac[n1], a1->acc_m->prefac[m1]));
                        u2 = z_by_z(u2,z_by_z(a2->acc_n->prefac[n2], a2->acc_m->prefac[m2]));
                        
                        fprintf(idfile,"%.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g\n",
                                    x,y,u1.re,u1.im,u2.re,u2.im,A,phi);
                    }

                    intops++;
                }
            }
        }
    }
    
    if (inter.debug && map->show_knm_neval && !options.quiet)
        message("   - %i%i->%i%i calculating with %i evaluations\n", n1, m1, n2, m2, intops);

    // finally multiply by the area of the each pixel in the map to correctly
    // scale the results
    i = 0;
    
    for (k = min_k; k <= max_k; k++) {
        unsigned int kk = ((unsigned int) (1 << (k - 1)));
        if ((knm_calc_flags & kk) == kk)
            results[i] = z_by_x(results[i], da);
        
        i++;
    }
}

