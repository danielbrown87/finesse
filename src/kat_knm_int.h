
#include "kat.h"
#include "kat_knm_mirror.h"
#include "kat_io.h"

#ifndef KAT_KNM_INT_H
#define	KAT_KNM_INT_H

// Default settings for interpolation routines
#define DEFAULT_INTERP_SIZE 3;   // must be odd integer > 3
#define DEFAULT_INTERP_METHOD 3; // 1 - spline, 2 - linear, 3 -NN, Nearest Neighbour

//here we define a macro that applies a round off to zero if the number is smaller than the error
//#define INT_VAL(u) ((abs(error[u]*1e-1) < abs(integral[u])) ? integral[u] : 0) * J
#define INT_VAL(u) integral[u] * J

void create_newton_cotes_composite_weights(double *W, size_t length, int order);

void interpolate_merged_map(surface_merged_map_t *map, KNM_COMPONENT_t knmcmp, int knum, const double px
        , const double py, double *const A, double *const phi, double nr1
        , double nr2, double angle);

void do_para_cuhre_map_int(void *p, KNM_COMPONENT_t knmcmp, complex_t *results, double abs_err, double rel_err);
void do_newton_cotes_int(void *userdata, KNM_COMPONENT_t knmcmp, complex_t *results, bitflag mismatching);
void do_riemann_sum_new_int(void *userdata, KNM_COMPONENT_t knmcmp, complex_t *results);

void do_serial_cuhre_map_int(void *p, KNM_COMPONENT_t knmcmp, complex_t *results, double abs_err, double rel_err);

void fill_unn_cache(unn_cache_t *c, u_n_accel_t *acc1, u_n_accel_t *acc2, bool mode_matched);
complex_t do_romhom_real_int(roq_weights_t *rom, dmatrix u_xy, unn_cache_t *cx, unn_cache_t *cy, int n1, int m1, int n2, int m2);

void allocate_unn_cache(unn_cache_t *c, size_t num_nodes);
void allocate_romhom_workspace(rom_map_t *rom, int component_type);
void allocate_newton_cotes_workspace(knm_workspace_t *ws, surface_merged_map_t *map, int component_type);

#endif	/* KAT_KNM_INT_H */

