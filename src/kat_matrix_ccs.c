#include "kat.h"
#include "kat_inline.c"
#include "kat_spa.h"
#include "kat_check.h"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_matrix_ccs.h"
#include "kat_aa.h"
#include "klu.h"
#if INCLUDE_NICSLU == 1
#include "nicsluc.h"
#endif

#if INCLUDE_NICSLU == 1
#include <kat_paralution.h>
#endif

#include "kat_aux.h"
#include <limits.h>

extern const complex_t complex_i;
extern const complex_t complex_1;
extern const complex_t complex_m1;
extern const complex_t complex_0;

//extern init_variables_t in;
extern interferometer_t inter;
extern options_t options;
extern memory_t mem;

extern ifo_matrix_vars_t M_ifo_car;
extern ifo_matrix_vars_t M_ifo_sig;

extern double *f_s;

void set_row_ptr(matrix_ccs_t *matrix, int ix, long val){
    assert(matrix != NULL);
    
    if(options.sparse_solver == KLU_FULL)
        ((UF_long*)matrix->row_idx)[ix] = val;
#if INCLUDE_NICSLU == 1
    else if(options.sparse_solver == NICSLU)
        ((uint__t*)matrix->row_idx)[ix] = val;
#endif
#if INCLUDE_PARALUTION == 1
    else if(options.sparse_solver == PARALUTION)
        ((int*)matrix->row_idx)[ix] = val;
#endif
    else
        bug_error("Unhandled solver\n");
}

void set_col_ptr(void *col_ptr, int ix, long val){
    assert(col_ptr != NULL);
    
    if(options.sparse_solver == KLU_FULL)
        ((UF_long*)col_ptr)[ix] = val;
#if INCLUDE_NICSLU == 1
    else if(options.sparse_solver == NICSLU)
        ((uint__t*)col_ptr)[ix] = val;
#endif
#if INCLUDE_PARALUTION == 1
    else if(options.sparse_solver == PARALUTION)
        ((int*)col_ptr)[ix] = val;
#endif
    else
        bug_error("Unhandled solver\n");
}

long get_col_ptr(void *col_ptr, int ix){
    assert(col_ptr != NULL);
    
    if(options.sparse_solver == KLU_FULL)
        return (long)((UF_long*)col_ptr)[ix];
#if INCLUDE_NICSLU == 1
    else if(options.sparse_solver == NICSLU)
        return (long)((uint__t*)col_ptr)[ix];
#endif
#if INCLUDE_PARALUTION == 1
    else if(options.sparse_solver == PARALUTION)
        return (long)((int*)col_ptr)[ix];
#endif
    else
        bug_error("Unhandled solver\n");
    
    return -1;
}

/**
 * Solves M x = y
 * 
 * @param M
 * @param x
 * @param y
 */
void ccs_zgemv(matrix_ccs_t *M, complex_t *x, complex_t *y){
    assert(y != NULL);
    assert(x != NULL);
    assert(M != NULL);
    assert(y != x);
    
    int i, j, col=0, el=0;
    
    memset(y, 0, M->num_eqns);
    
    if(options.sparse_solver == KLU_FULL){
        UF_long *row_ptr = (UF_long*)M->row_idx;
        
        for(i=0; i<M->num_eqns; i++){
            complex_t vj = x[i];

            for(j=col;j<col+M->row_col_count[i]; j++){
                z_inc_z(&(y[row_ptr[j]]), z_by_z(co(M->values[el], M->values[el+1]), vj));
                el += 2;
            }

            col += M->row_col_count[i];
        }
#if INCLUDE_NICSLU == 1
    } else if(options.sparse_solver == NICSLU){
        uint__t *row_ptr = (uint__t*)M->row_idx;
        
        for(i=0; i<M->num_eqns; i++){
            complex_t vj = x[i];

            for(j=col;j<col+M->row_col_count[i]; j++){
                z_inc_z(&(y[row_ptr[j]]), z_by_z(co(M->values[el], M->values[el+1]), vj));
                el += 2;
            }

            col += M->row_col_count[i];
        }
#endif
#if INCLUDE_PARALUTION == 1
    } else if(options.sparse_solver == PARALUTION){
        int *row_ptr = (int*)M->row_idx;
        
        for(i=0; i<M->num_eqns; i++){
            complex_t vj = x[i];

            for(j=col;j<col+M->row_col_count[i]; j++){
                z_inc_z(&(y[row_ptr[j]]), z_by_z(co(M->values[el], M->values[el+1]), vj));
                el += 2;
            }

            col += M->row_col_count[i];
        }
#endif
    } else
        bug_error("solver not handled");
}

int is_node_connected(int node_idx){ 
    return (inter.node_conn_list[node_idx].comp_1 == NULL || inter.node_conn_list[node_idx].comp_2 == NULL) ? 0 : 1;
}

int is_loose_node(ifo_matrix_vars_t *matrix, node_t *n){
    return (n->connect == 0 && (matrix->node_rhs_idx_1[n->list_index] < 0 || matrix->node_rhs_idx_2[n->list_index] < 0 ) && n->io == 0) ? 1 : 0;
}

void add_pcol(ifo_matrix_vars_t *m, pseudocolumn_t *p){
    // node we're adding should just be a single node
    assert(p->next == NULL);
    
    // if nothing has been added before add as head
    if(m->pcol_head == NULL) {
        m->pcol_head = p;
        m->pcol_curr = p;
        m->pcols_num = 1;
    } else {
        // if something take the current and set next
        // then set new end of list
        m->pcol_curr->next = (struct pseudocolumn*)p;
        m->pcol_curr = p;
        m->pcols_num++;
    }
}

void alloc_matrix_ccs_nnz(long *bytes, ifo_matrix_vars_t *matrix_ccs){

    assert(bytes != NULL);
    assert(matrix_ccs != NULL);
    assert(matrix_ccs->M.num_nonzeros > 0);
    
    int NNZ = matrix_ccs->M.num_nonzeros;
    int try_bytes = 0; 
    size_t size = 0;
    
    switch(options.sparse_solver){
        case KLU_FULL:
            size = sizeof(UF_long);
            try_bytes = NNZ * size;
            break;
#if INCLUDE_NICSLU == 1
        case NICSLU:
            size = sizeof(uint__t);
            try_bytes = NNZ * size;
            break;
#endif
#if INCLUDE_PARALUTION == 1
        case PARALUTION:
            size = sizeof(int);
            try_bytes = NNZ * size;
            break;
#endif
        default:
            bug_error("solver unhandled");
    }
    
    if ((matrix_ccs->M.row_idx = calloc(NNZ, size)) == NULL) {
        gerror("Could not allocate CCS row_idx");
    } else
        *bytes += try_bytes;

#if INCLUDE_PARALUTION == 1    
    if(options.sparse_solver == PARALUTION) {
        // Paralution needs COO format to assemble matrix
        if ((matrix_ccs->M.col_idx = calloc(NNZ, size)) == NULL) {
            gerror("Could not allocate col_idx");
        } else
            *bytes += try_bytes;
    } else {
        matrix_ccs->M.col_idx = NULL;
    }
#endif
    
    try_bytes = 2 * NNZ * sizeof (double);
    
    // allocate ride-hand-side vector (again using double for complex values)
    if ((matrix_ccs->M.values = (double *) calloc(2 * NNZ, sizeof (double))) == NULL) {
        gerror("Could not allocate CCS values");
    } else
        *bytes += try_bytes;
}

void __alloc_coupling_matrix(int ****does_f_couple, int ****f_couple_allocd, int ****f_couple_order, int num, int num_freqs){
    *does_f_couple = (int***)calloc(num, sizeof(int**));
    *f_couple_allocd = (int***)calloc(num, sizeof(int**));
    *f_couple_order = (int***)calloc(num, sizeof(int**));
    
    if(*does_f_couple == NULL || *f_couple_allocd == NULL || *f_couple_order == NULL)
        gerror("Insufficient memory to allocate modulator frequency coupling matrix");
    
    int i,j;
    
    for(i=0;i<num; i++){
        (*does_f_couple)[i]   = (int**)calloc(num_freqs, sizeof(int*));
        (*f_couple_allocd)[i] = (int**)calloc(num_freqs, sizeof(int*));
        (*f_couple_order)[i]  = (int**)calloc(num_freqs, sizeof(int*));
        
        if((*does_f_couple)[i] == NULL || (*f_couple_allocd)[i] == NULL || (*f_couple_order)[i] == NULL)
            gerror("Insufficient memory to allocate modulator frequency coupling matrix");
        
        for(j=0;j<num_freqs;j++){
            (*does_f_couple)[i][j]   = (int*)calloc(num_freqs, sizeof(int));
            (*f_couple_allocd)[i][j] = (int*)calloc(num_freqs, sizeof(int));
            (*f_couple_order)[i][j]  = (int*)calloc(num_freqs, sizeof(int));
            
            if((*does_f_couple)[i][j] == NULL || (*f_couple_allocd)[i][j] == NULL || (*f_couple_order)[i][j] == NULL)
                gerror("Insufficient memory to allocate modulator frequency coupling matrix");
        }
    }
}

void alloc_matrix_ccs_freq_couplings(ifo_matrix_vars_t *matrix){
    // all modulators couple frequencies
    __alloc_coupling_matrix(&matrix->mod_does_f_couple,
                            &matrix->mod_f_couple_allocd,
                            &matrix->mod_f_couple_order,
                            inter.num_modulators, matrix->num_frequencies);
    
    if (inter.num_mirror_dithers) {
        __alloc_coupling_matrix(&matrix->m_does_f_couple,
                                &matrix->m_f_couple_allocd,
                                &matrix->m_f_couple_order,
                                inter.num_mirror_dithers, matrix->num_frequencies);
    }
    
    if (inter.num_bs_dithers) {
        __alloc_coupling_matrix(&matrix->bs_does_f_couple,
                                &matrix->bs_f_couple_allocd,
                                &matrix->bs_f_couple_order,
                                inter.num_bs_dithers, matrix->num_frequencies);
    }
}

/**
 * For a particular matrix the memory for a CSC format matrix 
 * will be allocated.
 * 
 * @param bytes
 * @param matrix_ccs
 */
void alloc_ifo_matrix_ccs(long *bytes, ifo_matrix_vars_t *matrix_ccs){
    
    assert(bytes != NULL);
    assert(matrix_ccs != NULL);
    
    int num_eqns = matrix_ccs->M.num_eqns;
    
    assert(num_eqns > 0);
    
    // create the standard interferometer matrix
    debug_msg("Allocating all-in interferometer matrix\n");
    
    size_t solver_size=0;
    
    // Allocate vectors for storing matrix in CCS format for all nodes, frequencies
    // and fields in one large matrix
    switch(options.sparse_solver){
        case KLU_FULL:
            solver_size = sizeof(UF_long);
            matrix_ccs->solver_opts = calloc(1, sizeof(klu_objs_t));
            break;
#if INCLUDE_NICSLU == 1
        case NICSLU:
            solver_size = sizeof(uint__t);
            matrix_ccs->solver_opts = calloc(1, sizeof(nicslu_objs_t));
            break;
#endif
#if INCLUDE_PARALUTION == 1
        case PARALUTION:
            solver_size = sizeof(int);
            // !!! As paralution is a C++ library and compiled differently
            // we can get the size of the C++ struct for storing the data, thus
            // this is allocated in kat_paralution.cpp
            break;
#endif
        default:
            bug_error("Sparse solver not handled");
    }
    
    // allocate column pointers
    int try_bytes = (num_eqns + 1) * solver_size;
   
    matrix_ccs->M.col_ptr = calloc(num_eqns + 1, solver_size);
    
    if (matrix_ccs->M.col_ptr == NULL) {
        gerror("Could not allocate CCS col_ptr");
    } else
        *bytes += try_bytes;
    
    try_bytes = (num_eqns + 1) * sizeof (long);
    
    matrix_ccs->M.row_col_count = (long *) calloc(num_eqns + 1, sizeof(long));
    
    if (matrix_ccs->M.row_col_count == NULL) {
        gerror("Could not allocate CCS row_col_count");
    } else
        *bytes += try_bytes;
    
    try_bytes = 2 * num_eqns * sizeof (double);
   
    // allocate ride-hand-side vector (again using double for complex values)
    matrix_ccs->rhs_values = (double *) calloc(2 * num_eqns, sizeof (double));
    
    if (matrix_ccs->rhs_values == NULL) {
        gerror("Could not allocate CCS rhs_values");
    } else
        *bytes += try_bytes;  
    
    // allocate array for storing which index each node is at in the RHS vector
    try_bytes = inter.num_nodes * sizeof(int);
    
    matrix_ccs->node_rhs_idx_1 = (int *) malloc(try_bytes);
    matrix_ccs->node_rhs_idx_2 = (int *) malloc(try_bytes);
    
    if (matrix_ccs->node_rhs_idx_1 == NULL || matrix_ccs->node_rhs_idx_2 == NULL){
        gerror("could not allocated node rhs index array");
    } else 
        *bytes += 2*try_bytes;
    
    // default the node indices to -1, this is used in checks later to ensure that
    // a node has been positioned in the RHS vector
    memset(matrix_ccs->node_rhs_idx_1, -1, try_bytes);
    memset(matrix_ccs->node_rhs_idx_2, -1, try_bytes);
    
    try_bytes = inter.num_mirrors * sizeof(int);
    matrix_ccs->mirror_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->mirror_rhs_idx == NULL){
        gerror("could not allocated mirror rhs index array");
    } else {
        *bytes += try_bytes;  
        memset(matrix_ccs->mirror_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_beamsplitters * sizeof(int);
    matrix_ccs->bs_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->bs_rhs_idx == NULL){
        gerror("could not allocated beamsplitter rhs index array");
    } else {
        *bytes += try_bytes;  
        memset(matrix_ccs->bs_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_diodes * sizeof(int);
    matrix_ccs->diode_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->diode_rhs_idx == NULL){
        gerror("could not allocated diode rhs index array");
    } else  {
        *bytes += try_bytes;
        memset(matrix_ccs->diode_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_dbss * sizeof(int);
    matrix_ccs->dbs_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->dbs_rhs_idx == NULL){
        gerror("could not allocated dbs rhs index array");
    } else  {
        *bytes += try_bytes;
        memset(matrix_ccs->dbs_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_lenses * sizeof(int);
    matrix_ccs->lens_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->lens_rhs_idx == NULL){
        gerror("could not allocated lens rhs index array");
    } else 
        *bytes += try_bytes;
    
    memset(matrix_ccs->lens_rhs_idx, -1, try_bytes);
    
    try_bytes = inter.num_sagnacs * sizeof(int);
    matrix_ccs->sagnac_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->sagnac_rhs_idx == NULL){
        gerror("could not allocated sagnac rhs index array");
    } else {
        *bytes += try_bytes;
        memset(matrix_ccs->sagnac_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_spaces * sizeof(int);
    matrix_ccs->space_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->space_rhs_idx == NULL){
        gerror("could not allocated space rhs index array");
    } else {
        *bytes += try_bytes;
        memset(matrix_ccs->space_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_modulators * sizeof(int);
    matrix_ccs->modulator_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->modulator_rhs_idx == NULL){
        gerror("could not allocated modulator rhs index array");
    } else {
        *bytes += try_bytes;
        memset(matrix_ccs->modulator_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_light_outputs * sizeof(uint64_t);
    matrix_ccs->output_rhs_idx = (uint64_t *) malloc(try_bytes);
    
    if (matrix_ccs->output_rhs_idx == NULL){
        gerror("could not allocated output rhs index array");
    } else {
        *bytes += try_bytes;
        memset(matrix_ccs->output_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_gratings * sizeof(int);
    matrix_ccs->grating_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->grating_rhs_idx == NULL){
        gerror("could not allocated grating rhs index array");
    } else {
        *bytes += try_bytes;
        memset(matrix_ccs->grating_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_blocks * sizeof(int);
    matrix_ccs->block_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->block_rhs_idx == NULL){
        gerror("could not allocated block rhs index array");
    } else {
        *bytes += try_bytes;
        memset(matrix_ccs->block_rhs_idx, -1, try_bytes);
    }
    
    try_bytes = inter.num_slinks * sizeof(int);
    matrix_ccs->feedback_rhs_idx = (int *) malloc(try_bytes);
    
    if (matrix_ccs->feedback_rhs_idx == NULL){
        gerror("could not allocated feedback rhs index array");
    } else {
        *bytes += try_bytes;
        memset(matrix_ccs->feedback_rhs_idx, -1, try_bytes);
    }
}

/** Returns the rhs index where this components nodes start
 * 
 * @param type int value for component type
 * @param comp pointer to type
 * @return rhs vector index
 */
int get_comp_rhs_idx(ifo_matrix_vars_t *matrix, int type, void *comp){
    switch(type){
        case MIRROR: return matrix->mirror_rhs_idx[((mirror_t*)comp)->comp_index];
        case SAGNAC: return matrix->sagnac_rhs_idx[((sagnac_t*)comp)->comp_index];
        case BLOCK: return matrix->block_rhs_idx[((block_t*)comp)->comp_index];
        case MODULATOR: return matrix->modulator_rhs_idx[((modulator_t*)comp)->comp_index];
        case LENS: return matrix->lens_rhs_idx[((lens_t*)comp)->comp_index];
        case DIODE: return matrix->diode_rhs_idx[((diode_t*)comp)->comp_index];
        case DBS: return matrix->dbs_rhs_idx[((dbs_t*)comp)->comp_index];
        case SPACE: return matrix->space_rhs_idx[((space_t*)comp)->list_index];
        case BEAMSPLITTER: return matrix->bs_rhs_idx[((beamsplitter_t*)comp)->comp_index];
        case GRATING: return matrix->grating_rhs_idx[((grating_t*)comp)->comp_index];
        default:
            bug_error("Could not handle component type %i", type);
    }
    
    return -1;
}

/** Returns the rhs index where this components nodes start
 * 
 * @param type int value for component type
 * @param comp_index Index of component in global list
 * @return rhs vector index
 */
int get_comp_rhs_idx2(ifo_matrix_vars_t *matrix, int type, int comp_index){
    switch(type){
        case MIRROR: return matrix->mirror_rhs_idx[comp_index];
        case SAGNAC: return matrix->sagnac_rhs_idx[comp_index];
        case BLOCK: return matrix->block_rhs_idx[comp_index];
        case MODULATOR: return matrix->modulator_rhs_idx[comp_index];
        case LENS: return matrix->lens_rhs_idx[comp_index];
        case DIODE: return matrix->diode_rhs_idx[comp_index];
        case DBS: return matrix->dbs_rhs_idx[comp_index];
        case SPACE: return matrix->space_rhs_idx[comp_index];
        case BEAMSPLITTER: return matrix->bs_rhs_idx[comp_index];
        case GRATING: return matrix->grating_rhs_idx[comp_index];
        default:
            bug_error("Could not handle component type %i", type);
    }
    
    return -1;
}

/**
 * For a given component and space (which should be connected to one node) this
 * will return the node that is connecting the space and component and the other 
 * node
 */
void get_next_space_node(int type, void *comp, space_t *space, node_t **node_conn, node_t **node_next){
    int n1=-1, n2=-1, n3=-1;
    
    switch(type){
        case DIODE:
            if(n1 < 0) n1 = ((diode_t*)comp)->node1_index; 
            if(n2 < 0) n2 = ((diode_t*)comp)->node2_index;
            if(n3 < 0) n3 = ((diode_t*)comp)->node3_index;
             
        case LENS:
            if(n1 < 0) n1 = ((lens_t*)comp)->node1_index; 
            if(n2 < 0) n2 = ((lens_t*)comp)->node2_index;
        
        case SAGNAC:
            if(n1 < 0) n1 = ((sagnac_t*)comp)->node1_index; 
            if(n2 < 0) n2 = ((sagnac_t*)comp)->node2_index;
        
        case BLOCK:
            if(n1 < 0) n1 = ((block_t*)comp)->node1_index; 
            if(n2 < 0) n2 = ((block_t*)comp)->node2_index;
            
        case MODULATOR:
            if(n1 < 0) n1 = ((modulator_t*)comp)->node1_index; 
            if(n2 < 0) n2 = ((modulator_t*)comp)->node2_index;
            
        case MIRROR: 
            if(n1 < 0) n1 = ((mirror_t*)comp)->node1_index; 
            if(n2 < 0) n2 = ((mirror_t*)comp)->node2_index;
            
            if(n1 == space->node1_index || n2 == space->node1_index || n3 == space->node1_index) {
                *node_next = &inter.node_list[space->node2_index];
                *node_conn = &inter.node_list[space->node1_index];
            } else if(n1 == space->node2_index || n2 == space->node2_index || n3 == space->node2_index) {
                *node_next = &inter.node_list[space->node1_index];
                *node_conn = &inter.node_list[space->node2_index];
            } else
                bug_error("Space and component not attached");
            
            break;
            
        case BEAMSPLITTER:
            
            if( space->node1_index == ((beamsplitter_t *)comp)->node1_index
                || space->node1_index == ((beamsplitter_t *)comp)->node2_index
                || space->node1_index == ((beamsplitter_t *)comp)->node3_index
                || space->node1_index == ((beamsplitter_t *)comp)->node4_index) {
                    *node_next = &inter.node_list[space->node2_index];
                    *node_conn = &inter.node_list[space->node1_index];
            }
            else if( space->node2_index == ((beamsplitter_t *)comp)->node1_index
                     || space->node2_index == ((beamsplitter_t *)comp)->node2_index
                     || space->node2_index == ((beamsplitter_t *)comp)->node3_index
                     || space->node2_index == ((beamsplitter_t *)comp)->node4_index) {
                    *node_next = &inter.node_list[space->node1_index];
                    *node_conn = &inter.node_list[space->node2_index];
            } else
                bug_error("Space and component not attached");
            
            break;
            
        case GRATING:
            
            if( space->node1_index == ((grating_t *)comp)->node1_index
                || space->node1_index == ((grating_t *)comp)->node2_index
                || space->node1_index == ((grating_t *)comp)->node3_index
                || space->node1_index == ((grating_t *)comp)->node4_index) {
                    *node_next = &inter.node_list[space->node2_index];
                    *node_conn = &inter.node_list[space->node1_index];
            }
            else if( space->node2_index == ((grating_t *)comp)->node1_index
                     || space->node2_index == ((grating_t *)comp)->node2_index
                     || space->node2_index == ((grating_t *)comp)->node3_index
                     || space->node2_index == ((grating_t *)comp)->node4_index) {
                    *node_next = &inter.node_list[space->node1_index];
                    *node_conn = &inter.node_list[space->node2_index];
            } else
                bug_error("Space and component not attached");
            
            break;
            
        case DBS:
            
            if( space->node1_index == ((dbs_t *)comp)->node1_index
                || space->node1_index == ((dbs_t *)comp)->node2_index
                || space->node1_index == ((dbs_t *)comp)->node3_index
                || space->node1_index == ((dbs_t *)comp)->node4_index) {
                    *node_next = &inter.node_list[space->node2_index];
                    *node_conn = &inter.node_list[space->node1_index];
            }
            else if( space->node2_index == ((dbs_t *)comp)->node1_index
                     || space->node2_index == ((dbs_t *)comp)->node2_index
                     || space->node2_index == ((dbs_t *)comp)->node3_index
                     || space->node2_index == ((dbs_t *)comp)->node4_index) {
                    *node_next = &inter.node_list[space->node1_index];
                    *node_conn = &inter.node_list[space->node2_index];
            } else
                bug_error("Space and component not attached");
            
            break;
            
        case SPACE: 
            assert(comp == space);
            // Here we get return the connected node as the node of the space which
            // is connected to another component, the next node is always a loose on
            // if it isn't this should not end up being called...
            if(inter.node_list[space->node1_index].connect && !inter.node_list[space->node2_index].connect){
                *node_conn = &inter.node_list[space->node2_index];
                *node_next = &inter.node_list[space->node1_index];
            } else if(inter.node_list[space->node2_index].connect && !inter.node_list[space->node1_index].connect){
                *node_conn = &inter.node_list[space->node1_index];
                *node_next = &inter.node_list[space->node2_index];
            } else if(inter.node_conn_list[space->node1_index].comp_2 == NULL) {     
                // if comp2 is null then the only element connected to this node is this space
                *node_conn = &inter.node_list[space->node1_index];
                *node_next = &inter.node_list[space->node2_index];
            } else if(inter.node_conn_list[space->node2_index].comp_2 == NULL) {     
                // if comp2 is null then the only element connected to this node is this space
                *node_conn = &inter.node_list[space->node2_index];
                *node_next = &inter.node_list[space->node1_index];
            } else
                bug_error("Space connection setup not handled");
            
            break;
        
        default:
            bug_error("Could not handle component type %i", type);
    }
}

/**
 * Returns the value index in the CCS stored sparse matrix matrix_ccs_var.values
 * array for a given column. This also increases the column value count so can only
 * be used once to get each element in a column.
 * 
 * @param col column to get value index for
 * @param col_ptr array of column pointers in CCS storage format
 * @param nnz_count current number of elements already in this column
 */
int get_nnz_value_idx(ifo_matrix_vars_t* matrix, long col, void *col_ptr, long *nnz_count, int *el_curr_nnz, int el_max_nnz){
    (*el_curr_nnz) ++;
    
    if((*el_curr_nnz) > el_max_nnz)
        bug_error("Tried to get more non-zero elements for the component than have been previously counted");
    
    // check if there are too many elements in this column by seeing if we are
    // overlapping with next col_ptr
    long i =get_col_ptr(col_ptr, col) + nnz_count[col];
    
    // if we have an equal number of elements to the total in the matrix then
    // we have finished getting all the elements
    if(i >= get_col_ptr(col_ptr, col+1)) {
        if(i > get_col_ptr(col_ptr, matrix->M.num_eqns))
            bug_error("Tried to get more non-zeros than there are in the matrix");
        else if(i < get_col_ptr(col_ptr, matrix->M.num_eqns))
            bug_error("Too many elements in column %ld", col);
    }
    
    return get_col_ptr(col_ptr,col) + nnz_count[col]++;
}

UT_icd pcol_item_icd = {sizeof(pseudocolumn_item_t), NULL, NULL, NULL};

pseudocolumn_t* create_pseudocol(void* comp, int type, int nnz, int size){
    // create new pseudo-column to hold information on the optic and 
    // space components that will later need to be ordered
	  (void)size; // silence compiler warning
    pseudocolumn_t *p = (pseudocolumn_t*) malloc(sizeof(pseudocolumn_t));
    p->next = NULL;
    p->type = type;
    p->component = comp;
    p->nnz = 0;
    p->value_base_idx = nnz;
    
    utarray_new(p->items, &pcol_item_icd);
    
    return p;
}

void add_node_rhs_vector(ifo_matrix_vars_t *matrix, node_t *node, int *ccs_rhs_idx, int fields_per_port){
    // check if this node has been located in the rhs vector already...
    if(matrix->node_rhs_idx_1[node->list_index] < 0){
        matrix->node_rhs_idx_1[node->list_index] = *ccs_rhs_idx;
        *ccs_rhs_idx += fields_per_port;
    }

    // ...and for the 2nd field...
    if(matrix->node_rhs_idx_2[node->list_index] < 0){
        matrix->node_rhs_idx_2[node->list_index] = *ccs_rhs_idx;
        *ccs_rhs_idx += fields_per_port;
    }
    
    node->connect += 1; // mark node as connected
}

int sort_pseudocolumn_items(const void *_a, const void *_b) {
    pseudocolumn_item_t *a = (pseudocolumn_item_t*)_a;
    pseudocolumn_item_t *b = (pseudocolumn_item_t*)_b;
    
    if(a->index < b->index)
        return -1;
    else if(a->index > b->index)
        return 1;
    else
        bug_error("unexpected condition");
    
    return 0;
}

/*
 * Each optical component sub-matrix contains all the frequencies, modes for each
 * of the field for the component. This is a general index computation for a
 * component of N-fields. The layout is such that the modes are group together by
 * frequency and then the frequencies are grouped by the field:
 * 
 * [field 1][field 2]...[field N]
 *    |        \___________
 *    |                    \
 *    | freqs at node 1     \ freqs at node 2
 *   [f=0][f=1]...[f=fN]    [f=0][f=1]...[f=fN]
 *      |            \________________
 *      |                             \
 *      | modes at field 1, freq 0      \ modes at field 1, freq fN
 *     [nm=0][nm=1]...[nm=num_fields]   [nm=0][nm=1]...[nm=num_fields]
 * 
 * 
 * The returned index for the requested field is local to the sub-matrix
 * i.e. field=1,freq=0,mode=0 = 0
 * 
 * Thus the global position of the optical sub-matrix needs to be added to this
 * index to get the actual position in the full matrix.
 * 
 * mode index corresponds to value that is in mem.all_tem_HG (0 based index)
 * freq index corresponds to value that is in ????? (0 based index)
 * field index is a 1 based index, e.g. there is no node 0. Mirrors have nodes
 * 1,2,3 and 4 beamsplitters 1,2,3,4,5,6,7 and 8
 * idx is a 0 based index
 * 
 */
inline int get_node_rhs_idx(int port, int freq, int mode, int num_freqs){
    return (port - 1) * (num_freqs * inter.num_fields) + freq * inter.num_fields + mode;
}

/**
 * if you already have the specific port rhs index for a given node then you 
 * can get the rhs vector index with this.
 * @param freq
 * @param mode
 * @param num_freqs
 * @return 
 */
inline int get_port_rhs_idx(int freq, int mode){
    return freq * inter.num_fields + mode;
}

/**
 * For a given IFO matrix, node, port, frequency and mode you can get the RHS
 * vector index with this.
 * 
 * @param M
 * @param node
 * @param port
 * @param freq
 * @param mode
 * @return 
 */
int get_rhs_idx(ifo_matrix_vars_t *M, node_t *node, int port, int freq, int mode){
    assert(port > 0 && freq >= 0 && mode >= 0);
    assert(node != NULL);
    assert(M->node_rhs_idx_1[node->list_index] >= 0 && "Node RHS index not set");
    return M->node_rhs_idx_1[node->list_index] + get_node_rhs_idx(port, freq, mode, M->num_frequencies);
}

int get_submatrix_num_fields(int ports, int num_freqs){
    return ports * num_freqs * inter.num_fields;
}

/*
 * This sets the position of all the components in the sparse matrix. Components
 * occupy the diagonals i.e. optics fill in the diagonals, i.e.
 * 
 *      |O1      || a_O1 |
 *      |  O2    || a_O2 |
 *      |    O3  || a_O3 |
 *      |      O4|| a_O4 |
 * 
 * Each optic O is really a sub-matrix that relates the coupling at the component
 * between all the fields, modes and frequencies. The vectors a_O are then the
 * field amplitudes of each of the optics for each mode and frequency. 
 * 
 * The vector is setup as described by get_submatrix_field_index, so that any
 * frequency field for a given mode can be easily found in the matrix
 * 
 * Each space then occupies 2 sections to link the two optical components
 * 
 *      |O1 S1       || a_O1 |
 *      |S1 O2 S2    || a_O2 |
 *      |   S2 O3 S3 || a_O3 |
 *      |      S3 O4 || a_O4 |
 * 
 * The upper half of the matrix represents the space from node 1 to node 2 for that
 * space, the lower half connecting fields for node 2 to node 1.
 * 
 * To compute compressed column storage we then need to cycle through
 * column by column for each component and the spaces.
 *
 *      |O1 S1       || a_O1 |
 *      |S1 O2 S2    || a_O2 |
 *      |   S2 O3 S3 || a_O3 |
 *      |      S3 O4 || a_O4 |
 *       ||    ||
 *       ||    ||
 *       ||    \/
 *       ||    ca = [S2(1->2),O3,S3(2->1)]
 *       \/    
 *       ca = [O1, S1(2->1)]
 * 
 * Each component sub-matrix will span multiple actual columns, therefore we go
 * through each actual column which only contain values from the components in
 * the column of interest. This way we can fill up the matrix_ccs variables
 * easily.
 */

void get_unknown_space_node(ifo_matrix_vars_t *matrix, void *known_comp, int node_idx_known, node_t **unknown_node, space_t **s_conn){
    int type;
    void *comp = NULL;
    node_connections_t *nc1 = &inter.node_conn_list[node_idx_known];
    
    // check node1 connections. One of the components should be 
    // this object the other a space.
    if(nc1->comp_1 == known_comp) {
        type = nc1->type_2;
        comp = nc1->comp_2;
    } else if(nc1->comp_2 == known_comp){
        type = nc1->type_1;
        comp = nc1->comp_1;
    } else
        bug_error("component isn't actually attached to node %s", inter.node_list[node_idx_known].name);

    if(comp != NULL){
        if(type != SPACE)
            gerror("node %s connecting object is not a space, can't handle components not attached by spaces", inter.node_list[node_idx_known].name);

        space_t *s = (space_t*) comp;
        *s_conn = s;

        // now we have the space we want to get the other node index.
        // This node will then point to the RHS vector elements that we
        // need to join together.
        if(s->node1_index == node_idx_known)
            *unknown_node = (node_t*)&(inter.node_list[s->node2_index]);
        else if(s->node2_index == node_idx_known)
            *unknown_node = (node_t*)&(inter.node_list[s->node1_index]);
        else
            bug_error("known node index was neither of the nodes indices of the space");
        
        // check if this node has been assigned a place in the matrix RHS vector
        // if not it is a loose node
        if(is_loose_node(matrix, *unknown_node)){
            *unknown_node = NULL;
            *s_conn = NULL;
        }
                
    } else {
        *unknown_node = NULL;
        *s_conn = NULL;
    }
}

void set_diagonal_element(ifo_matrix_vars_t *matrix, int rhs_idx, int *col, int *nnz, int *el_nnz, int port, int f, int i, int mode){
    if(mode == GETELEMENT){
        int ix = get_nnz_value_idx(matrix, *col, matrix->M.col_ptr, matrix->M.row_col_count, nnz, *el_nnz);
        
        if(ix > matrix->M.num_nonzeros-1 || ix < 0)
            bug_error("index greater than number of non-zeros or less than 0");
        
        set_row_ptr(&matrix->M, ix, rhs_idx + get_node_rhs_idx(port, f, i, matrix->num_frequencies));
        
        ((complex_t*)matrix->M.values)[ix] = complex_1;
    } else if(mode == COUNT) {
        matrix->M.row_col_count[*col]++; 
        (*el_nnz)++;
    }
}

void set_diagonal_element_mech(ifo_matrix_vars_t *matrix, int row_idx, int *col_idx, int *nnz, int *el_nnz, int mode){
    if(mode == GETELEMENT){
        int ix = get_nnz_value_idx(matrix, *col_idx, matrix->M.col_ptr, matrix->M.row_col_count, nnz, *el_nnz);
        
        if(ix > matrix->M.num_nonzeros-1 || ix < 0)
            bug_error("index greater than number of non-zeros or less than 0");
        
        set_row_ptr(&matrix->M, ix, row_idx);
        
        ((complex_t*)matrix->M.values)[ix] = complex_1;
    } else if(mode == COUNT) {
        matrix->M.row_col_count[*col_idx]++; 
        (*el_nnz)++;
    }
}

void set_element(ifo_matrix_vars_t *matrix, complex_t** a, int rhs_idx, int *col, int *nnz, int *el_nnz, int port, int f, int i, int mode){
    if(mode == GETELEMENT) {
        int ix = get_nnz_value_idx(matrix, *col, matrix->M.col_ptr, matrix->M.row_col_count, nnz, *el_nnz);
        
        if(ix > matrix->M.num_nonzeros-1)
            bug_error("index greater than number of non-zeros");
        
        set_row_ptr(&matrix->M, ix, rhs_idx + get_node_rhs_idx(port, f, i, matrix->num_frequencies));
        *a = &(((complex_t*)matrix->M.values)[ix]);
        
    } else if(mode == COUNT) {
        matrix->M.row_col_count[*col]++;
        (*el_nnz)++;
    }
}

void set_element_mech(ifo_matrix_vars_t *matrix, complex_t** a, int rhs_idx, int *col, int *nnz, int *el_nnz, int mode){
    if(mode == GETELEMENT) {
        int ix = get_nnz_value_idx(matrix, *col, matrix->M.col_ptr, matrix->M.row_col_count, nnz, *el_nnz);
        
        if(ix > matrix->M.num_nonzeros-1)
            bug_error("index greater than number of non-zeros");
        
        set_row_ptr(&matrix->M, ix, rhs_idx);
        
        *a = &(((complex_t*)matrix->M.values)[ix]);
    } else if(mode == COUNT) {
        matrix->M.row_col_count[*col]++;
        (*el_nnz)++;
    }
}

/**
 * Gets the matrix elements that feed this feedback signal into other components
 * 
 * Assumes that the feedback rhs vector index is always greater than any component
 * motion it will be applied to
 * 
 * @param feedback
 * @param matrix
 * @param pcol_nnz
 * @param mode
 */
void get_feedback_output_elements(slink_t *feedback, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(feedback != NULL);
    assert(matrix != NULL);
    
    int got_nnz = 0;
    
    if(mode == COUNT) {
        assert(pcol_nnz != NULL);
        // number of non-zeros is just the number of motions this feedback signal 
        // is applied to, currently this is just 1, +1 for the diagonal element
        feedback->nnz_out_count = 0;
    } else {
        assert(matrix->M.values != NULL);
    }
    
    int col = matrix->feedback_rhs_idx[feedback->list_index];
    int rhs_idx = col; // store this value for referencing

    if(feedback->comp_type == MIRROR) {
        mirror_t *m = &(inter.mirror_list[feedback->comp_list_idx]);

        rhs_idx = m->x_rhs_idx + feedback->comp_motion_idx;
    } else if(feedback->comp_type == BEAMSPLITTER) {
        beamsplitter_t *bs = &(inter.bs_list[feedback->comp_list_idx]);

        rhs_idx = bs->x_rhs_idx + feedback->comp_motion_idx;
    }

    set_element_mech(matrix, &(feedback->d_x), rhs_idx, &col, &got_nnz, &(feedback->nnz_out_count), mode);

    set_diagonal_element_mech(matrix, col, &col, &got_nnz, &(feedback->nnz_out_count), mode);
    
    if(mode == GETELEMENT) {
        if(got_nnz != feedback->nnz_out_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT) {
        *pcol_nnz += feedback->nnz_out_count;
    } else
        bug_error("unexpected mode value");  
}

void get_motion_link_elements(motion_link_t *motion, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(motion != NULL);
    assert(matrix != NULL);
    
    int got_nnz = 0;
    
    if(mode == COUNT) {
        assert(pcol_nnz != NULL);
        motion->nnz_count = 0;
    } else {
        assert(matrix->M.values != NULL);
    }
    
    int row_idx = 0;
    
    if(motion->to_type == MIRROR) {
        row_idx = inter.mirror_list[motion->to_list_idx].x_rhs_idx;
    } else if(motion->to_type == BEAMSPLITTER){
        row_idx = inter.bs_list[motion->to_list_idx].x_rhs_idx;
    } else
        bug_error("unhandled");
    
    row_idx += motion->to_motion;
    
    int f,i;
    
    if(motion->from_type == MIRROR){
        mirror_t *m = &inter.mirror_list[motion->from_list_idx];
        node_t *n1 = &inter.node_list[m->node1_index];
        node_t *n2 = &inter.node_list[m->node2_index];
        
        int col;
        
        if(!n1->gnd_node){
            col = matrix->node_rhs_idx_1[n1->list_index];

            for(f=0; f<inter.num_signal_freqs; f++){
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a1i_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
            for(f=0; f<inter.num_signal_freqs; f++){    
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a1o_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
        }
        
        if(!n2->gnd_node){
            col = matrix->node_rhs_idx_1[n2->list_index];

            for(f=0; f<inter.num_signal_freqs; f++){    
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a2o_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
            
            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a2i_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
        }
        
    } else if(motion->from_type == BEAMSPLITTER) {
        beamsplitter_t *bs = &inter.bs_list[motion->from_list_idx];
        node_t *n1 = &inter.node_list[bs->node1_index];
        node_t *n2 = &inter.node_list[bs->node2_index];
        node_t *n3 = &inter.node_list[bs->node3_index];
        node_t *n4 = &inter.node_list[bs->node4_index];
        
        int col;
        
        if(!n1->gnd_node){
            col = matrix->node_rhs_idx_1[n1->list_index];

            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a1i_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
            
            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a1o_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
        }
        
        if(!n2->gnd_node){
            col = matrix->node_rhs_idx_1[n2->list_index];

            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a2i_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a2o_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
        }
        
        if(!n3->gnd_node){
            col = matrix->node_rhs_idx_1[n3->list_index];

            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a3i_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
            
            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a3o_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
        }
        
        if(!n4->gnd_node){
            col = matrix->node_rhs_idx_1[n4->list_index];

            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a4i_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
            for(f=0; f<inter.num_signal_freqs; f++){        
                for(i=0; i<inter.num_fields; i++,col++) {
                    set_element_mech(matrix, &(motion->a4o_x[f][i]), row_idx, &col, &got_nnz, &(motion->nnz_count), mode);
                }
            }
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != motion->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += motion->nnz_count;
    else
        bug_error("unexpected mode value");  
}

/**
 * Gets the matrix elements that are used to compute the feedback signal
 * 
 * @param feedback
 * @param matrix
 * @param pcol_nnz
 * @param mode
 */
void get_feedback_input_elements(slink_t *feedback, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(feedback != NULL);
    assert(matrix != NULL);
    
    int got_nnz = 0;
    
    if(mode == COUNT) {
        assert(pcol_nnz != NULL);
        feedback->nnz_in_count = 0;
    } else {
        assert(matrix->M.values != NULL);
    }
    
    int row_idx = matrix->feedback_rhs_idx[feedback->list_index];
    int col = (int) matrix->output_rhs_idx[feedback->output_list_idx];

    int f,i;
    
    for(f=0; f<inter.num_signal_freqs; f++){
        for(i=0; i<inter.num_fields; i++){
            set_element(matrix, &(feedback->a_d[f][i]), row_idx, &col, &got_nnz, &(feedback->nnz_in_count), 1, 0, 0, mode);
            col++;
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != feedback->nnz_in_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += feedback->nnz_in_count;
    else
        bug_error("unexpected mode value");  
}

/**
 * Given coupling information and input and output modes
 * return whether it should be included or not
 * 
 * @param clpng
 * @param ni
 * @param nj
 * @param mi
 * @param mj
 * @return 
 */
bool include_mode_coupling(coupling_info_t *clpng, int n1, int n2, int m1, int m2){
    // if no coupling set only zeroth order couplings are possible
    if(clpng->coupling_off)
        return n1 == n2 && m1 == m2;
    
    bool xeven = (n1-n2) % 2 == 0;
    bool yeven = (m1-m2) % 2 == 0;

    bool couples;
    if(abs(n1-n2) <= clpng->max_coupling_order
        && abs(m1-m2) <= clpng->max_coupling_order){
         
        couples = ((clpng->has_xeven &&  xeven) ^ (clpng->has_xodd  && !xeven))
                  || ((clpng->has_yeven &&  yeven) ^ (clpng->has_yodd  && !yeven));

    } else 
        couples = false;
    
    return couples;
}
/*
 *  a1 -> | -> a3
 *  a2 <- | <- a4
 * 
 *  |1         |a1|
 *  |X 1    X X|a2|
 *  |X   1  X X|a3|
 *  |       1  |a4|
 *  |X      X 1| x|
 */
void get_mirror_elements(mirror_t *mirror, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(mirror != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT) {
        assert(pcol_nnz != NULL);
        mirror->nnz_count = 0;
    } else
        assert(matrix->M.values != NULL);
    
    int i, j, fin, fout;

    int f_offset = 0;
    
    if(matrix->type==STANDARD) {
        f_offset = 0;
    } else if(matrix->type == SIGNAL_QCORR) {
        f_offset = M_ifo_car.num_frequencies;
    } else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[mirror->node1_index];
    node_t *node2 = &inter.node_list[mirror->node2_index];
    
    int has_optic_dof = mirror->num_motions > 0 && matrix->type == SIGNAL_QCORR;
    
    if(has_optic_dof){
        if(mirror->mass > 0) assert(mirror->long_tf != NULL);
        if(mirror->Ix > 0)   assert(mirror->rot_x_tf != NULL);
        if(mirror->Iy > 0)   assert(mirror->rot_y_tf != NULL);
    }
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int col = matrix->mirror_rhs_idx[mirror->comp_index];
    int mirror_rhs_idx = col; // store this value for referencing
    
    if(!node1->gnd_node) {
        // port a1 
        for(fin=0; fin < matrix->num_frequencies; fin++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                // set the value for the diagonal
                set_diagonal_element(matrix, mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 1, fin, i, mode);
                
                // couplings on side 1 reflection
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    if((mirror->dither_order == 0 && fin==fout) ^ (mirror->dither_order != 0 && matrix->m_f_couple_allocd[mirror->dither_list_index][fin][fout])){
                        if(mirror->a_cplng_11.coupling_off){
                            set_element(matrix, &(mirror->a11f[f_offset+fin][f_offset+fout][i][i]), mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 2, fout, i, mode);
                        } else {
                            for(j=0; j < inter.num_fields; j++){
                                int nj, mj;
                                get_tem_modes_from_field_index(&nj, &mj, j);
                                bool couples = i == j || include_mode_coupling(&mirror->a_cplng_11, ni, nj, mi, mj);
                                
                                if(couples)
                                    set_element(matrix, &(mirror->a11f[f_offset+fin][f_offset+fout][i][j]), mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 2, fout, j, mode);
                            }
                        }
                    }
                }
                
                if(!node2->gnd_node){
                    // couplings from side 1 to side 2
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((mirror->dither_order == 0 && fin==fout) ^ (mirror->dither_order != 0 && matrix->m_f_couple_allocd[mirror->dither_list_index][fin][fout])){
                            if(mirror->a_cplng_12.coupling_off){
                                set_element(matrix, &(mirror->a12f[f_offset+fin][f_offset+fout][i][i]), mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 3, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&mirror->a_cplng_12, ni, nj, mi, mj);
                                    if(couples) set_element(matrix, &(mirror->a12f[f_offset+fin][f_offset+fout][i][j]), mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 3, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                // if the mirror is free to move then include the coupling from optical field to optical motion
                if(has_optic_dof){
                    for(j=0; j < mirror->num_motions; j++){
                        set_element_mech(matrix, &mirror->a1i_x[j][fin][i], mirror->x_rhs_idx+j, &col, &got_nnz, &mirror->nnz_count, mode);
                    }
                }

                col++; // next column in sub-matrix
            }
        }
        
        // port a2 - just the diagonal elements
        for(fin=0; fin<matrix->num_frequencies; fin++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 2, fin, i, mode);
                
                // if the mirror is free to move then include the coupling from optical field to optical motion
                // the output motion is also needed as it carrier momentum away from the mirror
                if(has_optic_dof){
                    for(j=0; j<mirror->num_motions; j++){
                        set_element_mech(matrix, &mirror->a1o_x[j][fin][i], mirror->x_rhs_idx+j, &col, &got_nnz, &mirror->nnz_count, mode);
                    }
                }
                
                col++;
            }
        }
    }
    // Port difference is used in the scenario when node 1 is a dump node and isn't
    // added to the matrix, therefore node 2 is now port 1 and 2
    int portDiff = 0;

    if(node1->gnd_node)
        portDiff = 2;
    
    if(!node2->gnd_node){
        
        // port a3 - output
        for(fin=0; fin<matrix->num_frequencies; fin++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 3-portDiff, fin, i, mode);
                
                if(has_optic_dof){
                    for(j=0; j<mirror->num_motions; j++){
                        set_element_mech(matrix, &mirror->a2o_x[j][fin][i], mirror->x_rhs_idx+j, &col, &got_nnz, &mirror->nnz_count, mode);
                    }
                }
                
                col++;
            }
        }
        
        // port a4 - input
        for(fin=0; fin<matrix->num_frequencies; fin++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // coupling from side 2 to side 1
                if(!node1->gnd_node){
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((mirror->dither_order == 0 && fin==fout) ^ (mirror->dither_order != 0 && matrix->m_f_couple_allocd[mirror->dither_list_index][fin][fout])){
                            if(mirror->a_cplng_21.coupling_off){
                                set_element(matrix, &(mirror->a21f[f_offset+fin][f_offset+fout][i][i]), mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 2, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&mirror->a_cplng_21, ni, nj, mi, mj);
                                    if(couples) set_element(matrix, &(mirror->a21f[f_offset+fin][f_offset+fout][i][j]), mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 2, fout, j, mode);
                                }
                            }
                        }
                    }
                }

                // coupling reflection on side 2
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    if((mirror->dither_order == 0 && fin==fout) ^ (mirror->dither_order != 0 && matrix->m_f_couple_allocd[mirror->dither_list_index][fin][fout])){
                        if(mirror->a_cplng_22.coupling_off){
                            set_element(matrix, &(mirror->a22f[f_offset+fin][f_offset+fout][i][i]), mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 3-portDiff, fout, i, mode);
                        } else {
                            for(j=0; j < inter.num_fields; j++){
                                int nj, mj;
                                get_tem_modes_from_field_index(&nj, &mj, j);
                                bool couples = i == j || include_mode_coupling(&mirror->a_cplng_22, ni, nj, mi, mj);
                                if(couples) set_element(matrix, &(mirror->a22f[f_offset+fin][f_offset+fout][i][j]), mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 3-portDiff, fout, j, mode);
                            }
                        }
                    }
                }
                
                set_diagonal_element(matrix, mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 4-portDiff, fin, i, mode);
                
                // if the mirror is free to move then include the coupling from optical field to optical motion
                if(has_optic_dof){
                    for(j=0; j<mirror->num_motions; j++){
                        set_element_mech(matrix, &mirror->a2i_x[j][fin][i], mirror->x_rhs_idx+j, &col, &got_nnz, &mirror->nnz_count, mode);
                    }
                }
                
                col++; // next column in sub-matrix
            }
        }
    }
    
    // now we have all the optical couplings and the coupling from optical to motion elements assigned we now need to
    // set the elements for motion to optics
    if(has_optic_dof){
        for(j=0; j<mirror->num_motions; j++){
            int fout = 0;

            if(!node1->gnd_node){
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    for(i=0; i < inter.num_fields; i++){
                        set_element(matrix, &mirror->x_a1[j][fout][i], mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 2, fout, i, mode);
                    }
                }
            }

            if(!node2->gnd_node){
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    for(i=0; i < inter.num_fields; i++){
                        set_element(matrix, &mirror->x_a2[j][fout][i], mirror_rhs_idx, &col, &got_nnz, &mirror->nnz_count, 3-portDiff, fout, i, mode);
                    }
                }
            }
            
            // get the elements coupling motions of one component to another
            // if the output is BEFORE this mirror in the RHS vector
            if(mirror->motion_links){
                /*
                motion_link_t *p = NULL;
                int l = 0;
                
                while( (p=(motion_link_t*)utarray_next(mirror->motion_links, p))) {
                    assert(p->from_type == MIRROR);
                    assert(p->from_list_idx == mirror->comp_index);
                    
                    int o_rhs_idx = get_comp_rhs_idx2(&M_ifo_sig, p->to_type, p->to_list_idx);
                    int i_rhs_idx = get_comp_rhs_idx2(&M_ifo_sig, p->from_type, p->from_list_idx);
                    
                    // check if the component that we are outputting to is before or after
                    // the input component
                    if(o_rhs_idx < i_rhs_idx){
                        // compute the row for the output RHS vector index
                        int m_rhs_idx = -1;
                        
                        if(p->to_type == MIRROR)
                            m_rhs_idx = inter.mirror_list[p->to_list_idx].x_rhs_idx + p->to_motion;
                        else
                            bug_error("unhandled condition");
                        
                        set_element_mech(matrix, &p->ix_ox, m_rhs_idx, &col, &got_nnz, &mirror->nnz_count, mode);
                    }
                    
                    l++;
                }
                */
            }
            
            // allocate memory elements for coupling between various motions at this component
            for(i=0; i<mirror->num_motions; i++){
                if(i==j)
                    set_diagonal_element_mech(matrix, mirror->x_rhs_idx+j, &col, &got_nnz, &mirror->nnz_count, mode);
                else
                    set_element_mech(matrix, &mirror->x_x[j][i], mirror->x_rhs_idx + i, &col, &got_nnz, &mirror->nnz_count, mode);
            }
            
            // get the elements coupling motions of one component to another
            // if the output is AFTER this mirror in the RHS vector
            if(mirror->motion_links){
                /*
                motion_link_t *p = NULL;
                int l = 0;
                
                while( (p=(motion_link_t*)utarray_next(mirror->motion_links, p))) {
                    assert(p->from_type == MIRROR);
                    assert(p->from_list_idx == mirror->comp_index);
                    
                    int o_rhs_idx = get_comp_rhs_idx2(&M_ifo_sig, p->to_type, p->to_list_idx);
                    int i_rhs_idx = get_comp_rhs_idx2(&M_ifo_sig, p->from_type, p->from_list_idx);
                    
                    // check if the component that we are outputting to is after
                    // the input component
                    if(o_rhs_idx > i_rhs_idx){
                        // compute the row for the output RHS vector index
                        int m_rhs_idx = -1;
                        
                        if(p->to_type == MIRROR)
                            m_rhs_idx = inter.mirror_list[p->to_list_idx].x_rhs_idx + p->to_motion;
                        else
                            bug_error("unhandled condition");
                        
                        set_element_mech(matrix, &p->ix_ox, m_rhs_idx, &col, &got_nnz, &mirror->nnz_count, mode);
                    }
                    
                    l++;
                }
                */
            }
            
            col++;
        }
    }
    
    
    if(mode == GETELEMENT) {
        if(got_nnz != mirror->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += mirror->nnz_count;
    else
        bug_error("unexpected mode value");   
}

void get_grating_elements(grating_t *grating, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(grating != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
        grating->nnz_count = 0;
    }else
        assert(matrix->M.values != NULL);
    
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[grating->node1_index];
    node_t *node2 = &inter.node_list[grating->node2_index];
    node_t *node3 = (grating->num_of_ports >= 3) ? &inter.node_list[grating->node3_index] : NULL;
    node_t *node4 = (grating->num_of_ports == 4) ? &inter.node_list[grating->node4_index] : NULL;
    
    int n1_port=1, n2_port=3, n3_port=5, n4_port=7;
    
    if(node1->gnd_node){
       n2_port -= 2; 
       n3_port -= 2; 
       n4_port -= 2; 
    }
    
    if(node2->gnd_node){
       n3_port -= 2; 
       n4_port -= 2; 
    }

    if(node3 != NULL && node3->gnd_node){
       n4_port -= 2; 
    }     
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int grating_rhs_idx = matrix->grating_rhs_idx[grating->comp_index];
    int col = grating_rhs_idx;
    int fin, i, j;
    
    for(fin=0; fin <matrix->num_frequencies; fin++){
     
        switch(grating->num_of_ports){
            case 2:
                
                if(!node1->gnd_node){
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port, fin, i, mode);

                        for(j=0; j < inter.num_fields; j++){
                            set_element(matrix, &(grating->a11f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, j, mode);
                        }
                        
                        if(!node2->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a12f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, j, mode);
                            }
                        }
                    
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, i, mode);
                        col++;
                    }
                }
        
                if(!node2->gnd_node){
                    for(i=0; i < inter.num_fields; i++){

                        if(!node1->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a21f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, j, mode);
                            }
                        }
                    
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port, fin, i, mode);
                        
                        for(j=0; j < inter.num_fields; j++){
                            set_element(matrix, &(grating->a22f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, j, mode);
                        }
                        
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, i, mode);
                        col++;
                    }
                }
    
                break;
            case 3:
                assert(node3 != NULL);
                
                if(!node1->gnd_node){
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port, fin, i, mode);

                        for(j=0; j < inter.num_fields; j++){
                            set_element(matrix, &(grating->a11f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, j, mode);
                        }
                        
                        if(!node2->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a12f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, j, mode);
                            }
                        }
                    
                        if(!node3->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a13f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port+1, fin, j, mode);
                            }
                        }
                
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, i, mode);
                        col++;
                    }
                }
                
                if(!node2->gnd_node){
                    for(i=0; i < inter.num_fields; i++){

                        if(!node1->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a21f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, j, mode);
                            }
                        }
                    
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port, fin, i, mode);
                        
                        for(j=0; j < inter.num_fields; j++){
                            set_element(matrix, &(grating->a22f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, j, mode);
                        }
                        
                        if(!node3->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a23f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port+1, fin, j, mode);
                            }
                        }
                        
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, i, mode);
                        col++;
                    }
                }
        
                if(!node3->gnd_node){
                    for(i=0; i < inter.num_fields; i++){

                        if(!node1->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a31f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, j, mode);
                            }
                        }
                    
                        if(!node2->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a32f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, j, mode);
                            }
                        }
                        
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port, fin, i, mode);
                        
                        for(j=0; j < inter.num_fields; j++){
                            set_element(matrix, &(grating->a33f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port+1, fin, j, mode);
                        }
                        
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port+1, fin, i, mode);
                        col++;
                    }
                }
                
                break;
            case 4:
                if(!node1->gnd_node){
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port, fin, i, mode);
                        
                        if(!node2->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a12f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, j, mode);
                            }
                        }
                    
                        if(!node3->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a13f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port+1, fin, j, mode);
                            }
                        }
                
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, i, mode);
                        col++;
                    }
                }
    
                if(!node2->gnd_node){
                    for(i=0; i < inter.num_fields; i++){

                        if(!node1->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a21f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, j, mode);
                            }
                        }
                    
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port, fin, i, mode);
                                                
                        if(!node4->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a24f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n4_port+1, fin, j, mode);
                            }
                        }
                        
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, i, mode);
                        col++;
                    }
                }
    
                if(!node3->gnd_node){
                    for(i=0; i < inter.num_fields; i++){

                        if(!node1->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a31f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n1_port+1, fin, j, mode);
                            }
                        }
                                            
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port, fin, i, mode);
                        
                        if(!node4->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a34f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n4_port+1, fin, j, mode);
                            }
                        }
                        
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port+1, fin, i, mode);
                        col++;
                    }
                }
    
                if(!node4->gnd_node){
                    for(i=0; i < inter.num_fields; i++){

                        if(!node2->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a42f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n2_port+1, fin, j, mode);
                            }
                        }
                        
                        if(!node3->gnd_node){
                            for(j=0; j < inter.num_fields; j++){
                                set_element(matrix, &(grating->a43f[f_offset+fin][i][j]), grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n3_port+1, fin, j, mode);
                            }
                        }
                        
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n4_port, fin, i, mode);
                        
                        col++;
                    }
                    
                    for(i=0; i < inter.num_fields; i++){
                        set_diagonal_element(matrix, grating_rhs_idx, &col, &got_nnz, &grating->nnz_count, n4_port+1, fin, i, mode);
                        col++;
                    }
                }
                break;
            default:
                bug_error("not handled");
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != grating->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += grating->nnz_count;
    else
        bug_error("unexpected mode value");  
}

void get_beamsplitter_elements(beamsplitter_t *bs, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(bs != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
        bs->nnz_count = 0;
    }else
        assert(matrix->M.values != NULL);
    
    int i, j, fin, fout;    
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[bs->node1_index];
    node_t *node2 = &inter.node_list[bs->node2_index];
    node_t *node3 = &inter.node_list[bs->node3_index];
    node_t *node4 = &inter.node_list[bs->node4_index];
    
    int n1_port=1, n2_port=3, n3_port=5, n4_port=7;
    
    if(node1->gnd_node){
       n2_port -= 2; 
       n3_port -= 2; 
       n4_port -= 2; 
    }
    
    if(node2->gnd_node){
       n3_port -= 2; 
       n4_port -= 2; 
    }

    if(node3->gnd_node){
       n4_port -= 2; 
    }        
    
    int has_optic_dof = bs->num_motions > 0 && matrix->type == SIGNAL_QCORR;
    
    if(has_optic_dof){
        if(bs->mass > 0) assert(bs->long_tf != NULL);
        if(bs->Ix > 0)   assert(bs->rot_x_tf != NULL);
        if(bs->Iy > 0)   assert(bs->rot_y_tf != NULL);
    }
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int bs_rhs_idx = matrix->bs_rhs_idx[bs->comp_index];
    int col = bs_rhs_idx;
    
    bool isBackscattering = is_var_tuned(&bs->backscatter) || bs->backscatter > 0;
    
    if(!node1->gnd_node) {
        // port a1 
        for(fin=0; fin <matrix->num_frequencies; fin++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // set the value for the diagonal
                set_diagonal_element(matrix, bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port, fin, i, mode);

                // Back scatter 1->1
                if(isBackscattering) {
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order !=0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_11.coupling_off){
                                set_element(matrix, &(bs->a11f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_11, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a11f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                // couplings from a1 -> a4
                if(!node2->gnd_node) {
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order !=0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_12.coupling_off){
                                set_element(matrix, &(bs->a12f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_12, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a12f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                // couplings from a1 -> a3
                if(!node3->gnd_node){
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order != 0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_13.coupling_off){
                                set_element(matrix, &(bs->a13f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_13, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a13f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                // if the mirror is free to move then include the coupling from optical field to optical motion
                if(has_optic_dof){
                    for(j=0; j < bs->num_motions; j++){
                        set_element_mech(matrix, &bs->a1i_x[j][fin][i], bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                    }
                }
                col++; // next column in sub-matrix
            }
        }
        
        // port a2 - just the diagonal elements
        for(fin=0; fin<matrix->num_frequencies; fin++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port+1, fin, i, mode);
                
                if(has_optic_dof){
                    for(j=0; j<bs->num_motions; j++)
                        set_element_mech(matrix, &bs->a1o_x[j][fin][i], bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                }
                
                col++;
            }
        }
    }
    
    if(!node2->gnd_node) {
        // port a1 
        for(fin=0; fin <matrix->num_frequencies; fin++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                if(!node1->gnd_node){
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order != 0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_21.coupling_off){
                                set_element(matrix, &(bs->a21f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_21, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a21f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                set_diagonal_element(matrix, bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port, fin, i, mode);
                
                // Back scatter 2->2
                if(isBackscattering) {
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order !=0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_22.coupling_off){
                                set_element(matrix, &(bs->a22f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_22, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a22f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                if(!node4->gnd_node){
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order != 0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_24.coupling_off){
                                set_element(matrix, &(bs->a24f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_24, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a24f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }

                if(has_optic_dof){
                    for(j=0; j<bs->num_motions; j++)
                        set_element_mech(matrix, &bs->a2i_x[j][fin][i], bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        for(fin=0; fin<matrix->num_frequencies; fin++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port+1, fin, i, mode);
                
                if(has_optic_dof){
                    for(j=0; j<bs->num_motions; j++)
                        set_element_mech(matrix, &bs->a2o_x[j][fin][i], bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                }
                
                col++;
            }
        }
    }
    
    if(!node3->gnd_node) {
        // port a1 
        for(fin=0; fin <matrix->num_frequencies; fin++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                if(!node1->gnd_node){
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order != 0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_31.coupling_off){
                                set_element(matrix, &(bs->a31f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_31, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a31f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                set_diagonal_element(matrix, bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port, fin, i, mode);
                
                // Back scatter 3->3
                if(isBackscattering) {
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order !=0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_33.coupling_off){
                                set_element(matrix, &(bs->a33f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_33, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a33f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                if(!node4->gnd_node){
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order != 0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_34.coupling_off){
                                set_element(matrix, &(bs->a34f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_34, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a34f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }

                if(has_optic_dof){
                    for(j=0; j<bs->num_motions; j++)
                        set_element_mech(matrix, &bs->a3i_x[j][fin][i], bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        for(fin=0; fin<matrix->num_frequencies; fin++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port+1, fin, i, mode);
                
                if(has_optic_dof){
                    for(j=0; j<bs->num_motions; j++)
                        set_element_mech(matrix, &bs->a3o_x[j][fin][i], bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                }
                
                col++;
            }
        }
    }
    
    if(!node4->gnd_node) {
        // port a1 
        for(fin=0; fin <matrix->num_frequencies; fin++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // couplings from a1 -> a4
                if(!node2->gnd_node){
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order != 0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_42.coupling_off){
                                set_element(matrix, &(bs->a42f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_42, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a42f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                // couplings from a1 -> a3
                if(!node3->gnd_node){
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order != 0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_43.coupling_off){
                                set_element(matrix, &(bs->a43f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_43, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a43f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }

                // set the value for the diagonal
                set_diagonal_element(matrix, bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port, fin, i, mode);
                
                // Back scatter 4->4
                if(isBackscattering) {
                    for(fout=0; fout < matrix->num_frequencies; fout++){
                        if((bs->dither_order == 0 && fin==fout) ^ (bs->dither_order !=0 && matrix->bs_f_couple_allocd[bs->dither_list_index][fin][fout])){
                            if(bs->a_cplng_44.coupling_off){
                                set_element(matrix, &(bs->a44f[f_offset+fin][f_offset+fout][i][i]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port+1, fout, i, mode);
                            } else {
                                for(j=0; j < inter.num_fields; j++){
                                    int nj, mj;
                                    get_tem_modes_from_field_index(&nj, &mj, j);
                                    bool couples = i == j || include_mode_coupling(&bs->a_cplng_44, ni, nj, mi, mj);
                                    if(couples)
                                        set_element(matrix, &(bs->a44f[f_offset+fin][f_offset+fout][i][j]), bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port+1, fout, j, mode);
                                }
                            }
                        }
                    }
                }
                
                if(has_optic_dof){
                    for(j=0; j<bs->num_motions; j++)
                        set_element_mech(matrix, &bs->a4i_x[j][fin][i], bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        // port a2 - just the diagonal elements
        for(fin=0; fin<matrix->num_frequencies; fin++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port+1, fin, i, mode);
                
                if(has_optic_dof){
                    for(j=0; j<bs->num_motions; j++)
                        set_element_mech(matrix, &bs->a4o_x[j][fin][i], bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                }
                
                col++;
            }
        }
    }
    
    if(has_optic_dof){
        int fout = 0;
        for(j=0; j<bs->num_motions; j++) {
            if(!node1->gnd_node){
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    for(i=0; i < inter.num_fields; i++){
                        set_element(matrix, &bs->x_a1[j][fout][i], bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n1_port+1, fout, i, mode);
                    }
                }
            }

            if(!node2->gnd_node){
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    for(i=0; i < inter.num_fields; i++){
                        set_element(matrix, &bs->x_a2[j][fout][i], bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n2_port+1, fout, i, mode);
                    }
                }
            }

            if(!node3->gnd_node){
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    for(i=0; i < inter.num_fields; i++){
                        set_element(matrix, &bs->x_a3[j][fout][i], bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n3_port+1, fout, i, mode);
                    }
                }
            }

            if(!node4->gnd_node){
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    for(i=0; i < inter.num_fields; i++){
                        set_element(matrix, &bs->x_a4[j][fout][i], bs_rhs_idx, &col, &got_nnz, &bs->nnz_count, n4_port+1, fout, i, mode);
                    }
                }
            }
            
            // get the elements coupling motions of one component to another
            // if the output is BEFORE this mirror in the RHS vector
            if(bs->motion_links){
                /*
                motion_link_t *p = NULL;
                int l = 0;
                
                while( (p=(motion_link_t*)utarray_next(bs->motion_links, p))) {
                    assert(p->from_type == BEAMSPLITTER);
                    assert(p->from_list_idx == bs->comp_index);
                    
                    int o_rhs_idx = get_comp_rhs_idx2(&M_ifo_sig, p->to_type, p->to_list_idx);
                    int i_rhs_idx = get_comp_rhs_idx2(&M_ifo_sig, p->from_type, p->from_list_idx);
                    
                    // check if the component that we are outputting to is before or after
                    // the input component
                    if(o_rhs_idx < i_rhs_idx){
                        // compute the row for the output RHS vector index
                        int m_rhs_idx = -1;
                        
                        if(p->to_type == BEAMSPLITTER)
                            m_rhs_idx = inter.bs_list[p->to_list_idx].x_rhs_idx + p->to_motion;
                        else
                            bug_error("unhandled condition");
                        
                        set_element_mech(matrix, &p->ix_ox, m_rhs_idx, &col, &got_nnz, &bs->nnz_count, mode);
                    }
                    
                    l++;
                }
                */
            }
            
            // allocate memory elements for coupling between various motions at this component
            for(i=0; i<bs->num_motions; i++){
                if(i==j)
                    set_diagonal_element_mech(matrix, bs->x_rhs_idx+j, &col, &got_nnz, &bs->nnz_count, mode);
                else
                    set_element_mech(matrix, &bs->x_x[j][i], bs->x_rhs_idx + i, &col, &got_nnz, &bs->nnz_count, mode);
            }
            
            // get the elements coupling motions of one component to another
            // if the output is AFTER this mirror in the RHS vector
            if(bs->motion_links){
                /*
                motion_link_t *p = NULL;
                int l = 0;
                
                while( (p=(motion_link_t*)utarray_next(bs->motion_links, p))) {
                    assert(p->from_type == BEAMSPLITTER);
                    assert(p->from_list_idx == bs->comp_index);
                    
                    int o_rhs_idx = get_comp_rhs_idx2(&M_ifo_sig, p->to_type, p->to_list_idx);
                    int i_rhs_idx = get_comp_rhs_idx2(&M_ifo_sig, p->from_type, p->from_list_idx);
                    
                    // check if the component that we are outputting to is after
                    // the input component
                    if(o_rhs_idx > i_rhs_idx){
                        // compute the row for the output RHS vector index
                        int m_rhs_idx = -1;
                        
                        if(p->to_type == BEAMSPLITTER)
                            m_rhs_idx = inter.bs_list[p->to_list_idx].x_rhs_idx + p->to_motion;
                        else
                            bug_error("unhandled condition");
                        
                        set_element_mech(matrix, &p->ix_ox, m_rhs_idx, &col, &got_nnz, &bs->nnz_count, mode);
                    }
                    
                    l++;
                }
                */
            }
            
            col++;
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != bs->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += bs->nnz_count;
    else
        bug_error("unexpected mode value");   
}

void get_dbs_elements(dbs_t *dbs, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(dbs != NULL);
    assert(matrix != NULL); 
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
        dbs->nnz_count = 0;
    } else {
        assert(matrix->M.values != NULL);
    }
    
    int i, j, f;
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[dbs->node1_index];
    node_t *node2 = &inter.node_list[dbs->node2_index];
    node_t *node3 = &inter.node_list[dbs->node3_index];
    node_t *node4 = &inter.node_list[dbs->node4_index];
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int dbs_rhs_idx = matrix->dbs_rhs_idx[dbs->comp_index];
    int col = dbs_rhs_idx;
    
    int n1_port = 1;
    
    int n2_port = 3;
    if(node1->gnd_node) n2_port -= 2;
    
    int n3_port = 5;
    if(node1->gnd_node) n3_port -= 2;
    if(node2->gnd_node) n3_port -= 2;
    
    int n4_port = 7;
    if(node1->gnd_node) n4_port -= 2;
    if(node2->gnd_node) n4_port -= 2;
    if(node3->gnd_node) n4_port -= 2;
    
    
    if(!node1->gnd_node) {
        for(f=0; f <matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // set the value for the diagonal
                set_diagonal_element(matrix, dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n1_port, f, i, mode);
                
                if(!node2->gnd_node){
                    if(dbs->a_cplng_13.coupling_off){
                        set_element(matrix, &(dbs->a13f[f_offset+f][i][i]), dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n2_port+1, f, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&dbs->a_cplng_13, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(dbs->a13f[f_offset+f][i][j]),dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n2_port+1, f, j, mode);
                        }
                    }
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n1_port+1, f, i, mode);
                col++;
            }
        }
    }
    
    
    
    
    if(!node2->gnd_node) {
        for(f=0; f <matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // set the value for the diagonal
                set_diagonal_element(matrix, dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n2_port, f, i, mode);
                
                if(!node4->gnd_node){
                    if(dbs->a_cplng_21.coupling_off){
                        set_element(matrix, &(dbs->a21f[f_offset+f][i][i]), dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n4_port+1, f, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&dbs->a_cplng_21, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(dbs->a21f[f_offset+f][i][j]), dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n4_port+1, f, j, mode);
                        }
                    }
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n2_port+1, f, i, mode);
                col++;
            }
        }
    }
    
    
    
    
    if(!node3->gnd_node) {
        for(f=0; f <matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                
                if(!node1->gnd_node){
                    if(dbs->a_cplng_34.coupling_off){
                        set_element(matrix, &(dbs->a34f[f_offset+f][i][i]), dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n1_port+1, f, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&dbs->a_cplng_34, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(dbs->a34f[f_offset+f][i][j]), dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n1_port+1, f, j, mode);
                        }
                    }
                }
                
                // set the value for the diagonal
                set_diagonal_element(matrix, dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n3_port, f, i, mode);
                
                col++; // next column in sub-matrix
            }
        }
        
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n3_port+1, f, i, mode);
                col++;
            }
        }
    }
    
    
    
    
    
    if(!node4->gnd_node) {
        for(f=0; f <matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                
                if(!node1->gnd_node){
                    if(dbs->a_cplng_42.coupling_off){
                        set_element(matrix, &(dbs->a42f[f_offset+f][i][i]), dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n3_port+1, f, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&dbs->a_cplng_42, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(dbs->a42f[f_offset+f][i][j]), dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n3_port+1, f, j, mode);
                        }
                    }
                }
                
                // set the value for the diagonal
                set_diagonal_element(matrix, dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n4_port, f, i, mode);
                
                col++; // next column in sub-matrix
            }
        }
        
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, dbs_rhs_idx, &col, &got_nnz, &dbs->nnz_count, n4_port+1, f, i, mode);
                col++;
            }
        }
    }
    
    

    
    if(mode == GETELEMENT) {
        if(got_nnz != dbs->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += dbs->nnz_count;
    else
        bug_error("unexpected mode value");
}

void get_diode_elements(diode_t *diode, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(diode != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
        diode->nnz_count = 0;
    }else
        assert(matrix->M.values != NULL);
    
    int i, j, f;
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[diode->node1_index];
    node_t *node2 = &inter.node_list[diode->node2_index];
    node_t *node3 = &inter.node_list[diode->node3_index];
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int diode_rhs_idx = matrix->diode_rhs_idx[diode->comp_index];
    int col = diode_rhs_idx;
    
    int n1_port = 1;
    
    int n2_port = 3;
    if(node1->gnd_node) n2_port -= 2;
    
    int n3_port = 5;
    if(node1->gnd_node) n3_port -= 2;
    if(node2->gnd_node) n3_port -= 2;
    
    if(!node1->gnd_node) {
        for(f=0; f <matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // set the value for the diagonal
                set_diagonal_element(matrix, diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n1_port, f, i, mode);
                
                if(!node2->gnd_node){
                    if(diode->a_cplng_12.coupling_off){
                        set_element(matrix, &(diode->a12f[f_offset+f][i][i]), diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n2_port, f, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&diode->a_cplng_12, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(diode->a12f[f_offset+f][i][j]), diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n2_port, f, j, mode);
                        }
                    }
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n1_port+1, f, i, mode);
                col++;
            }
        }
    }

    if(!node2->gnd_node){
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n2_port, f, i, mode);
                col++;
            }
        }
        
        for(f=0; f< matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                if(!node1->gnd_node){
                    for(j=0; j < inter.num_fields; j++){
                        set_element(matrix, &(diode->a21f[f_offset+f][i][j]), diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n1_port+1, f, j, mode);
                    }
                }
                
                set_diagonal_element(matrix, diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n2_port+1, f, i, mode);
                
                if(!node3->gnd_node){
                    if(diode->a_cplng_23.coupling_off){
                        set_element(matrix, &(diode->a23f[f_offset+f][i][i]), diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n3_port+1, f, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&diode->a_cplng_23, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(diode->a23f[f_offset+f][i][j]), diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n3_port+1, f, j, mode);
                        }
                    }
                }
                
                col++; // next column in sub-matrix
            }
        }
    }
    
    if(!node3->gnd_node){
        for(f=0; f< matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                if(!node2->gnd_node){
                    if(diode->a_cplng_32.coupling_off){
                        set_element(matrix, &(diode->a32f[f_offset+f][i][i]), diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n2_port, f, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&diode->a_cplng_32, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(diode->a32f[f_offset+f][i][j]), diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n2_port, f, j, mode);
                        }
                    }
                }
                
                set_diagonal_element(matrix, diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n3_port, f, i, mode);
                
                col++; // next column in sub-matrix
            }
        }
        
        
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, diode_rhs_idx, &col, &got_nnz, &diode->nnz_count, n3_port+1, f, i, mode);
                col++;
            }
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != diode->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += diode->nnz_count;
    else
        bug_error("unexpected mode value");   
}

void get_lens_elements(lens_t *lens, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(lens != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
        lens->nnz_count = 0;
    }else
        assert(matrix->M.values != NULL);
    
    int i, j, f;
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[lens->node1_index];
    node_t *node2 = &inter.node_list[lens->node2_index];
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int lens_rhs_idx = matrix->lens_rhs_idx[lens->comp_index];
    int col = lens_rhs_idx;
    
    if(!node1->gnd_node) {
        // port a1 
        for(f=0; f < matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // set the value for the diagonal
                set_diagonal_element(matrix, lens_rhs_idx, &col, &got_nnz, &lens->nnz_count, 1, f, i, mode);
                
                // couplings from a1 -> a3
                if(lens->a_cplng_12.coupling_off){
                    set_element(matrix, &(lens->a12f[f_offset+f][i][i]), lens_rhs_idx, &col, &got_nnz, &lens->nnz_count, 3, f, i, mode);
                } else {
                    for(j=0; j < inter.num_fields; j++){
                        int nj, mj;
                        get_tem_modes_from_field_index(&nj, &mj, j);
                        bool couples = i == j || include_mode_coupling(&lens->a_cplng_12, ni, nj, mi, mj);
                        if(couples) 
                            set_element(matrix, &(lens->a12f[f_offset+f][i][j]), lens_rhs_idx, &col, &got_nnz, &lens->nnz_count, 3, f, j, mode);
                    }
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        // port a2 - just the diagonal elements
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, lens_rhs_idx, &col, &got_nnz, &lens->nnz_count, 2, f, i, mode);
                col++;
            }
        }
    }

    if(!node2->gnd_node){
        // Port difference is used in the scenario when node 1 is a dump node and isn't
        // added to the matrix, therefore node 2 is now port 1 and 2
        int portDiff = 0;
        
        if(node1->gnd_node)
            portDiff = 2;
        
        // port a3
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, lens_rhs_idx, &col, &got_nnz, &lens->nnz_count, 3-portDiff, f, i, mode);
                col++;
            }
        }
        
        // port a4 
        for(f=0; f< matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // couplings from a4 -> a2
                if(lens->a_cplng_21.coupling_off){
                    set_element(matrix, &(lens->a21f[f_offset+f][i][i]), lens_rhs_idx, &col, &got_nnz, &lens->nnz_count, 2, f, i, mode);
                } else {
                    for(j=0; j < inter.num_fields; j++){
                        int nj, mj;
                        get_tem_modes_from_field_index(&nj, &mj, j);
                        bool couples = i == j || include_mode_coupling(&lens->a_cplng_21, ni, nj, mi, mj);
                        if(couples) 
                            set_element(matrix, &(lens->a21f[f_offset+f][i][j]), lens_rhs_idx, &col, &got_nnz, &lens->nnz_count, 2, f, j, mode);
                    }
                }
                
                set_diagonal_element(matrix, lens_rhs_idx, &col, &got_nnz, &lens->nnz_count, 4-portDiff, f, i, mode);
                
                col++; // next column in sub-matrix
            }
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != lens->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += lens->nnz_count;
    else
        bug_error("unexpected mode value");   
}

void get_block_elements(block_t *block, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(block != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
        block->nnz_count = 0;
    }else
        assert(matrix->M.values != NULL);
    
    int i, j, f;
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[block->node1_index];
    node_t *node2 = &inter.node_list[block->node2_index];
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int block_rhs_idx = matrix->block_rhs_idx[block->comp_index];
    int col = block_rhs_idx;
    
    if(!node1->gnd_node) {
        // port a1 
        for(f=0; f < matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                // set the value for the diagonal
                set_diagonal_element(matrix, block_rhs_idx, &col, &got_nnz, &block->nnz_count, 1, f, i, mode);
                
                // couplings from a1 -> a3
                for(j=0; j < inter.num_fields; j++){
                    set_element(matrix, &(block->a12f[f_offset+f][i][j]), block_rhs_idx, &col, &got_nnz, &block->nnz_count, 3, f, j, mode);
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        // port a2 - just the diagonal elements
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, block_rhs_idx, &col, &got_nnz, &block->nnz_count, 2, f, i, mode);
                col++;
            }
        }
    }

    if(!node2->gnd_node){
        // Port difference is used in the scenario when node 1 is a dump node and isn't
        // added to the matrix, therefore node 2 is now port 1 and 2
        int portDiff = 0;
        
        if(node1->gnd_node)
            portDiff = 2;
        
        // port a3
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, block_rhs_idx, &col, &got_nnz, &block->nnz_count, 3-portDiff, f, i, mode);
                col++;
            }
        }
        
        // port a4 
        for(f=0; f< matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                // couplings from a4 -> a2
                for(j=0; j < inter.num_fields; j++){
                    set_element(matrix, &(block->a21f[f_offset+f][i][j]), block_rhs_idx, &col, &got_nnz, &block->nnz_count, 2, f, j, mode);
                }
                
                set_diagonal_element(matrix, block_rhs_idx, &col, &got_nnz, &block->nnz_count, 4-portDiff, f, i, mode);
                
                col++; // next column in sub-matrix
            }
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != block->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += block->nnz_count;
    else
        bug_error("unexpected mode value");
}

void get_sagnac_elements(sagnac_t *sagnac, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(sagnac != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
        sagnac->nnz_count = 0;
    }else
        assert(matrix->M.values != NULL);
    
    int i, j, f;
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[sagnac->node1_index];
    node_t *node2 = &inter.node_list[sagnac->node2_index];
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int sagnac_rhs_idx = matrix->sagnac_rhs_idx[sagnac->comp_index];
    int col = sagnac_rhs_idx;
    
    if(!node1->gnd_node) {
        // port a1 
        for(f=0; f < matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                // set the value for the diagonal
                set_diagonal_element(matrix, sagnac_rhs_idx, &col, &got_nnz, &sagnac->nnz_count, 1, f, i, mode);
                
                // couplings from a1 -> a3
                for(j=0; j < inter.num_fields; j++){
                    set_element(matrix, &(sagnac->a12f[f_offset+f][i][j]), sagnac_rhs_idx, &col, &got_nnz, &sagnac->nnz_count, 3, f, j, mode);
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        // port a2 - just the diagonal elements
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, sagnac_rhs_idx, &col, &got_nnz, &sagnac->nnz_count, 2, f, i, mode);
                col++;
            }
        }
    }

    if(!node2->gnd_node){
        // Port difference is used in the scenario when node 1 is a dump node and isn't
        // added to the matrix, therefore node 2 is now port 1 and 2
        int portDiff = 0;
        
        if(node1->gnd_node)
            portDiff = 2;
        
        // port a3
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, sagnac_rhs_idx, &col, &got_nnz, &sagnac->nnz_count, 3-portDiff, f, i, mode);
                col++;
            }
        }
        
        // port a4 
        for(f=0; f< matrix->num_frequencies; f++){
            for(i=0; i < inter.num_fields; i++){
                // couplings from a4 -> a2
                for(j=0; j < inter.num_fields; j++){
                    set_element(matrix, &(sagnac->a21f[f_offset+f][i][j]), sagnac_rhs_idx, &col, &got_nnz, &sagnac->nnz_count, 2, f, j, mode);
                }
                
                set_diagonal_element(matrix, sagnac_rhs_idx, &col, &got_nnz, &sagnac->nnz_count, 4-portDiff, f, i, mode);
                
                col++; // next column in sub-matrix
            }
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != sagnac->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += sagnac->nnz_count;
    else
        bug_error("unexpected mode value");   
}

void get_modulator_elements(modulator_t *mod, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(mod != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT) {
        assert(pcol_nnz != NULL);
        mod->nnz_count = 0;
    } else
        assert(matrix->M.values != NULL);
    
    int i, j, f, fin, fout;
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    node_t *node1 = &inter.node_list[mod->node1_index];
    node_t *node2 = &inter.node_list[mod->node2_index];
    
    int got_nnz = 0; //only used when mode = GETELEMENT, just compares the got to the counted nnz
    int mod_rhs_idx = matrix->modulator_rhs_idx[mod->comp_index];
    int col = mod_rhs_idx;
    
    if(!node1->gnd_node) {
        // port a1 
        for(fin=0; fin < matrix->num_frequencies; fin++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                // set the value for the diagonal
                set_diagonal_element(matrix, mod_rhs_idx, &col, &got_nnz, &mod->nnz_count, 1, fin, i, mode);
                
                // couplings from a1 -> a3
                for(fout=0; fout < matrix->num_frequencies; fout++){
                    if(matrix->mod_f_couple_allocd[mod->comp_index][fin][fout]){
                        if(mod->a_cplng_12.coupling_off){
                            set_element(matrix, &(mod->a12f[f_offset+fin][f_offset+fout][i][i]), mod_rhs_idx, &col, &got_nnz, &mod->nnz_count, 3, fout, i, mode);
                        } else {
                            for(j=0; j < inter.num_fields; j++){
                                int nj, mj;
                                get_tem_modes_from_field_index(&nj, &mj, j);
                                bool couples = include_mode_coupling(&mod->a_cplng_12, ni, nj, mi, mj);
                                if(couples)
                                    set_element(matrix, &(mod->a12f[f_offset+fin][f_offset+fout][i][j]), mod_rhs_idx, &col, &got_nnz, &mod->nnz_count, 3, fout, j, mode);
                            }
                        }
                    }
                }
                
                col++; // next column in sub-matrix
            }
        }
        
        // port a2 - just the diagonal elements
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, mod_rhs_idx, &col, &got_nnz, &mod->nnz_count, 2, f, i, mode);
                col++;
            }
        }
    }

    if(!node2->gnd_node){
        // Port difference is used in the scenario when node 1 is a dump node and isn't
        // added to the matrix, therefore node 2 is now port 1 and 2
        int portDiff = 0;
        
        if(node1->gnd_node)
            portDiff = 2;
        
        // port a3
        for(f=0; f<matrix->num_frequencies; f++){
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, mod_rhs_idx, &col, &got_nnz, &mod->nnz_count, 3-portDiff, f, i, mode);
                col++;
            }
        }
        
        // port a4 
        for(fin=0; fin< matrix->num_frequencies; fin++){
            for(i=0; i < inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                // couplings from a4 -> a2
                for(fout=0; fout<matrix->num_frequencies; fout++){
                    if(matrix->mod_f_couple_allocd[mod->comp_index][fin][fout]){
                        if(mod->a_cplng_21.coupling_off){
                            set_element(matrix, &(mod->a21f[f_offset+fin][f_offset+fout][i][i]), mod_rhs_idx, &col, &got_nnz, &mod->nnz_count, 2, fout, i, mode);
                        } else {
                            for(j=0; j < inter.num_fields; j++){
                                int nj, mj;
                                get_tem_modes_from_field_index(&nj, &mj, j);
                                bool couples = i == j || include_mode_coupling(&mod->a_cplng_21, ni, nj, mi, mj);
                                if(couples)
                                    set_element(matrix, &(mod->a21f[f_offset+fin][f_offset+fout][i][j]), mod_rhs_idx, &col, &got_nnz, &mod->nnz_count, 2, fout, j, mode);
                            }
                        }
                    }
                }

                set_diagonal_element(matrix, mod_rhs_idx, &col, &got_nnz, &mod->nnz_count, 4-portDiff, fin, i, mode);
                
                col++; // next column in sub-matrix
            }
        }
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != mod->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += mod->nnz_count;
    else
        bug_error("unexpected mode value");   
}

void get_space_elements(space_t *space, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode) {
    assert(mode == GETELEMENT || mode == COUNT);
    assert(space != NULL);
    assert(matrix != NULL);
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
        space->nnz_count = 0;
    }else
        assert(matrix->M.values != NULL);
    
    int i, j, fin;
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    int space_rhs_idx = matrix->space_rhs_idx[space->list_index];
    int col=space_rhs_idx;

    node_t *node1 = &inter.node_list[space->node1_index];
    node_connections_t *nc1 = &inter.node_conn_list[space->node1_index];
    node_t *node2 = &inter.node_list[space->node2_index];
    node_connections_t *nc2 = &inter.node_conn_list[space->node2_index];
    
    int got_nnz = 0;
    
    // The space element just has the diagonals in the submatrix, as it doesn't
    // couple anything between the ports
    if(node1->gnd_node && node2->gnd_node)
        bug_error("Both nodes are dump nodes for %s", space->name);
    
    if(nc1->comp_1 == NULL && nc1->comp_2 == NULL)
        bug_error("Node %s has no connections specified in node_connection_list", node1->name);
    
    if(nc2->comp_1 == NULL && nc2->comp_2 == NULL)
        bug_error("Node %s has no connections specified in node_connection_list", node2->name);
    
    int n1_loose_node = node1->connect && (nc1->comp_1==NULL)^(nc1->comp_2==NULL);
    int n2_loose_node = node2->connect && (nc2->comp_1==NULL)^(nc2->comp_2==NULL);
    
    int port = 1; // depending on how many dump nodes/loose nodes we have depends on how many ports
                  // there will be for this space in the matrix
    
    // we only want to get these elements if this node is not connected to anything
    // and is an output
    if(!node1->gnd_node && n1_loose_node) {
        // port a1 
        for(fin=0; fin<matrix->num_frequencies; fin++){ 
            for(i=0; i<inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);

                set_diagonal_element(matrix, space_rhs_idx, &col, &got_nnz, &space->nnz_count, port, fin, i, mode);
                
                if(n1_loose_node && n2_loose_node ){
                    if(space->a_cplng_12.coupling_off){
                        set_element(matrix, &(space->a12f[f_offset+fin][i][i]), space_rhs_idx, &col, &got_nnz, &space->nnz_count, 3, fin, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&space->a_cplng_12, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(space->a12f[f_offset+fin][i][j]), space_rhs_idx, &col, &got_nnz, &space->nnz_count, 3, fin, j, mode);
                        }
                    }
                }
                col++; // next column in sub-matrix
            }
        }
        
        port++; // increment to next port
        
        // port a2
        for(fin=0; fin<matrix->num_frequencies; fin++){ 
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, space_rhs_idx, &col, &got_nnz, &space->nnz_count, port, fin, i, mode);
                col++; // next column in sub-matrix
            }
        }
        
        port++; // increment to next port
    }

    if(!node2->gnd_node && n2_loose_node){
        // port a3
        for(fin=0; fin<matrix->num_frequencies; fin++){ 
            for(i=0; i<inter.num_fields; i++){
                set_diagonal_element(matrix, space_rhs_idx, &col, &got_nnz, &space->nnz_count, port, fin, i, mode);
                col++; // next column in sub-matrix
            }
        }
        
        port++; // increment to next port
        
        for(fin=0; fin<matrix->num_frequencies; fin++){ 
            for(i=0; i<inter.num_fields; i++){
                int ni, mi;
                get_tem_modes_from_field_index(&ni, &mi, i);
                
                if(n1_loose_node && n2_loose_node){
                    if(space->a_cplng_21.coupling_off){
                        set_element(matrix, &(space->a21f[f_offset+fin][i][i]), space_rhs_idx, &col, &got_nnz, &space->nnz_count, 2, fin, i, mode);
                    } else {
                        for(j=0; j < inter.num_fields; j++){
                            int nj, mj;
                            get_tem_modes_from_field_index(&nj, &mj, j);
                            bool couples = i == j || include_mode_coupling(&space->a_cplng_21, ni, nj, mi, mj);
                            if(couples) 
                                set_element(matrix, &(space->a21f[f_offset+fin][i][j]), space_rhs_idx, &col, &got_nnz, &space->nnz_count, 2, fin, j, mode);
                        }
                    }
                }
                
                set_diagonal_element(matrix, space_rhs_idx, &col, &got_nnz, &space->nnz_count, port, fin, i, mode);
                col++; // next column in sub-matrix
            }
        }
        
        port++; // increment to next port
    }
    
    if(mode == GETELEMENT) {
        if(got_nnz != space->nnz_count)
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT)
        *pcol_nnz += space->nnz_count;
    else
        bug_error("unexpected mode value");
}


/* Connections are formed with spaces in between the components, if both nodes
 * of a space are connected to components then the 'c' submatrices need to be
 * added with this function
 * 
 * |O c c|
 * |c O  |
 * |c   O|
 */
void get_connection_elements(int type, void *comp, space_t *space, ifo_matrix_vars_t *matrix, int *pcol_nnz, int mode){
    assert(comp != NULL);
    assert(type > 0);
    assert(space != NULL);
    assert(matrix != NULL);
    assert(matrix->M.col_ptr != NULL);
    assert(matrix->M.row_col_count != NULL);
    assert(mode == GETELEMENT || mode == COUNT);
    
    if(mode == COUNT){
        assert(pcol_nnz != NULL);
    }else
        assert(matrix->M.values != NULL);
    
    int i, j, fin;
    int f_offset = 0;
    
    if(matrix->type==STANDARD){
        f_offset = 0;
    }else if(matrix->type == SIGNAL_QCORR){
        f_offset = M_ifo_car.num_frequencies;
    }else 
        bug_error("matrix type not handled");
    
    int nnz = 0;
    int rhs_idx_col = 0;
    int rhs_idx_row = 0;
    node_t *conn = NULL, *next = NULL;
    
    get_next_space_node(type, comp, space, &conn, &next);
    
    if(conn->direction == IN_OUT) {
        rhs_idx_col = matrix->node_rhs_idx_2[conn->list_index];
    } else if(conn->direction == OUT_IN) {
        rhs_idx_col = matrix->node_rhs_idx_1[conn->list_index];
    } else
        bug_error("Node can not have direction EITHER_DIR when getting elements from matrix");
    
    int col = rhs_idx_col;
    
    coupling_info_t *a_cplng = NULL;
    complex_t ****a = NULL;
    int *space_nnz = NULL;
    
    if(conn->list_index == space->node1_index){
        a_cplng = &space->a_cplng_12;
        space_nnz = &space->nnz_count_conn_12;
        if(mode == COUNT) space->nnz_count_conn_12 = 0;
    }else if(conn->list_index == space->node2_index){
        a_cplng = &space->a_cplng_21;
        space_nnz = &space->nnz_count_conn_21;
        if(mode == COUNT) space->nnz_count_conn_21 = 0;
    }else
        bug_error("Connected node was not found attached to the space");
    
    if(mode == GETELEMENT){
        // need to determine if this connection is n1 -> n2 or n2 -> n1
        if(conn->list_index == space->node1_index){
            a = space->a12f;
        } else if(conn->list_index == space->node2_index){
            a = space->a21f;
        }else
            bug_error("Connected node was not found attached to the space");

        // now compute the row 
        if(conn->direction == EITHER_DIR || next->direction == EITHER_DIR)
            bug_error("Node directions should not be EITHER_DIR when positioning in the matrix");

        // now we only have 2 options for what the direction could be, IN_OUT or OUT_IN
        // if they are not equal port 1 of node 1 connects to port 1 of node 2, etc.
        if(conn->direction != next->direction){
            // if the connecting node is IN_OUT then we connect to the first port
            if(conn->direction == IN_OUT)
                rhs_idx_row = matrix->node_rhs_idx_2[next->list_index];
            else // else we have OUT_IN and get the second port
                rhs_idx_row = matrix->node_rhs_idx_1[next->list_index];

        } else if(conn->direction == IN_OUT && next->direction == IN_OUT){
            // If both nodes are IN_OUT then we connect the second port to the first
            rhs_idx_row = matrix->node_rhs_idx_1[next->list_index];;
        } else if(conn->direction == OUT_IN && next->direction == OUT_IN){
            rhs_idx_row = matrix->node_rhs_idx_2[next->list_index];
        } else
            bug_error("Node direction combination was not expected");
    }
    
    // now get the elements
    for(fin=0; fin < matrix->num_frequencies; fin++){
        for(i=0; i < inter.num_fields; i++){
            int ni, mi;
            get_tem_modes_from_field_index(&ni, &mi, i);
            
            if(a_cplng->coupling_off){
                if(mode == GETELEMENT) {
                    int ix = get_nnz_value_idx(matrix, col, matrix->M.col_ptr, matrix->M.row_col_count, &nnz, *space_nnz);
                    set_row_ptr(&matrix->M, ix, rhs_idx_row + get_node_rhs_idx(1, fin, i, matrix->num_frequencies));
                    a[f_offset+fin][i][i] = (complex_t*)&(matrix->M.values[2*ix]);
                } else if(mode == COUNT) {
                    matrix->M.row_col_count[col]++;
                    nnz++;
                }
            } else {
                for(j=0; j < inter.num_fields; j++){
                    int nj, mj;
                    get_tem_modes_from_field_index(&nj, &mj, j);
                    bool couples = i == j || include_mode_coupling(a_cplng, ni, nj, mi, mj);

                    if(couples) {
                        if(mode == GETELEMENT) {
                            int ix = get_nnz_value_idx(matrix, col, matrix->M.col_ptr, matrix->M.row_col_count, &nnz, *space_nnz);
                            set_row_ptr(&matrix->M, ix, rhs_idx_row + get_node_rhs_idx(1, fin, j, matrix->num_frequencies));
                            a[f_offset+fin][i][j] = (complex_t*)&(matrix->M.values[2*ix]);
                        } else if(mode == COUNT) {
                            matrix->M.row_col_count[col]++;
                            nnz++;
                        }
                    }
                }
            }
//            
//            for(j=0; j < inter.num_fields; j++){
//                if(mode == GETELEMENT) {
//                    int ix = get_nnz_value_idx(matrix, col, matrix->M.col_ptr, matrix->M.row_col_count, &nnz, *space_nnz);
//                    set_row_ptr(&matrix->M, ix, rhs_idx_row + get_node_rhs_idx(1, fin, j, matrix->num_frequencies));
//                    a[f_offset+fin][i][j] = (complex_t*)&(matrix->M.values[2*ix]);
//                } else if(mode == COUNT) {
//                    matrix->M.row_col_count[col]++;
//                    nnz++;
//                }
//            }
                
            col++;
        }
    }
    
    if(mode == GETELEMENT) {
        if(nnz != (*space_nnz))
            bug_error("Did not get as many elements from the matrix as were counted");
    } else if(mode == COUNT){
        *space_nnz += nnz;
        *pcol_nnz += nnz;
    } else
        bug_error("unexpected mode value");
    
}

void check_node_direction(){
    // before we start counting any elements we need to make sure all the directions
    // have been set correctly for each node, i.e. IN_OUT or OUT_IN and not EITHER_DIR
    // EITHER_DIR is set when the direction of fields aren't going in or out of a 
    // particular component. These are typically left over when a space has a loose
    // end
    int i;
    for(i=0;i<inter.num_nodes;i++){
        node_t *node = &inter.node_list[i];
        // if a dump node then don't bother with it
        if(!node->gnd_node){
            // check if the direction is not correctly set yet
            if(node->direction != IN_OUT && node->direction != OUT_IN){
                space_t *space;
                
                if(inter.node_conn_list[i].comp_2 != NULL)
                    bug_error("Node %s is connected to multiple components but does not have a direction specified", node->name);
                else if(inter.node_conn_list[i].type_1 == SPACE){
                    space = (space_t*)inter.node_conn_list[i].comp_1;
                    
                    if(space->node1_index == node->list_index) {
                        node->direction = OUT_IN;
                    } else if(space->node2_index == node->list_index) {
                        node->direction = IN_OUT;
                    } else
                        bug_error("node not actually attached to space");
                
                } else if(node->io & 2 && inter.node_conn_list[i].comp_2== NULL && inter.node_conn_list[i].comp_1 == NULL){
                    // if node isn't connected to anything
                    node->direction = OUT_IN;
                } else
                    bug_error("node %s couldn't define direction", node->name);
            }
        }
    }
}

/**
 * For a given matrix representing the system this returns which RHS index a 
 * particular node and direction is associated with.
 * 
 * @param matrix
 * @param node
 * @param nc
 * @param is_second_beam
 * @param name
 * @return 
 */
int __get_output_rhs_port(ifo_matrix_vars_t *matrix, node_t *node, node_connections_t *nc, bool is_second_beam, const char *name) {
    if(!(node->direction == IN_OUT || node->direction == OUT_IN))
        bug_error("node direction not specified");

    int type = 0;

    assert(!(nc->comp_1 == NULL && nc->comp_2 == NULL));

    // firstly get the type of component that the node is connected to.
    // spaces are least important, i.e.
    int loose_node = 0;

    // if one is null always take the other
    if(nc->comp_1 == NULL){
        type = nc->type_2;
        //loose_node = (node->io & 2) ? 0 : 1; // nothing else attached to it if not an input
        loose_node = 1;
    } else if(nc->comp_2 == NULL) {
        type = nc->type_1;
        //loose_node = (node->io & 2) ? 0 : 1; // nothing else attached to it if not an input
        loose_node = 1;
    } else{
        loose_node = 0;
        // if neither are null select the one that isn't a space...
        if(nc->type_1 == SPACE)
            type = nc->type_2;
        else if(nc->type_2 == SPACE)
            type = nc->type_1;
        else
            bug_error("unexpected node connections");

        if(type == SPACE)
            warn("Unexpected space connected to space, not correctly programmed for yet. Beam direction for %s is not defined", name);
    }

    int primary_port = 0;
    int secondary_port = 0;

    switch(type){
        case SPACE:                
            // space always gives the outgoing port as primary, should only happen if this is
            // a loose node
            if(node->direction == IN_OUT){
                primary_port = matrix->node_rhs_idx_1[node->list_index];
                secondary_port = matrix->node_rhs_idx_2[node->list_index];
            } else if(node->direction == OUT_IN) {
                primary_port = matrix->node_rhs_idx_2[node->list_index];
                secondary_port = matrix->node_rhs_idx_1[node->list_index];
            } else
                bug_error("direction unhandled");

            break;


            // these are all space like components i.e. there are no reflections
            // so that our primary node is the one going into the component.
            // If we happen to have nothing else connected to the node then then
            // we should always show the outgoing port as nothing will be going in...
            if((node->direction == IN_OUT && !loose_node) || (node->direction == OUT_IN && loose_node)) {
                primary_port = matrix->node_rhs_idx_1[node->list_index];
                secondary_port = matrix->node_rhs_idx_2[node->list_index];
            } else if((node->direction == OUT_IN && !loose_node) || (node->direction == IN_OUT && loose_node)) {
                primary_port = matrix->node_rhs_idx_2[node->list_index];
                secondary_port = matrix->node_rhs_idx_1[node->list_index];
            } else
                bug_error("direction unhandled");

            break;

        case BLOCK:
        case LENS:
        case SAGNAC:
        case DIODE:
        case MODULATOR:
        case MIRROR:
        case BEAMSPLITTER:
        case GRATING:
        case DBS:
            // for these the primary port is the reflected one, i.e. the outgoing port
            if(node->direction == IN_OUT){
                primary_port = matrix->node_rhs_idx_2[node->list_index]; // output
                secondary_port = matrix->node_rhs_idx_1[node->list_index]; // input
            } else if(node->direction == OUT_IN) {
                primary_port = matrix->node_rhs_idx_1[node->list_index]; // output 
                secondary_port = matrix->node_rhs_idx_2[node->list_index]; // input
            } else
                bug_error("direction unhandled");

            break;
        default:
            bug_error("unhandled component type");
    }


    // The lens seems to behave differently in Finesse 1.0 from other components
    // in that it shows the input if something is connected to it and output if not
    // by default we show the output node, swap this if not a loose_node
    if((type == LENS || type == DIODE || type == GRATING) && !loose_node) {        
        int p = primary_port;
        int s = secondary_port;
        primary_port = s;
        secondary_port = p;
    }

    if(!is_second_beam)
        return primary_port;
    else
        return secondary_port;
}

void set_output_data_rhs_indices(ifo_matrix_vars_t *matrix){
    int i;
    // Now that we have set all the RHS vector indicies then we can point
    // each output at the right position
    // TODO this might only need to be light_outputs?
    for(i=0; i<inter.num_light_outputs; i++){
        light_out_t *loutput = &inter.light_out_list[i];
        output_data_t *output = &inter.output_data_list[loutput->output_idx];
        
        // if output does not depend on a node
        if(output->node_index < 0 && output->detector_type != HOMODYNE)
            continue;
        
        node_t *node;
        node_connections_t *nc;    
        
        if (output->detector_type == HOMODYNE){
            // for homodyne detectors we need to store 2 node indicies
            // in one int value, so we store 2 32-bit ints in a 64 bit type.
            homodyne_t *hom = &inter.homodyne_list[output->detector_index];
            
            if(hom->light_out_1 == i){
                node = &inter.node_list[hom->node1_index];
                nc = &inter.node_conn_list[hom->node1_index];

                matrix->output_rhs_idx[hom->light_out_1] = __get_output_rhs_port(matrix, node, nc, hom->is_second_beam1, output->name);
            } else if(hom->light_out_2 == i) {
            
                node = &inter.node_list[hom->node2_index];
                nc = &inter.node_conn_list[hom->node2_index];

                matrix->output_rhs_idx[hom->light_out_2] = __get_output_rhs_port(matrix, node, nc, hom->is_second_beam2, output->name);
            } else
                bug_error("unhandled");
            
        } else {
            node = &inter.node_list[output->node_index];
            nc = &inter.node_conn_list[output->node_index];
            
            matrix->output_rhs_idx[i] = __get_output_rhs_port(matrix, node, nc, output->is_second_beam, output->name);
        }
    }
}

/**
 * Returns the psuedocolumn that this row/column index is in
 * 
 * @param rhs_idx
 * @return 
 */
pseudocolumn_t * get_pseudocolumn_by_rhs_idx(ifo_matrix_vars_t *matrix, int col_idx){
    pseudocolumn_t *a = NULL;
    
    if(col_idx >= matrix->M.num_eqns)
        bug_error("col_idx out of range\n");
        
    // firstly find which pseudo-column this output resides in...
    LL_FOREACH(matrix->pcol_head, a) {
        int this_rhs = get_comp_rhs_idx(matrix, a->type, a->component);
        
        if(a->next == NULL && (col_idx >= this_rhs)){
            break;
        } else if(col_idx >= this_rhs && col_idx < get_comp_rhs_idx(matrix, a->next->type, a->next->component)){
            break;
        }
    }
    
    return a;
}

void add_connected_spaces(ifo_matrix_vars_t *matrix, pseudocolumn_t *curr, node_t **nodes_next, size_t num_nodes, space_t **connect_to){
    int n;
    
    for(n=0; n<(int)num_nodes; n++) {
        if(nodes_next[n] != NULL && !nodes_next[n]->gnd_node) {
            utarray_extend_back(curr->items);
            pseudocolumn_item_t *itm = (pseudocolumn_item_t*) utarray_back(curr->items);
            itm->index = matrix->node_rhs_idx_1[nodes_next[n]->list_index];
            itm->type  = SPACECONNECTION;
            itm->ptr   = (void*)connect_to[n];
        }
    }
}

/*
 * The RHS vector is a vector of length NNZ (with dump nodes removed). This function
 * loops through all components and sets their position in the RHS vector to define
 * where the fields values will be stored. This also defines where in the matrix
 * each of the components lie. This can then be used to compute the CCS storage
 * as we will know which components come in which column.
 */
void build_ccs_matrix(ifo_matrix_vars_t *matrix){
    
    int ccs_rhs_idx = 0;
    
    int counted_nnz = 0;
    int eqns = 0; /** keep track of number of eqns we should have so that we can
                   * check this later against ccs_rhs_idx which should be the same
                   * if we have taken everything into account
                   */
    
    space_t *space = NULL;
    mirror_t *mirror = NULL;
    beamsplitter_t *bs = NULL;
    modulator_t *mod = NULL;
    lens_t *lens = NULL;
    diode_t *diode = NULL;
    dbs_t *isol = NULL;
    sagnac_t *sagnac = NULL;
    grating_t *gr = NULL;
    block_t *block = NULL;
    slink_t *feedback = NULL;
    
    matrix->pcol_head = NULL;
    
    int fields_per_port = get_submatrix_num_fields(1, matrix->num_frequencies);
    
    // loop through the optical components and set their position in the 
    // CCS matrix.
    int i;
    
    // reset node connection flags
    for(i=0; i<inter.num_nodes; i++)
        inter.node_list[i].connect = 0;
    
    for (i = 0; i < inter.num_mirrors; i++) {
        mirror = &(inter.mirror_list[i]);
        matrix->mirror_rhs_idx[i] = INT_MAX;
        
        node_t *nodes[2];
        nodes[0] = &inter.node_list[mirror->node1_index];
        nodes[1] = &inter.node_list[mirror->node2_index];
        
        int node_num = 2, n;
        int total_node_num = node_num;
        
        for(n=0; n<2; n++){
            node_t *node = nodes[n];
            
            if (!node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->mirror_rhs_idx[i] = min(matrix->mirror_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
        
        // get RHS vector index for motions
        if(mirror->num_motions > 0 && matrix->type == SIGNAL_QCORR){
            mirror->x_rhs_idx = ccs_rhs_idx;
            ccs_rhs_idx += mirror->num_motions;
        }
        
        if (node_num == 0) {
            gerror("No nodes were found on %s", mirror->name);
        } else {
            pseudocolumn_t *p = create_pseudocol((void*)mirror, MIRROR, counted_nnz, total_node_num);         
            get_mirror_elements(mirror, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix, p);
            
            pseudocolumn_item_t itm = {matrix->mirror_rhs_idx[i], MIRROR, (void*) mirror};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz;
            // make sure we include extra equation for mechanical motion
            eqns += node_num * 2 * fields_per_port;
            
            // now add in the mechanical motion equations
            if(mirror->num_motions > 0 && matrix->type == SIGNAL_QCORR) eqns += mirror->num_motions;
        }
    }
    
    for (i = 0; i < inter.num_beamsplitters; i++) {
        bs = &(inter.bs_list[i]);
        matrix->bs_rhs_idx[i] = INT_MAX;
        
        node_t *nodes[4];
        nodes[0] = &inter.node_list[bs->node1_index];
        nodes[1] = &inter.node_list[bs->node2_index];
        nodes[2] = &inter.node_list[bs->node3_index];
        nodes[3] = &inter.node_list[bs->node4_index];
        
        int node_num = sizeof(nodes)/sizeof(nodes[0]), n;
        int total_node_num = node_num;
        
        for(n=0; n<total_node_num; n++){
            node_t *node = nodes[n];
            
            if (!node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->bs_rhs_idx[i] = min(matrix->bs_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
        
        // get RHS vector index for motions
        if(bs->num_motions > 0 && matrix->type == SIGNAL_QCORR){
            bs->x_rhs_idx = ccs_rhs_idx;
            ccs_rhs_idx += bs->num_motions;
        }
        
        if (node_num == 0)
            gerror("No nodes were found on %s", bs->name);
        else {
            pseudocolumn_t *p = create_pseudocol((void*)bs, BEAMSPLITTER, counted_nnz, total_node_num);         
            get_beamsplitter_elements(bs, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            counted_nnz += p->nnz;
            
            pseudocolumn_item_t itm = {matrix->bs_rhs_idx[i], BEAMSPLITTER, (void*) bs};
            utarray_push_back(p->items, &itm);
            
            eqns += node_num * 2 * fields_per_port;
            
            // now add in the mechanical motion equations
            if(bs->num_motions > 0 && matrix->type == SIGNAL_QCORR)
                eqns += bs->num_motions;
        }
    }
    
    for(i = 0; i < inter.num_modulators; i++){
        mod = &inter.modulator_list[i];
        
        matrix->modulator_rhs_idx[i] = INT_MAX;
        
        node_t *nodes[2];
        nodes[0] = &inter.node_list[mod->node1_index];
        nodes[1] = &inter.node_list[mod->node2_index];
        
        int node_num = sizeof(nodes)/sizeof(nodes[0]), n;
        int total_node_num = node_num;
        
        for(n=0; n<total_node_num; n++){
            node_t *node = nodes[n];
            
            if (!node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->modulator_rhs_idx[i] = min(matrix->modulator_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
        
        if (node_num == 0)
            gerror("No nodes were found on %s", mod->name);
        else {
            pseudocolumn_t *p = create_pseudocol((void*)mod, MODULATOR, counted_nnz, total_node_num);         
            get_modulator_elements(mod, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            pseudocolumn_item_t itm = {matrix->modulator_rhs_idx[i], MODULATOR, (void*) mod};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz; // update total nnz
            eqns += node_num * 2 * fields_per_port; // update number of equations
        }
    }
    
    for(i = 0; i < inter.num_lenses; i++){
        lens = &inter.lens_list[i];
        
        matrix->lens_rhs_idx[i] = INT_MAX;
        
        node_t *nodes[2];
        nodes[0] = &inter.node_list[lens->node1_index];
        nodes[1] = &inter.node_list[lens->node2_index];
        
        int node_num = sizeof(nodes)/sizeof(nodes[0]), n;
        int total_node_num = node_num;
        
        for(n=0; n<total_node_num; n++){
            node_t *node = nodes[n];
            
            if (!node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->lens_rhs_idx[i] = min(matrix->lens_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
        
        if (node_num == 0)
            gerror("No nodes were found on %s", lens->name);
        else {
            pseudocolumn_t *p = create_pseudocol((void*)lens, LENS, counted_nnz, total_node_num);         
            get_lens_elements(lens, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            pseudocolumn_item_t itm = {matrix->lens_rhs_idx[i], LENS, (void*) lens};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz; // update total nnz
            eqns += node_num * 2 * fields_per_port; // update number of equations
        }
    }
    
    
    for(i = 0; i < inter.num_blocks; i++){
        block = &inter.block_list[i];
        
        matrix->block_rhs_idx[i] = INT_MAX;
        
        node_t *nodes[2];
        nodes[0] = &inter.node_list[block->node1_index];
        nodes[1] = &inter.node_list[block->node2_index];
        
        int node_num = sizeof(nodes)/sizeof(nodes[0]), n;
        int total_node_num = node_num;
        
        for(n=0; n<total_node_num; n++){
            node_t *node = nodes[n];
            
            if (!node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->block_rhs_idx[i] = min(matrix->block_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
        
        if (node_num == 0)
            gerror("No nodes were found on %s", block->name);
        else {
            pseudocolumn_t *p = create_pseudocol((void*)block, BLOCK, counted_nnz, total_node_num);         
            get_block_elements(block, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            pseudocolumn_item_t itm = {matrix->block_rhs_idx[i], BLOCK, (void*) block};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz; // update total nnz
            eqns += node_num * 2 * fields_per_port; // update number of equations
        }
    }
    
    for(i = 0; i < inter.num_sagnacs; i++){
        sagnac = &inter.sagnac_list[i];
        
        matrix->sagnac_rhs_idx[i] = INT_MAX;
        
        node_t *nodes[2];
        nodes[0] = &inter.node_list[sagnac->node1_index];
        nodes[1] = &inter.node_list[sagnac->node2_index];
        
        int node_num = sizeof(nodes)/sizeof(nodes[0]), n;
        int total_node_num = node_num;
        
        for(n=0; n<total_node_num; n++){
            node_t *node = nodes[n];
            
            if (!node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->sagnac_rhs_idx[i] = min(matrix->sagnac_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
        
        if (node_num == 0)
            gerror("No nodes were found on %s", sagnac->name);
        else {
            pseudocolumn_t *p = create_pseudocol((void*)sagnac, SAGNAC, counted_nnz, total_node_num);         
            get_sagnac_elements(sagnac, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            pseudocolumn_item_t itm = {matrix->sagnac_rhs_idx[i], SAGNAC, (void*) sagnac};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz; // update total nnz
            eqns += node_num * 2 * fields_per_port; // update number of equations
        }
    }
    
    for(i = 0; i < inter.num_diodes; i++){
        diode = &inter.diode_list[i];
        
        matrix->diode_rhs_idx[i] = INT_MAX;
        
        node_t *nodes[3];
        nodes[0] = &inter.node_list[diode->node1_index];
        nodes[1] = &inter.node_list[diode->node2_index];
        nodes[2] = &inter.node_list[diode->node3_index];
        
        int node_num = sizeof(nodes)/sizeof(nodes[0]), n;
        int total_node_num = node_num;
        
        for(n=0; n<total_node_num; n++){
            node_t *node = nodes[n];
            
            if (!node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->diode_rhs_idx[i] = min(matrix->diode_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
        
        if (node_num == 0)
            gerror("No nodes were found on %s", diode->name);
        else {
            pseudocolumn_t *p = create_pseudocol((void*)diode, DIODE, counted_nnz, total_node_num);         
            get_diode_elements(diode, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            pseudocolumn_item_t itm = {matrix->diode_rhs_idx[i], DIODE, (void*) diode};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz; // update total nnz
            eqns += node_num * 2 * fields_per_port; // update number of equations
        }
    }
    
    for (i = 0; i < inter.num_dbss; i++) {
        isol = &(inter.dbs_list[i]);
        matrix->dbs_rhs_idx[i] = INT_MAX;
        
        node_t *nodes[4];
        nodes[0] = &inter.node_list[isol->node1_index];
        nodes[1] = &inter.node_list[isol->node2_index];
        nodes[2] = &inter.node_list[isol->node3_index];
        nodes[3] = &inter.node_list[isol->node4_index];
        
        int node_num = sizeof(nodes)/sizeof(nodes[0]), n;
        int total_node_num = node_num;
        
        for(n=0; n<total_node_num; n++){
            node_t *node = nodes[n];
            
            if (!node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->dbs_rhs_idx[i] = min(matrix->dbs_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
       
        if (node_num == 0)
            gerror("No nodes were found on %s", isol->name);
        else {
            pseudocolumn_t *p = create_pseudocol((void*)isol, DBS, counted_nnz, total_node_num);         
            get_dbs_elements(isol, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            counted_nnz += p->nnz;
            
            pseudocolumn_item_t itm = {matrix->dbs_rhs_idx[i], DBS, (void*) isol};
            utarray_push_back(p->items, &itm);
            
            eqns += node_num * 2 * fields_per_port;
        }
    }
    
    for (i = 0; i < inter.num_gratings; i++) {
        gr = &(inter.grating_list[i]);
        matrix->grating_rhs_idx[i] = INT_MAX;
        
        assert(gr->num_of_ports <= 4);
        
        node_t *nodes[4];
        nodes[0] = &inter.node_list[gr->node1_index];
        nodes[1] = &inter.node_list[gr->node2_index];
        nodes[2] = (gr->num_of_ports >= 3) ? &inter.node_list[gr->node3_index] : NULL;
        nodes[3] = (gr->num_of_ports == 4) ? &inter.node_list[gr->node4_index] : NULL;
        
        int node_num = sizeof(nodes)/sizeof(nodes[0]), n;
        int total_node_num = node_num;
        
        for(n=0; n<total_node_num; n++){
            node_t *node = nodes[n];
            
            if (node != NULL && !node->gnd_node) {
                add_node_rhs_vector(matrix, node, &ccs_rhs_idx, fields_per_port);
                matrix->grating_rhs_idx[i] = min(matrix->grating_rhs_idx[i], matrix->node_rhs_idx_1[node->list_index]);
            } else
                node_num--;
        }
        
        if (node_num == 0)
            gerror("No nodes were found on %s", gr->name);
        else {
            pseudocolumn_t *p = create_pseudocol((void*)gr, GRATING, counted_nnz, total_node_num);         
            get_grating_elements(gr, matrix, &p->nnz, COUNT);
            
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            pseudocolumn_item_t itm = {matrix->grating_rhs_idx[i], GRATING, (void*) gr};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz;
            eqns += node_num * 2 * fields_per_port;
        }
    }
    
    /* Lastly we want to handle spaces slightly differently. If there are any
     * nodes that are not connected to any of the above components then they will
     * not have been added to the matrix. e.g.
     *
     * l l1 ...
     * m m1 ... n1 n2
     * s s1 n2 n3
     * 
     * Only nodes n1 and n2 will have been added as n3 isn't actually connected
     * to anything we also need to add that in, otherwise it won't be computed.
     */
    for (i = 0; i < inter.num_spaces; i++) {
        space = &inter.space_list[i];
        
        node_t *node1 = &inter.node_list[space->node1_index];
        node_t *node2 = &inter.node_list[space->node2_index];
        
        matrix->space_rhs_idx[i] = INT_MAX;
        
        int ports = 0;
        
        // if node isn't connected to anything but is an input or output
        // node then we need to include it in the matrix. Otherwise
        // there is no point computing the field at this node as nothing is read
        // or put there and it doesn't link anything up...
        if(!node1->gnd_node && !node1->connect){
            if(matrix->node_rhs_idx_1[node1->list_index] > 0)
                bug_error("node1 shouldn't be located in rhs vector as it isn't connected");
            else {
                matrix->node_rhs_idx_1[node1->list_index] = ccs_rhs_idx;
                ccs_rhs_idx += fields_per_port;
            }
              
            if(matrix->node_rhs_idx_2[node1->list_index] > 0)
                bug_error("node1 shouldn't be located in rhs vector as it isn't connected");
            else {
                matrix->node_rhs_idx_2[node1->list_index] = ccs_rhs_idx;
                ccs_rhs_idx += fields_per_port;
            }
            
            node1->connect++;
            matrix->space_rhs_idx[i] = min(matrix->space_rhs_idx[i], matrix->node_rhs_idx_1[node1->list_index]);
            ports += 2;
        }
         
        if(!node2->gnd_node && !node2->connect){
            if(matrix->node_rhs_idx_1[node2->list_index] > 0)
                bug_error("node1 shouldn't be located in rhs vector as it isn't connected");
            else {
                matrix->node_rhs_idx_1[node2->list_index] = ccs_rhs_idx;
                ccs_rhs_idx += fields_per_port;
            }
              
            if(matrix->node_rhs_idx_2[node2->list_index] > 0)
                bug_error("node1 shouldn't be located in rhs vector as it isn't connected");
            else {
                matrix->node_rhs_idx_2[node2->list_index] = ccs_rhs_idx;
                ccs_rhs_idx += fields_per_port;
            }
            
            node2->connect++;
            matrix->space_rhs_idx[i] = min(matrix->space_rhs_idx[i], matrix->node_rhs_idx_1[node2->list_index]);
            ports += 2;
        }
        
        if(ports){
            // create new pseudo-column to hold information on the optic and 
            // space components that will later need to be ordered
            pseudocolumn_t *p = create_pseudocol((void*)space, SPACE, counted_nnz, 2);
            // count number of space elements into pseudocolumn nnz
            get_space_elements(space, matrix, &p->nnz, COUNT);
                                    
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            space->ports = ports;
            
            pseudocolumn_item_t itm = {matrix->space_rhs_idx[i], SPACE, (void*) space};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz;
            eqns += ports * fields_per_port;
        }
        
        // TODO also need to add in check for spaces joined to spaces as they
        // will also need adding here
    }
    
    // now that we have set the indicies of all nodes we can go and set the rhs
    // vector indices that each output will extract
    set_output_data_rhs_indices(matrix);
    
    if(matrix->type == SIGNAL_QCORR){
        /**
         * Actually, lastly we need to include any feedback computations if building the
         * signal matrix. This is a bit more complicated as we are dealing with
         * global couplings not local at a component level. Here we take the output at
         * some place in the interferometer and need to feed it back into the motion
         * of another component.
         */
        for (i = 0; i < inter.num_slinks; i++) {
            feedback = &(inter.slink_list[i]);
            
            // feedback is only one extra equation to include in matrix
            matrix->feedback_rhs_idx[i] = ccs_rhs_idx++;
            
            pseudocolumn_t *p = create_pseudocol((void*)feedback, QFEEDBACK, counted_nnz, 0);
            
            get_feedback_input_elements(feedback, matrix, &p->nnz, COUNT);
            get_feedback_output_elements(feedback, matrix, &p->nnz, COUNT);
                                    
            // store pseudo-column in linked list
            add_pcol(matrix,p);
            
            pseudocolumn_item_t itm = {matrix->feedback_rhs_idx[i], QFEEDBACK, (void*) feedback};
            utarray_push_back(p->items, &itm);
            
            counted_nnz += p->nnz;
            eqns += 1;
            
            // also need to add the feedback input to the list of items in the
            // output detector column
            uint64_t col_idx = matrix->output_rhs_idx[feedback->output_list_idx];
            
            pseudocolumn_t *a = get_pseudocolumn_by_rhs_idx(matrix, col_idx);
            
            assert(a != NULL);
            // add this feedback item to the content of the pseudocolumn
            utarray_push_back(a->items, &itm);
        }
        
        // We also need to add in the motion links to the pseudocolumn items
        // as these need allocating matrix elements for too
        for (i = 0; i < inter.num_mirrors; i++) {
            mirror = &inter.mirror_list[i];
            
            if(mirror->motion_links != NULL && utarray_len(mirror->motion_links)){
                pseudocolumn_t *a = get_pseudocolumn_by_rhs_idx(matrix, matrix->mirror_rhs_idx[i]);
                motion_link_t *p = NULL;
                
                // For each component that this mirror links to we need to insert
                // a link item
                utarray_foreach(mirror->motion_links, p) {
                    // only allocate matrix elements for component-to-component 
                    // linking. Internal linking already allocates the elements
                    // and needs to be handled separately
                    
                    if(p->from_list_idx != p->to_list_idx) {
                        utarray_extend_back(a->items);
                        pseudocolumn_item_t *itm = (pseudocolumn_item_t*)utarray_back(a->items);
                        itm->type  = MOTIONLINK;
                        itm->ptr   = (void*)p;

                        if(p->to_type == MIRROR){
                            itm->index = inter.mirror_list[p->to_list_idx].x_rhs_idx + p->to_motion;
                        } else if(p->to_type == BEAMSPLITTER){
                            itm->index = inter.bs_list[p->to_list_idx].x_rhs_idx + p->to_motion;
                        } else
                            bug_error("Unexpected type %i", p->to_type);

                        int ml_nnz = 0;
                        get_motion_link_elements(p, matrix, &ml_nnz, COUNT);

                        // As we previously added the number of non-zeros to the total
                        // already for this pseudocolumn, we store the number from the 
                        // link and add in separately
                        a->nnz += ml_nnz;
                        counted_nnz += ml_nnz;
                    }
                }
            }
        }
        
        for (i = 0; i < inter.num_beamsplitters; i++) {
            bs = &inter.bs_list[i];
            
            if(bs->motion_links != NULL && utarray_len(bs->motion_links)){
                pseudocolumn_t *a = get_pseudocolumn_by_rhs_idx(matrix, matrix->bs_rhs_idx[i]);
                motion_link_t *p = NULL;
                
                // For each component that this mirror links to we need to insert
                // a link item
                utarray_foreach(bs->motion_links, p) {
                    // only allocate matrix elements for component-to-component 
                    // linking. Internal linking already allocates the elements
                    // and needs to be handled separately
                    if(p->from_list_idx != p->to_list_idx) {
                        utarray_extend_back(a->items);
                        pseudocolumn_item_t *itm = (pseudocolumn_item_t*)utarray_back(a->items);
                        itm->type  = MOTIONLINK;
                        itm->ptr   = (void*)p;

                        if(p->to_type == MIRROR){
                            itm->index = inter.mirror_list[p->to_list_idx].x_rhs_idx + p->to_motion;
                        } else if(p->to_type == BEAMSPLITTER){
                            itm->index = inter.bs_list[p->to_list_idx].x_rhs_idx + p->to_motion;
                        } else
                            bug_error("Unexpected type");

                        int ml_nnz = 0;
                        get_motion_link_elements(p, matrix, &ml_nnz, COUNT);

                        // As we previously added the number of non-zeros to the total
                        // already for this pseudocolumn, we store the number from the 
                        // link and add in separately
                        a->nnz += ml_nnz;
                        counted_nnz += ml_nnz;
                    }
                }
            }
        }
    }
    
    for(i=0; i<inter.num_nodes; i++){
        if(!inter.node_list[i].gnd_node && inter.node_list[i].connect
                && (matrix->node_rhs_idx_1[i] < 0 || matrix->node_rhs_idx_2[i] < 0))
            bug_error("node %s was not assigned in the RHS vector anywhere but is attached to a component, must have missed it", inter.node_list[i].name);
    }
    
    // double check the rhs index matches how many equations we should have
    if(ccs_rhs_idx != eqns)
        bug_error("rhs index isn't the same as the number of equations");
    
    // Loose nodes get lost from the matrix thus we might have less equations to
    // solve as they are not computed
    if(eqns > matrix->M.num_eqns)
        bug_error("Shouldn't have found more equations to solve then memory possibly allocated");
    
    // the counter now contains the number of non-zeros in the matrix
    matrix->M.num_nonzeros = counted_nnz;
    matrix->M.num_eqns = eqns;
    
    /*
     * Now that the components have been positioned in the matrix we need to 
     * group up the components into columns
     * 
     *      |O1 S1       || a_O1 |
     *      |S1 O2 S2    || a_O2 |
     *      |   S2 O3 S3 || a_O3 |
     *      |      S3 O4 || a_O4 |
     * 
     * A maximum of 3 components per column should appear: 1 optic and 2 spaces.
     * Only 1 optic component should be located in each pseudo-column. However in
     * the case of the beamsplitter this is 1 optic and 4 spaces.
     */
    
    node_connections_t *nc1 = NULL, *nc2 = NULL;
    
    // set starting head node
    pseudocolumn_t *curr = matrix->pcol_head;
    
    if(curr == NULL) {
        bug_error("pseudo column linked list head was NULL");
    }
    
    space_t *spaces[4] = {0};
    node_t *nodes_next[4] = {0}, *node1, *node2;
    
    // iterate through each pseudo-column and start adding to the CCS matrix
    // structure the connections between nodes connected by spaces
    // An error will be thrown if no space is between 2 components
    do {
        // Ensure that we have NULL values in the various pointer arrays
        memset(nodes_next, 0, sizeof(nodes_next));
        memset(spaces, 0, sizeof(spaces));
        
        // check out which optical component occupies the pseudo-column...
        switch(curr->type) {
            case MIRROR:
                mirror = (mirror_t *) curr->component;
                
                get_unknown_space_node(matrix, curr->component, mirror->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix, curr->component, mirror->node2_index, &nodes_next[1], &spaces[1]);
                break;
            
            case SAGNAC:
                sagnac = (sagnac_t *) curr->component;
                get_unknown_space_node(matrix, curr->component, sagnac->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix, curr->component, sagnac->node2_index, &nodes_next[1], &spaces[1]);
                break;
            
            case BLOCK:
                block = (block_t *) curr->component;
                get_unknown_space_node(matrix, curr->component, block->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix, curr->component, block->node2_index, &nodes_next[1], &spaces[1]);
                break;
                
            case BEAMSPLITTER:
                bs = (beamsplitter_t *) curr->component;
                get_unknown_space_node(matrix,(void*)bs, bs->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix,(void*)bs, bs->node2_index, &nodes_next[1], &spaces[1]);
                get_unknown_space_node(matrix,(void*)bs, bs->node3_index, &nodes_next[2], &spaces[2]);
                get_unknown_space_node(matrix,(void*)bs, bs->node4_index, &nodes_next[3], &spaces[3]);
                break;
                 
            case MODULATOR:
                mod = (modulator_t *) curr->component;
                get_unknown_space_node(matrix, curr->component, mod->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix, curr->component, mod->node2_index, &nodes_next[1], &spaces[1]);
                break;
            
            case LENS:
                lens = (lens_t *) curr->component;
                get_unknown_space_node(matrix, curr->component, lens->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix, curr->component, lens->node2_index, &nodes_next[1], &spaces[1]);
                break;
               
            case DIODE:
                diode = (diode_t *) curr->component;
                get_unknown_space_node(matrix, curr->component, diode->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix, curr->component, diode->node2_index, &nodes_next[1], &spaces[1]);
                get_unknown_space_node(matrix, curr->component, diode->node3_index, &nodes_next[2], &spaces[2]);
                break;
               
            case DBS:
                isol = (dbs_t *) curr->component;
                get_unknown_space_node(matrix, curr->component, isol->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix, curr->component, isol->node2_index, &nodes_next[1], &spaces[1]);
                get_unknown_space_node(matrix, curr->component, isol->node3_index, &nodes_next[2], &spaces[2]);
                get_unknown_space_node(matrix, curr->component, isol->node4_index, &nodes_next[3], &spaces[3]);
                break;
                
            case GRATING:
                gr = (grating_t*) curr->component;
                get_unknown_space_node(matrix,(void*)gr, gr->node1_index, &nodes_next[0], &spaces[0]);
                get_unknown_space_node(matrix,(void*)gr, gr->node2_index, &nodes_next[1], &spaces[1]);
                if(gr->num_of_ports >= 3) get_unknown_space_node(matrix,(void*)gr, gr->node3_index, &nodes_next[2], &spaces[2]);
                if(gr->num_of_ports == 4) get_unknown_space_node(matrix,(void*)gr, gr->node4_index, &nodes_next[3], &spaces[3]);
                
                break;
                
            case SPACE:
                // Spaces only get added when the nodes they attach do not connect
                // to anything but are output/input nodes.
                space = (space_t*) curr->component;
                
                // determine if connected to any components
                nc1 = &inter.node_conn_list[space->node1_index];
                nc2 = &inter.node_conn_list[space->node2_index];
                node1 = &inter.node_list[space->node1_index];
                node2 = &inter.node_list[space->node2_index];
                
                // find the node that is connected to something, one should always
                // be null
                if(nc1->comp_1 == space && nc1->comp_2 == NULL && nc2->comp_1 == space && nc2->comp_2 == NULL){
                    // if all are null then this space isn't connected to anything
                    // and just the 4 ports for this space should exist
                    if (space->ports != 4)
                        bug_error("4 ports expected for space");
                    
                    spaces[0] = NULL;
                    spaces[1] = NULL;
                    
                } else if (nc1->comp_1 == NULL || nc1->comp_2 == NULL) {
                    // If there is nothing connected to node1
                    spaces[0] = space;
                    spaces[1] = NULL;
                    nodes_next[0] = node2;
                    nodes_next[1] = NULL;
                    
                } else if (nc2->comp_1 == NULL || nc2->comp_2 == NULL) {
                    spaces[0] = space;
                    spaces[1] = NULL;
                    nodes_next[0] = node1;
                    nodes_next[1] = NULL;
                } 
                
                break;
                
            case QFEEDBACK:
                // Feedback isn't a typical component so things don't connect to it
                // with spaces so no need to do anything here
                break;
            default:
                bug_error("Cannot handle type %i",curr->type);
        }
        
        add_connected_spaces(matrix, curr, nodes_next, 4, spaces);

        // Now that we have added all the items to the pseudocolumn we
        // have to sort them in ascending RHS vector index
        utarray_sort(curr->items, sort_pseudocolumn_items);
        
        // now count up the connection non-zeros
        int i;
        
        for(i=0; i < (int)(sizeof(spaces)/sizeof(space_t *)); i++) {
            if(spaces[i] != NULL){
                int nnz = 0;
                get_connection_elements(curr->type, curr->component, spaces[i], matrix, &nnz, COUNT);
                curr->nnz += nnz;
                matrix->M.num_nonzeros += nnz;
            }
        }
        
        curr = curr->next; // get next pseudo-column
    } while(curr != NULL);
    
    curr = matrix->pcol_head;
    int sum_nnz = 0;
    
    do {
        sum_nnz += curr->nnz;
        curr = curr->next;
    } while(curr != NULL);
    
    if(matrix->M.num_nonzeros != sum_nnz)
        bug_error("Number of non-zeros in pseudo-columns doesn't match inter.num_zeros");
    
    // Now we know how many NNZ we have we can allocate the value and row_idx arrays
    long int bytes = 0;
    
    alloc_matrix_ccs_nnz(&bytes, matrix);
    
    // compute the col_ptr as we now know how many rows are in each column
    set_col_ptr(matrix->M.col_ptr, 0, 0);
    
    for(i=1; i < eqns; i++){
        // take previous col pointer and then add how many rows there are
        set_col_ptr(matrix->M.col_ptr, i, get_col_ptr(matrix->M.col_ptr, i) + get_col_ptr(matrix->M.col_ptr, i-1) + matrix->M.row_col_count[i-1]);
    }
    
    // by definition the last eqn + 1 must be the total number of nnz
    set_col_ptr(matrix->M.col_ptr, eqns, matrix->M.num_nonzeros);

#if INCLUDE_PARALUTION == 1    
    // Now build the column index storage needed by certain solvers
    if (options.sparse_solver == PARALUTION) {
        int column = 0, i = 0, tmp = 0;
        
        for(column=0; column < matrix->M.num_eqns; column++){
            for (i=0; i < matrix->M.row_col_count[column]; i++) {
                ((int*)matrix->M.col_idx)[i + tmp] = column;
            }
            
            tmp += matrix->M.row_col_count[column];
        }
    }
#endif
    
    if(inter.debug) {
        if(matrix->type == STANDARD){
            message("Carrier matrix non-zeros = %i\n", matrix->M.num_nonzeros);
            message("Carrier matrix equations = %i\n", matrix->M.num_eqns);
        } else {
            message("Signal matrix non-zeros = %i\n", matrix->M.num_nonzeros);
            message("Signal matrix equations = %i\n", matrix->M.num_eqns);
        }
    }
}

/**
 * Typically RHS vector is also sparse for Finesse models, thus we can be efficient
 * and determine which values are non-zero beforehand.
 * 
 * @param matrix
 */
void get_ccs_matrix_sparse_input_blocks() {
    
    M_ifo_car.input_blocks = (long*)malloc(sizeof(long) * inter.num_light_inputs * inter.num_fields);
    
    M_ifo_car.input_num_blocks = 0;
    M_ifo_sig.input_num_blocks = 0;
    
    int i, j;
    
    for(i=0; i<inter.num_light_inputs; i++){
        light_in_t *light_input = &(inter.light_in_list[i]);
        
        node_t* n = &inter.node_list[light_input->node_index];
        
        int rhs_idx_car = 0;
        
        // first get the 
        if(light_input->node_port == 2) { // first port in the input
            rhs_idx_car = M_ifo_car.node_rhs_idx_2[n->list_index];
        } else if(light_input->node_port == 1) { // second port is the input
            rhs_idx_car = M_ifo_car.node_rhs_idx_1[n->list_index];
        } else
            bug_error("Node did not have direction set when filling in RHS vector");
        
        int inblock = false, idx = 0;
        
        for (j = 0; j < inter.num_fields; j++) {
            idx = rhs_idx_car + get_port_rhs_idx(light_input->f->index, j);
            
            complex_t v = light_input->power_coeff_list[j];
            
            if(v.re == 0.0 && v.im == 0.0 && inblock) {
                // we're in a block but now there is no power
                inblock = false;
            } else if((v.re != 0.0 || v.im != 0.0) && !inblock) {
                // have a non-zero value and not in block so create one
                M_ifo_car.input_num_blocks++;
                M_ifo_car.input_blocks[2*(M_ifo_car.input_num_blocks-1)] = idx;
                M_ifo_car.input_blocks[2*(M_ifo_car.input_num_blocks-1) + 1] = 1;
                
                inblock = true;
            } else if(inblock) {
                M_ifo_car.input_blocks[2*(M_ifo_car.input_num_blocks-1) + 1] += 1;
            }
        }
    }
    
    
    message("\n");
    warn("-----------------------------\n");
    warn("- Carrier RHS vector blocks\n");
    warn("-----------------------------\n");
    for(i=0; i<M_ifo_car.input_num_blocks; i++) {
        warn("Block %i: %i -> %i\n", i,  M_ifo_car.input_blocks[2*i], M_ifo_car.input_blocks[2*i+1]);
    }
    
    warn("-----------------------------\n");
}

void get_ccs_matrix(ifo_matrix_vars_t *matrix){
    // reset pseudo-column linked list
    pseudocolumn_t *curr = matrix->pcol_head;
    
    // we no longer need the values in row_col_count array, we now use
    // the same array to keep track of the current nnz value index when 
    // getting the elements for each component
    memset(matrix->M.row_col_count, 0, matrix->M.num_eqns*sizeof(long));
    
    // iterate through each pseudo-column and start adding to the CCS matrix
    do {
        // get the number of fields this mirror has, this is the
        // the number of columns the sub-matrix will have
        
        pseudocolumn_item_t *itm = NULL;
        
        utarray_foreach(curr->items, itm) {
            
             if(itm->type == SPACECONNECTION){
                get_connection_elements(curr->type, curr->component, (space_t*)itm->ptr, matrix, NULL, GETELEMENT);
             } else if(curr->component == itm->ptr){
                switch(curr->type){
                    case MIRROR:
                        get_mirror_elements((mirror_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    case SAGNAC:
                        get_sagnac_elements((sagnac_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    case BEAMSPLITTER:
                        get_beamsplitter_elements((beamsplitter_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    case GRATING:
                        get_grating_elements((grating_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    case MODULATOR:
                        get_modulator_elements((modulator_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    case LENS:
                        get_lens_elements((lens_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    case DIODE:
                        get_diode_elements((diode_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    case DBS:
                        get_dbs_elements((dbs_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    case SPACE:
                        get_space_elements((space_t*)curr->component,  matrix, NULL, GETELEMENT);
                        break;
                    case BLOCK:
                        get_block_elements((block_t*)curr->component,  matrix, NULL, GETELEMENT);
                        break;
                    case QFEEDBACK:
                        get_feedback_output_elements((slink_t*)curr->component, matrix, NULL, GETELEMENT);
                        break;
                    default:
                        bug_error("Can't handle component type %i",curr->type);
                }
            } else if(itm->type == QFEEDBACK) {
                get_feedback_input_elements((slink_t*)itm->ptr, matrix, NULL, GETELEMENT);
            } else if(itm->type == MOTIONLINK){
                get_motion_link_elements((motion_link_t*)itm->ptr, matrix, NULL, GETELEMENT);
            } else
                bug_error("Unhandled column type");
        }
        
        curr = curr->next; // get next pseudo-column
    } while(curr != NULL);   
}

// The normal matrix fill does so for each frequency individually whereas now we have
// to fill all frequencies
void fill_ccs_matrix(ifo_matrix_vars_t *matrix){
    assert(matrix != NULL);
    assert(matrix->type == STANDARD || matrix->type == SIGNAL_QCORR);
    
    frequency_t **f_list = matrix->frequencies;
    int num_frequencies = matrix->num_frequencies;
    // As the total number of frequencies are the carriers + the signal
    // the carriers are 'first' in the vector of overall frequencies and then the
    // the signals, hence we need to offset correctly to ensure we are picking the right
    // ones.
    int f_offset = 0;
    
    if(matrix->type == STANDARD){
        f_offset = 0;
    } else if(matrix->type == SIGNAL_QCORR) {
        f_offset = M_ifo_car.num_frequencies;
    }

    int fin, fout;

    for(fin=0; fin < num_frequencies; fin++){
        // we also have the problem that the previous fill functions all want 
        // to use the internal a12, a21,..., etc. pointers. First we need to point
        // the old pointers to the different frequencies
        int i;
        
        // if we have a signal matrix then we need to conjugate the lower sidebands
        bool do_conj = (matrix->type == SIGNAL_QCORR && matrix->frequencies[fin]->order == -1)? true : false;
        
        for(i=0; i<inter.num_mirrors; i++){
            mirror_t *m = &inter.mirror_list[i];
            
            if(m->dither_order == 0) {
                // if not dithering then we can just compute zeroth order coupling
                m->a11 = m->a11f[f_offset+fin][f_offset+fin];
                m->a12 = m->a12f[f_offset+fin][f_offset+fin];
                m->a21 = m->a21f[f_offset+fin][f_offset+fin];
                m->a22 = m->a22f[f_offset+fin][f_offset+fin];

                fill_matrix_mirror_elements(m, f_list[fin]->f, f_list[fin]->f, 0, do_conj);
            } else {
                for(fout=0; fout< num_frequencies; fout++) {
                    if(matrix->m_does_f_couple[m->dither_list_index][fin][fout]) {
                        m->a11 = m->a11f[f_offset+fin][f_offset+fout];
                        m->a12 = m->a12f[f_offset+fin][f_offset+fout];
                        m->a21 = m->a21f[f_offset+fin][f_offset+fout];
                        m->a22 = m->a22f[f_offset+fin][f_offset+fout];

                        fill_matrix_mirror_elements(m, f_list[fin]->f, f_list[fout]->f, matrix->m_f_couple_order[m->dither_list_index][fin][fout], do_conj);
                    }
                }
            }
        }

        for(i=0; i<inter.num_beamsplitters; i++){
            beamsplitter_t *bs = &inter.bs_list[i];
            
            if (bs->dither_order == 0){
                bs->a12 = bs->a12f[f_offset+fin][f_offset+fin];
                bs->a21 = bs->a21f[f_offset+fin][f_offset+fin];
                bs->a34 = bs->a34f[f_offset+fin][f_offset+fin];
                bs->a43 = bs->a43f[f_offset+fin][f_offset+fin];
                bs->a13 = bs->a13f[f_offset+fin][f_offset+fin];
                bs->a31 = bs->a31f[f_offset+fin][f_offset+fin];
                bs->a24 = bs->a24f[f_offset+fin][f_offset+fin];
                bs->a42 = bs->a42f[f_offset+fin][f_offset+fin];
                bs->a11 = bs->a11f[f_offset+fin][f_offset+fin];
                bs->a22 = bs->a22f[f_offset+fin][f_offset+fin];
                bs->a33 = bs->a33f[f_offset+fin][f_offset+fin];
                bs->a44 = bs->a44f[f_offset+fin][f_offset+fin];
                
                fill_matrix_beamsplitter_elements(bs, f_list[fin], f_list[fin], 0, do_conj);
            } else {
                for(fout=0; fout< num_frequencies; fout++) {
                    if(matrix->bs_does_f_couple[bs->dither_list_index][fin][fout]) {
                        bs->a12 = bs->a12f[f_offset+fin][f_offset+fout];
                        bs->a21 = bs->a21f[f_offset+fin][f_offset+fout];
                        bs->a34 = bs->a34f[f_offset+fin][f_offset+fout];
                        bs->a43 = bs->a43f[f_offset+fin][f_offset+fout];
                        bs->a13 = bs->a13f[f_offset+fin][f_offset+fout];
                        bs->a31 = bs->a31f[f_offset+fin][f_offset+fout];
                        bs->a24 = bs->a24f[f_offset+fin][f_offset+fout];
                        bs->a42 = bs->a42f[f_offset+fin][f_offset+fout];
                        bs->a11 = bs->a11f[f_offset+fin][f_offset+fout];
                        bs->a22 = bs->a22f[f_offset+fin][f_offset+fout];
                        bs->a33 = bs->a33f[f_offset+fin][f_offset+fout];
                        bs->a44 = bs->a44f[f_offset+fin][f_offset+fout];
                        
                        fill_matrix_beamsplitter_elements(bs, f_list[fin], f_list[fout], matrix->bs_f_couple_order[bs->dither_list_index][fin][fout], do_conj);
                    }
                }
            }
        }

        for(i=0; i<inter.num_gratings; i++){
            grating_t *gr = &inter.grating_list[i];

            gr->a11 = gr->a11f[f_offset+fin];
            gr->a12 = gr->a12f[f_offset+fin];
            gr->a13 = gr->a13f[f_offset+fin];
            gr->a14 = gr->a14f[f_offset+fin];
            gr->a21 = gr->a21f[f_offset+fin];
            gr->a22 = gr->a22f[f_offset+fin];
            gr->a23 = gr->a23f[f_offset+fin];
            gr->a24 = gr->a24f[f_offset+fin];
            gr->a31 = gr->a31f[f_offset+fin];
            gr->a32 = gr->a32f[f_offset+fin];
            gr->a33 = gr->a33f[f_offset+fin];
            gr->a34 = gr->a34f[f_offset+fin];
            gr->a41 = gr->a41f[f_offset+fin];
            gr->a42 = gr->a42f[f_offset+fin];
            gr->a43 = gr->a43f[f_offset+fin];
            gr->a44 = gr->a44f[f_offset+fin];
            
            fill_matrix_grating_elements(gr, f_list[fin]->f, do_conj);
        }

        for(i=0; i<inter.num_spaces; i++){
            space_t *s = &inter.space_list[i];

            s->a12 = s->a12f[f_offset+fin];
            s->a21 = s->a21f[f_offset+fin];

            fill_matrix_space_elements(s, f_list[fin]->f, do_conj);
        }

        for(i=0; i<inter.num_sagnacs; i++){
            sagnac_t *s = &inter.sagnac_list[i];
            s->a12 = s->a12f[f_offset+fin];
            s->a21 = s->a21f[f_offset+fin];
        }

        fill_matrix_sagnac_elements(do_conj);

        for(i=0; i<inter.num_lenses; i++){
            lens_t *l = &inter.lens_list[i];
            l->a12 = l->a12f[f_offset+fin];
            l->a21 = l->a21f[f_offset+fin];
        }

        fill_matrix_lens_elements(do_conj);

        for(i=0; i<inter.num_diodes; i++){
            diode_t *d = &inter.diode_list[i];
            d->a12 = d->a12f[f_offset+fin];
            d->a21 = d->a21f[f_offset+fin];
            d->a23 = d->a23f[f_offset+fin];
            d->a32 = d->a32f[f_offset+fin];
        }

        fill_matrix_diode_elements(do_conj);
    }
    
    // we can no longer use the old modulator filling function as we now have
    // to consider more coupling between frequencies
    // Can't use! fill_matrix_modulator_elements();
    fill_matrix_modulator_elements_new(matrix);
    
    fill_matrix_block_elements(matrix);
    
    fill_matrix_dbs_elements(matrix);
        
    // if we have some optic motion then compute any radiation pressure effects
    if(matrix->type == SIGNAL_QCORR && inter.num_motion_eqns > 0) { 
        fill_matrix_feedback_elements(matrix);
        fill_optic_motion_coupling();
    }
}

void clear_ccs_matrix_rhs(ifo_matrix_vars_t *matrix){
    memset(matrix->rhs_values, 0, sizeof(double)*2*matrix->M.num_eqns);
}

void set_rhs_ccs_matrix(long idx, complex_t value, ifo_matrix_vars_t *matrix){
    assert(idx >= 0 && idx+1 <= matrix->M.num_eqns);
    
    ((complex_t*)matrix->rhs_values)[idx] = value;
}

void refactor_ccs_matrix(ifo_matrix_vars_t *matrix){
    if(options.sparse_solver == KLU_FULL){
        klu_objs_t *ko = (klu_objs_t*)matrix->solver_opts;
        
        
        if(ko->Numeric == NULL){
            dump_matrix_ccs(&M_ifo_sig.M, "klu_full_matrix_sig.dat");
            
            ko->Numeric = klu_zl_factor((UF_long*)M_ifo_sig.M.col_ptr,
                                        (UF_long*)M_ifo_sig.M.row_idx,
                                        M_ifo_sig.M.values, 
                                        ko->Symbolic, &(ko->Common));
            
            if(ko->Common.status == KLU_SINGULAR)
                gerror("Signal matrix is singular!!\n", ko->Common.status);
            else if(ko->Common.status != KLU_OK)
                gerror("An error occurred in KLU during factorisation: STATUS=%i\n", ko->Common.status);

            klu_zl_sort(ko->Symbolic, ko->Numeric, &(ko->Common));
        }
        
        klu_zl_refactor((UF_long*)matrix->M.col_ptr,
                        (UF_long*)matrix->M.row_idx,
                        matrix->M.values, ko->Symbolic, ko->Numeric, &ko->Common);
#if INCLUDE_NICSLU == 1
    } else if(options.sparse_solver == NICSLU) {
        
        nicslu_objs_t *objs = (nicslu_objs_t*)matrix->solver_opts;
        
        if(options.nicslu_threads > 1)
            handle_NICSLU(NicsLUc_ReFactorize_MT(&objs->nicslu, (complex__t*)(void*)matrix->M.values));
        else
            handle_NICSLU(NicsLUc_ReFactorize(&objs->nicslu, (complex__t*)(void*)matrix->M.values));
#endif
#if INCLUDE_PARALUTION == 1
    } else if(options.sparse_solver == PARALUTION) {
        refactor_paralution_matrix(matrix);
#endif
    } else {
        bug_error("Sparse solver not handled");
    }
}

//int a = 0;

void solve_ccs_matrix(ifo_matrix_vars_t *matrix, double *rhs, bool transpose, bool conjugate){
    if(options.sparse_solver == KLU_FULL){
        klu_objs_t *ko = (klu_objs_t*)matrix->solver_opts;
        
        if(transpose) {
            //if (!options.use_coupling_reduction)
					  klu_zl_tsolve(ko->Symbolic, ko->Numeric, matrix->M.num_eqns, 1, rhs, conjugate, &ko->Common);
            //else
            //    klu_zl_tsolve_sparse(ko->Symbolic, ko->Numeric, matrix->M.num_eqns, 1, rhs, matrix->input_num_blocks, matrix->input_blocks, conjugate, &ko->Common);
        } else {
            //if (!options.use_coupling_reduction)
            klu_zl_solve(ko->Symbolic, ko->Numeric, matrix->M.num_eqns, 1, rhs, &ko->Common);
            //else
            //klu_zl_solve_sparse(ko->Symbolic, ko->Numeric, matrix->M.num_eqns, 1, rhs, matrix->input_num_blocks, matrix->input_blocks, &ko->Common);
        }
        
        if(ko->Common.status != 0)
            gerror("KLU error whilst solving: %i", ko->Common.status);
#if INCLUDE_NICSLU == 1        
    } else if(options.sparse_solver == NICSLU) {
        
        nicslu_objs_t *objs = (nicslu_objs_t*)matrix->solver_opts;
        
        handle_NICSLU(NicsLUc_SolveFast(&objs->nicslu, (complex__t*)(void*)rhs));
#endif
#if INCLUDE_PARALUTION == 1        
    } else if(options.sparse_solver == PARALUTION) {
        solve_paralution_matrix(matrix, rhs, transpose, conjugate);
#endif
    } else
        bug_error("Sparse solver not handled");
}

void solve_ccs_matrix_sparse_input(ifo_matrix_vars_t *matrix, double *rhs, bool transpose, long nblocks, long *blocks){
    assert(matrix != NULL);
    assert(rhs != NULL);
    assert(blocks != NULL);
    
    if(options.sparse_solver == KLU_FULL){
        klu_objs_t *ko = (klu_objs_t*)matrix->solver_opts;
        
        if(transpose){
					klu_zl_tsolve_sparse(ko->Symbolic, ko->Numeric, matrix->M.num_eqns, 1, rhs, nblocks, blocks, 1, &ko->Common);
				}
        else{
					klu_zl_solve_sparse(ko->Symbolic, ko->Numeric, matrix->M.num_eqns, 1, rhs, nblocks, blocks, &ko->Common);
				}
        if(ko->Common.status != 0)
            gerror("KLU error whilst solving: %i", ko->Common.status);
#if INCLUDE_NICSLU == 1        
    } else if(options.sparse_solver == NICSLU){
        gerror("NICSLU doesn't support conjugate solving at the moment...");
#endif
#if INCLUDE_PARALUTION == 1
    } else if(options.sparse_solver == PARALUTION){
        bug_error("NOT DONE");
#endif
    } else
        bug_error("Sparse solver not handled");
}

void free_ccs_matrix(ifo_matrix_vars_t *matrix){    
        
    free(matrix->rhs_values);
    free(matrix->M.col_ptr);
    free(matrix->M.row_col_count);
    free(matrix->M.row_idx);
    free(matrix->M.values);
    free(matrix->solver_opts);

    pseudocolumn_t *curr = matrix->pcol_head, *tmp=NULL;

    while(curr != NULL){
        tmp = curr;

        if(curr->next)
            curr = curr->next;
        else
            curr = NULL;

        free(tmp);
    }
}


void dump_matrix_ccs_labels(ifo_matrix_vars_t *ifo_matrix, const char* fname){
    return;
    
    pseudocolumn_t *curr = ifo_matrix->pcol_head;
    FILE *fmat = fopen(fname, "w");
    int i,j,k, n, m;
    int f;
    int rhs_idx = -1;
    int num = 0;
    
    while(curr != NULL){
        const char *name = get_comp_name(curr->type, curr->component);
        char *node;
        char *dir;
        node_t* nodes[4] = {0};
        char sIN[3] = "in";
        char sOUT[4] = "out";
        
        
        if(curr->type == MIRROR){ 
            nodes[0] = &inter.node_list[((mirror_t*)curr->component)->node1_index];
            nodes[1] = &inter.node_list[((mirror_t*)curr->component)->node2_index];
        } else if(curr->type == SPACE){     
            nodes[0] = &inter.node_list[((space_t*)curr->component)->node1_index];
            nodes[1] = &inter.node_list[((space_t*)curr->component)->node2_index];
        } else
            bug_error("Not implemented");
        
        for(j=0; j<4; j++){
            if(nodes[j] == NULL || nodes[j]->gnd_node)
                break;
                        
            node = nodes[j]->name;
            
            for(k=0;k<=1; k++){
                if(nodes[j]->direction == IN_OUT){
                    if(k==0){
                        dir = sIN;
                    } else {
                        dir = sOUT;
                    }
                } else {
                    if(k==0){
                        dir = sOUT;
                    } else {
                        dir = sIN;
                    }
                }
                
                if(ifo_matrix->node_rhs_idx_1[nodes[j]->list_index] < rhs_idx)
                    continue;
                
                rhs_idx = ifo_matrix->node_rhs_idx_1[nodes[j]->list_index];
                
                for(f=0; f<ifo_matrix->num_frequencies; f++){
                    frequency_t *freq = ifo_matrix->frequencies[f];
                    
                    for(i=0; i<inter.num_fields; i++){
                        get_tem_modes_from_field_index(&n, &m, i);
                        num++;
                        fprintf(fmat,"%i %s %s %s %gHz %i%i\n", get_rhs_idx(ifo_matrix, nodes[j], k+1, f, i), name, node, dir, freq->f, n, m);
                    }
                }
            }
        }
        
        curr = curr->next;
    }
    
    assert(num == ifo_matrix->M.num_eqns && "Didn't print all the labels!");
    
    fflush(fmat);
    fclose(fmat);
}

void dump_matrix_ccs(matrix_ccs_t *matrix, const char* fname){
    FILE *fmat = fopen(fname, "w");
    long i=0, cidx = 0, column = 0;

    warn("Printing matrix to file %s...\n", fname);
    
    for(column=0; column<matrix->num_eqns; column++){
        
        for (i=0; i < matrix->row_col_count[column]; i++) {
            if(options.sparse_solver == KLU_FULL) 
                fprintf(fmat,"%ld %ld %.15g %.15g\n", ((UF_long*)matrix->row_idx)[i+cidx]+1, column+1, matrix->values[2 * (i+cidx)], matrix->values[2 * (i+cidx) + 1]);
#if INCLUDE_NICSLU == 1
            else if(options.sparse_solver == NICSLU) 
                fprintf(fmat,"%u %ld %.15g %.15g\n", ((uint__t*)matrix->row_idx)[i+cidx]+1, column+1, matrix->values[2 * (i+cidx)], matrix->values[2 * (i+cidx) + 1]);
#endif
#if INCLUDE_PARALUTION == 1
            else if(options.sparse_solver == PARALUTION) 
                fprintf(fmat,"%i %ld %.15g %.15g\n", ((int*)matrix->row_idx)[i+cidx]+1, column+1, matrix->values[2 * (i+cidx)], matrix->values[2 * (i+cidx) + 1]);
#endif
        }
        
        cidx += i;
    }
    
    fflush(fmat);
    fclose(fmat);
}
