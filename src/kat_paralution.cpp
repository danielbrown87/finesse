#if INCLUDE_PARALUTION == 1

#include "kat_paralution.h"
#include "kat_matrix_ccs.h"

#include <paralution.hpp>

using namespace paralution;

typedef std::complex<double> zcomplex;

/** Stores objects/structs for using paralution as solver for matrices*/
typedef struct paralution_objs {
    LocalVector<int> *permutation;
    LocalMatrix<zcomplex> *M;
    LocalVector<zcomplex> *RHS;
    Solver<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex> *p;
    Solver<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex> *ls;
} paralution_objs_t;

extern "C" {
    int _solve_num = 0; // counter for number of solve steps  
    
    void setup_paralution(int num_threads) {
        assert(num_threads >= 1);
        
        init_paralution();
        set_omp_threads_paralution(num_threads);
        
        info_paralution();
    }

    void setup_paralution_matrix(ifo_matrix_vars_t *M) {
        assert(M != NULL);
        
        paralution_objs_t *objs = (paralution_objs_t*) malloc(sizeof(paralution_objs_t));
        M->solver_opts = (void*)objs;
        
        int **col_ptr = (int**) &M->M.col_ptr;
        int **row_idx = (int**) &M->M.row_idx;
        zcomplex **values = (zcomplex**) &M->M.values;
        zcomplex **rhs = (zcomplex**)&M->rhs_values;
        
        objs->M = new LocalMatrix<zcomplex>();
        
        objs->RHS = new LocalVector<zcomplex>();
        
        objs->M->SetDataPtrCSR(col_ptr, row_idx, values,
                               "Matrix", M->M.num_nonzeros,
                                M->M.num_eqns, M->M.num_eqns);
        objs->RHS->SetDataPtr(rhs, "RHS", M->M.num_eqns);
               
        MultiColoredSGS<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex> *p = 
                new MultiColoredSGS<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex>();
        p->Verbose(3);
        objs->p = p;
        
        GMRES<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex> *gmres = 
                new GMRES<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex>();
        
        gmres->InitTol(0.1, 0.1, 10);
        gmres->SetBasisSize(30);
        gmres->SetPreconditioner(*objs->p);
        
        objs->ls = gmres;
        objs->ls->Verbose(3);
        
        objs->ls->SetOperator(*objs->M);
        objs->ls->Build();
        
        objs->M->LeaveDataPtrCSR(col_ptr, row_idx, values);
        objs->RHS->LeaveDataPtr(rhs);
    }

    void refactor_paralution_matrix(ifo_matrix_vars_t *matrix) {
        paralution_objs_t *objs = (paralution_objs_t*) matrix->solver_opts;

        int **col_ptr = (int**) &matrix->M.col_ptr;
        int **row_idx = (int**) &matrix->M.row_idx;
        zcomplex **values = (zcomplex**) &matrix->M.values;
        zcomplex **rhs = (zcomplex**)&matrix->rhs_values;

        objs->M->SetDataPtrCSR(col_ptr, row_idx, values,
                               "Matrix", matrix->M.num_nonzeros,
                                matrix->M.num_eqns, matrix->M.num_eqns);

//        objs->ls->ResetOperator(*objs->M);
//        objs->p->ResetOperator(*objs->M);
//        objs->p->ReBuildNumeric();
//        objs->ls->ReBuildNumeric();

        objs->M->LeaveDataPtrCSR(col_ptr, row_idx, values);
    }

    void solve_paralution_matrix(ifo_matrix_vars_t *matrix, double *out_rhs, bool transpose, bool conjugate) {
        (void) transpose;
        (void) conjugate;
        
        paralution_objs_t *objs = (paralution_objs_t*) matrix->solver_opts;
        
        int **col_ptr = (int**) &matrix->M.col_ptr;
        int **row_idx = (int**) &matrix->M.row_idx;
        zcomplex **values = (zcomplex**) &matrix->M.values;
        zcomplex **rhs = (zcomplex**)&matrix->rhs_values;
        
        objs->M->SetDataPtrCSR(col_ptr, row_idx, values,
                               "Matrix", matrix->M.num_nonzeros,
                                matrix->M.num_eqns, matrix->M.num_eqns);
        
        objs->RHS->SetDataPtr(rhs, "RHS", matrix->M.num_eqns);
        
        LocalVector<zcomplex> in;
        in.Allocate("RHS", matrix->M.num_eqns);
        in.CopyFromData((zcomplex*)out_rhs);
        
        std::ostringstream tmp;
        tmp << _solve_num++;
        
        objs->M->Transpose();
        objs->ls->Solve(in, objs->RHS);
        
        objs->M->Transpose();
        
        //objs->M->WriteFileMTX("matrix_" + tmp.str() + ".mtx");
        //in.WriteFileASCII("rhs_in_" + tmp.str());
        //objs->RHS->WriteFileASCII("rhs_out_" + tmp.str());
        
        objs->RHS->LeaveDataPtr(rhs);
        objs->M->LeaveDataPtrCSR(col_ptr, row_idx, values);
    }
    
/**
 *  This code tries to use assemble but that doesn't seem to work...
 */
//    void setup_paralution_matrix(ifo_matrix_vars_t *M) {
//        assert(M != NULL);
//        
//        paralution_objs_t *objs = (paralution_objs_t*) malloc(sizeof(paralution_objs_t));
//        M->solver_opts = (void*)objs;
//        
//        int *row_idx = (int*) M->M.row_idx;
//        int *col_idx = (int*) M->M.col_idx;
//        
//        zcomplex *values = (zcomplex*) M->M.values;
//        
//        objs->M = new LocalMatrix<zcomplex>();
//        objs->M->Assemble(row_idx, col_idx, values, M->M.num_nonzeros, "Matrix", M->M.num_eqns, M->M.num_eqns);
//        
//        objs->M->WriteFileMTX("matrix");
//        
//        objs->M->info();
//        
//        objs->RHS = new LocalVector<zcomplex>();
//        
//        ILU<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex> *p = 
//                new ILU<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex>();
//        objs->p = p;
//        
////        Inversion<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex> *inv = 
////                                        new Inversion<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex>();
////        inv->Verbose(1);
////        objs->ls = inv;
//          
//        GMRES<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex> *gmres = 
//                new GMRES<LocalMatrix<zcomplex>, LocalVector<zcomplex>, zcomplex>();
//        
//        gmres->SetPreconditioner(*objs->p);
//        objs->ls = gmres;
//        
//        objs->ls->SetOperator(*objs->M);
//        objs->ls->Build();
//    }
//    
//    
//    void refactor_paralution_matrix(ifo_matrix_vars_t *matrix) {
//        paralution_objs_t *objs = (paralution_objs_t*) matrix->solver_opts;
//        std::cout << "!!! REFACTOR" << std::endl;
//        
////        objs->M->AssembleUpdate((zcomplex*) matrix->M.values);
//        objs->ls->ResetOperator(*objs->M);
//        objs->ls->ReBuildNumeric();
//    }
//    
//    void solve_paralution_matrix(ifo_matrix_vars_t *matrix, double *out_rhs, bool transpose, bool conjugate) {
//        (void) transpose;
//        (void) conjugate;
//        
//        paralution_objs_t *objs = (paralution_objs_t*) matrix->solver_opts;
//        std::cout << "!!! SOLVE" << std::endl;
//        
//        zcomplex **rhs = (zcomplex**)&matrix->rhs_values;
//        objs->RHS->SetDataPtr(rhs, "RHS", matrix->M.num_eqns);
//        
//        LocalVector<zcomplex> in;
//        in.Allocate("RHS", matrix->M.num_eqns);
//        in.CopyFromData((zcomplex*)out_rhs);
//        
//        std::ostringstream tmp;
//        tmp << _solve_num++;
//        
//        objs->ls->Solve(in, objs->RHS);
//        
//        objs->M->WriteFileMTX("matrix_" + tmp.str() + ".mtx");
//        in.WriteFileASCII("rhs_in_" + tmp.str());
//        objs->RHS->WriteFileASCII("rhs_out_" + tmp.str());
//        
//        objs->RHS->LeaveDataPtr(rhs);
//    }
    
    void cleanup_paralution_matrix(ifo_matrix_vars_t *M) {
        assert(M != NULL);
        
        free(M->solver_opts);
    }
    
    void cleanup_paralution() {
        stop_paralution();
    }
}

#endif // INCLUDE_PARALUTION == 1

    
