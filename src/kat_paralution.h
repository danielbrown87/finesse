/* 
 * File:   kat_paralution.h
 * Author: ddb
 *
 * Created on 10 March 2015, 16:06
 */

#if INCLUDE_PARALUTION == 1
#ifndef KAT_PARALUTION_H
#define	KAT_PARALUTION_H

#include "kat_matrix_ccs.h"

#ifdef __cplusplus
extern "C" {
#endif
    
    void setup_paralution(int num_threads);
    void setup_paralution_matrix(ifo_matrix_vars_t *M);
    void cleanup_paralution();
    
    void solve_paralution_matrix(ifo_matrix_vars_t *matrix, double *rhs, bool transpose, bool conjugate);
    void refactor_paralution_matrix(ifo_matrix_vars_t *M);

#ifdef __cplusplus
}
#endif
#endif	/* KAT_PARALUTION_H */
#endif

