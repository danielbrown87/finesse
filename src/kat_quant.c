// $Id$

/*!
 * \file kat_quant.c
 * \brief Quantum noise and radiation pressure specific routines
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#include "kat.h"
#include "kat_inline.c"
#include "kat_quant.h"
#include "kat_spa.h"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_calc.h"
#include "kat_aa.h"
#include "kat_mem.h"
#include "utlist.h"
#include <math.h>

extern const complex_t complex_i;
extern const complex_t complex_1;
extern const complex_t complex_m1;
extern const complex_t complex_0;

//extern init_variables_t in;
extern interferometer_t inter;
extern options_t options;
extern ifo_matrix_vars_t M_ifo_car; // carrier and modulator field matrix
extern ifo_matrix_vars_t M_ifo_sig;  // signal fields and optic degrees of freedom
extern matrix_ccs_t M_qni;

extern double *f_s; //!< Frequency value list [frequency]
extern complex_t ***a_s; //!< Amplitude list [field] [output] [frequency]
extern int *t_s; //!< Type_of_Signal list [frequency]

complex_t *quant_s=NULL, *quant_w=NULL, *quant_v=NULL;

/** For a given light output the demodulation table is generated for both the 
 * frequencies and phases
 */
void fill_demod_f_table(light_out_t *out){
    int i,j,k;
    int Ndm = pow(2, out->num_demods);
    
    for(i=0; i<inter.num_carrier_freqs; i++){
        for(j=0; j < Ndm; j++){
            out->demod_f[i][j] = inter.carrier_f_list[i]->f;
            
            for(k=0; k<out->num_demods; k++){
                // use bit shifting here to determine whether we add a plus or minus
                // to the total frequency and phases
                if(((j >> k) & 0x01)){
                    out->demod_f[i][j] += out->f[k+1];
                } else {
                    out->demod_f[i][j] -= out->f[k+1];
                }
            }
        }
    }
    
    for(k=0; k<Ndm; k++) out->demod_phi[k] = 0;
    
    for(k=0; k<Ndm; k++){
        for(i=0; i<out->num_demods; i++)
            out->demod_phi[k] -= pow(-1, (k >> i) & 0x01) * out->phi[i+1];
    }
}

/**
 * Fills the out->demod_vac_contri linked list with the contribution each
 * carrier field makes to a quantum noise frequency bin. Each contribution can
 * either have 1 or many carriers.
 *  
 * @param out 
 * @param pureVacuum True if no signal sidebands should be considered
 */
void fill_carrier_qnoise_contributions(light_out_t *out, bool pureVacuum) {
    
    demod_vac_t *elt=NULL, *tmp=NULL;
    int i, j, k, count = 0;
    int Ndm = pow(2, out->num_demods);
    
    // if we already have a list of which carriers contribute to which noise bin
    // we need to delete them.
    if(out->demod_vac_contri != NULL) {    
        LL_FOREACH_SAFE(out->demod_vac_contri, elt, tmp) {
            LL_DELETE(out->demod_vac_contri, elt);
            free(elt);
            elt = NULL;
        }
        
        out->demod_vac_contri = NULL;
    }
    
    // now check all frequencies to see if they match up to any signal sidebands
    for(i=0; i < inter.num_carrier_freqs; i++){
        for(j=0; j < Ndm; j++){
            out->demod_f_sig[i][j] = -1;
            // if we should consider non-pure vacuum states (e.g. when using qnoised)
            // then we can check frequencies against any existing signals
            if(!pureVacuum) {
                // check if demodulation hits a signal frequency
                for(k=0; k < inter.num_signal_freqs; k++){
                    if(eq(out->demod_f[i][j], inter.signal_f_list[k]->f)){
                        out->demod_f_sig[i][j] = k;

                        break;
                    }
                }
            }
            
            // if no signal frequency could be found then this is a pure vacuum
            // noise field so need to add it to the contribution list
            if(out->demod_f_sig[i][j] == -1){
                // check if there are any other contributions listed already for this
                // carrier
                if(count > 0){
                    // search the 'f' field for the demodulation frequency
                    LL_SEARCH_SCALAR(out->demod_vac_contri, elt, f, out->demod_f[i][j]);
                }
                
                if(elt){
                    // if we have already found another item with this carrier frequency
                    // then these noises are correlated
                    assert(out->demod_f[i][j] == elt->f);
                    
                    if(elt->num_carriers >= (int)(sizeof(elt->cidx)/sizeof(int)))
                        bug_error("vacuum noise already correlated with maximum num of carriers, need larger array");
                    
                    elt->cidx[elt->num_carriers] = i;
                    elt->phi_idx[elt->num_carriers] = j;
                    elt->num_carriers++;
                } else {
                    demod_vac_t *itm = (demod_vac_t*) calloc(1, sizeof(demod_vac_t));
                    if(itm == NULL) gerror("Could not allocate memory for qnoised vac list item\n");
                    
                    itm->cidx[0] = i;
                    itm->phi_idx[0] = j;
                    itm->num_carriers = 1;
                    itm->f = out->demod_f[i][j];
                    itm->next = NULL;
                    
                    LL_APPEND(out->demod_vac_contri, itm);
                    count++;
                }
                
                elt = NULL;
            }
        }
    }
}

void __fill_qnoised_selection_vector(complex_t factor, complex_t *bc, int os_idx, int oc_idx, int num_demods, double **demod_f, double *demod_phi, int **demod_f_sig, complex_t *s, complex_t *s_copy){
    
    int j, k, i;
    complex_t fsqrt2 = z_by_x(factor, SQRTTWO);
    (void) oc_idx; //silence compiler warning
    (void) **demod_f; //silence compiler warning
                        
    // For each carrier field check if the corresponding demodulated frequency is 
    // a signal sidebands, if so we need to include it in the selection vector. 
    for(j=0; j<inter.num_carrier_freqs; j++){
        frequency_t *fc = inter.carrier_f_list[j];
        int c_idx = get_port_rhs_idx(fc->index, 0);
        
        for(k=0; k<pow(2, num_demods); k++){
            if(demod_f_sig[j][k] >= 0){
                frequency_t *fs = inter.signal_f_list[demod_f_sig[j][k]];
                // if this is a signal sideband frequency this is the product
                // between the j'th carrier and this. 
                
                // RHS vector index of the first mode for this signal sideband
                int s_idx = os_idx + get_port_rhs_idx(fs->index, 0);
                
                if(fs->order > 0){
                    for(i=0; i<inter.num_fields; i++){
                        z_inc_z(&s[s_idx+i], (z_by_ph(z_by_z(bc[c_idx+i], fsqrt2), - demod_phi[k])));
                    }
                } else {
                    for(i=0; i<inter.num_fields; i++){
                        z_inc_z(&s[s_idx+i], cconj(z_by_ph(z_by_z(bc[c_idx+i], fsqrt2), - demod_phi[k])));
                    }
                }
            }
        }
    }
    
    if(s_copy != NULL){
        memcpy(s_copy, s, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
    }
}


/**
 * Computes the selection vector for a given qnoised detector used in quantum noise
 * computation. Depending on the solver used the original selection vector 
 * s may get overwritten with the weighting vector computation. However s
 * is needed again later and a copy can be stored of this if s_copy is not
 * NULL.
 * 
 * @param detector_index
 * @param s
 * @param s_copy
 */
void fill_qnoised_selection_vector(int detector_index, complex_t *s, complex_t *s_copy){
    assert(s != NULL);
                    
    light_out_t *pd = &inter.light_out_list[detector_index];
    
    // rhs vector index of the node port which this detector is attached to
    int os_idx = (int)M_ifo_sig.output_rhs_idx[pd->list_index];
    int oc_idx = (int)M_ifo_car.output_rhs_idx[pd->list_index];
    
    // a vector containing all the carrier frequencies and modes at this output port
    complex_t *bc = &(((complex_t*)M_ifo_car.rhs_values)[oc_idx]);
    
    // Now we must loop over the frequencies that will be used in this demodulation.
    assert(pd->demod_f != NULL && "Demod f table not allocated");
    assert(pd->demod_phi != NULL && "Demod phi table not allocated");
    assert(pd->demod_f_sig != NULL && "Demod f_sig table not allocated");
    
    __fill_qnoised_selection_vector(complex_1, bc, os_idx, oc_idx, pd->num_demods, pd->demod_f, pd->demod_phi, pd->demod_f_sig, s, s_copy);
}

void fill_homodyne_qnoised_selection_vector(homodyne_t *hom, complex_t *s, complex_t *s_copy){
    assert(s != NULL);
                    
    int sidx[2], cidx[2];
    complex_t *bc;
    
    get_homodyne_sig_node_indices(hom->light_out_1, hom->light_out_2, &(sidx[0]), &(sidx[1]));
    get_homodyne_car_node_indices(hom->light_out_1, hom->light_out_2, &(cidx[0]), &(cidx[1]));
    
    // a vector containing all the carrier frequencies and modes at this output port
    bc = &(((complex_t*)M_ifo_car.rhs_values)[cidx[0]]);
    __fill_qnoised_selection_vector(complex_1, bc, sidx[0], cidx[0], 1, hom->demod_f, hom->demod_phi, hom->demod_f_sig, s, s_copy);
    
    bc = &(((complex_t*)M_ifo_car.rhs_values)[cidx[1]]);
    __fill_qnoised_selection_vector(z_by_ph(complex_1, hom->phase), bc, sidx[1], cidx[1], 1, hom->demod_f, hom->demod_phi, hom->demod_f_sig, s, s_copy);
}

void fill_mhomodyne_qnoised_selection_vector(mhomodyne_t *hom, complex_t *s, complex_t *s_copy){
    assert(s != NULL);
                    
    int sidx[2], cidx[2], j;
    complex_t *bc = NULL;
    
    for(j=0; j<hom->num_node_pairs; j++) {
        node_t  *node1 = &inter.node_list[hom->pairs[j].node1_index];
        node_connections_t *nc1 = &inter.node_conn_list[hom->pairs[j].node1_index];
        node_t  *node2 = &inter.node_list[hom->pairs[j].node2_index];
        node_connections_t *nc2= &inter.node_conn_list[hom->pairs[j].node2_index];
        char *name = inter.output_data_list[hom->output_index].name;
        ifo_matrix_vars_t *matrix = &M_ifo_sig;

        sidx[0] = __get_output_rhs_port(matrix, node1, nc1, hom->pairs[j].is_second_beam1, name);
        sidx[1] = __get_output_rhs_port(matrix, node2, nc2, hom->pairs[j].is_second_beam2, name);
        
        
        node1 = &inter.node_list[hom->pairs[j].node1_index];
        nc1 = &inter.node_conn_list[hom->pairs[j].node1_index];
        node2 = &inter.node_list[hom->pairs[j].node2_index];
        nc2= &inter.node_conn_list[hom->pairs[j].node2_index];
        name = inter.output_data_list[hom->output_index].name;
        matrix = &M_ifo_car;

        cidx[0] = __get_output_rhs_port(matrix, node1, nc1, hom->pairs[j].is_second_beam1, name);
        cidx[1] = __get_output_rhs_port(matrix, node2, nc2, hom->pairs[j].is_second_beam2, name);
        
        // TODO - this finding the function index should be put somewhere
        // better really, in some sort of post-parse method.
        int idx = get_function_idx(hom->pairs[j].func_name);

        if(idx == -1){
            gerror("Homodyne detector: Can't find function %s\n", hom->pairs[j].func_name);
        }

        func_command_t *func = &inter.function_list[idx];
        
        // a vector containing all the carrier frequencies and modes at this output port
        bc = &(((complex_t*)M_ifo_car.rhs_values)[cidx[0]]);
        __fill_qnoised_selection_vector(z_by_x(complex_1,func->result), bc, sidx[0], cidx[0], 1, hom->demod_f, hom->demod_phi, hom->demod_f_sig, s, s_copy);

        bc = &(((complex_t*)M_ifo_car.rhs_values)[cidx[1]]);
        __fill_qnoised_selection_vector(z_by_x(complex_m1,func->result), bc, sidx[1], cidx[1], 1, hom->demod_f, hom->demod_phi, hom->demod_f_sig, s, s_copy);
    }
}

/**
 * Returns true if there is nothing attached to this node except one component.
 */
bool is_open_port(int node_index) {
    node_t *n = &inter.node_list[node_index];
    node_connections_t *nc = &inter.node_conn_list[node_index];

    // TODO - check if outputs should be a source of quantum noise ??
    if(((nc->comp_1 == NULL) ^ (nc->comp_2 == NULL)) && !(n->io & 2)){
        return true;
    } else {
        return false;
    }
}

/**
 * For a given qauntum noise input the qnoise->port field is computed.
 * 
 * @param qnoise qnoise_input_t type to set the port for
 */
void set_noise_port(qnoise_input_t *qn, int comp_list_index, int ctype, qnoise_input_type_t qntype, int node_index){
    assert(qn != NULL);
    
    qn->component_index = comp_list_index;
    qn->component_type = ctype;
    qn->type = qntype;
    qn->node = &inter.node_list[node_index];
    
    if(qn->type == OPEN_PORT || qn->type == LASER_INPUT_NOISE || qn->type == SQUEEZED_INPUT) {
        // if we have an open port at this node then some vacuum fluctuations will
        // get in to the system. Thus we need to inject into the component it is
        // attached to. 
        
        // The node direction refers to whether we are going in or out of a component
        // thus into a space is actually out of it
        if(qn->component_type == SPACE){
            qn->port = (qn->node->direction == IN_OUT) ? 2 : 1;
        } else if(qn->component_type == LIGHT_INPUT) {
            qn->port = inter.light_in_list[comp_list_index].node_port;
        } else if(qn->component_type == DIODE && inter.node_list[inter.diode_list[qn->component_index].node3_index].gnd_node) {
            // if the diode is a 2 port diode then we need to inject the
            // noise at node 2 in the output
            qn->port = (qn->node->direction == IN_OUT) ? 2 : 1; 
        } else {
            qn->port = (qn->node->direction == IN_OUT) ? 1 : 2; 
        }
    
    } else if(qn->type == COMPONENT_LOSS) {
        // If the component has a loss then noise is injected in the output of
        // the components node
        qn->port = (qn->node->direction == IN_OUT) ? 2 : 1; 
        
    } else if(qn->type == MODULATOR_NOISE) {
        assert(qn->component_type == MODULATOR);
        qn->port = (qn->node->direction == IN_OUT) ? 2 : 1; 
    } else 
        bug_error("unhandled qnoise type");
}

/**
 * When all components are specified as being in quantum noise inputs, this
 * function actually counts and adds the components which contribute to the
 * inter.quantum_components list and increments the num_quantum_components
 * variable.
 */
void find_quantum_components(){
    assert(inter.all_quantum_components && "all quantum components not sets");
    assert(inter.num_quantum_components == 0 && "no quantum noise input components should have been counted yet");
    int i;
    
    // all light inputs must have some quantum noise
    for(i=0; i<inter.num_light_inputs; i++){
        light_in_t *l = &inter.light_in_list[i];
        inter.quantum_components[inter.num_quantum_components++] = get_overall_component_index(LIGHT_INPUT, l->comp_index);
    }
    
    for(i=0; i<inter.num_mirrors; i++){
        mirror_t *m = &inter.mirror_list[i];
        
        // if the mirror is lossy or has an open port, add it.
        // TODO - need to check for maps here too as they are also lossy technically
        if((1.0 - m->R - m->T > 1e-14) || is_var_tuned(&m->R)
                || is_var_tuned(&m->T)
                || is_open_port(m->node1_index) || is_open_port(m->node2_index)
                || inter.tem_is_set){
            inter.quantum_components[inter.num_quantum_components++] = get_overall_component_index(MIRROR, m->comp_index);
        }
    }
    
    for(i=0; i<inter.num_beamsplitters; i++){
        beamsplitter_t *bs = &inter.bs_list[i];
        
        // if the mirror is lossy or has an open port, add it.
        // TODO - need to check for maps here too as they are also lossy technically
        if((bs->R+bs->T < 1) || is_var_tuned(&bs->R)
                || is_var_tuned(&bs->T)
                || is_open_port(bs->node1_index)
                || is_open_port(bs->node2_index)
                || is_open_port(bs->node3_index)
                || is_open_port(bs->node4_index)
                || inter.tem_is_set){
            inter.quantum_components[inter.num_quantum_components++] = get_overall_component_index(BEAMSPLITTER, bs->comp_index);
        }
    }
    
    for(i=0; i<inter.num_dbss; i++){
        dbs_t *dbs = &inter.dbs_list[i];
        
        if(is_open_port(dbs->node1_index)
                || is_open_port(dbs->node2_index)
                || is_open_port(dbs->node3_index)
                || is_open_port(dbs->node4_index)){
            inter.quantum_components[inter.num_quantum_components++] = get_overall_component_index(DBS, dbs->comp_index);
        }
    }
    
    for(i=0; i<inter.num_spaces; i++){
        space_t *s = &inter.space_list[i];
        
        if(is_open_port(s->node1_index) || is_open_port(s->node2_index))
            inter.quantum_components[inter.num_quantum_components++] = get_overall_component_index(SPACE, s->list_index);
    }
    
    for(i=0; i<inter.num_lenses; i++){
        lens_t *l = &inter.lens_list[i];
        
        if(is_open_port(l->node1_index) || is_open_port(l->node2_index))
            inter.quantum_components[inter.num_quantum_components++] = get_overall_component_index(LENS, l->comp_index);
    }
    
    for(i=0; i<inter.num_diodes; i++){
        diode_t *d = &inter.diode_list[i];
        
        // check if any are open ports or if there is suppression
        if(is_open_port(d->node1_index) || is_open_port(d->node2_index) || is_open_port(d->node3_index)
                ||  d->S != 0.0 || is_var_tuned(&(d->S))){
            inter.quantum_components[inter.num_quantum_components++] = get_overall_component_index(DIODE, d->comp_index);
        }
    }
    
    for(i=0; i<inter.num_modulators; i++){
        modulator_t *m = &inter.modulator_list[i];
        
        // TODO modulators will always just add vacuum noise on the output regardless
        // this won't work for inputing squeezed field into a modulator
        if(is_open_port(m->node1_index) || is_open_port(m->node2_index) || m->loss)
            inter.quantum_components[inter.num_quantum_components++] = get_overall_component_index(MODULATOR, m->comp_index);
    }
}

void print_qnoise_output(const char *comp_name, int node_index, const char *type) {
    node_t *n = &inter.node_list[node_index];
    
    message(" |- %10-.10s - %s Node %s\n", comp_name, type, n->name);
}

void __check_quant_noise_for_node(bool count, int node_index, size_t *num, const char *comp_name, int comp_list_index, int comp_type, double loss, bool loss_tuned){
    if(!inter.node_list[node_index].gnd_node){
        bool isOpenPort = is_open_port(node_index);
        
        if(isOpenPort){
            if(!count) {
                set_noise_port(&inter.qnoise_in_list[*num], comp_list_index, comp_type, OPEN_PORT, node_index);                    
                
                if(inter.printqnoiseinputs)
                    print_qnoise_output(comp_name, node_index, "OPEN PORT");
            }
            
            (*num)++;
        }
        
        // of any loss or if HOM are used as coupling matrix might have losses in
        if(loss != 0 || loss_tuned || inter.tem_is_set){
            if(!count) {
                set_noise_port(&inter.qnoise_in_list[*num], comp_list_index, comp_type, COMPONENT_LOSS, node_index);

                if(inter.printqnoiseinputs)
                    print_qnoise_output(comp_name, node_index, "LOSS");
            }

            (*num)++;
        }
    }
}

/**
 * This function iterates over all the components marked as a quantum input.
 * It should then compute how many uncorrelated quantum inputs are required per
 * component and add them to the inter.qnoise_in_list array.
 * 
 * @param count [true or false] if true will only count how many inputs for every
 *              component.
 * @return 
 */
size_t count_quantum_components(bool count){
    int i;
    size_t num = 0;
    
    // if not counting ensure that the qnoise input list has been allocated
    if(!count)
        assert(inter.qnoise_in_list != NULL);
    
    if(inter.printqnoiseinputs && !count)
        message("\n-- Quantum noise inputs --\n");
    
    for(i=0; i < inter.num_quantum_components; i++){
        int j = get_component_index(inter.quantum_components[i]);
        int type = get_component_type(inter.quantum_components[i]);
        
        // if a component has some loss then vacuum noise will be injected out
        // incoherently at both nodes.
        if(type == MIRROR){
            mirror_t *m = &inter.mirror_list[j];
            bool loss_tuned = is_var_tuned(&m->R) || is_var_tuned(&m->T);
            __check_quant_noise_for_node(count, m->node1_index, &num, m->name, j, type, 1-m->R-m->T, loss_tuned);
            __check_quant_noise_for_node(count, m->node2_index, &num, m->name, j, type, 1-m->R-m->T, loss_tuned);
            
        } else if(type == SPACE){
            space_t *s = &inter.space_list[j];
            
            if(is_open_port(s->node1_index)){
                if(count)
                    num++;
                else
                    set_noise_port(&(inter.qnoise_in_list[num++]), s->list_index, SPACE, OPEN_PORT, s->node1_index);
            }
            
            if(is_open_port(s->node2_index)){
                if(count)
                    num++;
                else
                    set_noise_port(&(inter.qnoise_in_list[num++]), s->list_index, SPACE, OPEN_PORT, s->node2_index);
            }
        
        } else if(type == DIODE){
            diode_t *m = &inter.diode_list[j];
            (void)*m; //silence compiler warning
            // TODO Handle loss at diode properly
            //__check_quant_noise_for_node(count, m->node1_index, &num, m->name, j, type, 0, false);
            //__check_quant_noise_for_node(count, m->node2_index, &num, m->name, j, type, 0, false);
            //__check_quant_noise_for_node(count, m->node3_index, &num, m->name, j, type, 0, false);
            
        } else if(type == LENS){
            lens_t *m = &inter.lens_list[j];
            // TODO Handle loss at lens properly
            __check_quant_noise_for_node(count, m->node1_index, &num, m->name, j, type, 0, false);
            __check_quant_noise_for_node(count, m->node2_index, &num, m->name, j, type, 0, false);
        
        } else if(type == MODULATOR){
            // modulators always add noise regardless of open ports, etc.
            modulator_t *m = &inter.modulator_list[j];
            
            __check_quant_noise_for_node(count, m->node1_index, &num, m->name, j, type, m->loss, is_var_tuned(&m->loss));
            __check_quant_noise_for_node(count, m->node2_index, &num, m->name, j, type, m->loss, is_var_tuned(&m->loss));
            
        } else if(type == BEAMSPLITTER) {
            beamsplitter_t *bs = &inter.bs_list[j];
            bool loss_tuned = is_var_tuned(&bs->R) || is_var_tuned(&bs->T);
            double loss = 1 - bs->R - bs->T;
            __check_quant_noise_for_node(count, bs->node1_index, &num, bs->name, j, type, loss, loss_tuned);
            __check_quant_noise_for_node(count, bs->node2_index, &num, bs->name, j, type, loss, loss_tuned);
            __check_quant_noise_for_node(count, bs->node3_index, &num, bs->name, j, type, loss, loss_tuned);
            __check_quant_noise_for_node(count, bs->node4_index, &num, bs->name, j, type, loss, loss_tuned);
        
        } else if(type == DBS) {
            dbs_t *dbs = &inter.dbs_list[j];
            
            __check_quant_noise_for_node(count, dbs->node1_index, &num, dbs->name, j, type, 0, false);
            __check_quant_noise_for_node(count, dbs->node2_index, &num, dbs->name, j, type, 0, false);
            __check_quant_noise_for_node(count, dbs->node3_index, &num, dbs->name, j, type, 0, false);
            __check_quant_noise_for_node(count, dbs->node4_index, &num, dbs->name, j, type, 0, false);
            
        } else if(type == LIGHT_INPUT) {
            // lasers can have noise input so put this in here
            light_in_t *l = &inter.light_in_list[j];
            
            if(l->noise_value > 0){
                
                if(!count) {
                    if(l->isSqueezed) {
                        set_noise_port(&inter.qnoise_in_list[num], j, type, SQUEEZED_INPUT, l->node_index);
                        inter.qnoise_in_list[num].isSqueezed = true;
                        
                        if(inter.printqnoiseinputs)
                            print_qnoise_output(l->name, l->node_index, "SQUEEZED INPUT");
                    } else {
                        set_noise_port(&inter.qnoise_in_list[num], j, type, LASER_INPUT_NOISE, l->node_index);
                        
                        if(inter.printqnoiseinputs)
                            print_qnoise_output(l->name, l->node_index, "LASER NOISE");
                    }
                }
                
                num++;
            }
        } else {
            bug_error("unhandled component type %i", type);
        }
    }
    
    return num;
}

/**
 * To be used with a UTArray for sorting qnoise inputs by their RHS vector index
 * from low to high.
 * @param a
 * @param b
 * @return 1 if b preceeds a -1 if a preceeds b
 */
int _qnoise_sort(const void *a, const void *b){
    qnoise_input_t *qa = (qnoise_input_t*) ((LL_ptr_el_t*)a)->ptr;
    qnoise_input_t *qb = (qnoise_input_t*) ((LL_ptr_el_t*)b)->ptr;
    
    int ia = M_ifo_sig.node_rhs_idx_1[qa->node->list_index];
    int ib = M_ifo_sig.node_rhs_idx_1[qb->node->list_index];
    
    // first check if the node indices are higher/lower
    if (ia < ib)
        return -1;
    else if (ia > ib)
        return 1;
    else {
        // if both noises are at the same node we need to compare the ports
        if (qa->port < qb->port)
            return -1;
        else if (qa->port > qb->port)
            return 1;
        else
            bug_error("unexpected qnoise ordering whilst sorting, most likely more than 1 noise source at this node's port");
    }
    return 0;
}

/**
 * Allocates and creates the incoherent quantum noise input matrix.
 */
void create_qnoise_input_matrix(matrix_ccs_t *M){
    assert(M != NULL);
    
    M->num_eqns = M_ifo_sig.M.num_eqns;
    
    assert(M->num_eqns != 0);
    
    int i=0, j=0, k=0;
    
    M->row_col_count = (long*) calloc(M->num_eqns+1, sizeof(long));
    M->col_ptr = (long*) calloc(M->num_eqns, sizeof(long));
    
    // first we need to sort the order of the quantum noise inputs with respect
    // to their port RHS index. 
    LL_ptr_el_t *head = NULL, *el;
    LL_ptr_el_t *data = (LL_ptr_el_t*) calloc(inter.num_qnoise_inputs, sizeof(LL_ptr_el_t)); 
    
    for(i=0; i < inter.num_qnoise_inputs; i++){
        data[i].ptr = (void*)&(inter.qnoise_in_list[i]);
        LL_APPEND(head, &(data[i]));
    }

    LL_SORT(head, _qnoise_sort);
    
    // First we loop through the input noises and count how many elements are in 
    // each column
    LL_FOREACH(head, el){
        qnoise_input_t *qin = (qnoise_input_t*) el->ptr;
        
        // set how many non-zero rows will be in each column
        for(j=0; j<inter.num_signal_freqs; j++){
            for(k=0; k<inter.num_fields; k++){
                int m = get_rhs_idx(&M_ifo_sig, qin->node, qin->port, j, k);
                
                assert(M->row_col_count[m] == 0 && "Noise input matrix column has already been added too"); 
                
                if(qin->isSqueezed) {
                    M->row_col_count[m] = 2;
                } else {
                    M->row_col_count[m] = 1;
                }
            }
        }
        
        // create matrix elements pointers for later filling
        if(qin->isSqueezed){
            // Two 2x2 block for upper and lower sidebands between each entangled carrier
            M->num_nonzeros += 2 * inter.num_fields * inter.num_signal_freqs * 2; 
            
            if(inter.light_in_list[qin->component_index].use_entangled_carriers) {
                qin->M_EC_f0u_fcl_noise_inputs = (complex_t***) calloc(inter.num_fields * inter.num_signal_freqs, sizeof(complex_t**));
                qin->M_EC_f0l_fcu_noise_inputs = (complex_t***) calloc(inter.num_fields * inter.num_signal_freqs, sizeof(complex_t**));

                for(i=0; i<(int)inter.num_fields * inter.num_signal_freqs; i++) {
                    qin->M_EC_f0u_fcl_noise_inputs[i] = (complex_t**) calloc(2, sizeof(complex_t*));
                    qin->M_EC_f0l_fcu_noise_inputs[i] = (complex_t**) calloc(2, sizeof(complex_t*));
                }
            } 
            
            qin->M_2x2_noise_inputs = (complex_t***) calloc(inter.num_fields * inter.num_signal_freqs, sizeof(complex_t**));

            for(i=0; i<(int)inter.num_fields * inter.num_signal_freqs; i++) {
                qin->M_2x2_noise_inputs[i] = (complex_t**) calloc(2, sizeof(complex_t*));
            }
        } else {
            M->num_nonzeros += inter.num_fields * inter.num_signal_freqs; // diagonal elements for upper and lower sidebands
            qin->M_diag_noise_inputs = (complex_t**) calloc(inter.num_fields * inter.num_signal_freqs, sizeof(complex_t*));
        }
    }
    
    // allocate the memory for the actual sparse matrix values
    M->values  = (double*) calloc(M->num_nonzeros, sizeof(complex_t));
    
    switch(options.sparse_solver){
        case KLU_FULL:
            M->row_idx = (void*) calloc(M->num_nonzeros, sizeof(UF_long)); break;
#if INCLUDE_NICSLU == 1
        case NICSLU:
            M->row_idx = (void*) calloc(M->num_nonzeros, sizeof(uint__t)); break;
#endif
#if INCLUDE_PARALUTION == 1
        case PARALUTION:
            M->row_idx = (void*) calloc(M->num_nonzeros, sizeof(int)); break;
#endif
        default:
            bug_error("Solver not handled");
    }
    
    int el_idx = 0;
    
    LL_FOREACH(head, el){
        qnoise_input_t *qin = (qnoise_input_t*) el->ptr;
        
        int base_idx = get_rhs_idx(&M_ifo_sig, qin->node, qin->port, 0, 0);
        
        if(qin->isSqueezed) {
            // squeezed inputs require a covariance between the upper and lower
            // sidebands for each mode
            for(j=0; j<inter.num_signal_freqs; j++){
                frequency_t *fs = inter.signal_f_list[j];
                int other;
                light_in_t *lin = & inter.light_in_list[qin->component_index];
                
                bool an_entangled_carrier = lin->use_entangled_carriers;
                
                if(an_entangled_carrier){
                    // is entangled squeezing is possible here, check if
                    // we have a sideband of a squeezed carrier
                    if(!(fs->carrier == lin->f_entangled_zero ||
                            fs->carrier == lin->f)) {
                        an_entangled_carrier = false;
                    }
                }
                
                if(an_entangled_carrier){
                    // first get the other carrier to entangle with
                    frequency_t *other_car = NULL;
                    
                    if(fs->carrier == lin->f) {
                        other_car = lin->f_entangled_zero;
                    } else {
                        other_car = lin->f;
                    }
                    
                    // We then have to get the index of the opposite sideband
                    // of the other carrier...
                    if(fs->order > 0) { // upper sideband...
                        other = other_car->sig_lower->index;
                    } else {
                        other = other_car->sig_upper->index;
                    }
                    
                } else {
                    if(fs->order > 0) { // is upper sideband, so get the lower index
                        other = fs->carrier->sig_lower->index;
                    } else {
                        other = fs->carrier->sig_upper->index;
                    }
                }
                
                // put the lowest index in the column first...
                int idx[] = {min(j, other), max(j, other)};
                int col, row;
                
                if(idx[0] == j) {
                    col = 0;
                } else {
                    col = 1;
                }
                
                zmatrix *noiseM = NULL;
                    
                if(qin->isSqueezed && !an_entangled_carrier) {
                    noiseM = qin->M_2x2_noise_inputs;
                } else {
                    if((fs->order == 1 && fs->carrier == lin->f_entangled_zero) ||
                            (fs->order == -1 && fs->carrier == lin->f)) {
                        // We have the upper sideband of the zero carrier and
                        // lower of the changeable carrier
                        noiseM = qin->M_EC_f0u_fcl_noise_inputs;
                    } else {
                        noiseM = qin->M_EC_f0l_fcu_noise_inputs;
                    }
                }
                
                for(k=0; k<inter.num_fields; k++){
                    row = idx[0] * inter.num_fields + k;
                    
                    set_row_ptr(M, el_idx, base_idx + row);
                    noiseM[row][col] = &(((complex_t*)M->values)[el_idx++]);
                    
                    row = idx[1] * inter.num_fields + k;
                    set_row_ptr(M, el_idx, base_idx + row);
                    noiseM[row][col] = &(((complex_t*)M->values)[el_idx++]);
                }
            }
        } else {
            for(j=0; j<inter.num_signal_freqs; j++){
                for(k=0; k<inter.num_fields; k++){
                    int m = j*inter.num_fields + k;
                    qin->M_diag_noise_inputs[m] = &(((complex_t*)M->values)[el_idx+m]);
                    set_row_ptr(M, el_idx+m, base_idx + m);
                    
                    // if we have an open port then the noise being injected in
                    // is static, it is just vacuum noise, we just need to set it once here.
                    if(qin->type == OPEN_PORT) {
                        qin->M_diag_noise_inputs[m]->re = UNIT_VACUUM/2.0 * (1.0 + inter.signal_f_list[j]->carrier->f /inter.f0);
                        qin->M_diag_noise_inputs[m]->im = 0.0;
                    }
                }
            }
            
            el_idx += inter.num_signal_freqs * inter.num_fields;
        }
    }
    
    assert(el_idx = M->num_nonzeros);
    
    switch(options.sparse_solver){
        case KLU_FULL:
            ((UF_long*)M->col_ptr)[0] = 0;
    
            for(i=1; i < M->num_eqns; i++){ // take previous col pointer and then add how many rows there are
                ((UF_long*)M->col_ptr)[i] += ((UF_long*)M->col_ptr)[i-1] + M->row_col_count[i-1];
            }
            break;
#if INCLUDE_NICSLU == 1
        case NICSLU:
            ((uint__t*)M->col_ptr)[0] = 0;
    
            for(i=1; i < M->num_eqns; i++){ // take previous col pointer and then add how many rows there are
                ((uint__t*)M->col_ptr)[i] += ((uint__t*)M->col_ptr)[i-1] + M->row_col_count[i-1];
            }
            break;
#endif
#if INCLUDE_PARALUTION == 1
        case PARALUTION:
            ((int*)M->col_ptr)[0] = 0;
    
            for(i=1; i < M->num_eqns; i++){ // take previous col pointer and then add how many rows there are
                ((int*)M->col_ptr)[i] += ((int*)M->col_ptr)[i-1] + M->row_col_count[i-1];
            }
            break;
#endif
        default:
            bug_error("Solver not handled");
    }
   
    // get rid of the linked list memory
    free(data);
    
    //dump_matrix_ccs(M, "qnoise_input_structure.dat");
}

void get_knm_loss(qnoise_input_t *qin, double *loss) {
    int k;
    
    if(qin->component_type == MIRROR){
        mirror_t *mirror = &inter.mirror_list[qin->component_index];

        for (k = 0; k < inter.num_fields; k++) {
            if(mirror->node1_index == qin->node->list_index){
                loss[k] = mirror->R*(1 - mirror->k11_sqrd_sum[k]) + mirror->T*(1 - mirror->k21_sqrd_sum[k]);
            } else {
                loss[k] = mirror->R*(1 - mirror->k22_sqrd_sum[k]) + mirror->T*(1 - mirror->k12_sqrd_sum[k]);
            }
        }
    } else if(qin->component_type == BEAMSPLITTER){
        beamsplitter_t *bs = &inter.bs_list[qin->component_index];

        for (k = 0; k < inter.num_fields; k++) {
            if(bs->node1_index == qin->node->list_index){
                loss[k] = bs->R*(1 - bs->k21_sqrd_sum[k]) + bs->T*(1 - bs->k31_sqrd_sum[k]);
            } else if(bs->node2_index == qin->node->list_index){
                loss[k] = bs->R*(1 - bs->k12_sqrd_sum[k]) + bs->T*(1 - bs->k42_sqrd_sum[k]);
            } else if(bs->node3_index == qin->node->list_index){
                loss[k] = bs->R*(1 - bs->k43_sqrd_sum[k]) + bs->T*(1 - bs->k13_sqrd_sum[k]);
            } else if(bs->node4_index == qin->node->list_index){
                loss[k] = bs->R*(1 - bs->k34_sqrd_sum[k]) + bs->T*(1 - bs->k24_sqrd_sum[k]);
            }
        }
    } else if(qin->component_type == DBS){
        dbs_t *dbs = &inter.dbs_list[qin->component_index];

        for (k = 0; k < inter.num_fields; k++) {
            if(dbs->node1_index == qin->node->list_index){
                loss[k] = (1 - dbs->k21_sqrd_sum[k]);
            } else if(dbs->node2_index == qin->node->list_index){
                loss[k] = (1 - dbs->k42_sqrd_sum[k]);
            } else if(dbs->node3_index == qin->node->list_index){
                loss[k] = (1 - dbs->k13_sqrd_sum[k]);
            } else if(dbs->node4_index == qin->node->list_index){
                loss[k] = (1 - dbs->k34_sqrd_sum[k]);
            }
        }
    }  else if(qin->component_type == MODULATOR){
        modulator_t *mod = &inter.modulator_list[qin->component_index];

        for (k = 0; k < inter.num_fields; k++) {
            if(mod->node1_index == qin->node->list_index){
                loss[k] = 1 - mod->k21_sqrd_sum[k];
            } else if(mod->node2_index == qin->node->list_index){
                loss[k] = 1 - mod->k12_sqrd_sum[k];
            }
        }
    } else if(qin->component_type == LENS){
        lens_t *lens = &inter.lens_list[qin->component_index];

        for (k = 0; k < inter.num_fields; k++) {
            if(lens->node1_index == qin->node->list_index){
                loss[k] = 1 - lens->k21_sqrd_sum[k];
            } else if(lens->node2_index == qin->node->list_index){
                loss[k] = 1 - lens->k12_sqrd_sum[k];
            }
        }
    } else if(qin->component_type == SPACE){
        space_t *s = &inter.space_list[qin->component_index];

        for (k = 0; k < inter.num_fields; k++) {
            if(s->node1_index == qin->node->list_index){
                loss[k] = 1 - s->k21_sqrd_sum[k];
            } else if(s->node2_index == qin->node->list_index){
                loss[k] = 1 - s->k12_sqrd_sum[k];
            }
        }
    } else if(qin->component_type == DIODE){
        diode_t *diode = &inter.diode_list[qin->component_index];

        for (k = 0; k < inter.num_fields; k++) {
            if(diode->node1_index == qin->node->list_index){
                loss[k] = 1 - diode->k21_sqrd_sum[k];
            } else if(diode->node2_index == qin->node->list_index){
                loss[k] = 2 - diode->k12_sqrd_sum[k] - diode->k32_sqrd_sum[k];
            }  else if(diode->node3_index == qin->node->list_index){
                loss[k] = 1 - diode->k23_sqrd_sum[k];
            }
        }
    } else
        bug_error("Not handled\n");
}


int count;

void get_squeezing_AB(light_in_t *l, int k, complex_t *A, complex_t *B){
    double sq_dB, r, expr, expmr, coshr, sinhr;
    complex_t phs, noise;
    
    // need to ensure we inject at least vacuum noise
    // regardless of mode content scale of input
    noise = z_by_x(complex_1, max(l->noise_value, UNIT_VACUUM) /2.0);

    // Phase (angle) and squeezing factor in dB. The phase
    // is the sum of the squeezer phase and the phsae set in 
    // the tem command. The squeezing factor is the product
    // of the squeezer factor (dB) and the tem factor.
    phs = z_by_ph(complex_1, 2.0 * (zdeg(l->power_coeff_list[k]) + l->squeeze_angle));
    sq_dB = l->squeeze_db*zabs(l->power_coeff_list[k]);

    // convert db to the actual r factor r = r_db / (20 log_10(e^1)))
    r = sq_dB / R2DB;

    expr = exp(2*r);
    expmr = exp(-2*r);

    coshr = (expr + expmr) * 0.5;
    sinhr = (expr - expmr) * 0.5;

    *A = z_by_x(noise, coshr);
    *B = z_by_x(z_by_z(noise, phs), sinhr);
}

/**
 * Fills the incoherent quantum noise input matrix.
 */
void fill_qnoise_input_matrix(){
    int i,j,k;
    
    for(i=0; i < inter.num_qnoise_inputs; i++){
        qnoise_input_t *qin = &inter.qnoise_in_list[i];
        
        // need to count how many non-zeros we have in the input matrix for 
        // allocating memory. 
        if(qin->type == LASER_INPUT_NOISE){
            assert(qin->component_type == LIGHT_INPUT);
            assert(!qin->isSqueezed);
            
            light_in_t *l = &inter.light_in_list[qin->component_index];
            
            // quantum noise is at all modes so we just inject the amount of
            // laser noise at each signal frequency and mode
            if(!qin->isSqueezed){
                assert(qin->M_diag_noise_inputs != NULL);
                
                for(j=0; j<inter.num_signal_freqs; j++){
                    for(k=0; k<inter.num_fields; k++){
                        qin->M_diag_noise_inputs[j*inter.num_fields + k]->re = max(l->noise_value, UNIT_VACUUM)/2.0 * (1.0 + inter.signal_f_list[j]->carrier->f /inter.f0);
                        qin->M_diag_noise_inputs[j*inter.num_fields + k]->im = 0;
                    }       
                }
            } else {
                bug_error("not implemented");
            }
        } else if(qin->type == SQUEEZED_INPUT) {   
            
            assert(qin->component_type == LIGHT_INPUT);
            assert(qin->isSqueezed);
            
            light_in_t *l = &inter.light_in_list[qin->component_index];
            
            // quantum noise is at all modes so we just inject the amount of
            // laser noise at each signal frequency and mode
            complex_t A, B;
            
            if(l->use_entangled_carriers){
                assert(qin->M_EC_f0l_fcu_noise_inputs != NULL);
                assert(qin->M_EC_f0u_fcl_noise_inputs != NULL);
            } else {
                assert(qin->M_2x2_noise_inputs != NULL);
            }
            
            zmatrix *noiseM = NULL;
            
            for(j=0; j<inter.num_signal_freqs; j++){
                frequency_t *fs = inter.signal_f_list[j];
                
                bool an_entangled_carrier = l->use_entangled_carriers;
                
                if(an_entangled_carrier){
                    // is entangled squeezing is possible here, check if
                    // we have a sideband of a squeezed carrier
                    if(!(fs->carrier == l->f_entangled_zero ||
                            fs->carrier == l->f)) {
                        an_entangled_carrier = false;
                    }
                }
                
                for(k=0; k<inter.num_fields; k++){
                    // only apply squeezing to the squeezed laser frequency
                    if(qin->isSqueezed && fs->carrier == l->f) {
                        get_squeezing_AB(l, k, &A, &B);
                    } else if(qin->isSqueezed && an_entangled_carrier) {
                        get_squeezing_AB(l, k, &A, &B);
                    } else {
                        A = co(0.5 * UNIT_VACUUM, 0.0);
                        B = complex_0;
                    }

                    int row = j * inter.num_fields + k;
                    
                    if(qin->isSqueezed && !an_entangled_carrier) {
                        noiseM = qin->M_2x2_noise_inputs;
                    } else {
                        if((fs->order == 1 && fs->carrier == l->f_entangled_zero) ||
                                (fs->order == -1 && fs->carrier == l->f)) {
                            // We have the upper sideband of the zero carrier and
                            // lower of the changeable carrier
                            noiseM = qin->M_EC_f0u_fcl_noise_inputs;
                        } else {
                            noiseM = qin->M_EC_f0l_fcu_noise_inputs;
                        }
                    }
                    
                    if(qin->isSqueezed && !an_entangled_carrier) {
                        // Do usual squeezing
                        if(fs->order > 0){
                            *noiseM[row][0] = A;
                            *noiseM[row][1] = B;
                        } else {
                            *noiseM[row][0] = cconj(B);
                            *noiseM[row][1] = cconj(A);
                        }
                    } else {
                        if (noiseM == qin->M_EC_f0u_fcl_noise_inputs) {
                            if(fs->order > 0){
                                *noiseM[row][0] = A; // co(1,0);
                                *noiseM[row][1] = B; // co(2,0);
                            } else {
                                *noiseM[row][0] = cconj(B); //co(3,0);
                                *noiseM[row][1] = cconj(A); //co(4,0);
                            }
                        }
                        
                        
                        if (noiseM == qin->M_EC_f0l_fcu_noise_inputs) {
                            if(fs->order > 0){
                                *(noiseM[row][0]) = cconj(B); //co(3,1);
                                *(noiseM[row][1]) = cconj(A); //co(4,1);
                            } else {
                                *(noiseM[row][0]) = A; //co(1,1);
                                *(noiseM[row][1]) = B; //co(2,1);
                            }
                        }
                    }
                }       
            }
            
        } else if(qin->type == OPEN_PORT) {    
            assert(!qin->isSqueezed && "can't have squeezed open port");
            assert(qin->M_diag_noise_inputs != NULL);
            assert(qin->M_2x2_noise_inputs == NULL);
            
            /* We don't actually have to so anything for the open port
               quantum noise input, the value is constant and set during the
               building of the matrix. Unless it is a 2 port diode, in which
             * case we have to inject the noise into node 2 output
             */
            
            if(qin->component_type == DIODE) {
                diode_t *d = &inter.diode_list[qin->component_index];
                double out = sqrt(1.0 - pow(10.0, -d->S / 10.0));
                
                for(j=0; j<inter.num_signal_freqs; j++){
                    for(k=0; k<inter.num_fields; k++){
                        qin->M_diag_noise_inputs[j*inter.num_fields + k]->re = out/2.0 * (1.0 + inter.signal_f_list[j]->carrier->f /inter.f0);
                        qin->M_diag_noise_inputs[j*inter.num_fields + k]->im = 0;
                    }       
                }
            }
        } else if(qin->type == MODULATOR_NOISE) {
            assert(qin->component_type == MODULATOR);
            assert(!qin->isSqueezed && "can't have squeezed modulator noise");
            
            for(j=0; j<inter.num_signal_freqs; j++){
                for(k=0; k<inter.num_fields; k++){
                    qin->M_diag_noise_inputs[j*inter.num_fields + k]->re = 0;//UNIT_VACUUM/2.0 * (1.0 + inter.signal_f_list[j]->carrier->f /inter.f0);
                    qin->M_diag_noise_inputs[j*inter.num_fields + k]->im = 0;
                }       
            }
            
        } else if(qin->type == COMPONENT_LOSS) {
            assert(!qin->isSqueezed && "can't have squeezed component loss");
            assert(qin->M_diag_noise_inputs != NULL);
            assert(qin->M_2x2_noise_inputs == NULL);
            
            double loss = 0;
            
            // set internal amount of power loss.
            if(qin->component_type == MIRROR) {
                loss = 1 - inter.mirror_list[qin->component_index].R - inter.mirror_list[qin->component_index].T;
            } else if(qin->component_type == BEAMSPLITTER) {
                loss = 1 - inter.bs_list[qin->component_index].R - inter.bs_list[qin->component_index].T;
            } else if(qin->component_type == DIODE) {
                loss = 1 - inter.diode_list[qin->component_index].loss;
            } else {
                //bug_error("not handled (%i)", qin->component_type);
                // ddb - this isn't a bug if it's missing, just no internal loss
                loss = 0;
            }
            
            // Here the loss computation differs depending on whether we have
            // higher order modes or not.
            if (inter.tem_is_set){
                // With HOM the coupling matrices *might*
                // introduce a per mode losses because power is scattered out
                // so vacuum noise must be injected back in again.
                double *loss_knm = (double*)calloc(sizeof(double), inter.num_fields);
                
                get_knm_loss(qin, loss_knm);
                
                for(j=0; j<inter.num_signal_freqs; j++){
                    for(k=0; k<inter.num_fields; k++){
                        qin->M_diag_noise_inputs[j*inter.num_fields + k]->re = (loss_knm[k] + loss)/2.0;
                        qin->M_diag_noise_inputs[j*inter.num_fields + k]->im = 0;
                    }       
                }
                
                free(loss_knm);
            } else {
                /* If this component has some loss then we need to fill this in */
                for(j=0; j<inter.num_signal_freqs; j++){
                    for(k=0; k<inter.num_fields; k++){
                        qin->M_diag_noise_inputs[j*inter.num_fields + k]->re = loss/2.0;
                        qin->M_diag_noise_inputs[j*inter.num_fields + k]->im = 0;
                    }       
                }
            }
        } else {
            bug_error("unhandled value %i", qin->type);
        }
    }
    
}

int __sort_int(const void *a,const void*b) {
    int _a = *(int*)a;
    int _b = *(int*)b;
    return _a - _b;
}

/**
 * For all components that have been marked with the vacuum command the nodes that
 * need to be computed are marked and put in a list for iterating over later.
 */
void init_quantum_components(){
    // first check if we have any quantum
    int i=0, use_quantum = 0;
    int j;
     
    // if any qshot/qnoised detectors are used then we need to allocate memory for the
    // demodulation tables
    for(i=0; i<inter.num_light_outputs; i++){
        light_out_t *out = &inter.light_out_list[i];

        if(inter.output_data_list[out->output_idx].quantum_scaling == NOTSET)
            inter.output_data_list[out->output_idx].quantum_scaling = init.default_quantum_output_scale;
        
        if(inter.output_data_list[out->output_idx].detector_type == QSHOT ||
                inter.output_data_list[out->output_idx].detector_type == QNOISE){
            
            out->demod_f = (double**)malloc(sizeof(double*) * inter.num_carrier_freqs);
            out->demod_phi = (double*)calloc(pow(2,out->num_demods), sizeof(double));
            out->demod_f_sig = (int**)malloc(sizeof(int*) * inter.num_carrier_freqs);
            
            for(j=0; j<inter.num_carrier_freqs; j++){
                out->demod_f[j] = (double*)calloc(pow(2,out->num_demods), sizeof(double));
                out->demod_f_sig[j] = (int*)calloc(pow(2,out->num_demods), sizeof(int));
            }
        }
    }
    
    // check here for outputs that require the quantum noise matrix being solved
    for(i=0;i<inter.num_outputs;i++){
        int t = inter.output_data_list[i].detector_type;
        if(t == QNOISE || t == SD || t ==QD)
            use_quantum++;
        
        if(t == HOMODYNE && inter.homodyne_list[inter.output_data_list[i].detector_index].is_quantum)
            use_quantum++;
        
        
        if(t == MHOMODYNE && inter.mhomodyne_list[inter.output_data_list[i].detector_index].is_quantum)
            use_quantum++;
    }
    
    // if none, don't initialise everything
    if(!use_quantum)
        return;
    
    // if all components have been set to be quantum noise inputs we must first
    // determine which of them definitely have some effect, e.g. have an open port
    // are a laser, or have some loss.
    if(inter.all_quantum_components) {
        find_quantum_components();
        assert(inter.num_quantum_components <= mem.num_quantum_components);
    }
    
    inter.num_qnoise_inputs = count_quantum_components(true);
    
    if(inter.num_qnoise_inputs > 0 && inter.num_signal_freqs > 0){
        int err = allocate_memory_for_quantum_input_list();

        if(err)
            gerror("Could not allocate memory for the quantum output matrix");
        
        // now iterate over and fill the new quantum input list
        count_quantum_components(false);
        
        // now create and fill the variance-covariance matrix with the noise inputs
        create_qnoise_input_matrix(&M_qni);
        
        quant_s = (complex_t*) malloc(M_ifo_sig.M.num_eqns * sizeof(complex_t)); // Selection vector
        quant_w = (complex_t*) malloc(M_ifo_sig.M.num_eqns * sizeof(complex_t)); // Input weighting vector, w = M^{-1 T} s
        quant_v = (complex_t*) malloc(M_ifo_sig.M.num_eqns * sizeof(complex_t)); // Input vector, v = <a a^T> w
    }
    
    for(i=0; i<inter.num_homodynes; i++){
        homodyne_t *out = &inter.homodyne_list[i];
        
        out->demod_f = (double**)malloc(sizeof(double*) * inter.num_carrier_freqs);
        out->demod_phi = (double*)calloc(2, sizeof(double) );
        out->demod_f_sig = (int**)malloc(sizeof(int*) * inter.num_carrier_freqs);
        
        for(j=0; j<inter.num_carrier_freqs; j++){
            out->demod_f[j] = (double*)calloc(2, sizeof(double));
            out->demod_f_sig[j] = (int*)calloc(2, sizeof(int));
        }
        
        if(inter.output_data_list[out->output_index].quantum_scaling == NOTSET)
            inter.output_data_list[out->output_index].quantum_scaling = init.default_quantum_output_scale;
    }
    
    for(i=0; i<inter.num_mhomodynes; i++){
        mhomodyne_t *out = &inter.mhomodyne_list[i];
        
        out->demod_f = (double**)malloc(sizeof(double*) * inter.num_carrier_freqs);
        out->demod_phi = (double*)calloc(2, sizeof(double) );
        out->demod_f_sig = (int**)malloc(sizeof(int*) * inter.num_carrier_freqs);
        
        for(j=0; j<inter.num_carrier_freqs; j++){
            out->demod_f[j] = (double*)calloc(2, sizeof(double));
            out->demod_f_sig[j] = (int*)calloc(2, sizeof(int));
        }
        
        if(inter.output_data_list[out->output_index].quantum_scaling == NOTSET)
            inter.output_data_list[out->output_index].quantum_scaling = init.default_quantum_output_scale;
    }
    
    for(i=0; i<inter.num_slinks; i++){
        slink_t *fb = &inter.slink_list[i];
        light_out_t *out = &inter.light_out_list[fb->output_list_idx];

        if(inter.output_data_list[out->output_idx].detector_type == QSHOT ||
                inter.output_data_list[out->output_idx].detector_type == QNOISE ||
                inter.output_data_list[out->output_idx].detector_type == PD1){
            
            
            if(out->demod_f == NULL) out->demod_f = (double**)malloc(sizeof(double*) * inter.num_carrier_freqs);
            if(out->demod_phi == NULL) out->demod_phi = (double*)calloc(pow(2,out->num_demods), sizeof(double));
            if(out->demod_f_sig == NULL) out->demod_f_sig = (int**)malloc(sizeof(int*) * inter.num_carrier_freqs);
            
            for(j=0; j<inter.num_carrier_freqs; j++){
                if(out->demod_f[j] == NULL) out->demod_f[j] = (double*)calloc(pow(2,out->num_demods), sizeof(double));
                if(out->demod_f_sig[j] == NULL) out->demod_f_sig[j] = (int*)calloc(pow(2,out->num_demods), sizeof(int));
            }
        }
    }
    
    // do a final sweep of all dump nodes and warn about when they are attached
    // to components that might couple vacuum noise into the system but won't
    node_t *node;
    int n;
    
    UT_array *comps;
    utarray_new(comps, &ut_int_icd);
    
    for(n=0; n<inter.num_nodes; n++){
        node = &inter.node_list[n];
        
        if(!node->gnd_node)
            continue;
        
        node_connections_t *nc = &inter.node_conn_list[n];
        
        if((nc->comp_1 != NULL) && (nc->comp_2 != NULL))
            bug_error("dump node attached to two components, this shouldn't happen");
        
        if((nc->comp_1 == NULL) && (nc->comp_2 == NULL))
            bug_error("dump node not attached to any components, this shouldn't happen");
        
        void *cmp = (nc->comp_1 != NULL) ? nc->comp_1 : nc->comp_2;
        int type  = (nc->comp_1 != NULL) ? nc->type_1 : nc->type_2;
        int foundidx = -1;
        
        if( type == MIRROR) {
            mirror_t *m = (mirror_t*) cmp;
            
            if (m->T != 0) {
                foundidx = get_component_index_from_name(m->name);
            }
            
        } else if(type == BEAMSPLITTER){
            beamsplitter_t *bs = (beamsplitter_t*) cmp;
            if (bs->T != 0) {
                foundidx = get_component_index_from_name(bs->name);   
            }
            
            // if one node and one dump node on each side with non-zero R
            if(bs->R > 0 && 
                    ((!inter.node_list[bs->node1_index].gnd_node ^ !inter.node_list[bs->node2_index].gnd_node)
                    || (!inter.node_list[bs->node3_index].gnd_node ^ !inter.node_list[bs->node4_index].gnd_node))){
                foundidx = get_component_index_from_name(bs->name);
            }
        } else if(type == LENS) {
            foundidx = get_component_index_from_name(get_comp_name(type, cmp));
        }  else if(type == DBS) {
            foundidx = get_component_index_from_name(get_comp_name(type, cmp));
        }  else if(type == DIODE) {
            foundidx = get_component_index_from_name(get_comp_name(type, cmp));
        }  else if(type == SPACE) {
            foundidx = get_component_index_from_name(get_comp_name(type, cmp));
        }  else if(type == MODULATOR) {
            foundidx = get_component_index_from_name(get_comp_name(type, cmp));
        }
        
        if(foundidx >= 0) {
            utarray_sort(comps, __sort_int);
            if(!utarray_find(comps, &foundidx, __sort_int)){
                utarray_push_back(comps, &foundidx);
            }
        }
    }
    
    int *p;
    if(utarray_len(comps)){
        char *msg = "Found dump nodes attached to components that have non-zero transmission.\n"
                    "   This will produce incorrect results for quantum noise calculations as noise\n"
                    "   is lost and not injected back in. Change dump nodes to proper nodes at the\n"
                    "   at the following components:\n";
        
        warn(msg);
        
        for(p=(int*)utarray_front(comps);
                p!=NULL;
                p=(int*)utarray_next(comps,p)) {
            int type = get_component_type_decriment_index(p);
            warn("     - %s\n", get_comp_name2(type, *p));
        }
    }
    
    utarray_free(comps);
}

int ___a = 0;
void compute_q_matrix() {
    int n, m;
    fill_qnoise_input_matrix();
    char name[100] = {0};
    sprintf(name, "qnoise.%i", ___a);
    FILE *f = fopen(name,"w");
    ___a++;
    
    for(n=0; n < M_ifo_sig.M.num_eqns; n++){
        // zero the memory for the selection vector filling
        memset(quant_w, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
        memset(quant_v, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);

        // fill selection vector to select column
        quant_w[n] = complex_1;
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_w, true, true);
        ccs_zgemv(&M_qni, quant_w, quant_v);
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_v, false, false);
        
        for(m=0; m < M_ifo_sig.M.num_eqns; m++){
            fprintf(f,"%s ", complex_form(quant_v[m]));
        }
        fprintf(f,"\n");
    }
    
    fclose(f);
}

double compute_qshot_output(int detector_index) {
    assert(detector_index >= 0);
    assert(detector_index < inter.num_light_outputs);
    
    light_out_t *out = &inter.light_out_list[detector_index];
    
    fill_demod_f_table(out);
    fill_carrier_qnoise_contributions(out, true);
    
    // Now we must loop over the contributions from the demodulation for frequencies
    // that are not signal sidebands, i.e. we pick up pure vacuum noise and demodulate
    // that into our signal.
    demod_vac_t *el=NULL;
    // index of the output port in the RHS vector for the carrier matrix
    uint64_t oc_idx = M_ifo_car.output_rhs_idx[out->list_index];
    int i, j,k;
    // a vector containing all the carrier frequencies and modes at this output port
    complex_t *bc = &(((complex_t*)M_ifo_car.rhs_values)[oc_idx]);
    
    double rtn = 0.0;
    
    LL_FOREACH(out->demod_vac_contri, el) {
        
        if(el->num_carriers > 1){
            for(i=0; i<inter.num_fields; i++){
                complex_t val = complex_0;
                
                for(j=0; j<el->num_carriers; j++){
                    k = get_port_rhs_idx(el->cidx[j], i);
                    z_inc_z(&val, z_by_ph(cconj(bc[k]), out->demod_phi[el->phi_idx[j]]));
                }
                
                rtn += zabs(val) * (1.0 + el->f / inter.f0);
            }
        } else {
            j = inter.num_fields*el->cidx[0]; // RHS index of carrier at port
            double val = 0;
            for(i=0; i<inter.num_fields; i++){
                val += zabs(bc[j+i]);
            }
            
            rtn += val * (1.0 + el->f / inter.f0);
        }
    }
   
    for(k=0; k<out->num_demods; k++){
        // check if any demodulations are at a signal frequency, regardless of 
        // ordering
        
        if(eq(out->f[k+1], inter.fsig)) {
            if (inter.debug & 8) {
                message("qshot %s compensating signal mixer 0.5 factor\n", out->name);
            }
            // compensate for demod 0.5 factor if demodulating a signal frequency
            // as this is what a network analyser would do.
            rtn *= 2.0;
            break;
        }   
    }
    
    // Scale the result in either factors of hf or not, or PSD or ASD
    switch(inter.output_data_list[out->output_idx].quantum_scaling){
        case PSD_HF:
            return UNIT_VACUUM * rtn * pow(0.25, out->num_demods);
        case PSD:
            return UNIT_VACUUM * rtn * H_PLANCK * inter.f0 * pow(0.25, out->num_demods);
        case ASD_HF:
            return sqrt(UNIT_VACUUM * rtn * pow(0.25, out->num_demods));
        case ASD:
            return sqrt(UNIT_VACUUM * rtn * H_PLANCK * inter.f0 * pow(0.25, out->num_demods));
        case NOTSET:
            bug_error("No scaling set");
    }
    
    bug_error("Unknown scale for shotnoise computation");
    return 0; //silence compiler warning.
}

void print_covariance_matrix(light_out_t *sd){
    assert(sd != NULL);
    
    char filename[FILENAME_MAX] = {0};
    char filenamev[FILENAME_MAX] = {0};
    
    sprintf(filename, "B_%s.mat", sd->name);
    sprintf(filenamev, "B_%s.vec", sd->name);
    
    FILE *file = fopen(filename, "w");
    FILE *filev = fopen(filenamev, "w");
    
    if(inter.num_signal_freqs > 0 && inter.num_qnoise_inputs > 0) {
        fill_qnoise_input_matrix();
        
        int i, j, k, l;
        // the RHS index of the node where this detector has been placed
        uint64_t d_idx = M_ifo_sig.output_rhs_idx[sd->list_index];
        
        // zero the memory for the selection vector filling
        memset(quant_w, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
        memset(quant_v, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
        
        //fill_qnoised_selection_vector(sd->list_index, quant_s, quant_w);
        
        for(i=0; i<inter.num_signal_freqs; i++){
            for(j=0; j<inter.num_fields; j++){
                fprintf(filev, "%E\n", zabs(quant_s[d_idx+get_port_rhs_idx(i, j)]));
            }
        }
        
        fclose(filev);
        
        for(i=0; i<inter.num_signal_freqs; i++){
            for(j=0; j<inter.num_fields; j++){
                // get the RHS index of each signal frequency and mode
                uint64_t idx = d_idx + get_port_rhs_idx(i, j);

                // zero the memory for the selection vector filling
                memset(quant_w, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
                memset(quant_v, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
                
                // fill selection vector to select column
                quant_w[idx] = complex_1;
                // compute the w = M^-1^T s, to get the weighting vector
                solve_ccs_matrix(&M_ifo_sig, (double*)quant_w, true, true);
                // apply input noise matrix the weighting vectors
                // i = M_qn * w
                ccs_zgemv(&M_qni, quant_w, quant_v);
                // now compute the final vector <b b^T>s = M^-1 v
                solve_ccs_matrix(&M_ifo_sig, (double*)quant_v, false, false);
                
                // Now we have selected out one column, write it to the file...
                for(k=0; k<inter.num_signal_freqs; k++){
                    for(l=0; l<inter.num_fields; l++){
                        fprintf(file, "%.2E+%.2Ej  ", quant_v[d_idx+get_port_rhs_idx(k, l)].re, quant_v[d_idx+get_port_rhs_idx(k, l)].im);
                    }
                }
                
                fprintf(file, "\n");
            }
        }
    }
    
    fclose(file);
}

void compute_quadrature(light_out_t *sd, double *quad){
    
    assert(sd != NULL);
    
    // set default return values
    *quad = 0;
    
    if(inter.num_signal_freqs > 0 && inter.num_qnoise_inputs > 0) {
        fill_qnoise_input_matrix();
        frequency_t *f = get_carrier_frequency(sd->freq);
        
        if(f == NULL) // return default values set previously
            return;
        
        uint64_t cidx = M_ifo_car.output_rhs_idx[sd->list_index] + get_port_rhs_idx(f->index, sd->field);
        complex_t carrier_field = ((complex_t*)M_ifo_car.rhs_values)[cidx];
        
        uint64_t idx[2];
        
        idx[0] = M_ifo_sig.output_rhs_idx[sd->list_index] + get_port_rhs_idx(f->sig_upper->index, sd->field);
        idx[1] = M_ifo_sig.output_rhs_idx[sd->list_index] + get_port_rhs_idx(f->sig_lower->index, sd->field);
        
        complex_t c = z_by_ph(complex_1, zdeg(carrier_field) + sd->homodyne_angle);
        
        // zero the memory for the selection vector filling
        memset(quant_w, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
        memset(quant_v, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);

        // fill selection vector to select column
        quant_w[idx[0]] = c;
        quant_w[idx[1]] = cconj(c);
        
        // compute the w = M^-1^T s, to get the weighting vector
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_w, true, true);

        // apply input noise matrix the weighting vectors
        // i = M_qn * w
        ccs_zgemv(&M_qni, quant_w, quant_v);

        // now compute the final vector <b b^T>s = M^-1 v
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_v, false, false);

        *quad = z_pl_z(z_by_z(quant_v[idx[1]],c), z_by_zc(quant_v[idx[0]],c)).re;
    }
}

/**
 * The code has been built to assume one node per output, but homodyne requires
 * 2 for the output thus we play a trick to store the 2 node indices in the 
 * output_rhs_idx to store the 2 RHS indices for each node for one output.
 */
void get_homodyne_sig_node_indices(int light_out_1, int light_out_2, int *sidx1, int *sidx2){
    assert(sidx1 != NULL);
    assert(sidx2 != NULL);
    
    *sidx1 = M_ifo_sig.output_rhs_idx[light_out_1];
    *sidx2 = M_ifo_sig.output_rhs_idx[light_out_2];
}

/**
 * The code has been built to assume one node per output, but homodyne requires
 * 2 for the output thus we play a trick to store the 2 node indices in the 
 * output_rhs_idx to store the 2 RHS indices for each node for one output.
 */
void get_homodyne_car_node_indices(int light_out_1, int light_out_2, int *idx1, int *idx2){
    assert(idx1 != NULL);
    assert(idx2 != NULL);
    
    *idx1 = M_ifo_car.output_rhs_idx[light_out_1];
    *idx2 = M_ifo_car.output_rhs_idx[light_out_2];
}

double compute_homodyne_output(homodyne_t *out) {
    assert(out != NULL);
    
    if(inter.num_signal_freqs == 0)
        return 0;
        
    int sidx1, sidx2;
    
    get_homodyne_sig_node_indices(out->light_out_1, out->light_out_2, &sidx1, &sidx2);
    
    int i;
    double rtn = 0;
    
    // return 0 if no noises are present
    if(inter.num_qnoise_inputs <= 0){
        warn("No quantum noise sources set but qnoised used");
        return 0;
    }
    
    // need to fill in the homodyne demodulation table, although we aren't
    // demodulating per se, we are demodulating at the signal frequency
    for(i=0; i<inter.num_carrier_freqs; i++){
        frequency_t *f = inter.carrier_f_list[i];
        out->demod_f[i][0] = f->f + inter.fsig;
        out->demod_f[i][1] = f->f - inter.fsig;
        out->demod_f_sig[i][0] = f->sig_upper->index;
        out->demod_f_sig[i][1] = f->sig_lower->index;
    }
    
    if(inter.num_signal_freqs > 0 && inter.num_qnoise_inputs > 0) {
        fill_qnoise_input_matrix();
        
        // zero the memory for the selection vector and v vector. The w vector does
        // not require this as s is copied into w.
        memset(quant_s, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
        memset(quant_v, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);

        // first compute the selection vector
        fill_homodyne_qnoised_selection_vector(out, quant_s, quant_w);
        
        // compute the w = M^-1^T s, to get the weighting vector
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_w, true, true);

        // apply input noise matrix the weighting vectors
        // i = M_qn * w
        ccs_zgemv(&M_qni, quant_w, quant_v);

        // now compute the final <b b^T>s = M^-1 v
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_v, false, false);

        // and compute s^T <b b^T> s
        // we know that the selection vector only has values which are non-zero at
        // the elements which are associated with the detector port
        complex_t tmp1;
        complex_t tmp2;
        
        for(i=0; i<inter.num_fields*inter.num_signal_freqs; i++){
            tmp1 = z_by_zc(quant_v[sidx1 + i], quant_s[sidx1 + i]);
            rtn += tmp1.re;
            tmp2 = z_by_zc(quant_v[sidx2 + i], quant_s[sidx2 + i]);
            rtn += tmp2.re;
        }
    }
    
    // compensate for demod 0.5 factor if demodulating as we are demodulating at signal frequency
    // as this is what a network analyser would do.
    rtn *= 2.0;
    
    // Scale the result in either factors of hf or not, or PSD or ASD
    switch(inter.output_data_list[out->output_index].quantum_scaling){
        case PSD_HF:
            return rtn * pow(0.25, 1);
        case PSD:
            return rtn * H_PLANCK * inter.f0 * pow(0.25, 1);
        case ASD_HF:
            return sqrt(rtn * pow(0.25, 1));
        case ASD:
            return sqrt(rtn * H_PLANCK * inter.f0 * pow(0.25, 1));
        case NOTSET:
            bug_error("No scaling set");
    }
    
    bug_error("Unknown scale for shotnoise computation");
    return 0; //silence compiler warning.
}


double compute_mhomodyne_output(mhomodyne_t *out) {
    assert(out != NULL);
    
    if(inter.num_signal_freqs == 0)
        return 0;
        
    int i;
    double rtn = 0;
    
    // return 0 if no noises are present
    if(inter.num_qnoise_inputs <= 0){
        warn("No quantum noise sources set but qnoised used");
        return 0;
    }
    
    // need to fill in the homodyne demodulation table, although we aren't
    // demodulating per se, we are demodulating at the signal frequency
    for(i=0; i<inter.num_carrier_freqs; i++){
        frequency_t *f = inter.carrier_f_list[i];
        out->demod_f[i][0] = f->f + inter.fsig;
        out->demod_f[i][1] = f->f - inter.fsig;
        out->demod_f_sig[i][0] = f->sig_upper->index;
        out->demod_f_sig[i][1] = f->sig_lower->index;
    }
    
    if(inter.num_signal_freqs > 0 && inter.num_qnoise_inputs > 0) {
        fill_qnoise_input_matrix();
        
        // zero the memory for the selection vector and v vector. The w vector does
        // not require this as s is copied into w.
        memset(quant_s, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
        memset(quant_v, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);

        // first compute the selection vector
        fill_mhomodyne_qnoised_selection_vector(out, quant_s, quant_w);
        
        // compute the w = M^-1^T s, to get the weighting vector
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_w, true, true);

        // apply input noise matrix the weighting vectors
        // i = M_qn * w
        ccs_zgemv(&M_qni, quant_w, quant_v);

        // now compute the final <b b^T>s = M^-1 v
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_v, false, false);

        // and compute s^T <b b^T> s
        // we know that the selection vector only has values which are non-zero at
        // the elements which are associated with the detector port
        int j;
        
        for(j=0; j<out->num_node_pairs; j++){
            //int sidx1 = M_ifo_sig.output_rhs_idx[out->pairs[j].light_out_1];
            //int sidx2 = M_ifo_sig.output_rhs_idx[out->pairs[j].light_out_2];
            
            node_t  *node1 = &inter.node_list[out->pairs[j].node1_index];
            node_connections_t *nc1 = &inter.node_conn_list[out->pairs[j].node1_index];
            node_t  *node2 = &inter.node_list[out->pairs[j].node2_index];
            node_connections_t *nc2= &inter.node_conn_list[out->pairs[j].node2_index];
            char *name = inter.output_data_list[out->output_index].name;
            ifo_matrix_vars_t *matrix = &M_ifo_sig;
            
            int sidx1 = __get_output_rhs_port(matrix, node1, nc1, out->pairs[j].is_second_beam1, name);
            int sidx2 = __get_output_rhs_port(matrix, node2, nc2, out->pairs[j].is_second_beam2, name);
            double tmp1 = 0, tmp2 = 0;
            
            for(i=0; i<inter.num_fields*inter.num_signal_freqs; i++){
                tmp1 += z_by_zc(quant_v[sidx1 + i], quant_s[sidx1 + i]).re;
                tmp2 += z_by_zc(quant_v[sidx2 + i], quant_s[sidx2 + i]).re;
            }
            
            rtn += (tmp1+tmp2);
        }
    }
    
    // compensate for demod 0.5 factor if demodulating as we are demodulating at signal frequency
    // as this is what a network analyser would do.
    rtn *= 2.0;
    
    // Scale the result in either factors of hf or not, or PSD or ASD
    switch(inter.output_data_list[out->output_index].quantum_scaling){
        case PSD_HF:
            return rtn * pow(0.25, 1);
        case PSD:
            return rtn * H_PLANCK * inter.f0 * pow(0.25, 1);
        case ASD_HF:
            assert(rtn >= 0);
            return sqrt(rtn * pow(0.25, 1));
        case ASD:
            assert(rtn >= 0);
            return sqrt(rtn * H_PLANCK * inter.f0 * pow(0.25, 1));
        case NOTSET:
            bug_error("No scaling set");
    }
    
    bug_error("Unknown scale for shotnoise computation");
    return 0; //silence compiler warning.
}


void compute_squeezing_factor(light_out_t *sd, double *r, double *phi, double *det){
    
    assert(sd != NULL);
    
    // set default return values
    *r = 0;
    *phi = 0;
    *det = 1;
    
    if(inter.num_signal_freqs > 0 && inter.num_qnoise_inputs > 0) {
        fill_qnoise_input_matrix();
        frequency_t *f = get_carrier_frequency(sd->f[1]);
        
        if(f == NULL) // return default values set previously
            return;
        
        long idx[2];
        
        idx[0] = M_ifo_sig.output_rhs_idx[sd->list_index] + get_port_rhs_idx(f->sig_upper->index, sd->field);
        idx[1] = M_ifo_sig.output_rhs_idx[sd->list_index] + get_port_rhs_idx(f->sig_lower->index, sd->field);
        
        complex_t b[2][2] = {{{0}}};
        int j;
        
        uint64_t cidx = M_ifo_car.output_rhs_idx[sd->list_index] + get_port_rhs_idx(f->index, sd->field);
        complex_t carrier_field = ((complex_t*)M_ifo_car.rhs_values)[cidx];
        
        double car_phase = zphase(carrier_field);
        
        for(j=0;j<2;j++){
            // zero the memory for the selection vector filling
            memset(quant_w, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
            memset(quant_v, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
            
            // fill selection vector to select column
            quant_w[idx[j]] = complex_1;
    
            // compute the w = M^-1^T s, to get the weighting vector
            solve_ccs_matrix(&M_ifo_sig, (double*)quant_w, true, true);

            // apply input noise matrix the weighting vectors
            // i = M_qn * w
            ccs_zgemv(&M_qni, quant_w, quant_v);

            // now compute the final vector <b b^T>s = M^-1 v
            solve_ccs_matrix(&M_ifo_sig, (double*)quant_v, false, false);

            b[j][0] = z_by_x(quant_v[idx[0]], 1);
            b[j][1] = z_by_x(quant_v[idx[1]], 1);
        }
        
        // Apply carrier phase
//        b[0][0] = z_by_phr(b[0][0], -car_phase);
        b[0][1] = z_by_phr(b[0][1], -2*car_phase);
        b[1][0] = z_by_phr(b[1][0], -2*car_phase);
//        b[1][1] = z_by_phr(b[1][1], -car_phase);
        
        // combine a1, a2 and c to get the squeezing amplitude and phase
        
        // These values are based on the 2x2 sidebands quantum noise covariance
        // matrix and extracting the squeezing and angle terms.
        // However they are altered to ensure that the output matches up with the
        // squeezer component output, and that at open ports it only measures 
        // vacuum noise.
        
        *det = zmod(b[0][0]) * zmod(b[1][1]) - zmod(b[0][1])*zmod(b[1][0]);
        *r = acosh(2*zmod(b[0][0])) / 2.0 * R2DB;
        *phi = 0.5*zphase(b[1][0]);
    }
}

/**
 * Calculate the quantum noise at detector
 * 
 * \param detector_index detector index
 */
double compute_qnoised_output(int detector_index) {
    
    // sanity check on input
    assert(detector_index >= 0);
    assert(detector_index < inter.num_light_outputs);
    
    light_out_t *out = &inter.light_out_list[detector_index];
    int i;
    double rtn = 0;
    
    // return 0 if no noises are present
    if(inter.num_qnoise_inputs <= 0){
        warn("No quantum noise sources set but qnoised used");
        return 0;
    }
    
    fill_demod_f_table(out);
    fill_carrier_qnoise_contributions(out, false);
    
    if(inter.num_signal_freqs > 0 && inter.num_qnoise_inputs > 0) {
        fill_qnoise_input_matrix();
        
        // zero the memory for the selection vector and v vector. The w vector does
        // not require this as s is copied into w.
        memset(quant_s, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);
        memset(quant_v, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);

        // first compute the selection vector
        fill_qnoised_selection_vector(detector_index, quant_s, quant_w);

        // compute the w = M^-1^T s, to get the weighting vector
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_w, true, true);

        // apply input noise matrix the weighting vectors
        // i = M_qn * w
        ccs_zgemv(&M_qni, quant_w, quant_v);
        
        // now compute the final <b b^T>s = M^-1 v
        solve_ccs_matrix(&M_ifo_sig, (double*)quant_v, false, false);

        // and compute s^T <b b^T> s
        uint64_t os_idx = M_ifo_sig.output_rhs_idx[out->list_index];

        // we know that the selection vector only has values which are non-zero at
        // the elements which are associated with the detector port
        for(i=0; i<inter.num_fields*inter.num_signal_freqs; i++){
            complex_t tmp = z_by_zc(quant_v[os_idx + i], quant_s[os_idx + i]);
            rtn += tmp.re;
        }
    }
    
    // Now we must loop over the contributions from the demodulation for frequencies
    // that are not signal sidebands, i.e. we pick up pure vacuum noise and demodulate
    // that into our signal.
    demod_vac_t *el=NULL;
    // index of the output port in the RHS vector for the carrier matrix
    uint64_t oc_idx = M_ifo_car.output_rhs_idx[out->list_index];
    int j,k;
    // a vector containing all the carrier frequencies and modes at this output port
    complex_t *bc = &(((complex_t*)M_ifo_car.rhs_values)[oc_idx]);
    
    double vac_contributions = 0;
    
    LL_FOREACH(out->demod_vac_contri, el) {
        
        // we assume here that the demodulation frequencies are small compared to
        // the optical frequency so h(f_0+f) ~ h*f_0
        if(el->num_carriers > 1){
            for(i=0; i<inter.num_fields; i++){
                complex_t val = complex_0;
                
                for(j=0; j<el->num_carriers; j++){
                    k = get_port_rhs_idx(el->cidx[j], i);
                    z_inc_z(&val, z_by_ph(cconj(z_by_x(bc[k], SQRTTWO)), out->demod_phi[el->phi_idx[j]]));
                }
                
                vac_contributions += zabs(val) * (1.0 + el->f / inter.f0);
            }
        } else {
            j = inter.num_fields*el->cidx[0]; // RHS index of carrier at port
            double val = 0;
            for(i=0; i<inter.num_fields; i++){
                val += zabs(bc[j+i]);
            }
            
            vac_contributions += val * (1.0 + el->f / inter.f0);
        }
    }
    
    rtn += vac_contributions;
   
    for(k=0; k<out->num_demods; k++){
        // check if any demodulations are at a signal frequency, regardless of 
        // ordering
        if(eq(out->f[k+1], inter.fsig)) {
            if (inter.debug & 8) {
                message("qnoised %s compensating signal mixer 0.5 factor\n", out->name);
            }
            // compensate for demod 0.5 factor if demodulating a signal frequency
            // as this is what a network analyser would do.
            rtn *= 2.0;
            break;
        }   
    }
    
    // Scale the result in either factors of hf or not, or PSD or ASD
    switch(inter.output_data_list[out->output_idx].quantum_scaling){
        case PSD_HF:
            return rtn * pow(0.25, out->num_demods);
        case PSD:
            return rtn * H_PLANCK * inter.f0 * pow(0.25, out->num_demods);
        case ASD_HF:
            return sqrt(rtn * pow(0.25, out->num_demods));
        case ASD:
            return sqrt(rtn * H_PLANCK * inter.f0 * pow(0.25, out->num_demods));
        case NOTSET:
            bug_error("No scaling set");
    }
    
    bug_error("Unknown scale for shotnoise computation");
    return 0; //silence compiler warning.
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
