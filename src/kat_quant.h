// $Id$

/*!
 * \file kat_quant.h
 * \brief Routines for calculation of quantum noise
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_QUANT_H
#define KAT_QUANT_H

/* from kat_quant.c */
void fill_qnoised_selection_vector(int k, complex_t *s, complex_t *s_copy);

void get_homodyne_sig_node_indices(int light_out_1, int light_out_2, int *sidx1, int *sidx2);
void get_homodyne_car_node_indices(int light_out_1, int light_out_2, int *idx1, int *idx2);

void compute_q_matrix();

double compute_qshot_output(int detector_index);
void compute_quadrature(light_out_t *sd, double *quad);
double get_qnoise_value(qnoise_input_t *qn);
void init_quantum_components();
void quantum_point(void);
void fill_qnoise_input_matrix();
double get_input_noise_at(int node_index);
double get_output_noise_at(int node_index);
double get_noise_at_mirror(int component_index);
double get_noise_at_beamsplitter(int component_index);
double get_noise_at_modulator(int component_index);
void store_quantum_solution(double frequency, int frequency_index,
        int node_index, int field_type);
void quantum_signals_rhs(int carrier, int modulator_index, double node_noise);
double compute_qnoised_output(int detector_index);

void compute_squeezing_factor(light_out_t *sd, double *r, double *phi, double *size);
void print_covariance_matrix(light_out_t *sd);

double quantum_shotnoise(int detector_index, int num_demods);

double compute_homodyne_output(homodyne_t *out);
double compute_mhomodyne_output(mhomodyne_t *out);

complex_t get_noise_amplitude_sum(
        int field_index_outer,
        int field_index_inner,
        int detector_index,
        double f_ref);

bool both_fields(int signal_type_left, int signal_type_right);
bool both_signals(int signal_type_left, int signal_type_right);
bool is_signal(int signal_type);
double get_light_input_noise_at(int component_index);
complex_t get_mixed_noise_amplitude(
        int detector_index,
        double f_ref,
        light_out_t *light_output);

void fill_demod_f_table(light_out_t *out);
void fill_carrier_qnoise_contributions(light_out_t *out, bool pureVacuum);

void build_demod_frequency_matrix(
        double **demod_freq_matrix,
        const double *base_freqs,
        const double *demod_freqs,
        const int num_demods);
void build_base_frequency_list(
        double *base_freqs,
        double *f_s,
        int *index_table);
void build_base_amplitude_list(
        complex_t *base_amplitudes,
        complex_t *unsorted_amplitudes,
        int *index_table);
double get_shotnoise_spectral_density(
        double **demod_freq_matrix,
        complex_t *base_amplitudes,
        double *demod_phases,
        int num_demods);
int get_num_non_signal_frequencies(int *t_s);
void strip_signal_frequencies(
        double *f_s,
        int *t_s,
        double *stripped_frequencies);
void strip_signal_amplitudes(
        complex_t *unsorted_amplitudes,
        int *t_s,
        complex_t *stripped_amplitudes);
void build_frequency_index_table(int *index_table, double *f_s);

#endif // KAT_QUANT_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
