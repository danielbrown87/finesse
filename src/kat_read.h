// $Id$

/*!
 * \file kat_read.h
 * \brief Header file for command reading routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_READ_H
#define KAT_READ_H

#include "kat.h"


//! structure of variables used when setting an attribute of a component

typedef struct component_attribute {
    int component_index; //!< the component index
    int attribute_type; //!< the type of attribute
    double parameter_value; //!< the value of the attribute to set
    const char *parameter_string; //!< string value of the attribute to set
    const char *command_string; //!< command string used to set the attribute
    const char *component_name; //!< name of the component to set
} component_attribute_t;

bool check_for_include_kat(FILE **ifp);
void read_mismatches_command(const char *command_string);
void read_dof(const char* command_string);
void read_slink(const char* command_string);
void read_qd_out(const char *command_string);
void read_force_out(const char* command_string);
void read_openloopTF_out(const char* command_string);
void read_surface_motion_map(const char *command_string);
void read_rom_map(rom_map_t *rom);
void read_motion_link(const char *command_string);
void read_quadrature_out(const char *command_string);
void read_vacuum(const char *command_string, bool count);
void read_frequency_command(const char *command_string);
void read_general_mech_transfer_function(const char *command_string);
void read_manual_frequency(const char *command_string, int mem_count);
void read_motion_out(const char *command_string);
void read_file(FILE *fp);
void read_const(const char *s);
void read_variable(const char *s);
int get_modulation_order(const char *s);
void read_noise(const char *s);
void read_maxtem(const char *s);
void read_knm(const char *s);
void read_debug(const char *s);
void set_qnoise_type(int qcorr_value);
void read_conf(const char *s);
void read_space(const char *s);
void read_sagnac(const char *s);
void read_block(const char *s);
void read_mirror(const char *s, int mode);
void read_mirror2(const char *s);
void read_beamsplitter(const char *s, int mode);
void read_beamsplitter2(const char *s);
void read_cavity(const char *s);
void read_startnode(const char *s);
void read_gauss(const char *s);
void read_gauss2(const char *s);
void read_sd_out(const char *command_string);
void read_hd_out(const char *command_string);
void read_gaussRc(const char *s);
void read_lens(const char *command_string, bool alternative);
void read_ltem(const char *s, bool inputLG);
void read_mask(const char *s);
void read_retrace(const char *s);
void read_light_in(const char *s);
void read_light_out(const char *s);
void read_grating(const char *s);
void read_squeezer(const char *s);
void read_beam(const char *s);
void read_amp_out(const char *s);
void read_convolution_out(const char *s);
void read_mirror_phase_out(const char *s);
void read_amp_out(const char *s);
void read_qnoised_cmd(const char *s);
void read_beamparam_out(const char *s);
void read_cavity_param_out(const char *s);
void read_gouy_out(const char *s);
void read_shot(const char *s);
void read_quantum_shot(const char *s);
void read_dither(const char *command_string);
int get_dither_order(const char *command_string);
void read_modulator(const char *s);
void read_fsig(const char *s);
void read_and_set_map_function(const char *command_string);
void read_map_coefficient_header(FILE *mapfile, surface_map_t *map);
void read_map_data_header(FILE *mapfile, surface_map_t *map);
int search_file_for_headers(FILE *mapfile, int header_type);
void read_surface_map(surface_map_t *map);
//void read_surface_map_coefficients(surface_map_t *map);
//void read_coefficients(const char *command_string);
void read_attribute(const char *s);
void read_maps(const char *s);
int get_map_type(const char *parameter_name, const char *x_y_string, const char *command_string);
void read_and_set_function(char *command_string);
void read_func_command(const char *s);
//void read_func2(const char *s);
void read_put_star_command(const char *s);
void read_put_command(const char *s);
void read_set_command(const char *s);
void read_noplot(const char *s);
void read_showiterate(const char *s);
void read_minimize(const char *command_string);
void read_lock2(const char *s);
void read_lock_command(const char *s);
void read_xaxis(const char *s);
void read_xaxis2(const char *s);
void read_x2axis(const char *s);
void read_x2axis2(const char *s);
void read_x3axis(const char *s);
void read_x3axis2(const char *s);
void read_yaxis(const char *s);
void read_pdtype(const char *s);
void read_gnuterm(const char *s);
void read_pyterm(const char *s);
void read_deriv(const char *s);
void read_color(const char *s);
void read_width(const char *s);
void read_deriv_h(const char *s);
void read_scale(const char *s);
void read_trace(const char *s);
void read_powers(const char *s);
void read_verbose_grating(const char *s);
void read_phase(const char *s);
void read_diode(const char *s);
void read_dbs(const char *command_string);
void read_intmethod(const char *command_string);
void read_QF_mech_transfer_function(const char *command_string);
void read_mhd_out(const char *command_string);

int check_mhomodyne(char* s);

int get_xparam(double **xparam, int i, const char *parameter, char *unit,
        const char *s, int *lborder, int *uborder, double *min, double *max);
void check_xparam(int lborder, int uborder, double min, double max,
        double x1, double x2, int type, const char *s, int axis_or_param);
void check_and_set_attribute(const char *parameter,
        const char *string,
        const char *s,
        const char *component);
int get_attribute_type(const char *parameter_name,
        int *eta,
        const char *command_string);

void set_light_output_attribute(component_attribute_t *component_attribute);
void set_mirror_attribute(component_attribute_t *component_attribute);
void set_beamsplitter_attribute(component_attribute_t *component_attribute);
void set_space_attribute(component_attribute_t *component_attribute);
void set_grating_attribute(component_attribute_t *component_attribute,
        int eta);
void set_laser_attribute(component_attribute_t *component_attribute);

int assign_mirror_parameter(component_param_t *component_param);
int assign_beamsplitter_parameter(component_param_t *component_param);
int assign_space_parameter(component_param_t *component_param);
int assign_sagnac_parameter(component_param_t *component_param);
int assign_lens_parameter(component_param_t *component_param);
int assign_diode_parameter(component_param_t *component_param);
int assign_laser_parameter(component_param_t *component_param);
int assign_modulator_parameter(component_param_t *component_param);
int assign_grating_parameter(component_param_t *component_param);
int assign_fsig_parameter(component_param_t *component_param);
int assign_detector_parameter(component_param_t *component_param);
int assign_tf_parameter(component_param_t *component_param);
void assign_qd_detector_params( light_out_t *detector, component_param_t *component_param);
void assign_homodyne_detector_params( homodyne_t *hom,component_param_t *component_param);

int assign_variable_parameter(component_param_t *component_param);

void assign_beam_detector_params(light_out_t *detector, component_param_t *component_param);
int assign_amplitude_detector_params(light_out_t *detector, component_param_t *component_param);
int assign_one_demod_detector_params(light_out_t *detector, component_param_t *component_param);
int assign_multi_demod_detector_params(light_out_t *detector, component_param_t *component_param);
int assign_beam_parameter(component_param_t *component_param);
int assign_tf_parameter(component_param_t *component_param);
//void assign_lock_params( lock_command_t *lock, component_param_t *component_param);

void init_beamsplitter_signal(signal_t *signal,
        int component_index,
        bool type_is_set,
        const char *command_string);
void init_mirror_signal(
        signal_t *signal,
        int component_index,
        bool type_is_set,
        const char *signal_type_string,
        const char *command_string);
void init_space_signal(
        signal_t *signal,
        int component_index,
        bool type_is_set,
        const char *command_string);
void init_light_input_signal(
        signal_t *signal,
        int component_index,
        bool type_is_set,
        const char *command_string);
void init_modulator_signal(
        signal_t *signal,
        int component_index,
        bool type_is_set,
        const char *command_string);

#endif // KAT_READ_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
