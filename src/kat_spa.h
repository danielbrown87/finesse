// $Id$

/*!
 * \file kat_spa.h
 * \brief Header file for sparse matrix routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef KAT_SPA_H
#define KAT_SPA_H

//! format string for matrix formatting statements
#define MAT_FMT "%5.2g" 

//! format string for matrix formatting statements
#define SPA_FMT "%4.2g" 

//! the format string to use in fprintf statements
#define SOLN_FMT "%4.2g" 

//! the format string to use in fprintf statements
#define RHS_FMT "%4.2g" 

#include "kat_matrix_ccs.h"

//complex_t *get_matrix_elem(int i, int j, int matrix_type);
//complex_t *get_sparse_matrix_elem(int i, int j, int matrix_type);

void solve_matrix(int type);
void fill_matrix(double f, int type);
void set_rhs(int vector_index, complex_t z_value, int type);
void clear_rhs(int type);
void factor_matrix(int type);

void dump_solution(void);
void dump_solution_vector(int type, char *output_stream);
void dump_rhs_vector(int type, char *output_stream);
void dump_matrix(int type, char *output_stream);
void dump_sparse_matrix(int type, char *output_stream);

void check_connections(void);

complex_t get_modulation_amplitude(int component_index, int modulation_order, int sideband_index);

void fill_matrix_feedback_elements(ifo_matrix_vars_t *matrix);
void fill_matrix_signal_elements(signal_t *signal);
void fill_optic_motion_coupling();
void fill_matrix_mirror_elements(mirror_t *mirror, double fin, double fout, int order, bool conjugate);
void fill_matrix_beamsplitter_elements(beamsplitter_t *bs, frequency_t *_fin, frequency_t *_fout, int K, bool conjugate);
void fill_matrix_grating_elements(grating_t *gr, double f,bool conjugate);
void fill_matrix_space_elements(space_t *space, double f,bool conjugate);
void fill_matrix_squeezer_elements(void);
void fill_matrix_modulator_elements(void);
void fill_matrix_modulator_elements_new(ifo_matrix_vars_t *matrix);
void fill_matrix_modulator_elements_quantum(ifo_matrix_vars_t *matrix);
void fill_matrix_lens_elements(bool conjugate);
void fill_matrix_diode_elements(bool conjugate);
void fill_matrix_sagnac_elements(bool conjugate);
void fill_matrix_block_elements(ifo_matrix_vars_t *matrix);
void fill_matrix_dbs_elements(ifo_matrix_vars_t *matrix);

void dump_matrix_mirror_elements(FILE *fp);
void dump_matrix_beamsplitter_elements(FILE *fp);
void dump_matrix_space_elements(FILE *fp);

void update_element_count(int node_index1,int node_index2, int col_index_1, int col_index_2, int mode);
void reset_connect_count(void);

#endif // KAT_SPA_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
